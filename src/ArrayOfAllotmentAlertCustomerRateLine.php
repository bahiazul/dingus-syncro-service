<?php

namespace Dingus\SyncroService;

class ArrayOfAllotmentAlertCustomerRateLine implements \JsonSerializable, \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var AllotmentAlertCustomerRateLine[] $AllotmentAlertCustomerRateLine
     */
    protected $AllotmentAlertCustomerRateLine = null;

    
    public function __construct()
    {
    
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'AllotmentAlertCustomerRateLine' => $this->getAllotmentAlertCustomerRateLine(),
      );
    }

    /**
     * @return AllotmentAlertCustomerRateLine[]
     */
    public function getAllotmentAlertCustomerRateLine()
    {
      return $this->AllotmentAlertCustomerRateLine;
    }

    /**
     * @param AllotmentAlertCustomerRateLine[] $AllotmentAlertCustomerRateLine
     * @return \Dingus\SyncroService\ArrayOfAllotmentAlertCustomerRateLine
     */
    public function setAllotmentAlertCustomerRateLine(array $AllotmentAlertCustomerRateLine = null)
    {
      $this->AllotmentAlertCustomerRateLine = $AllotmentAlertCustomerRateLine;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->AllotmentAlertCustomerRateLine[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return AllotmentAlertCustomerRateLine
     */
    public function offsetGet($offset)
    {
      return $this->AllotmentAlertCustomerRateLine[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param AllotmentAlertCustomerRateLine $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      $this->AllotmentAlertCustomerRateLine[$offset] = $value;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->AllotmentAlertCustomerRateLine[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return AllotmentAlertCustomerRateLine Return the current element
     */
    public function current()
    {
      return current($this->AllotmentAlertCustomerRateLine);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->AllotmentAlertCustomerRateLine);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->AllotmentAlertCustomerRateLine);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->AllotmentAlertCustomerRateLine);
    }

    /**
     * Countable implementation
     *
     * @return AllotmentAlertCustomerRateLine Return count of elements
     */
    public function count()
    {
      return count($this->AllotmentAlertCustomerRateLine);
    }

}
