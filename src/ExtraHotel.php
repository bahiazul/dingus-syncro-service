<?php

namespace Dingus\SyncroService;

class ExtraHotel implements \JsonSerializable
{

    /**
     * @var ArrayOfMlText $ExtraRecNameMlList
     */
    protected $ExtraRecNameMlList = null;

    /**
     * @var Action $Action
     */
    protected $Action = null;

    /**
     * @var string $ExtraCode
     */
    protected $ExtraCode = null;

    /**
     * @var string $ExtraName
     */
    protected $ExtraName = null;

    /**
     * @var string $PMSCode
     */
    protected $PMSCode = null;

    /**
     * @var int $MaxQuantity
     */
    protected $MaxQuantity = null;

    /**
     * @var boolean $ForDay
     */
    protected $ForDay = null;

    /**
     * @var boolean $ForPax
     */
    protected $ForPax = null;

    /**
     * @var boolean $UserInventory
     */
    protected $UserInventory = null;

    /**
     * @var boolean $ForAllPaxes
     */
    protected $ForAllPaxes = null;

    /**
     * @var ContainedServiceRecc $ContainedService
     */
    protected $ContainedService = null;

    /**
     * @param Action $Action
     * @param int $MaxQuantity
     * @param boolean $ForDay
     * @param boolean $ForPax
     * @param boolean $UserInventory
     * @param boolean $ForAllPaxes
     */
    public function __construct($Action, $MaxQuantity, $ForDay, $ForPax, $UserInventory, $ForAllPaxes)
    {
      $this->Action = $Action;
      $this->MaxQuantity = $MaxQuantity;
      $this->ForDay = $ForDay;
      $this->ForPax = $ForPax;
      $this->UserInventory = $UserInventory;
      $this->ForAllPaxes = $ForAllPaxes;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'ExtraRecNameMlList' => $this->getExtraRecNameMlList(),
        'Action' => $this->getAction(),
        'ExtraCode' => $this->getExtraCode(),
        'ExtraName' => $this->getExtraName(),
        'PMSCode' => $this->getPMSCode(),
        'MaxQuantity' => $this->getMaxQuantity(),
        'ForDay' => $this->getForDay(),
        'ForPax' => $this->getForPax(),
        'UserInventory' => $this->getUserInventory(),
        'ForAllPaxes' => $this->getForAllPaxes(),
        'ContainedService' => $this->getContainedService(),
      );
    }

    /**
     * @return ArrayOfMlText
     */
    public function getExtraRecNameMlList()
    {
      return $this->ExtraRecNameMlList;
    }

    /**
     * @param ArrayOfMlText $ExtraRecNameMlList
     * @return \Dingus\SyncroService\ExtraHotel
     */
    public function setExtraRecNameMlList($ExtraRecNameMlList)
    {
      $this->ExtraRecNameMlList = $ExtraRecNameMlList;
      return $this;
    }

    /**
     * @return Action
     */
    public function getAction()
    {
      return $this->Action;
    }

    /**
     * @param Action $Action
     * @return \Dingus\SyncroService\ExtraHotel
     */
    public function setAction($Action)
    {
      $this->Action = $Action;
      return $this;
    }

    /**
     * @return string
     */
    public function getExtraCode()
    {
      return $this->ExtraCode;
    }

    /**
     * @param string $ExtraCode
     * @return \Dingus\SyncroService\ExtraHotel
     */
    public function setExtraCode($ExtraCode)
    {
      $this->ExtraCode = $ExtraCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getExtraName()
    {
      return $this->ExtraName;
    }

    /**
     * @param string $ExtraName
     * @return \Dingus\SyncroService\ExtraHotel
     */
    public function setExtraName($ExtraName)
    {
      $this->ExtraName = $ExtraName;
      return $this;
    }

    /**
     * @return string
     */
    public function getPMSCode()
    {
      return $this->PMSCode;
    }

    /**
     * @param string $PMSCode
     * @return \Dingus\SyncroService\ExtraHotel
     */
    public function setPMSCode($PMSCode)
    {
      $this->PMSCode = $PMSCode;
      return $this;
    }

    /**
     * @return int
     */
    public function getMaxQuantity()
    {
      return $this->MaxQuantity;
    }

    /**
     * @param int $MaxQuantity
     * @return \Dingus\SyncroService\ExtraHotel
     */
    public function setMaxQuantity($MaxQuantity)
    {
      $this->MaxQuantity = $MaxQuantity;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getForDay()
    {
      return $this->ForDay;
    }

    /**
     * @param boolean $ForDay
     * @return \Dingus\SyncroService\ExtraHotel
     */
    public function setForDay($ForDay)
    {
      $this->ForDay = $ForDay;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getForPax()
    {
      return $this->ForPax;
    }

    /**
     * @param boolean $ForPax
     * @return \Dingus\SyncroService\ExtraHotel
     */
    public function setForPax($ForPax)
    {
      $this->ForPax = $ForPax;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getUserInventory()
    {
      return $this->UserInventory;
    }

    /**
     * @param boolean $UserInventory
     * @return \Dingus\SyncroService\ExtraHotel
     */
    public function setUserInventory($UserInventory)
    {
      $this->UserInventory = $UserInventory;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getForAllPaxes()
    {
      return $this->ForAllPaxes;
    }

    /**
     * @param boolean $ForAllPaxes
     * @return \Dingus\SyncroService\ExtraHotel
     */
    public function setForAllPaxes($ForAllPaxes)
    {
      $this->ForAllPaxes = $ForAllPaxes;
      return $this;
    }

    /**
     * @return ContainedServiceRecc
     */
    public function getContainedService()
    {
      return $this->ContainedService;
    }

    /**
     * @param ContainedServiceRecc $ContainedService
     * @return \Dingus\SyncroService\ExtraHotel
     */
    public function setContainedService($ContainedService)
    {
      $this->ContainedService = $ContainedService;
      return $this;
    }

}
