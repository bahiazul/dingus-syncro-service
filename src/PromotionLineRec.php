<?php

namespace Dingus\SyncroService;

class PromotionLineRec implements \JsonSerializable
{

    /**
     * @var Action $Action
     */
    protected $Action = null;

    /**
     * @var int $IdPromotionLine
     */
    protected $IdPromotionLine = null;

    /**
     * @var TipoLineaSuplementoDescuento $PromType
     */
    protected $PromType = null;

    /**
     * @var string $PromotionLineCode
     */
    protected $PromotionLineCode = null;

    /**
     * @var string $RoomCode
     */
    protected $RoomCode = null;

    /**
     * @var string $CustomerCode
     */
    protected $CustomerCode = null;

    /**
     * @var int $AdultFrom
     */
    protected $AdultFrom = null;

    /**
     * @var int $AdultTo
     */
    protected $AdultTo = null;

    /**
     * @var int $AgeFrom
     */
    protected $AgeFrom = null;

    /**
     * @var int $AgeTo
     */
    protected $AgeTo = null;

    /**
     * @var boolean $ApplyToBoard
     */
    protected $ApplyToBoard = null;

    /**
     * @var boolean $ApplyToRoom
     */
    protected $ApplyToRoom = null;

    /**
     * @var boolean $ApplyToSupl
     */
    protected $ApplyToSupl = null;

    /**
     * @var string $BoardCode
     */
    protected $BoardCode = null;

    /**
     * @var int $ChildFrom
     */
    protected $ChildFrom = null;

    /**
     * @var int $ChildTo
     */
    protected $ChildTo = null;

    /**
     * @var float $ChildPrice
     */
    protected $ChildPrice = null;

    /**
     * @var \DateTime $EffectiveDate
     */
    protected $EffectiveDate = null;

    /**
     * @var \DateTime $DateFrom
     */
    protected $DateFrom = null;

    /**
     * @var \DateTime $DateFrom2
     */
    protected $DateFrom2 = null;

    /**
     * @var TipoFechasPrecio $DatesType
     */
    protected $DatesType = null;

    /**
     * @var \DateTime $DateTo
     */
    protected $DateTo = null;

    /**
     * @var \DateTime $DateTo2
     */
    protected $DateTo2 = null;

    /**
     * @var \DateTime $HourFrom
     */
    protected $HourFrom = null;

    /**
     * @var \DateTime $HourTo
     */
    protected $HourTo = null;

    /**
     * @var int $DaysBeforeArrivalFrom
     */
    protected $DaysBeforeArrivalFrom = null;

    /**
     * @var int $DaysBeforeArrivalTo
     */
    protected $DaysBeforeArrivalTo = null;

    /**
     * @var TipoPax $PaxType
     */
    protected $PaxType = null;

    /**
     * @var boolean $PaxTypeSpecified
     */
    protected $PaxTypeSpecified = null;

    /**
     * @var boolean $IsPercent
     */
    protected $IsPercent = null;

    /**
     * @var int $PaxFrom
     */
    protected $PaxFrom = null;

    /**
     * @var int $PaxNo
     */
    protected $PaxNo = null;

    /**
     * @var int $PaxTo
     */
    protected $PaxTo = null;

    /**
     * @var int $DaysClosePrice
     */
    protected $DaysClosePrice = null;

    /**
     * @var float $Price
     */
    protected $Price = null;

    /**
     * @var float $SecondChidlPrice
     */
    protected $SecondChidlPrice = null;

    /**
     * @var float $ThirdChildPrice
     */
    protected $ThirdChildPrice = null;

    /**
     * @var float $ThirdPaxPrice
     */
    protected $ThirdPaxPrice = null;

    /**
     * @var float $FourthChildPrice
     */
    protected $FourthChildPrice = null;

    /**
     * @var float $FourthPaxPrice
     */
    protected $FourthPaxPrice = null;

    /**
     * @var float $EnfantPrice
     */
    protected $EnfantPrice = null;

    /**
     * @var float $ExtraNightPrice
     */
    protected $ExtraNightPrice = null;

    /**
     * @var int $NightFrom
     */
    protected $NightFrom = null;

    /**
     * @var int $NightTo
     */
    protected $NightTo = null;

    /**
     * @var int $Stay
     */
    protected $Stay = null;

    /**
     * @var int $StayToInvice
     */
    protected $StayToInvice = null;

    /**
     * @var boolean $Excluding
     */
    protected $Excluding = null;

    /**
     * @var int $PriorityExcluding
     */
    protected $PriorityExcluding = null;

    /**
     * @var boolean $UniqueApplication
     */
    protected $UniqueApplication = null;

    /**
     * @var boolean $Monday
     */
    protected $Monday = null;

    /**
     * @var boolean $Tuesday
     */
    protected $Tuesday = null;

    /**
     * @var boolean $Wednesday
     */
    protected $Wednesday = null;

    /**
     * @var boolean $Thursday
     */
    protected $Thursday = null;

    /**
     * @var boolean $Friday
     */
    protected $Friday = null;

    /**
     * @var boolean $Saturday
     */
    protected $Saturday = null;

    /**
     * @var boolean $Sunday
     */
    protected $Sunday = null;

    /**
     * @var boolean $StayAxBNoExact
     */
    protected $StayAxBNoExact = null;

    /**
     * @var TipoNocheDescontarAxB $NightTypeDiscountAxB
     */
    protected $NightTypeDiscountAxB = null;

    /**
     * @var boolean $ArrivalAtMonday
     */
    protected $ArrivalAtMonday = null;

    /**
     * @var boolean $ArrivalAtTuesday
     */
    protected $ArrivalAtTuesday = null;

    /**
     * @var boolean $ArrivalAtWednesday
     */
    protected $ArrivalAtWednesday = null;

    /**
     * @var boolean $ArrivalAtThursday
     */
    protected $ArrivalAtThursday = null;

    /**
     * @var boolean $ArrivalAtFriday
     */
    protected $ArrivalAtFriday = null;

    /**
     * @var boolean $ArrivalAtSaturday
     */
    protected $ArrivalAtSaturday = null;

    /**
     * @var boolean $ArrivalAtSunday
     */
    protected $ArrivalAtSunday = null;

    /**
     * @param Action $Action
     * @param int $IdPromotionLine
     * @param TipoLineaSuplementoDescuento $PromType
     * @param int $AdultFrom
     * @param int $AdultTo
     * @param int $AgeFrom
     * @param int $AgeTo
     * @param boolean $ApplyToBoard
     * @param boolean $ApplyToRoom
     * @param boolean $ApplyToSupl
     * @param int $ChildFrom
     * @param int $ChildTo
     * @param float $ChildPrice
     * @param \DateTime $EffectiveDate
     * @param \DateTime $DateFrom
     * @param \DateTime $DateFrom2
     * @param TipoFechasPrecio $DatesType
     * @param \DateTime $DateTo
     * @param \DateTime $DateTo2
     * @param \DateTime $HourFrom
     * @param \DateTime $HourTo
     * @param int $DaysBeforeArrivalFrom
     * @param int $DaysBeforeArrivalTo
     * @param boolean $PaxTypeSpecified
     * @param boolean $IsPercent
     * @param int $PaxFrom
     * @param int $PaxNo
     * @param int $PaxTo
     * @param int $DaysClosePrice
     * @param float $Price
     * @param float $SecondChidlPrice
     * @param float $ThirdChildPrice
     * @param float $ThirdPaxPrice
     * @param float $FourthChildPrice
     * @param float $FourthPaxPrice
     * @param float $EnfantPrice
     * @param float $ExtraNightPrice
     * @param int $NightFrom
     * @param int $NightTo
     * @param int $Stay
     * @param int $StayToInvice
     * @param boolean $Excluding
     * @param int $PriorityExcluding
     * @param boolean $UniqueApplication
     * @param boolean $Monday
     * @param boolean $Tuesday
     * @param boolean $Wednesday
     * @param boolean $Thursday
     * @param boolean $Friday
     * @param boolean $Saturday
     * @param boolean $Sunday
     * @param boolean $StayAxBNoExact
     * @param TipoNocheDescontarAxB $NightTypeDiscountAxB
     * @param boolean $ArrivalAtMonday
     * @param boolean $ArrivalAtTuesday
     * @param boolean $ArrivalAtWednesday
     * @param boolean $ArrivalAtThursday
     * @param boolean $ArrivalAtFriday
     * @param boolean $ArrivalAtSaturday
     * @param boolean $ArrivalAtSunday
     */
    public function __construct($Action, $IdPromotionLine, $PromType, $AdultFrom, $AdultTo, $AgeFrom, $AgeTo, $ApplyToBoard, $ApplyToRoom, $ApplyToSupl, $ChildFrom, $ChildTo, $ChildPrice, \DateTime $EffectiveDate, \DateTime $DateFrom, \DateTime $DateFrom2, $DatesType, \DateTime $DateTo, \DateTime $DateTo2, \DateTime $HourFrom, \DateTime $HourTo, $DaysBeforeArrivalFrom, $DaysBeforeArrivalTo, $PaxTypeSpecified, $IsPercent, $PaxFrom, $PaxNo, $PaxTo, $DaysClosePrice, $Price, $SecondChidlPrice, $ThirdChildPrice, $ThirdPaxPrice, $FourthChildPrice, $FourthPaxPrice, $EnfantPrice, $ExtraNightPrice, $NightFrom, $NightTo, $Stay, $StayToInvice, $Excluding, $PriorityExcluding, $UniqueApplication, $Monday, $Tuesday, $Wednesday, $Thursday, $Friday, $Saturday, $Sunday, $StayAxBNoExact, $NightTypeDiscountAxB, $ArrivalAtMonday, $ArrivalAtTuesday, $ArrivalAtWednesday, $ArrivalAtThursday, $ArrivalAtFriday, $ArrivalAtSaturday, $ArrivalAtSunday)
    {
      $this->Action = $Action;
      $this->IdPromotionLine = $IdPromotionLine;
      $this->PromType = $PromType;
      $this->AdultFrom = $AdultFrom;
      $this->AdultTo = $AdultTo;
      $this->AgeFrom = $AgeFrom;
      $this->AgeTo = $AgeTo;
      $this->ApplyToBoard = $ApplyToBoard;
      $this->ApplyToRoom = $ApplyToRoom;
      $this->ApplyToSupl = $ApplyToSupl;
      $this->ChildFrom = $ChildFrom;
      $this->ChildTo = $ChildTo;
      $this->ChildPrice = $ChildPrice;
      $this->EffectiveDate = $EffectiveDate->format(\DateTime::ATOM);
      $this->DateFrom = $DateFrom->format(\DateTime::ATOM);
      $this->DateFrom2 = $DateFrom2->format(\DateTime::ATOM);
      $this->DatesType = $DatesType;
      $this->DateTo = $DateTo->format(\DateTime::ATOM);
      $this->DateTo2 = $DateTo2->format(\DateTime::ATOM);
      $this->HourFrom = $HourFrom->format(\DateTime::ATOM);
      $this->HourTo = $HourTo->format(\DateTime::ATOM);
      $this->DaysBeforeArrivalFrom = $DaysBeforeArrivalFrom;
      $this->DaysBeforeArrivalTo = $DaysBeforeArrivalTo;
      $this->PaxTypeSpecified = $PaxTypeSpecified;
      $this->IsPercent = $IsPercent;
      $this->PaxFrom = $PaxFrom;
      $this->PaxNo = $PaxNo;
      $this->PaxTo = $PaxTo;
      $this->DaysClosePrice = $DaysClosePrice;
      $this->Price = $Price;
      $this->SecondChidlPrice = $SecondChidlPrice;
      $this->ThirdChildPrice = $ThirdChildPrice;
      $this->ThirdPaxPrice = $ThirdPaxPrice;
      $this->FourthChildPrice = $FourthChildPrice;
      $this->FourthPaxPrice = $FourthPaxPrice;
      $this->EnfantPrice = $EnfantPrice;
      $this->ExtraNightPrice = $ExtraNightPrice;
      $this->NightFrom = $NightFrom;
      $this->NightTo = $NightTo;
      $this->Stay = $Stay;
      $this->StayToInvice = $StayToInvice;
      $this->Excluding = $Excluding;
      $this->PriorityExcluding = $PriorityExcluding;
      $this->UniqueApplication = $UniqueApplication;
      $this->Monday = $Monday;
      $this->Tuesday = $Tuesday;
      $this->Wednesday = $Wednesday;
      $this->Thursday = $Thursday;
      $this->Friday = $Friday;
      $this->Saturday = $Saturday;
      $this->Sunday = $Sunday;
      $this->StayAxBNoExact = $StayAxBNoExact;
      $this->NightTypeDiscountAxB = $NightTypeDiscountAxB;
      $this->ArrivalAtMonday = $ArrivalAtMonday;
      $this->ArrivalAtTuesday = $ArrivalAtTuesday;
      $this->ArrivalAtWednesday = $ArrivalAtWednesday;
      $this->ArrivalAtThursday = $ArrivalAtThursday;
      $this->ArrivalAtFriday = $ArrivalAtFriday;
      $this->ArrivalAtSaturday = $ArrivalAtSaturday;
      $this->ArrivalAtSunday = $ArrivalAtSunday;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'Action' => $this->getAction(),
        'IdPromotionLine' => $this->getIdPromotionLine(),
        'PromType' => $this->getPromType(),
        'PromotionLineCode' => $this->getPromotionLineCode(),
        'RoomCode' => $this->getRoomCode(),
        'CustomerCode' => $this->getCustomerCode(),
        'AdultFrom' => $this->getAdultFrom(),
        'AdultTo' => $this->getAdultTo(),
        'AgeFrom' => $this->getAgeFrom(),
        'AgeTo' => $this->getAgeTo(),
        'ApplyToBoard' => $this->getApplyToBoard(),
        'ApplyToRoom' => $this->getApplyToRoom(),
        'ApplyToSupl' => $this->getApplyToSupl(),
        'BoardCode' => $this->getBoardCode(),
        'ChildFrom' => $this->getChildFrom(),
        'ChildTo' => $this->getChildTo(),
        'ChildPrice' => $this->getChildPrice(),
        'EffectiveDate' => $this->getEffectiveDate(),
        'DateFrom' => $this->getDateFrom(),
        'DateFrom2' => $this->getDateFrom2(),
        'DatesType' => $this->getDatesType(),
        'DateTo' => $this->getDateTo(),
        'DateTo2' => $this->getDateTo2(),
        'HourFrom' => $this->getHourFrom(),
        'HourTo' => $this->getHourTo(),
        'DaysBeforeArrivalFrom' => $this->getDaysBeforeArrivalFrom(),
        'DaysBeforeArrivalTo' => $this->getDaysBeforeArrivalTo(),
        'PaxType' => $this->getPaxType(),
        'PaxTypeSpecified' => $this->getPaxTypeSpecified(),
        'IsPercent' => $this->getIsPercent(),
        'PaxFrom' => $this->getPaxFrom(),
        'PaxNo' => $this->getPaxNo(),
        'PaxTo' => $this->getPaxTo(),
        'DaysClosePrice' => $this->getDaysClosePrice(),
        'Price' => $this->getPrice(),
        'SecondChidlPrice' => $this->getSecondChidlPrice(),
        'ThirdChildPrice' => $this->getThirdChildPrice(),
        'ThirdPaxPrice' => $this->getThirdPaxPrice(),
        'FourthChildPrice' => $this->getFourthChildPrice(),
        'FourthPaxPrice' => $this->getFourthPaxPrice(),
        'EnfantPrice' => $this->getEnfantPrice(),
        'ExtraNightPrice' => $this->getExtraNightPrice(),
        'NightFrom' => $this->getNightFrom(),
        'NightTo' => $this->getNightTo(),
        'Stay' => $this->getStay(),
        'StayToInvice' => $this->getStayToInvice(),
        'Excluding' => $this->getExcluding(),
        'PriorityExcluding' => $this->getPriorityExcluding(),
        'UniqueApplication' => $this->getUniqueApplication(),
        'Monday' => $this->getMonday(),
        'Tuesday' => $this->getTuesday(),
        'Wednesday' => $this->getWednesday(),
        'Thursday' => $this->getThursday(),
        'Friday' => $this->getFriday(),
        'Saturday' => $this->getSaturday(),
        'Sunday' => $this->getSunday(),
        'StayAxBNoExact' => $this->getStayAxBNoExact(),
        'NightTypeDiscountAxB' => $this->getNightTypeDiscountAxB(),
        'ArrivalAtMonday' => $this->getArrivalAtMonday(),
        'ArrivalAtTuesday' => $this->getArrivalAtTuesday(),
        'ArrivalAtWednesday' => $this->getArrivalAtWednesday(),
        'ArrivalAtThursday' => $this->getArrivalAtThursday(),
        'ArrivalAtFriday' => $this->getArrivalAtFriday(),
        'ArrivalAtSaturday' => $this->getArrivalAtSaturday(),
        'ArrivalAtSunday' => $this->getArrivalAtSunday(),
      );
    }

    /**
     * @return Action
     */
    public function getAction()
    {
      return $this->Action;
    }

    /**
     * @param Action $Action
     * @return \Dingus\SyncroService\PromotionLineRec
     */
    public function setAction($Action)
    {
      $this->Action = $Action;
      return $this;
    }

    /**
     * @return int
     */
    public function getIdPromotionLine()
    {
      return $this->IdPromotionLine;
    }

    /**
     * @param int $IdPromotionLine
     * @return \Dingus\SyncroService\PromotionLineRec
     */
    public function setIdPromotionLine($IdPromotionLine)
    {
      $this->IdPromotionLine = $IdPromotionLine;
      return $this;
    }

    /**
     * @return TipoLineaSuplementoDescuento
     */
    public function getPromType()
    {
      return $this->PromType;
    }

    /**
     * @param TipoLineaSuplementoDescuento $PromType
     * @return \Dingus\SyncroService\PromotionLineRec
     */
    public function setPromType($PromType)
    {
      $this->PromType = $PromType;
      return $this;
    }

    /**
     * @return string
     */
    public function getPromotionLineCode()
    {
      return $this->PromotionLineCode;
    }

    /**
     * @param string $PromotionLineCode
     * @return \Dingus\SyncroService\PromotionLineRec
     */
    public function setPromotionLineCode($PromotionLineCode)
    {
      $this->PromotionLineCode = $PromotionLineCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getRoomCode()
    {
      return $this->RoomCode;
    }

    /**
     * @param string $RoomCode
     * @return \Dingus\SyncroService\PromotionLineRec
     */
    public function setRoomCode($RoomCode)
    {
      $this->RoomCode = $RoomCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getCustomerCode()
    {
      return $this->CustomerCode;
    }

    /**
     * @param string $CustomerCode
     * @return \Dingus\SyncroService\PromotionLineRec
     */
    public function setCustomerCode($CustomerCode)
    {
      $this->CustomerCode = $CustomerCode;
      return $this;
    }

    /**
     * @return int
     */
    public function getAdultFrom()
    {
      return $this->AdultFrom;
    }

    /**
     * @param int $AdultFrom
     * @return \Dingus\SyncroService\PromotionLineRec
     */
    public function setAdultFrom($AdultFrom)
    {
      $this->AdultFrom = $AdultFrom;
      return $this;
    }

    /**
     * @return int
     */
    public function getAdultTo()
    {
      return $this->AdultTo;
    }

    /**
     * @param int $AdultTo
     * @return \Dingus\SyncroService\PromotionLineRec
     */
    public function setAdultTo($AdultTo)
    {
      $this->AdultTo = $AdultTo;
      return $this;
    }

    /**
     * @return int
     */
    public function getAgeFrom()
    {
      return $this->AgeFrom;
    }

    /**
     * @param int $AgeFrom
     * @return \Dingus\SyncroService\PromotionLineRec
     */
    public function setAgeFrom($AgeFrom)
    {
      $this->AgeFrom = $AgeFrom;
      return $this;
    }

    /**
     * @return int
     */
    public function getAgeTo()
    {
      return $this->AgeTo;
    }

    /**
     * @param int $AgeTo
     * @return \Dingus\SyncroService\PromotionLineRec
     */
    public function setAgeTo($AgeTo)
    {
      $this->AgeTo = $AgeTo;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getApplyToBoard()
    {
      return $this->ApplyToBoard;
    }

    /**
     * @param boolean $ApplyToBoard
     * @return \Dingus\SyncroService\PromotionLineRec
     */
    public function setApplyToBoard($ApplyToBoard)
    {
      $this->ApplyToBoard = $ApplyToBoard;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getApplyToRoom()
    {
      return $this->ApplyToRoom;
    }

    /**
     * @param boolean $ApplyToRoom
     * @return \Dingus\SyncroService\PromotionLineRec
     */
    public function setApplyToRoom($ApplyToRoom)
    {
      $this->ApplyToRoom = $ApplyToRoom;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getApplyToSupl()
    {
      return $this->ApplyToSupl;
    }

    /**
     * @param boolean $ApplyToSupl
     * @return \Dingus\SyncroService\PromotionLineRec
     */
    public function setApplyToSupl($ApplyToSupl)
    {
      $this->ApplyToSupl = $ApplyToSupl;
      return $this;
    }

    /**
     * @return string
     */
    public function getBoardCode()
    {
      return $this->BoardCode;
    }

    /**
     * @param string $BoardCode
     * @return \Dingus\SyncroService\PromotionLineRec
     */
    public function setBoardCode($BoardCode)
    {
      $this->BoardCode = $BoardCode;
      return $this;
    }

    /**
     * @return int
     */
    public function getChildFrom()
    {
      return $this->ChildFrom;
    }

    /**
     * @param int $ChildFrom
     * @return \Dingus\SyncroService\PromotionLineRec
     */
    public function setChildFrom($ChildFrom)
    {
      $this->ChildFrom = $ChildFrom;
      return $this;
    }

    /**
     * @return int
     */
    public function getChildTo()
    {
      return $this->ChildTo;
    }

    /**
     * @param int $ChildTo
     * @return \Dingus\SyncroService\PromotionLineRec
     */
    public function setChildTo($ChildTo)
    {
      $this->ChildTo = $ChildTo;
      return $this;
    }

    /**
     * @return float
     */
    public function getChildPrice()
    {
      return $this->ChildPrice;
    }

    /**
     * @param float $ChildPrice
     * @return \Dingus\SyncroService\PromotionLineRec
     */
    public function setChildPrice($ChildPrice)
    {
      $this->ChildPrice = $ChildPrice;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getEffectiveDate()
    {
      if ($this->EffectiveDate == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->EffectiveDate);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $EffectiveDate
     * @return \Dingus\SyncroService\PromotionLineRec
     */
    public function setEffectiveDate(\DateTime $EffectiveDate)
    {
      $this->EffectiveDate = $EffectiveDate->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateFrom()
    {
      if ($this->DateFrom == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->DateFrom);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $DateFrom
     * @return \Dingus\SyncroService\PromotionLineRec
     */
    public function setDateFrom(\DateTime $DateFrom)
    {
      $this->DateFrom = $DateFrom->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateFrom2()
    {
      if ($this->DateFrom2 == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->DateFrom2);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $DateFrom2
     * @return \Dingus\SyncroService\PromotionLineRec
     */
    public function setDateFrom2(\DateTime $DateFrom2)
    {
      $this->DateFrom2 = $DateFrom2->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return TipoFechasPrecio
     */
    public function getDatesType()
    {
      return $this->DatesType;
    }

    /**
     * @param TipoFechasPrecio $DatesType
     * @return \Dingus\SyncroService\PromotionLineRec
     */
    public function setDatesType($DatesType)
    {
      $this->DatesType = $DatesType;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateTo()
    {
      if ($this->DateTo == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->DateTo);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $DateTo
     * @return \Dingus\SyncroService\PromotionLineRec
     */
    public function setDateTo(\DateTime $DateTo)
    {
      $this->DateTo = $DateTo->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateTo2()
    {
      if ($this->DateTo2 == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->DateTo2);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $DateTo2
     * @return \Dingus\SyncroService\PromotionLineRec
     */
    public function setDateTo2(\DateTime $DateTo2)
    {
      $this->DateTo2 = $DateTo2->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getHourFrom()
    {
      if ($this->HourFrom == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->HourFrom);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $HourFrom
     * @return \Dingus\SyncroService\PromotionLineRec
     */
    public function setHourFrom(\DateTime $HourFrom)
    {
      $this->HourFrom = $HourFrom->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getHourTo()
    {
      if ($this->HourTo == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->HourTo);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $HourTo
     * @return \Dingus\SyncroService\PromotionLineRec
     */
    public function setHourTo(\DateTime $HourTo)
    {
      $this->HourTo = $HourTo->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return int
     */
    public function getDaysBeforeArrivalFrom()
    {
      return $this->DaysBeforeArrivalFrom;
    }

    /**
     * @param int $DaysBeforeArrivalFrom
     * @return \Dingus\SyncroService\PromotionLineRec
     */
    public function setDaysBeforeArrivalFrom($DaysBeforeArrivalFrom)
    {
      $this->DaysBeforeArrivalFrom = $DaysBeforeArrivalFrom;
      return $this;
    }

    /**
     * @return int
     */
    public function getDaysBeforeArrivalTo()
    {
      return $this->DaysBeforeArrivalTo;
    }

    /**
     * @param int $DaysBeforeArrivalTo
     * @return \Dingus\SyncroService\PromotionLineRec
     */
    public function setDaysBeforeArrivalTo($DaysBeforeArrivalTo)
    {
      $this->DaysBeforeArrivalTo = $DaysBeforeArrivalTo;
      return $this;
    }

    /**
     * @return TipoPax
     */
    public function getPaxType()
    {
      return $this->PaxType;
    }

    /**
     * @param TipoPax $PaxType
     * @return \Dingus\SyncroService\PromotionLineRec
     */
    public function setPaxType($PaxType)
    {
      $this->PaxType = $PaxType;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getPaxTypeSpecified()
    {
      return $this->PaxTypeSpecified;
    }

    /**
     * @param boolean $PaxTypeSpecified
     * @return \Dingus\SyncroService\PromotionLineRec
     */
    public function setPaxTypeSpecified($PaxTypeSpecified)
    {
      $this->PaxTypeSpecified = $PaxTypeSpecified;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getIsPercent()
    {
      return $this->IsPercent;
    }

    /**
     * @param boolean $IsPercent
     * @return \Dingus\SyncroService\PromotionLineRec
     */
    public function setIsPercent($IsPercent)
    {
      $this->IsPercent = $IsPercent;
      return $this;
    }

    /**
     * @return int
     */
    public function getPaxFrom()
    {
      return $this->PaxFrom;
    }

    /**
     * @param int $PaxFrom
     * @return \Dingus\SyncroService\PromotionLineRec
     */
    public function setPaxFrom($PaxFrom)
    {
      $this->PaxFrom = $PaxFrom;
      return $this;
    }

    /**
     * @return int
     */
    public function getPaxNo()
    {
      return $this->PaxNo;
    }

    /**
     * @param int $PaxNo
     * @return \Dingus\SyncroService\PromotionLineRec
     */
    public function setPaxNo($PaxNo)
    {
      $this->PaxNo = $PaxNo;
      return $this;
    }

    /**
     * @return int
     */
    public function getPaxTo()
    {
      return $this->PaxTo;
    }

    /**
     * @param int $PaxTo
     * @return \Dingus\SyncroService\PromotionLineRec
     */
    public function setPaxTo($PaxTo)
    {
      $this->PaxTo = $PaxTo;
      return $this;
    }

    /**
     * @return int
     */
    public function getDaysClosePrice()
    {
      return $this->DaysClosePrice;
    }

    /**
     * @param int $DaysClosePrice
     * @return \Dingus\SyncroService\PromotionLineRec
     */
    public function setDaysClosePrice($DaysClosePrice)
    {
      $this->DaysClosePrice = $DaysClosePrice;
      return $this;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
      return $this->Price;
    }

    /**
     * @param float $Price
     * @return \Dingus\SyncroService\PromotionLineRec
     */
    public function setPrice($Price)
    {
      $this->Price = $Price;
      return $this;
    }

    /**
     * @return float
     */
    public function getSecondChidlPrice()
    {
      return $this->SecondChidlPrice;
    }

    /**
     * @param float $SecondChidlPrice
     * @return \Dingus\SyncroService\PromotionLineRec
     */
    public function setSecondChidlPrice($SecondChidlPrice)
    {
      $this->SecondChidlPrice = $SecondChidlPrice;
      return $this;
    }

    /**
     * @return float
     */
    public function getThirdChildPrice()
    {
      return $this->ThirdChildPrice;
    }

    /**
     * @param float $ThirdChildPrice
     * @return \Dingus\SyncroService\PromotionLineRec
     */
    public function setThirdChildPrice($ThirdChildPrice)
    {
      $this->ThirdChildPrice = $ThirdChildPrice;
      return $this;
    }

    /**
     * @return float
     */
    public function getThirdPaxPrice()
    {
      return $this->ThirdPaxPrice;
    }

    /**
     * @param float $ThirdPaxPrice
     * @return \Dingus\SyncroService\PromotionLineRec
     */
    public function setThirdPaxPrice($ThirdPaxPrice)
    {
      $this->ThirdPaxPrice = $ThirdPaxPrice;
      return $this;
    }

    /**
     * @return float
     */
    public function getFourthChildPrice()
    {
      return $this->FourthChildPrice;
    }

    /**
     * @param float $FourthChildPrice
     * @return \Dingus\SyncroService\PromotionLineRec
     */
    public function setFourthChildPrice($FourthChildPrice)
    {
      $this->FourthChildPrice = $FourthChildPrice;
      return $this;
    }

    /**
     * @return float
     */
    public function getFourthPaxPrice()
    {
      return $this->FourthPaxPrice;
    }

    /**
     * @param float $FourthPaxPrice
     * @return \Dingus\SyncroService\PromotionLineRec
     */
    public function setFourthPaxPrice($FourthPaxPrice)
    {
      $this->FourthPaxPrice = $FourthPaxPrice;
      return $this;
    }

    /**
     * @return float
     */
    public function getEnfantPrice()
    {
      return $this->EnfantPrice;
    }

    /**
     * @param float $EnfantPrice
     * @return \Dingus\SyncroService\PromotionLineRec
     */
    public function setEnfantPrice($EnfantPrice)
    {
      $this->EnfantPrice = $EnfantPrice;
      return $this;
    }

    /**
     * @return float
     */
    public function getExtraNightPrice()
    {
      return $this->ExtraNightPrice;
    }

    /**
     * @param float $ExtraNightPrice
     * @return \Dingus\SyncroService\PromotionLineRec
     */
    public function setExtraNightPrice($ExtraNightPrice)
    {
      $this->ExtraNightPrice = $ExtraNightPrice;
      return $this;
    }

    /**
     * @return int
     */
    public function getNightFrom()
    {
      return $this->NightFrom;
    }

    /**
     * @param int $NightFrom
     * @return \Dingus\SyncroService\PromotionLineRec
     */
    public function setNightFrom($NightFrom)
    {
      $this->NightFrom = $NightFrom;
      return $this;
    }

    /**
     * @return int
     */
    public function getNightTo()
    {
      return $this->NightTo;
    }

    /**
     * @param int $NightTo
     * @return \Dingus\SyncroService\PromotionLineRec
     */
    public function setNightTo($NightTo)
    {
      $this->NightTo = $NightTo;
      return $this;
    }

    /**
     * @return int
     */
    public function getStay()
    {
      return $this->Stay;
    }

    /**
     * @param int $Stay
     * @return \Dingus\SyncroService\PromotionLineRec
     */
    public function setStay($Stay)
    {
      $this->Stay = $Stay;
      return $this;
    }

    /**
     * @return int
     */
    public function getStayToInvice()
    {
      return $this->StayToInvice;
    }

    /**
     * @param int $StayToInvice
     * @return \Dingus\SyncroService\PromotionLineRec
     */
    public function setStayToInvice($StayToInvice)
    {
      $this->StayToInvice = $StayToInvice;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getExcluding()
    {
      return $this->Excluding;
    }

    /**
     * @param boolean $Excluding
     * @return \Dingus\SyncroService\PromotionLineRec
     */
    public function setExcluding($Excluding)
    {
      $this->Excluding = $Excluding;
      return $this;
    }

    /**
     * @return int
     */
    public function getPriorityExcluding()
    {
      return $this->PriorityExcluding;
    }

    /**
     * @param int $PriorityExcluding
     * @return \Dingus\SyncroService\PromotionLineRec
     */
    public function setPriorityExcluding($PriorityExcluding)
    {
      $this->PriorityExcluding = $PriorityExcluding;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getUniqueApplication()
    {
      return $this->UniqueApplication;
    }

    /**
     * @param boolean $UniqueApplication
     * @return \Dingus\SyncroService\PromotionLineRec
     */
    public function setUniqueApplication($UniqueApplication)
    {
      $this->UniqueApplication = $UniqueApplication;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getMonday()
    {
      return $this->Monday;
    }

    /**
     * @param boolean $Monday
     * @return \Dingus\SyncroService\PromotionLineRec
     */
    public function setMonday($Monday)
    {
      $this->Monday = $Monday;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getTuesday()
    {
      return $this->Tuesday;
    }

    /**
     * @param boolean $Tuesday
     * @return \Dingus\SyncroService\PromotionLineRec
     */
    public function setTuesday($Tuesday)
    {
      $this->Tuesday = $Tuesday;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getWednesday()
    {
      return $this->Wednesday;
    }

    /**
     * @param boolean $Wednesday
     * @return \Dingus\SyncroService\PromotionLineRec
     */
    public function setWednesday($Wednesday)
    {
      $this->Wednesday = $Wednesday;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getThursday()
    {
      return $this->Thursday;
    }

    /**
     * @param boolean $Thursday
     * @return \Dingus\SyncroService\PromotionLineRec
     */
    public function setThursday($Thursday)
    {
      $this->Thursday = $Thursday;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getFriday()
    {
      return $this->Friday;
    }

    /**
     * @param boolean $Friday
     * @return \Dingus\SyncroService\PromotionLineRec
     */
    public function setFriday($Friday)
    {
      $this->Friday = $Friday;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getSaturday()
    {
      return $this->Saturday;
    }

    /**
     * @param boolean $Saturday
     * @return \Dingus\SyncroService\PromotionLineRec
     */
    public function setSaturday($Saturday)
    {
      $this->Saturday = $Saturday;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getSunday()
    {
      return $this->Sunday;
    }

    /**
     * @param boolean $Sunday
     * @return \Dingus\SyncroService\PromotionLineRec
     */
    public function setSunday($Sunday)
    {
      $this->Sunday = $Sunday;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getStayAxBNoExact()
    {
      return $this->StayAxBNoExact;
    }

    /**
     * @param boolean $StayAxBNoExact
     * @return \Dingus\SyncroService\PromotionLineRec
     */
    public function setStayAxBNoExact($StayAxBNoExact)
    {
      $this->StayAxBNoExact = $StayAxBNoExact;
      return $this;
    }

    /**
     * @return TipoNocheDescontarAxB
     */
    public function getNightTypeDiscountAxB()
    {
      return $this->NightTypeDiscountAxB;
    }

    /**
     * @param TipoNocheDescontarAxB $NightTypeDiscountAxB
     * @return \Dingus\SyncroService\PromotionLineRec
     */
    public function setNightTypeDiscountAxB($NightTypeDiscountAxB)
    {
      $this->NightTypeDiscountAxB = $NightTypeDiscountAxB;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getArrivalAtMonday()
    {
      return $this->ArrivalAtMonday;
    }

    /**
     * @param boolean $ArrivalAtMonday
     * @return \Dingus\SyncroService\PromotionLineRec
     */
    public function setArrivalAtMonday($ArrivalAtMonday)
    {
      $this->ArrivalAtMonday = $ArrivalAtMonday;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getArrivalAtTuesday()
    {
      return $this->ArrivalAtTuesday;
    }

    /**
     * @param boolean $ArrivalAtTuesday
     * @return \Dingus\SyncroService\PromotionLineRec
     */
    public function setArrivalAtTuesday($ArrivalAtTuesday)
    {
      $this->ArrivalAtTuesday = $ArrivalAtTuesday;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getArrivalAtWednesday()
    {
      return $this->ArrivalAtWednesday;
    }

    /**
     * @param boolean $ArrivalAtWednesday
     * @return \Dingus\SyncroService\PromotionLineRec
     */
    public function setArrivalAtWednesday($ArrivalAtWednesday)
    {
      $this->ArrivalAtWednesday = $ArrivalAtWednesday;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getArrivalAtThursday()
    {
      return $this->ArrivalAtThursday;
    }

    /**
     * @param boolean $ArrivalAtThursday
     * @return \Dingus\SyncroService\PromotionLineRec
     */
    public function setArrivalAtThursday($ArrivalAtThursday)
    {
      $this->ArrivalAtThursday = $ArrivalAtThursday;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getArrivalAtFriday()
    {
      return $this->ArrivalAtFriday;
    }

    /**
     * @param boolean $ArrivalAtFriday
     * @return \Dingus\SyncroService\PromotionLineRec
     */
    public function setArrivalAtFriday($ArrivalAtFriday)
    {
      $this->ArrivalAtFriday = $ArrivalAtFriday;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getArrivalAtSaturday()
    {
      return $this->ArrivalAtSaturday;
    }

    /**
     * @param boolean $ArrivalAtSaturday
     * @return \Dingus\SyncroService\PromotionLineRec
     */
    public function setArrivalAtSaturday($ArrivalAtSaturday)
    {
      $this->ArrivalAtSaturday = $ArrivalAtSaturday;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getArrivalAtSunday()
    {
      return $this->ArrivalAtSunday;
    }

    /**
     * @param boolean $ArrivalAtSunday
     * @return \Dingus\SyncroService\PromotionLineRec
     */
    public function setArrivalAtSunday($ArrivalAtSunday)
    {
      $this->ArrivalAtSunday = $ArrivalAtSunday;
      return $this;
    }

}
