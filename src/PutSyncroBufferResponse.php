<?php

namespace Dingus\SyncroService;

class PutSyncroBufferResponse implements \JsonSerializable
{

    /**
     * @var SyncroRS $PutSyncroBufferResult
     */
    protected $PutSyncroBufferResult = null;

    /**
     * @param SyncroRS $PutSyncroBufferResult
     */
    public function __construct($PutSyncroBufferResult)
    {
      $this->PutSyncroBufferResult = $PutSyncroBufferResult;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'PutSyncroBufferResult' => $this->getPutSyncroBufferResult(),
      );
    }

    /**
     * @return SyncroRS
     */
    public function getPutSyncroBufferResult()
    {
      return $this->PutSyncroBufferResult;
    }

    /**
     * @param SyncroRS $PutSyncroBufferResult
     * @return \Dingus\SyncroService\PutSyncroBufferResponse
     */
    public function setPutSyncroBufferResult($PutSyncroBufferResult)
    {
      $this->PutSyncroBufferResult = $PutSyncroBufferResult;
      return $this;
    }

}
