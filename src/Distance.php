<?php

namespace Dingus\SyncroService;

class Distance implements \JsonSerializable
{

    /**
     * @var ArrayOfMlText $DistanceFromDescriptionMlList
     */
    protected $DistanceFromDescriptionMlList = null;

    /**
     * @var ArrayOfPicture $Pictures
     */
    protected $Pictures = null;

    /**
     * @var Action $Action
     */
    protected $Action = null;

    /**
     * @var int $Id
     */
    protected $Id = null;

    /**
     * @var string $DistanceFrom
     */
    protected $DistanceFrom = null;

    /**
     * @var string $DistanceFromDescription
     */
    protected $DistanceFromDescription = null;

    /**
     * @var int $DistanceInMeters
     */
    protected $DistanceInMeters = null;

    /**
     * @var int $DistanceInMinutes
     */
    protected $DistanceInMinutes = null;

    /**
     * @param Action $Action
     * @param int $Id
     * @param int $DistanceInMeters
     * @param int $DistanceInMinutes
     */
    public function __construct($Action, $Id, $DistanceInMeters, $DistanceInMinutes)
    {
      $this->Action = $Action;
      $this->Id = $Id;
      $this->DistanceInMeters = $DistanceInMeters;
      $this->DistanceInMinutes = $DistanceInMinutes;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'DistanceFromDescriptionMlList' => $this->getDistanceFromDescriptionMlList(),
        'Pictures' => $this->getPictures(),
        'Action' => $this->getAction(),
        'Id' => $this->getId(),
        'DistanceFrom' => $this->getDistanceFrom(),
        'DistanceFromDescription' => $this->getDistanceFromDescription(),
        'DistanceInMeters' => $this->getDistanceInMeters(),
        'DistanceInMinutes' => $this->getDistanceInMinutes(),
      );
    }

    /**
     * @return ArrayOfMlText
     */
    public function getDistanceFromDescriptionMlList()
    {
      return $this->DistanceFromDescriptionMlList;
    }

    /**
     * @param ArrayOfMlText $DistanceFromDescriptionMlList
     * @return \Dingus\SyncroService\Distance
     */
    public function setDistanceFromDescriptionMlList($DistanceFromDescriptionMlList)
    {
      $this->DistanceFromDescriptionMlList = $DistanceFromDescriptionMlList;
      return $this;
    }

    /**
     * @return ArrayOfPicture
     */
    public function getPictures()
    {
      return $this->Pictures;
    }

    /**
     * @param ArrayOfPicture $Pictures
     * @return \Dingus\SyncroService\Distance
     */
    public function setPictures($Pictures)
    {
      $this->Pictures = $Pictures;
      return $this;
    }

    /**
     * @return Action
     */
    public function getAction()
    {
      return $this->Action;
    }

    /**
     * @param Action $Action
     * @return \Dingus\SyncroService\Distance
     */
    public function setAction($Action)
    {
      $this->Action = $Action;
      return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
      return $this->Id;
    }

    /**
     * @param int $Id
     * @return \Dingus\SyncroService\Distance
     */
    public function setId($Id)
    {
      $this->Id = $Id;
      return $this;
    }

    /**
     * @return string
     */
    public function getDistanceFrom()
    {
      return $this->DistanceFrom;
    }

    /**
     * @param string $DistanceFrom
     * @return \Dingus\SyncroService\Distance
     */
    public function setDistanceFrom($DistanceFrom)
    {
      $this->DistanceFrom = $DistanceFrom;
      return $this;
    }

    /**
     * @return string
     */
    public function getDistanceFromDescription()
    {
      return $this->DistanceFromDescription;
    }

    /**
     * @param string $DistanceFromDescription
     * @return \Dingus\SyncroService\Distance
     */
    public function setDistanceFromDescription($DistanceFromDescription)
    {
      $this->DistanceFromDescription = $DistanceFromDescription;
      return $this;
    }

    /**
     * @return int
     */
    public function getDistanceInMeters()
    {
      return $this->DistanceInMeters;
    }

    /**
     * @param int $DistanceInMeters
     * @return \Dingus\SyncroService\Distance
     */
    public function setDistanceInMeters($DistanceInMeters)
    {
      $this->DistanceInMeters = $DistanceInMeters;
      return $this;
    }

    /**
     * @return int
     */
    public function getDistanceInMinutes()
    {
      return $this->DistanceInMinutes;
    }

    /**
     * @param int $DistanceInMinutes
     * @return \Dingus\SyncroService\Distance
     */
    public function setDistanceInMinutes($DistanceInMinutes)
    {
      $this->DistanceInMinutes = $DistanceInMinutes;
      return $this;
    }

}
