<?php

namespace Dingus\SyncroService;

class BlockAllotmentRQ implements \JsonSerializable
{

    /**
     * @var \DateTime $Date
     */
    protected $Date = null;

    /**
     * @var string $RoomCode
     */
    protected $RoomCode = null;

    /**
     * @var int $NoRooms
     */
    protected $NoRooms = null;

    /**
     * @param \DateTime $Date
     * @param int $NoRooms
     */
    public function __construct(\DateTime $Date, $NoRooms)
    {
      $this->Date = $Date->format(\DateTime::ATOM);
      $this->NoRooms = $NoRooms;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'Date' => $this->getDate(),
        'RoomCode' => $this->getRoomCode(),
        'NoRooms' => $this->getNoRooms(),
      );
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
      if ($this->Date == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->Date);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $Date
     * @return \Dingus\SyncroService\BlockAllotmentRQ
     */
    public function setDate(\DateTime $Date)
    {
      $this->Date = $Date->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return string
     */
    public function getRoomCode()
    {
      return $this->RoomCode;
    }

    /**
     * @param string $RoomCode
     * @return \Dingus\SyncroService\BlockAllotmentRQ
     */
    public function setRoomCode($RoomCode)
    {
      $this->RoomCode = $RoomCode;
      return $this;
    }

    /**
     * @return int
     */
    public function getNoRooms()
    {
      return $this->NoRooms;
    }

    /**
     * @param int $NoRooms
     * @return \Dingus\SyncroService\BlockAllotmentRQ
     */
    public function setNoRooms($NoRooms)
    {
      $this->NoRooms = $NoRooms;
      return $this;
    }

}
