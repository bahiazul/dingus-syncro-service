<?php

namespace Dingus\SyncroService;

class ArrayOfCustomerPromotionsRec implements \JsonSerializable, \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var CustomerPromotionsRec[] $CustomerPromotionsRec
     */
    protected $CustomerPromotionsRec = null;

    
    public function __construct()
    {
    
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'CustomerPromotionsRec' => $this->getCustomerPromotionsRec(),
      );
    }

    /**
     * @return CustomerPromotionsRec[]
     */
    public function getCustomerPromotionsRec()
    {
      return $this->CustomerPromotionsRec;
    }

    /**
     * @param CustomerPromotionsRec[] $CustomerPromotionsRec
     * @return \Dingus\SyncroService\ArrayOfCustomerPromotionsRec
     */
    public function setCustomerPromotionsRec(array $CustomerPromotionsRec = null)
    {
      $this->CustomerPromotionsRec = $CustomerPromotionsRec;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->CustomerPromotionsRec[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return CustomerPromotionsRec
     */
    public function offsetGet($offset)
    {
      return $this->CustomerPromotionsRec[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param CustomerPromotionsRec $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      $this->CustomerPromotionsRec[$offset] = $value;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->CustomerPromotionsRec[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return CustomerPromotionsRec Return the current element
     */
    public function current()
    {
      return current($this->CustomerPromotionsRec);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->CustomerPromotionsRec);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->CustomerPromotionsRec);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->CustomerPromotionsRec);
    }

    /**
     * Countable implementation
     *
     * @return CustomerPromotionsRec Return count of elements
     */
    public function count()
    {
      return count($this->CustomerPromotionsRec);
    }

}
