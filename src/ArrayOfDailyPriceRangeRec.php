<?php

namespace Dingus\SyncroService;

class ArrayOfDailyPriceRangeRec implements \JsonSerializable, \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var DailyPriceRangeRec[] $DailyPriceRangeRec
     */
    protected $DailyPriceRangeRec = null;

    
    public function __construct()
    {
    
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'DailyPriceRangeRec' => $this->getDailyPriceRangeRec(),
      );
    }

    /**
     * @return DailyPriceRangeRec[]
     */
    public function getDailyPriceRangeRec()
    {
      return $this->DailyPriceRangeRec;
    }

    /**
     * @param DailyPriceRangeRec[] $DailyPriceRangeRec
     * @return \Dingus\SyncroService\ArrayOfDailyPriceRangeRec
     */
    public function setDailyPriceRangeRec(array $DailyPriceRangeRec = null)
    {
      $this->DailyPriceRangeRec = $DailyPriceRangeRec;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->DailyPriceRangeRec[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return DailyPriceRangeRec
     */
    public function offsetGet($offset)
    {
      return $this->DailyPriceRangeRec[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param DailyPriceRangeRec $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      $this->DailyPriceRangeRec[$offset] = $value;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->DailyPriceRangeRec[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return DailyPriceRangeRec Return the current element
     */
    public function current()
    {
      return current($this->DailyPriceRangeRec);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->DailyPriceRangeRec);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->DailyPriceRangeRec);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->DailyPriceRangeRec);
    }

    /**
     * Countable implementation
     *
     * @return DailyPriceRangeRec Return count of elements
     */
    public function count()
    {
      return count($this->DailyPriceRangeRec);
    }

}
