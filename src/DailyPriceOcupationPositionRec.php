<?php

namespace Dingus\SyncroService;

class DailyPriceOcupationPositionRec implements \JsonSerializable
{

    /**
     * @var Action $Action
     */
    protected $Action = null;

    /**
     * @var int $PaxNo
     */
    protected $PaxNo = null;

    /**
     * @var int $Id
     */
    protected $Id = null;

    /**
     * @var PaxType $PaxType
     */
    protected $PaxType = null;

    /**
     * @var int $Position
     */
    protected $Position = null;

    /**
     * @param Action $Action
     * @param int $PaxNo
     * @param int $Id
     * @param PaxType $PaxType
     * @param int $Position
     */
    public function __construct($Action, $PaxNo, $Id, $PaxType, $Position)
    {
      $this->Action = $Action;
      $this->PaxNo = $PaxNo;
      $this->Id = $Id;
      $this->PaxType = $PaxType;
      $this->Position = $Position;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'Action' => $this->getAction(),
        'PaxNo' => $this->getPaxNo(),
        'Id' => $this->getId(),
        'PaxType' => $this->getPaxType(),
        'Position' => $this->getPosition(),
      );
    }

    /**
     * @return Action
     */
    public function getAction()
    {
      return $this->Action;
    }

    /**
     * @param Action $Action
     * @return \Dingus\SyncroService\DailyPriceOcupationPositionRec
     */
    public function setAction($Action)
    {
      $this->Action = $Action;
      return $this;
    }

    /**
     * @return int
     */
    public function getPaxNo()
    {
      return $this->PaxNo;
    }

    /**
     * @param int $PaxNo
     * @return \Dingus\SyncroService\DailyPriceOcupationPositionRec
     */
    public function setPaxNo($PaxNo)
    {
      $this->PaxNo = $PaxNo;
      return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
      return $this->Id;
    }

    /**
     * @param int $Id
     * @return \Dingus\SyncroService\DailyPriceOcupationPositionRec
     */
    public function setId($Id)
    {
      $this->Id = $Id;
      return $this;
    }

    /**
     * @return PaxType
     */
    public function getPaxType()
    {
      return $this->PaxType;
    }

    /**
     * @param PaxType $PaxType
     * @return \Dingus\SyncroService\DailyPriceOcupationPositionRec
     */
    public function setPaxType($PaxType)
    {
      $this->PaxType = $PaxType;
      return $this;
    }

    /**
     * @return int
     */
    public function getPosition()
    {
      return $this->Position;
    }

    /**
     * @param int $Position
     * @return \Dingus\SyncroService\DailyPriceOcupationPositionRec
     */
    public function setPosition($Position)
    {
      $this->Position = $Position;
      return $this;
    }

}
