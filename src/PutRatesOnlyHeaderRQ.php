<?php

namespace Dingus\SyncroService;

class PutRatesOnlyHeaderRQ implements \JsonSerializable
{

    /**
     * @var Action $Action
     */
    protected $Action = null;

    /**
     * @var string $RateCode
     */
    protected $RateCode = null;

    /**
     * @var string $RateName
     */
    protected $RateName = null;

    /**
     * @var string $CustomerRatesGroupCode
     */
    protected $CustomerRatesGroupCode = null;

    /**
     * @var string $CustomerRatesGroupName
     */
    protected $CustomerRatesGroupName = null;

    /**
     * @var ArrayOfString $HotelCodeList
     */
    protected $HotelCodeList = null;

    /**
     * @var RateType $RateType
     */
    protected $RateType = null;

    /**
     * @var boolean $Active
     */
    protected $Active = null;

    /**
     * @var boolean $ByDefault
     */
    protected $ByDefault = null;

    /**
     * @var string $CurrencyCode
     */
    protected $CurrencyCode = null;

    /**
     * @var string $CurrencyName
     */
    protected $CurrencyName = null;

    /**
     * @var RateParentInfo $RateParentInfo
     */
    protected $RateParentInfo = null;

    /**
     * @param Action $Action
     * @param RateType $RateType
     * @param boolean $Active
     * @param boolean $ByDefault
     */
    public function __construct($Action, $RateType, $Active, $ByDefault)
    {
      $this->Action = $Action;
      $this->RateType = $RateType;
      $this->Active = $Active;
      $this->ByDefault = $ByDefault;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'Action' => $this->getAction(),
        'RateCode' => $this->getRateCode(),
        'RateName' => $this->getRateName(),
        'CustomerRatesGroupCode' => $this->getCustomerRatesGroupCode(),
        'CustomerRatesGroupName' => $this->getCustomerRatesGroupName(),
        'HotelCodeList' => $this->getHotelCodeList(),
        'RateType' => $this->getRateType(),
        'Active' => $this->getActive(),
        'ByDefault' => $this->getByDefault(),
        'CurrencyCode' => $this->getCurrencyCode(),
        'CurrencyName' => $this->getCurrencyName(),
        'RateParentInfo' => $this->getRateParentInfo(),
      );
    }

    /**
     * @return Action
     */
    public function getAction()
    {
      return $this->Action;
    }

    /**
     * @param Action $Action
     * @return \Dingus\SyncroService\PutRatesOnlyHeaderRQ
     */
    public function setAction($Action)
    {
      $this->Action = $Action;
      return $this;
    }

    /**
     * @return string
     */
    public function getRateCode()
    {
      return $this->RateCode;
    }

    /**
     * @param string $RateCode
     * @return \Dingus\SyncroService\PutRatesOnlyHeaderRQ
     */
    public function setRateCode($RateCode)
    {
      $this->RateCode = $RateCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getRateName()
    {
      return $this->RateName;
    }

    /**
     * @param string $RateName
     * @return \Dingus\SyncroService\PutRatesOnlyHeaderRQ
     */
    public function setRateName($RateName)
    {
      $this->RateName = $RateName;
      return $this;
    }

    /**
     * @return string
     */
    public function getCustomerRatesGroupCode()
    {
      return $this->CustomerRatesGroupCode;
    }

    /**
     * @param string $CustomerRatesGroupCode
     * @return \Dingus\SyncroService\PutRatesOnlyHeaderRQ
     */
    public function setCustomerRatesGroupCode($CustomerRatesGroupCode)
    {
      $this->CustomerRatesGroupCode = $CustomerRatesGroupCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getCustomerRatesGroupName()
    {
      return $this->CustomerRatesGroupName;
    }

    /**
     * @param string $CustomerRatesGroupName
     * @return \Dingus\SyncroService\PutRatesOnlyHeaderRQ
     */
    public function setCustomerRatesGroupName($CustomerRatesGroupName)
    {
      $this->CustomerRatesGroupName = $CustomerRatesGroupName;
      return $this;
    }

    /**
     * @return ArrayOfString
     */
    public function getHotelCodeList()
    {
      return $this->HotelCodeList;
    }

    /**
     * @param ArrayOfString $HotelCodeList
     * @return \Dingus\SyncroService\PutRatesOnlyHeaderRQ
     */
    public function setHotelCodeList($HotelCodeList)
    {
      $this->HotelCodeList = $HotelCodeList;
      return $this;
    }

    /**
     * @return RateType
     */
    public function getRateType()
    {
      return $this->RateType;
    }

    /**
     * @param RateType $RateType
     * @return \Dingus\SyncroService\PutRatesOnlyHeaderRQ
     */
    public function setRateType($RateType)
    {
      $this->RateType = $RateType;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getActive()
    {
      return $this->Active;
    }

    /**
     * @param boolean $Active
     * @return \Dingus\SyncroService\PutRatesOnlyHeaderRQ
     */
    public function setActive($Active)
    {
      $this->Active = $Active;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getByDefault()
    {
      return $this->ByDefault;
    }

    /**
     * @param boolean $ByDefault
     * @return \Dingus\SyncroService\PutRatesOnlyHeaderRQ
     */
    public function setByDefault($ByDefault)
    {
      $this->ByDefault = $ByDefault;
      return $this;
    }

    /**
     * @return string
     */
    public function getCurrencyCode()
    {
      return $this->CurrencyCode;
    }

    /**
     * @param string $CurrencyCode
     * @return \Dingus\SyncroService\PutRatesOnlyHeaderRQ
     */
    public function setCurrencyCode($CurrencyCode)
    {
      $this->CurrencyCode = $CurrencyCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getCurrencyName()
    {
      return $this->CurrencyName;
    }

    /**
     * @param string $CurrencyName
     * @return \Dingus\SyncroService\PutRatesOnlyHeaderRQ
     */
    public function setCurrencyName($CurrencyName)
    {
      $this->CurrencyName = $CurrencyName;
      return $this;
    }

    /**
     * @return RateParentInfo
     */
    public function getRateParentInfo()
    {
      return $this->RateParentInfo;
    }

    /**
     * @param RateParentInfo $RateParentInfo
     * @return \Dingus\SyncroService\PutRatesOnlyHeaderRQ
     */
    public function setRateParentInfo($RateParentInfo)
    {
      $this->RateParentInfo = $RateParentInfo;
      return $this;
    }

}
