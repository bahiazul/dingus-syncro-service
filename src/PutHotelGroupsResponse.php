<?php

namespace Dingus\SyncroService;

class PutHotelGroupsResponse implements \JsonSerializable
{

    /**
     * @var SyncroRS $PutHotelGroupsResult
     */
    protected $PutHotelGroupsResult = null;

    /**
     * @param SyncroRS $PutHotelGroupsResult
     */
    public function __construct($PutHotelGroupsResult)
    {
      $this->PutHotelGroupsResult = $PutHotelGroupsResult;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'PutHotelGroupsResult' => $this->getPutHotelGroupsResult(),
      );
    }

    /**
     * @return SyncroRS
     */
    public function getPutHotelGroupsResult()
    {
      return $this->PutHotelGroupsResult;
    }

    /**
     * @param SyncroRS $PutHotelGroupsResult
     * @return \Dingus\SyncroService\PutHotelGroupsResponse
     */
    public function setPutHotelGroupsResult($PutHotelGroupsResult)
    {
      $this->PutHotelGroupsResult = $PutHotelGroupsResult;
      return $this;
    }

}
