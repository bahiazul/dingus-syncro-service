<?php

namespace Dingus\SyncroService;

class PriceType
{
    const __default = 'Room';
    const Room = 'Room';
    const Board = 'Board';
    const Extra = 'Extra';
    const Supl = 'Supl';
    const Nothing = 'Nothing';
    const CancellationFee = 'CancellationFee';
    const Modify = 'Modify';
    const CashDiscount = 'CashDiscount';


}
