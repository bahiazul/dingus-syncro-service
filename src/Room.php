<?php

namespace Dingus\SyncroService;

class Room implements \JsonSerializable
{

    /**
     * @var ArrayOfMlText $RoomRecNameMlList
     */
    protected $RoomRecNameMlList = null;

    /**
     * @var ArrayOfMlText $RoomRecDescriptionMlList
     */
    protected $RoomRecDescriptionMlList = null;

    /**
     * @var ArrayOfMlText $BasicRoomNameMlList
     */
    protected $BasicRoomNameMlList = null;

    /**
     * @var ArrayOfPicture $Pictures
     */
    protected $Pictures = null;

    /**
     * @var ArrayOfInstalaciones $Instalaciones
     */
    protected $Instalaciones = null;

    /**
     * @var Action $Action
     */
    protected $Action = null;

    /**
     * @var string $RoomCode
     */
    protected $RoomCode = null;

    /**
     * @var string $RoomName
     */
    protected $RoomName = null;

    /**
     * @var string $RoomDescription
     */
    protected $RoomDescription = null;

    /**
     * @var string $BasicRoomCode
     */
    protected $BasicRoomCode = null;

    /**
     * @var string $BasicRoomName
     */
    protected $BasicRoomName = null;

    /**
     * @var string $PMSCode
     */
    protected $PMSCode = null;

    /**
     * @var int $MaxAd
     */
    protected $MaxAd = null;

    /**
     * @var int $MaxBb
     */
    protected $MaxBb = null;

    /**
     * @var int $MaxChd
     */
    protected $MaxChd = null;

    /**
     * @var int $MaxPax
     */
    protected $MaxPax = null;

    /**
     * @var int $MinAd
     */
    protected $MinAd = null;

    /**
     * @var int $MinBb
     */
    protected $MinBb = null;

    /**
     * @var int $MinChd
     */
    protected $MinChd = null;

    /**
     * @var int $MinPax
     */
    protected $MinPax = null;

    /**
     * @var int $NumRooms
     */
    protected $NumRooms = null;

    /**
     * @var int $HalfPax
     */
    protected $HalfPax = null;

    /**
     * @var int $MinAllotment
     */
    protected $MinAllotment = null;

    /**
     * @var float $AdultWeight
     */
    protected $AdultWeight = null;

    /**
     * @var float $ChildrenWeight
     */
    protected $ChildrenWeight = null;

    /**
     * @var float $MaxWeight
     */
    protected $MaxWeight = null;

    /**
     * @var boolean $HalfPrice
     */
    protected $HalfPrice = null;

    /**
     * @var boolean $BbAsAd
     */
    protected $BbAsAd = null;

    /**
     * @param Action $Action
     * @param int $MaxAd
     * @param int $MaxBb
     * @param int $MaxChd
     * @param int $MaxPax
     * @param int $MinAd
     * @param int $MinBb
     * @param int $MinChd
     * @param int $MinPax
     * @param int $NumRooms
     * @param int $HalfPax
     * @param int $MinAllotment
     * @param float $AdultWeight
     * @param float $ChildrenWeight
     * @param float $MaxWeight
     * @param boolean $HalfPrice
     * @param boolean $BbAsAd
     */
    public function __construct($Action, $MaxAd, $MaxBb, $MaxChd, $MaxPax, $MinAd, $MinBb, $MinChd, $MinPax, $NumRooms, $HalfPax, $MinAllotment, $AdultWeight, $ChildrenWeight, $MaxWeight, $HalfPrice, $BbAsAd)
    {
      $this->Action = $Action;
      $this->MaxAd = $MaxAd;
      $this->MaxBb = $MaxBb;
      $this->MaxChd = $MaxChd;
      $this->MaxPax = $MaxPax;
      $this->MinAd = $MinAd;
      $this->MinBb = $MinBb;
      $this->MinChd = $MinChd;
      $this->MinPax = $MinPax;
      $this->NumRooms = $NumRooms;
      $this->HalfPax = $HalfPax;
      $this->MinAllotment = $MinAllotment;
      $this->AdultWeight = $AdultWeight;
      $this->ChildrenWeight = $ChildrenWeight;
      $this->MaxWeight = $MaxWeight;
      $this->HalfPrice = $HalfPrice;
      $this->BbAsAd = $BbAsAd;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'RoomRecNameMlList' => $this->getRoomRecNameMlList(),
        'RoomRecDescriptionMlList' => $this->getRoomRecDescriptionMlList(),
        'BasicRoomNameMlList' => $this->getBasicRoomNameMlList(),
        'Pictures' => $this->getPictures(),
        'Instalaciones' => $this->getInstalaciones(),
        'Action' => $this->getAction(),
        'RoomCode' => $this->getRoomCode(),
        'RoomName' => $this->getRoomName(),
        'RoomDescription' => $this->getRoomDescription(),
        'BasicRoomCode' => $this->getBasicRoomCode(),
        'BasicRoomName' => $this->getBasicRoomName(),
        'PMSCode' => $this->getPMSCode(),
        'MaxAd' => $this->getMaxAd(),
        'MaxBb' => $this->getMaxBb(),
        'MaxChd' => $this->getMaxChd(),
        'MaxPax' => $this->getMaxPax(),
        'MinAd' => $this->getMinAd(),
        'MinBb' => $this->getMinBb(),
        'MinChd' => $this->getMinChd(),
        'MinPax' => $this->getMinPax(),
        'NumRooms' => $this->getNumRooms(),
        'HalfPax' => $this->getHalfPax(),
        'MinAllotment' => $this->getMinAllotment(),
        'AdultWeight' => $this->getAdultWeight(),
        'ChildrenWeight' => $this->getChildrenWeight(),
        'MaxWeight' => $this->getMaxWeight(),
        'HalfPrice' => $this->getHalfPrice(),
        'BbAsAd' => $this->getBbAsAd(),
      );
    }

    /**
     * @return ArrayOfMlText
     */
    public function getRoomRecNameMlList()
    {
      return $this->RoomRecNameMlList;
    }

    /**
     * @param ArrayOfMlText $RoomRecNameMlList
     * @return \Dingus\SyncroService\Room
     */
    public function setRoomRecNameMlList($RoomRecNameMlList)
    {
      $this->RoomRecNameMlList = $RoomRecNameMlList;
      return $this;
    }

    /**
     * @return ArrayOfMlText
     */
    public function getRoomRecDescriptionMlList()
    {
      return $this->RoomRecDescriptionMlList;
    }

    /**
     * @param ArrayOfMlText $RoomRecDescriptionMlList
     * @return \Dingus\SyncroService\Room
     */
    public function setRoomRecDescriptionMlList($RoomRecDescriptionMlList)
    {
      $this->RoomRecDescriptionMlList = $RoomRecDescriptionMlList;
      return $this;
    }

    /**
     * @return ArrayOfMlText
     */
    public function getBasicRoomNameMlList()
    {
      return $this->BasicRoomNameMlList;
    }

    /**
     * @param ArrayOfMlText $BasicRoomNameMlList
     * @return \Dingus\SyncroService\Room
     */
    public function setBasicRoomNameMlList($BasicRoomNameMlList)
    {
      $this->BasicRoomNameMlList = $BasicRoomNameMlList;
      return $this;
    }

    /**
     * @return ArrayOfPicture
     */
    public function getPictures()
    {
      return $this->Pictures;
    }

    /**
     * @param ArrayOfPicture $Pictures
     * @return \Dingus\SyncroService\Room
     */
    public function setPictures($Pictures)
    {
      $this->Pictures = $Pictures;
      return $this;
    }

    /**
     * @return ArrayOfInstalaciones
     */
    public function getInstalaciones()
    {
      return $this->Instalaciones;
    }

    /**
     * @param ArrayOfInstalaciones $Instalaciones
     * @return \Dingus\SyncroService\Room
     */
    public function setInstalaciones($Instalaciones)
    {
      $this->Instalaciones = $Instalaciones;
      return $this;
    }

    /**
     * @return Action
     */
    public function getAction()
    {
      return $this->Action;
    }

    /**
     * @param Action $Action
     * @return \Dingus\SyncroService\Room
     */
    public function setAction($Action)
    {
      $this->Action = $Action;
      return $this;
    }

    /**
     * @return string
     */
    public function getRoomCode()
    {
      return $this->RoomCode;
    }

    /**
     * @param string $RoomCode
     * @return \Dingus\SyncroService\Room
     */
    public function setRoomCode($RoomCode)
    {
      $this->RoomCode = $RoomCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getRoomName()
    {
      return $this->RoomName;
    }

    /**
     * @param string $RoomName
     * @return \Dingus\SyncroService\Room
     */
    public function setRoomName($RoomName)
    {
      $this->RoomName = $RoomName;
      return $this;
    }

    /**
     * @return string
     */
    public function getRoomDescription()
    {
      return $this->RoomDescription;
    }

    /**
     * @param string $RoomDescription
     * @return \Dingus\SyncroService\Room
     */
    public function setRoomDescription($RoomDescription)
    {
      $this->RoomDescription = $RoomDescription;
      return $this;
    }

    /**
     * @return string
     */
    public function getBasicRoomCode()
    {
      return $this->BasicRoomCode;
    }

    /**
     * @param string $BasicRoomCode
     * @return \Dingus\SyncroService\Room
     */
    public function setBasicRoomCode($BasicRoomCode)
    {
      $this->BasicRoomCode = $BasicRoomCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getBasicRoomName()
    {
      return $this->BasicRoomName;
    }

    /**
     * @param string $BasicRoomName
     * @return \Dingus\SyncroService\Room
     */
    public function setBasicRoomName($BasicRoomName)
    {
      $this->BasicRoomName = $BasicRoomName;
      return $this;
    }

    /**
     * @return string
     */
    public function getPMSCode()
    {
      return $this->PMSCode;
    }

    /**
     * @param string $PMSCode
     * @return \Dingus\SyncroService\Room
     */
    public function setPMSCode($PMSCode)
    {
      $this->PMSCode = $PMSCode;
      return $this;
    }

    /**
     * @return int
     */
    public function getMaxAd()
    {
      return $this->MaxAd;
    }

    /**
     * @param int $MaxAd
     * @return \Dingus\SyncroService\Room
     */
    public function setMaxAd($MaxAd)
    {
      $this->MaxAd = $MaxAd;
      return $this;
    }

    /**
     * @return int
     */
    public function getMaxBb()
    {
      return $this->MaxBb;
    }

    /**
     * @param int $MaxBb
     * @return \Dingus\SyncroService\Room
     */
    public function setMaxBb($MaxBb)
    {
      $this->MaxBb = $MaxBb;
      return $this;
    }

    /**
     * @return int
     */
    public function getMaxChd()
    {
      return $this->MaxChd;
    }

    /**
     * @param int $MaxChd
     * @return \Dingus\SyncroService\Room
     */
    public function setMaxChd($MaxChd)
    {
      $this->MaxChd = $MaxChd;
      return $this;
    }

    /**
     * @return int
     */
    public function getMaxPax()
    {
      return $this->MaxPax;
    }

    /**
     * @param int $MaxPax
     * @return \Dingus\SyncroService\Room
     */
    public function setMaxPax($MaxPax)
    {
      $this->MaxPax = $MaxPax;
      return $this;
    }

    /**
     * @return int
     */
    public function getMinAd()
    {
      return $this->MinAd;
    }

    /**
     * @param int $MinAd
     * @return \Dingus\SyncroService\Room
     */
    public function setMinAd($MinAd)
    {
      $this->MinAd = $MinAd;
      return $this;
    }

    /**
     * @return int
     */
    public function getMinBb()
    {
      return $this->MinBb;
    }

    /**
     * @param int $MinBb
     * @return \Dingus\SyncroService\Room
     */
    public function setMinBb($MinBb)
    {
      $this->MinBb = $MinBb;
      return $this;
    }

    /**
     * @return int
     */
    public function getMinChd()
    {
      return $this->MinChd;
    }

    /**
     * @param int $MinChd
     * @return \Dingus\SyncroService\Room
     */
    public function setMinChd($MinChd)
    {
      $this->MinChd = $MinChd;
      return $this;
    }

    /**
     * @return int
     */
    public function getMinPax()
    {
      return $this->MinPax;
    }

    /**
     * @param int $MinPax
     * @return \Dingus\SyncroService\Room
     */
    public function setMinPax($MinPax)
    {
      $this->MinPax = $MinPax;
      return $this;
    }

    /**
     * @return int
     */
    public function getNumRooms()
    {
      return $this->NumRooms;
    }

    /**
     * @param int $NumRooms
     * @return \Dingus\SyncroService\Room
     */
    public function setNumRooms($NumRooms)
    {
      $this->NumRooms = $NumRooms;
      return $this;
    }

    /**
     * @return int
     */
    public function getHalfPax()
    {
      return $this->HalfPax;
    }

    /**
     * @param int $HalfPax
     * @return \Dingus\SyncroService\Room
     */
    public function setHalfPax($HalfPax)
    {
      $this->HalfPax = $HalfPax;
      return $this;
    }

    /**
     * @return int
     */
    public function getMinAllotment()
    {
      return $this->MinAllotment;
    }

    /**
     * @param int $MinAllotment
     * @return \Dingus\SyncroService\Room
     */
    public function setMinAllotment($MinAllotment)
    {
      $this->MinAllotment = $MinAllotment;
      return $this;
    }

    /**
     * @return float
     */
    public function getAdultWeight()
    {
      return $this->AdultWeight;
    }

    /**
     * @param float $AdultWeight
     * @return \Dingus\SyncroService\Room
     */
    public function setAdultWeight($AdultWeight)
    {
      $this->AdultWeight = $AdultWeight;
      return $this;
    }

    /**
     * @return float
     */
    public function getChildrenWeight()
    {
      return $this->ChildrenWeight;
    }

    /**
     * @param float $ChildrenWeight
     * @return \Dingus\SyncroService\Room
     */
    public function setChildrenWeight($ChildrenWeight)
    {
      $this->ChildrenWeight = $ChildrenWeight;
      return $this;
    }

    /**
     * @return float
     */
    public function getMaxWeight()
    {
      return $this->MaxWeight;
    }

    /**
     * @param float $MaxWeight
     * @return \Dingus\SyncroService\Room
     */
    public function setMaxWeight($MaxWeight)
    {
      $this->MaxWeight = $MaxWeight;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getHalfPrice()
    {
      return $this->HalfPrice;
    }

    /**
     * @param boolean $HalfPrice
     * @return \Dingus\SyncroService\Room
     */
    public function setHalfPrice($HalfPrice)
    {
      $this->HalfPrice = $HalfPrice;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getBbAsAd()
    {
      return $this->BbAsAd;
    }

    /**
     * @param boolean $BbAsAd
     * @return \Dingus\SyncroService\Room
     */
    public function setBbAsAd($BbAsAd)
    {
      $this->BbAsAd = $BbAsAd;
      return $this;
    }

}
