<?php

namespace Dingus\SyncroService;

class ArrayOfHotelsPriceRules implements \JsonSerializable, \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var HotelsPriceRules[] $HotelsPriceRules
     */
    protected $HotelsPriceRules = null;

    
    public function __construct()
    {
    
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'HotelsPriceRules' => $this->getHotelsPriceRules(),
      );
    }

    /**
     * @return HotelsPriceRules[]
     */
    public function getHotelsPriceRules()
    {
      return $this->HotelsPriceRules;
    }

    /**
     * @param HotelsPriceRules[] $HotelsPriceRules
     * @return \Dingus\SyncroService\ArrayOfHotelsPriceRules
     */
    public function setHotelsPriceRules(array $HotelsPriceRules = null)
    {
      $this->HotelsPriceRules = $HotelsPriceRules;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->HotelsPriceRules[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return HotelsPriceRules
     */
    public function offsetGet($offset)
    {
      return $this->HotelsPriceRules[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param HotelsPriceRules $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      $this->HotelsPriceRules[$offset] = $value;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->HotelsPriceRules[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return HotelsPriceRules Return the current element
     */
    public function current()
    {
      return current($this->HotelsPriceRules);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->HotelsPriceRules);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->HotelsPriceRules);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->HotelsPriceRules);
    }

    /**
     * Countable implementation
     *
     * @return HotelsPriceRules Return count of elements
     */
    public function count()
    {
      return count($this->HotelsPriceRules);
    }

}
