<?php

namespace Dingus\SyncroService;

class PutComissionsHotelResponse implements \JsonSerializable
{

    /**
     * @var SyncroRS $PutComissionsHotelResult
     */
    protected $PutComissionsHotelResult = null;

    /**
     * @param SyncroRS $PutComissionsHotelResult
     */
    public function __construct($PutComissionsHotelResult)
    {
      $this->PutComissionsHotelResult = $PutComissionsHotelResult;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'PutComissionsHotelResult' => $this->getPutComissionsHotelResult(),
      );
    }

    /**
     * @return SyncroRS
     */
    public function getPutComissionsHotelResult()
    {
      return $this->PutComissionsHotelResult;
    }

    /**
     * @param SyncroRS $PutComissionsHotelResult
     * @return \Dingus\SyncroService\PutComissionsHotelResponse
     */
    public function setPutComissionsHotelResult($PutComissionsHotelResult)
    {
      $this->PutComissionsHotelResult = $PutComissionsHotelResult;
      return $this;
    }

}
