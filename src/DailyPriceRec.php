<?php

namespace Dingus\SyncroService;

class DailyPriceRec implements \JsonSerializable
{

    /**
     * @var Action $Action
     */
    protected $Action = null;

    /**
     * @var int $Id
     */
    protected $Id = null;

    /**
     * @var \DateTime $Date
     */
    protected $Date = null;

    /**
     * @var string $RoomCode
     */
    protected $RoomCode = null;

    /**
     * @var string $BoardCode
     */
    protected $BoardCode = null;

    /**
     * @var boolean $StopSales
     */
    protected $StopSales = null;

    /**
     * @var float $IndividualUsePrice
     */
    protected $IndividualUsePrice = null;

    /**
     * @var float $RoomPrice
     */
    protected $RoomPrice = null;

    /**
     * @var string $Reference
     */
    protected $Reference = null;

    /**
     * @var int $Stay
     */
    protected $Stay = null;

    /**
     * @var ArrayOfDailyPricePricesRec $Prices
     */
    protected $Prices = null;

    /**
     * @param Action $Action
     * @param int $Id
     * @param \DateTime $Date
     * @param boolean $StopSales
     * @param float $IndividualUsePrice
     * @param float $RoomPrice
     * @param int $Stay
     */
    public function __construct($Action, $Id, \DateTime $Date, $StopSales, $IndividualUsePrice, $RoomPrice, $Stay)
    {
      $this->Action = $Action;
      $this->Id = $Id;
      $this->Date = $Date->format(\DateTime::ATOM);
      $this->StopSales = $StopSales;
      $this->IndividualUsePrice = $IndividualUsePrice;
      $this->RoomPrice = $RoomPrice;
      $this->Stay = $Stay;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'Action' => $this->getAction(),
        'Id' => $this->getId(),
        'Date' => $this->getDate(),
        'RoomCode' => $this->getRoomCode(),
        'BoardCode' => $this->getBoardCode(),
        'StopSales' => $this->getStopSales(),
        'IndividualUsePrice' => $this->getIndividualUsePrice(),
        'RoomPrice' => $this->getRoomPrice(),
        'Reference' => $this->getReference(),
        'Stay' => $this->getStay(),
        'Prices' => $this->getPrices(),
      );
    }

    /**
     * @return Action
     */
    public function getAction()
    {
      return $this->Action;
    }

    /**
     * @param Action $Action
     * @return \Dingus\SyncroService\DailyPriceRec
     */
    public function setAction($Action)
    {
      $this->Action = $Action;
      return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
      return $this->Id;
    }

    /**
     * @param int $Id
     * @return \Dingus\SyncroService\DailyPriceRec
     */
    public function setId($Id)
    {
      $this->Id = $Id;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
      if ($this->Date == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->Date);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $Date
     * @return \Dingus\SyncroService\DailyPriceRec
     */
    public function setDate(\DateTime $Date)
    {
      $this->Date = $Date->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return string
     */
    public function getRoomCode()
    {
      return $this->RoomCode;
    }

    /**
     * @param string $RoomCode
     * @return \Dingus\SyncroService\DailyPriceRec
     */
    public function setRoomCode($RoomCode)
    {
      $this->RoomCode = $RoomCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getBoardCode()
    {
      return $this->BoardCode;
    }

    /**
     * @param string $BoardCode
     * @return \Dingus\SyncroService\DailyPriceRec
     */
    public function setBoardCode($BoardCode)
    {
      $this->BoardCode = $BoardCode;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getStopSales()
    {
      return $this->StopSales;
    }

    /**
     * @param boolean $StopSales
     * @return \Dingus\SyncroService\DailyPriceRec
     */
    public function setStopSales($StopSales)
    {
      $this->StopSales = $StopSales;
      return $this;
    }

    /**
     * @return float
     */
    public function getIndividualUsePrice()
    {
      return $this->IndividualUsePrice;
    }

    /**
     * @param float $IndividualUsePrice
     * @return \Dingus\SyncroService\DailyPriceRec
     */
    public function setIndividualUsePrice($IndividualUsePrice)
    {
      $this->IndividualUsePrice = $IndividualUsePrice;
      return $this;
    }

    /**
     * @return float
     */
    public function getRoomPrice()
    {
      return $this->RoomPrice;
    }

    /**
     * @param float $RoomPrice
     * @return \Dingus\SyncroService\DailyPriceRec
     */
    public function setRoomPrice($RoomPrice)
    {
      $this->RoomPrice = $RoomPrice;
      return $this;
    }

    /**
     * @return string
     */
    public function getReference()
    {
      return $this->Reference;
    }

    /**
     * @param string $Reference
     * @return \Dingus\SyncroService\DailyPriceRec
     */
    public function setReference($Reference)
    {
      $this->Reference = $Reference;
      return $this;
    }

    /**
     * @return int
     */
    public function getStay()
    {
      return $this->Stay;
    }

    /**
     * @param int $Stay
     * @return \Dingus\SyncroService\DailyPriceRec
     */
    public function setStay($Stay)
    {
      $this->Stay = $Stay;
      return $this;
    }

    /**
     * @return ArrayOfDailyPricePricesRec
     */
    public function getPrices()
    {
      return $this->Prices;
    }

    /**
     * @param ArrayOfDailyPricePricesRec $Prices
     * @return \Dingus\SyncroService\DailyPriceRec
     */
    public function setPrices($Prices)
    {
      $this->Prices = $Prices;
      return $this;
    }

}
