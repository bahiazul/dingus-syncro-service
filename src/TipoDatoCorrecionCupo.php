<?php

namespace Dingus\SyncroService;

class TipoDatoCorrecionCupo
{
    const __default = 'Cupo';
    const Cupo = 'Cupo';
    const Release = 'Release';
    const EstanciaMinima = 'EstanciaMinima';
    const EstanciaMaxima = 'EstanciaMaxima';
    const AumentoCupo = 'AumentoCupo';
    const DescuentoCupo = 'DescuentoCupo';
    const Disponible = 'Disponible';


}
