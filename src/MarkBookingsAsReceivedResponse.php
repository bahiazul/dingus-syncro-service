<?php

namespace Dingus\SyncroService;

class MarkBookingsAsReceivedResponse implements \JsonSerializable
{

    /**
     * @var SyncroRS $MarkBookingsAsReceivedResult
     */
    protected $MarkBookingsAsReceivedResult = null;

    /**
     * @param SyncroRS $MarkBookingsAsReceivedResult
     */
    public function __construct($MarkBookingsAsReceivedResult)
    {
      $this->MarkBookingsAsReceivedResult = $MarkBookingsAsReceivedResult;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'MarkBookingsAsReceivedResult' => $this->getMarkBookingsAsReceivedResult(),
      );
    }

    /**
     * @return SyncroRS
     */
    public function getMarkBookingsAsReceivedResult()
    {
      return $this->MarkBookingsAsReceivedResult;
    }

    /**
     * @param SyncroRS $MarkBookingsAsReceivedResult
     * @return \Dingus\SyncroService\MarkBookingsAsReceivedResponse
     */
    public function setMarkBookingsAsReceivedResult($MarkBookingsAsReceivedResult)
    {
      $this->MarkBookingsAsReceivedResult = $MarkBookingsAsReceivedResult;
      return $this;
    }

}
