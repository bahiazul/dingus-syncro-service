<?php

namespace Dingus\SyncroService;

class ArrayOfSuplementRateRec implements \JsonSerializable, \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var SuplementRateRec[] $SuplementRateRec
     */
    protected $SuplementRateRec = null;

    
    public function __construct()
    {
    
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'SuplementRateRec' => $this->getSuplementRateRec(),
      );
    }

    /**
     * @return SuplementRateRec[]
     */
    public function getSuplementRateRec()
    {
      return $this->SuplementRateRec;
    }

    /**
     * @param SuplementRateRec[] $SuplementRateRec
     * @return \Dingus\SyncroService\ArrayOfSuplementRateRec
     */
    public function setSuplementRateRec(array $SuplementRateRec = null)
    {
      $this->SuplementRateRec = $SuplementRateRec;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->SuplementRateRec[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return SuplementRateRec
     */
    public function offsetGet($offset)
    {
      return $this->SuplementRateRec[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param SuplementRateRec $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      $this->SuplementRateRec[$offset] = $value;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->SuplementRateRec[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return SuplementRateRec Return the current element
     */
    public function current()
    {
      return current($this->SuplementRateRec);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->SuplementRateRec);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->SuplementRateRec);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->SuplementRateRec);
    }

    /**
     * Countable implementation
     *
     * @return SuplementRateRec Return count of elements
     */
    public function count()
    {
      return count($this->SuplementRateRec);
    }

}
