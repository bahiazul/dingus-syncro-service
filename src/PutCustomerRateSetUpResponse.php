<?php

namespace Dingus\SyncroService;

class PutCustomerRateSetUpResponse implements \JsonSerializable
{

    /**
     * @var SyncroRS $PutCustomerRateSetUpResult
     */
    protected $PutCustomerRateSetUpResult = null;

    /**
     * @param SyncroRS $PutCustomerRateSetUpResult
     */
    public function __construct($PutCustomerRateSetUpResult)
    {
      $this->PutCustomerRateSetUpResult = $PutCustomerRateSetUpResult;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'PutCustomerRateSetUpResult' => $this->getPutCustomerRateSetUpResult(),
      );
    }

    /**
     * @return SyncroRS
     */
    public function getPutCustomerRateSetUpResult()
    {
      return $this->PutCustomerRateSetUpResult;
    }

    /**
     * @param SyncroRS $PutCustomerRateSetUpResult
     * @return \Dingus\SyncroService\PutCustomerRateSetUpResponse
     */
    public function setPutCustomerRateSetUpResult($PutCustomerRateSetUpResult)
    {
      $this->PutCustomerRateSetUpResult = $PutCustomerRateSetUpResult;
      return $this;
    }

}
