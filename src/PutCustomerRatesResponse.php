<?php

namespace Dingus\SyncroService;

class PutCustomerRatesResponse implements \JsonSerializable
{

    /**
     * @var SyncroRS $PutCustomerRatesResult
     */
    protected $PutCustomerRatesResult = null;

    /**
     * @param SyncroRS $PutCustomerRatesResult
     */
    public function __construct($PutCustomerRatesResult)
    {
      $this->PutCustomerRatesResult = $PutCustomerRatesResult;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'PutCustomerRatesResult' => $this->getPutCustomerRatesResult(),
      );
    }

    /**
     * @return SyncroRS
     */
    public function getPutCustomerRatesResult()
    {
      return $this->PutCustomerRatesResult;
    }

    /**
     * @param SyncroRS $PutCustomerRatesResult
     * @return \Dingus\SyncroService\PutCustomerRatesResponse
     */
    public function setPutCustomerRatesResult($PutCustomerRatesResult)
    {
      $this->PutCustomerRatesResult = $PutCustomerRatesResult;
      return $this;
    }

}
