<?php

namespace Dingus\SyncroService;

class PutPriceRules implements \JsonSerializable
{

    /**
     * @var Credentials $Credentials
     */
    protected $Credentials = null;

    /**
     * @var PutPriceRulesRQ $RQ
     */
    protected $RQ = null;

    /**
     * @param Credentials $Credentials
     * @param PutPriceRulesRQ $RQ
     */
    public function __construct($Credentials, $RQ)
    {
      $this->Credentials = $Credentials;
      $this->RQ = $RQ;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'Credentials' => $this->getCredentials(),
        'RQ' => $this->getRQ(),
      );
    }

    /**
     * @return Credentials
     */
    public function getCredentials()
    {
      return $this->Credentials;
    }

    /**
     * @param Credentials $Credentials
     * @return \Dingus\SyncroService\PutPriceRules
     */
    public function setCredentials($Credentials)
    {
      $this->Credentials = $Credentials;
      return $this;
    }

    /**
     * @return PutPriceRulesRQ
     */
    public function getRQ()
    {
      return $this->RQ;
    }

    /**
     * @param PutPriceRulesRQ $RQ
     * @return \Dingus\SyncroService\PutPriceRules
     */
    public function setRQ($RQ)
    {
      $this->RQ = $RQ;
      return $this;
    }

}
