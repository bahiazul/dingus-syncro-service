<?php

namespace Dingus\SyncroService;

class CustomerRateLine implements \JsonSerializable
{

    /**
     * @var Action $Action
     */
    protected $Action = null;

    /**
     * @var int $IdCustomerRateLine
     */
    protected $IdCustomerRateLine = null;

    /**
     * @var string $RoomCode
     */
    protected $RoomCode = null;

    /**
     * @var string $BoardCode
     */
    protected $BoardCode = null;

    /**
     * @var string $AllotmentGroupCode
     */
    protected $AllotmentGroupCode = null;

    /**
     * @var string $RateGroupCode
     */
    protected $RateGroupCode = null;

    /**
     * @var string $ReglaNetoCode
     */
    protected $ReglaNetoCode = null;

    /**
     * @var int $Adults
     */
    protected $Adults = null;

    /**
     * @var int $Children
     */
    protected $Children = null;

    /**
     * @var int $ChildrenAge
     */
    protected $ChildrenAge = null;

    /**
     * @var int $ChildrenAge2
     */
    protected $ChildrenAge2 = null;

    /**
     * @var int $ChildrenAge3
     */
    protected $ChildrenAge3 = null;

    /**
     * @var int $ChildrenAge4
     */
    protected $ChildrenAge4 = null;

    /**
     * @var int $Babies
     */
    protected $Babies = null;

    /**
     * @var string $RateCodeCustomerRate
     */
    protected $RateCodeCustomerRate = null;

    /**
     * @var string $RoomCodeCustomerRate
     */
    protected $RoomCodeCustomerRate = null;

    /**
     * @var string $BoardCodeCustomerRate
     */
    protected $BoardCodeCustomerRate = null;

    /**
     * @var boolean $PendingChanges
     */
    protected $PendingChanges = null;

    /**
     * @var boolean $CalcCustomerRate
     */
    protected $CalcCustomerRate = null;

    /**
     * @var boolean $PricexRoom
     */
    protected $PricexRoom = null;

    /**
     * @var boolean $NotAllotment
     */
    protected $NotAllotment = null;

    /**
     * @var int $MinAllotment
     */
    protected $MinAllotment = null;

    /**
     * @var boolean $NotAcceptStopSales
     */
    protected $NotAcceptStopSales = null;

    /**
     * @var \DateTime $DateFrom
     */
    protected $DateFrom = null;

    /**
     * @var \DateTime $DateTo
     */
    protected $DateTo = null;

    /**
     * @var string $ExtraCode1
     */
    protected $ExtraCode1 = null;

    /**
     * @var string $ExtraCode2
     */
    protected $ExtraCode2 = null;

    /**
     * @var string $ExtraCode3
     */
    protected $ExtraCode3 = null;

    /**
     * @var string $BookingDataCode1
     */
    protected $BookingDataCode1 = null;

    /**
     * @var string $BookingDataValue1
     */
    protected $BookingDataValue1 = null;

    /**
     * @var string $BookingDataCode2
     */
    protected $BookingDataCode2 = null;

    /**
     * @var string $BookingDataValue2
     */
    protected $BookingDataValue2 = null;

    /**
     * @var string $BookingDataCode3
     */
    protected $BookingDataCode3 = null;

    /**
     * @var string $BookingDataValue3
     */
    protected $BookingDataValue3 = null;

    /**
     * @var string $CustomerRateLineDescription
     */
    protected $CustomerRateLineDescription = null;

    /**
     * @var string $PromotionCode
     */
    protected $PromotionCode = null;

    /**
     * @var int $Stay
     */
    protected $Stay = null;

    /**
     * @var boolean $IsBase
     */
    protected $IsBase = null;

    /**
     * @var int $MinStay
     */
    protected $MinStay = null;

    /**
     * @var int $MaxStay
     */
    protected $MaxStay = null;

    /**
     * @var boolean $NoSendOffers
     */
    protected $NoSendOffers = null;

    /**
     * @var boolean $SendCheapestPrice
     */
    protected $SendCheapestPrice = null;

    /**
     * @var string $BookingAllotmentGroupCode
     */
    protected $BookingAllotmentGroupCode = null;

    /**
     * @var int $Release
     */
    protected $Release = null;

    /**
     * @var boolean $ReleaseIndicated
     */
    protected $ReleaseIndicated = null;

    /**
     * @var string $AllotmentGroupStay
     */
    protected $AllotmentGroupStay = null;

    /**
     * @var string $MinAllotmentGroupStay
     */
    protected $MinAllotmentGroupStay = null;

    /**
     * @param Action $Action
     * @param int $IdCustomerRateLine
     * @param int $Adults
     * @param int $Children
     * @param int $ChildrenAge
     * @param int $ChildrenAge2
     * @param int $ChildrenAge3
     * @param int $ChildrenAge4
     * @param int $Babies
     * @param boolean $PendingChanges
     * @param boolean $CalcCustomerRate
     * @param boolean $PricexRoom
     * @param boolean $NotAllotment
     * @param int $MinAllotment
     * @param boolean $NotAcceptStopSales
     * @param \DateTime $DateFrom
     * @param \DateTime $DateTo
     * @param int $Stay
     * @param boolean $IsBase
     * @param int $MinStay
     * @param int $MaxStay
     * @param boolean $NoSendOffers
     * @param boolean $SendCheapestPrice
     * @param int $Release
     * @param boolean $ReleaseIndicated
     */
    public function __construct($Action, $IdCustomerRateLine, $Adults, $Children, $ChildrenAge, $ChildrenAge2, $ChildrenAge3, $ChildrenAge4, $Babies, $PendingChanges, $CalcCustomerRate, $PricexRoom, $NotAllotment, $MinAllotment, $NotAcceptStopSales, \DateTime $DateFrom, \DateTime $DateTo, $Stay, $IsBase, $MinStay, $MaxStay, $NoSendOffers, $SendCheapestPrice, $Release, $ReleaseIndicated)
    {
      $this->Action = $Action;
      $this->IdCustomerRateLine = $IdCustomerRateLine;
      $this->Adults = $Adults;
      $this->Children = $Children;
      $this->ChildrenAge = $ChildrenAge;
      $this->ChildrenAge2 = $ChildrenAge2;
      $this->ChildrenAge3 = $ChildrenAge3;
      $this->ChildrenAge4 = $ChildrenAge4;
      $this->Babies = $Babies;
      $this->PendingChanges = $PendingChanges;
      $this->CalcCustomerRate = $CalcCustomerRate;
      $this->PricexRoom = $PricexRoom;
      $this->NotAllotment = $NotAllotment;
      $this->MinAllotment = $MinAllotment;
      $this->NotAcceptStopSales = $NotAcceptStopSales;
      $this->DateFrom = $DateFrom->format(\DateTime::ATOM);
      $this->DateTo = $DateTo->format(\DateTime::ATOM);
      $this->Stay = $Stay;
      $this->IsBase = $IsBase;
      $this->MinStay = $MinStay;
      $this->MaxStay = $MaxStay;
      $this->NoSendOffers = $NoSendOffers;
      $this->SendCheapestPrice = $SendCheapestPrice;
      $this->Release = $Release;
      $this->ReleaseIndicated = $ReleaseIndicated;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'Action' => $this->getAction(),
        'IdCustomerRateLine' => $this->getIdCustomerRateLine(),
        'RoomCode' => $this->getRoomCode(),
        'BoardCode' => $this->getBoardCode(),
        'AllotmentGroupCode' => $this->getAllotmentGroupCode(),
        'RateGroupCode' => $this->getRateGroupCode(),
        'ReglaNetoCode' => $this->getReglaNetoCode(),
        'Adults' => $this->getAdults(),
        'Children' => $this->getChildren(),
        'ChildrenAge' => $this->getChildrenAge(),
        'ChildrenAge2' => $this->getChildrenAge2(),
        'ChildrenAge3' => $this->getChildrenAge3(),
        'ChildrenAge4' => $this->getChildrenAge4(),
        'Babies' => $this->getBabies(),
        'RateCodeCustomerRate' => $this->getRateCodeCustomerRate(),
        'RoomCodeCustomerRate' => $this->getRoomCodeCustomerRate(),
        'BoardCodeCustomerRate' => $this->getBoardCodeCustomerRate(),
        'PendingChanges' => $this->getPendingChanges(),
        'CalcCustomerRate' => $this->getCalcCustomerRate(),
        'PricexRoom' => $this->getPricexRoom(),
        'NotAllotment' => $this->getNotAllotment(),
        'MinAllotment' => $this->getMinAllotment(),
        'NotAcceptStopSales' => $this->getNotAcceptStopSales(),
        'DateFrom' => $this->getDateFrom(),
        'DateTo' => $this->getDateTo(),
        'ExtraCode1' => $this->getExtraCode1(),
        'ExtraCode2' => $this->getExtraCode2(),
        'ExtraCode3' => $this->getExtraCode3(),
        'BookingDataCode1' => $this->getBookingDataCode1(),
        'BookingDataValue1' => $this->getBookingDataValue1(),
        'BookingDataCode2' => $this->getBookingDataCode2(),
        'BookingDataValue2' => $this->getBookingDataValue2(),
        'BookingDataCode3' => $this->getBookingDataCode3(),
        'BookingDataValue3' => $this->getBookingDataValue3(),
        'CustomerRateLineDescription' => $this->getCustomerRateLineDescription(),
        'PromotionCode' => $this->getPromotionCode(),
        'Stay' => $this->getStay(),
        'IsBase' => $this->getIsBase(),
        'MinStay' => $this->getMinStay(),
        'MaxStay' => $this->getMaxStay(),
        'NoSendOffers' => $this->getNoSendOffers(),
        'SendCheapestPrice' => $this->getSendCheapestPrice(),
        'BookingAllotmentGroupCode' => $this->getBookingAllotmentGroupCode(),
        'Release' => $this->getRelease(),
        'ReleaseIndicated' => $this->getReleaseIndicated(),
        'AllotmentGroupStay' => $this->getAllotmentGroupStay(),
        'MinAllotmentGroupStay' => $this->getMinAllotmentGroupStay(),
      );
    }

    /**
     * @return Action
     */
    public function getAction()
    {
      return $this->Action;
    }

    /**
     * @param Action $Action
     * @return \Dingus\SyncroService\CustomerRateLine
     */
    public function setAction($Action)
    {
      $this->Action = $Action;
      return $this;
    }

    /**
     * @return int
     */
    public function getIdCustomerRateLine()
    {
      return $this->IdCustomerRateLine;
    }

    /**
     * @param int $IdCustomerRateLine
     * @return \Dingus\SyncroService\CustomerRateLine
     */
    public function setIdCustomerRateLine($IdCustomerRateLine)
    {
      $this->IdCustomerRateLine = $IdCustomerRateLine;
      return $this;
    }

    /**
     * @return string
     */
    public function getRoomCode()
    {
      return $this->RoomCode;
    }

    /**
     * @param string $RoomCode
     * @return \Dingus\SyncroService\CustomerRateLine
     */
    public function setRoomCode($RoomCode)
    {
      $this->RoomCode = $RoomCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getBoardCode()
    {
      return $this->BoardCode;
    }

    /**
     * @param string $BoardCode
     * @return \Dingus\SyncroService\CustomerRateLine
     */
    public function setBoardCode($BoardCode)
    {
      $this->BoardCode = $BoardCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getAllotmentGroupCode()
    {
      return $this->AllotmentGroupCode;
    }

    /**
     * @param string $AllotmentGroupCode
     * @return \Dingus\SyncroService\CustomerRateLine
     */
    public function setAllotmentGroupCode($AllotmentGroupCode)
    {
      $this->AllotmentGroupCode = $AllotmentGroupCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getRateGroupCode()
    {
      return $this->RateGroupCode;
    }

    /**
     * @param string $RateGroupCode
     * @return \Dingus\SyncroService\CustomerRateLine
     */
    public function setRateGroupCode($RateGroupCode)
    {
      $this->RateGroupCode = $RateGroupCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getReglaNetoCode()
    {
      return $this->ReglaNetoCode;
    }

    /**
     * @param string $ReglaNetoCode
     * @return \Dingus\SyncroService\CustomerRateLine
     */
    public function setReglaNetoCode($ReglaNetoCode)
    {
      $this->ReglaNetoCode = $ReglaNetoCode;
      return $this;
    }

    /**
     * @return int
     */
    public function getAdults()
    {
      return $this->Adults;
    }

    /**
     * @param int $Adults
     * @return \Dingus\SyncroService\CustomerRateLine
     */
    public function setAdults($Adults)
    {
      $this->Adults = $Adults;
      return $this;
    }

    /**
     * @return int
     */
    public function getChildren()
    {
      return $this->Children;
    }

    /**
     * @param int $Children
     * @return \Dingus\SyncroService\CustomerRateLine
     */
    public function setChildren($Children)
    {
      $this->Children = $Children;
      return $this;
    }

    /**
     * @return int
     */
    public function getChildrenAge()
    {
      return $this->ChildrenAge;
    }

    /**
     * @param int $ChildrenAge
     * @return \Dingus\SyncroService\CustomerRateLine
     */
    public function setChildrenAge($ChildrenAge)
    {
      $this->ChildrenAge = $ChildrenAge;
      return $this;
    }

    /**
     * @return int
     */
    public function getChildrenAge2()
    {
      return $this->ChildrenAge2;
    }

    /**
     * @param int $ChildrenAge2
     * @return \Dingus\SyncroService\CustomerRateLine
     */
    public function setChildrenAge2($ChildrenAge2)
    {
      $this->ChildrenAge2 = $ChildrenAge2;
      return $this;
    }

    /**
     * @return int
     */
    public function getChildrenAge3()
    {
      return $this->ChildrenAge3;
    }

    /**
     * @param int $ChildrenAge3
     * @return \Dingus\SyncroService\CustomerRateLine
     */
    public function setChildrenAge3($ChildrenAge3)
    {
      $this->ChildrenAge3 = $ChildrenAge3;
      return $this;
    }

    /**
     * @return int
     */
    public function getChildrenAge4()
    {
      return $this->ChildrenAge4;
    }

    /**
     * @param int $ChildrenAge4
     * @return \Dingus\SyncroService\CustomerRateLine
     */
    public function setChildrenAge4($ChildrenAge4)
    {
      $this->ChildrenAge4 = $ChildrenAge4;
      return $this;
    }

    /**
     * @return int
     */
    public function getBabies()
    {
      return $this->Babies;
    }

    /**
     * @param int $Babies
     * @return \Dingus\SyncroService\CustomerRateLine
     */
    public function setBabies($Babies)
    {
      $this->Babies = $Babies;
      return $this;
    }

    /**
     * @return string
     */
    public function getRateCodeCustomerRate()
    {
      return $this->RateCodeCustomerRate;
    }

    /**
     * @param string $RateCodeCustomerRate
     * @return \Dingus\SyncroService\CustomerRateLine
     */
    public function setRateCodeCustomerRate($RateCodeCustomerRate)
    {
      $this->RateCodeCustomerRate = $RateCodeCustomerRate;
      return $this;
    }

    /**
     * @return string
     */
    public function getRoomCodeCustomerRate()
    {
      return $this->RoomCodeCustomerRate;
    }

    /**
     * @param string $RoomCodeCustomerRate
     * @return \Dingus\SyncroService\CustomerRateLine
     */
    public function setRoomCodeCustomerRate($RoomCodeCustomerRate)
    {
      $this->RoomCodeCustomerRate = $RoomCodeCustomerRate;
      return $this;
    }

    /**
     * @return string
     */
    public function getBoardCodeCustomerRate()
    {
      return $this->BoardCodeCustomerRate;
    }

    /**
     * @param string $BoardCodeCustomerRate
     * @return \Dingus\SyncroService\CustomerRateLine
     */
    public function setBoardCodeCustomerRate($BoardCodeCustomerRate)
    {
      $this->BoardCodeCustomerRate = $BoardCodeCustomerRate;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getPendingChanges()
    {
      return $this->PendingChanges;
    }

    /**
     * @param boolean $PendingChanges
     * @return \Dingus\SyncroService\CustomerRateLine
     */
    public function setPendingChanges($PendingChanges)
    {
      $this->PendingChanges = $PendingChanges;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getCalcCustomerRate()
    {
      return $this->CalcCustomerRate;
    }

    /**
     * @param boolean $CalcCustomerRate
     * @return \Dingus\SyncroService\CustomerRateLine
     */
    public function setCalcCustomerRate($CalcCustomerRate)
    {
      $this->CalcCustomerRate = $CalcCustomerRate;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getPricexRoom()
    {
      return $this->PricexRoom;
    }

    /**
     * @param boolean $PricexRoom
     * @return \Dingus\SyncroService\CustomerRateLine
     */
    public function setPricexRoom($PricexRoom)
    {
      $this->PricexRoom = $PricexRoom;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getNotAllotment()
    {
      return $this->NotAllotment;
    }

    /**
     * @param boolean $NotAllotment
     * @return \Dingus\SyncroService\CustomerRateLine
     */
    public function setNotAllotment($NotAllotment)
    {
      $this->NotAllotment = $NotAllotment;
      return $this;
    }

    /**
     * @return int
     */
    public function getMinAllotment()
    {
      return $this->MinAllotment;
    }

    /**
     * @param int $MinAllotment
     * @return \Dingus\SyncroService\CustomerRateLine
     */
    public function setMinAllotment($MinAllotment)
    {
      $this->MinAllotment = $MinAllotment;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getNotAcceptStopSales()
    {
      return $this->NotAcceptStopSales;
    }

    /**
     * @param boolean $NotAcceptStopSales
     * @return \Dingus\SyncroService\CustomerRateLine
     */
    public function setNotAcceptStopSales($NotAcceptStopSales)
    {
      $this->NotAcceptStopSales = $NotAcceptStopSales;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateFrom()
    {
      if ($this->DateFrom == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->DateFrom);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $DateFrom
     * @return \Dingus\SyncroService\CustomerRateLine
     */
    public function setDateFrom(\DateTime $DateFrom)
    {
      $this->DateFrom = $DateFrom->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateTo()
    {
      if ($this->DateTo == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->DateTo);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $DateTo
     * @return \Dingus\SyncroService\CustomerRateLine
     */
    public function setDateTo(\DateTime $DateTo)
    {
      $this->DateTo = $DateTo->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return string
     */
    public function getExtraCode1()
    {
      return $this->ExtraCode1;
    }

    /**
     * @param string $ExtraCode1
     * @return \Dingus\SyncroService\CustomerRateLine
     */
    public function setExtraCode1($ExtraCode1)
    {
      $this->ExtraCode1 = $ExtraCode1;
      return $this;
    }

    /**
     * @return string
     */
    public function getExtraCode2()
    {
      return $this->ExtraCode2;
    }

    /**
     * @param string $ExtraCode2
     * @return \Dingus\SyncroService\CustomerRateLine
     */
    public function setExtraCode2($ExtraCode2)
    {
      $this->ExtraCode2 = $ExtraCode2;
      return $this;
    }

    /**
     * @return string
     */
    public function getExtraCode3()
    {
      return $this->ExtraCode3;
    }

    /**
     * @param string $ExtraCode3
     * @return \Dingus\SyncroService\CustomerRateLine
     */
    public function setExtraCode3($ExtraCode3)
    {
      $this->ExtraCode3 = $ExtraCode3;
      return $this;
    }

    /**
     * @return string
     */
    public function getBookingDataCode1()
    {
      return $this->BookingDataCode1;
    }

    /**
     * @param string $BookingDataCode1
     * @return \Dingus\SyncroService\CustomerRateLine
     */
    public function setBookingDataCode1($BookingDataCode1)
    {
      $this->BookingDataCode1 = $BookingDataCode1;
      return $this;
    }

    /**
     * @return string
     */
    public function getBookingDataValue1()
    {
      return $this->BookingDataValue1;
    }

    /**
     * @param string $BookingDataValue1
     * @return \Dingus\SyncroService\CustomerRateLine
     */
    public function setBookingDataValue1($BookingDataValue1)
    {
      $this->BookingDataValue1 = $BookingDataValue1;
      return $this;
    }

    /**
     * @return string
     */
    public function getBookingDataCode2()
    {
      return $this->BookingDataCode2;
    }

    /**
     * @param string $BookingDataCode2
     * @return \Dingus\SyncroService\CustomerRateLine
     */
    public function setBookingDataCode2($BookingDataCode2)
    {
      $this->BookingDataCode2 = $BookingDataCode2;
      return $this;
    }

    /**
     * @return string
     */
    public function getBookingDataValue2()
    {
      return $this->BookingDataValue2;
    }

    /**
     * @param string $BookingDataValue2
     * @return \Dingus\SyncroService\CustomerRateLine
     */
    public function setBookingDataValue2($BookingDataValue2)
    {
      $this->BookingDataValue2 = $BookingDataValue2;
      return $this;
    }

    /**
     * @return string
     */
    public function getBookingDataCode3()
    {
      return $this->BookingDataCode3;
    }

    /**
     * @param string $BookingDataCode3
     * @return \Dingus\SyncroService\CustomerRateLine
     */
    public function setBookingDataCode3($BookingDataCode3)
    {
      $this->BookingDataCode3 = $BookingDataCode3;
      return $this;
    }

    /**
     * @return string
     */
    public function getBookingDataValue3()
    {
      return $this->BookingDataValue3;
    }

    /**
     * @param string $BookingDataValue3
     * @return \Dingus\SyncroService\CustomerRateLine
     */
    public function setBookingDataValue3($BookingDataValue3)
    {
      $this->BookingDataValue3 = $BookingDataValue3;
      return $this;
    }

    /**
     * @return string
     */
    public function getCustomerRateLineDescription()
    {
      return $this->CustomerRateLineDescription;
    }

    /**
     * @param string $CustomerRateLineDescription
     * @return \Dingus\SyncroService\CustomerRateLine
     */
    public function setCustomerRateLineDescription($CustomerRateLineDescription)
    {
      $this->CustomerRateLineDescription = $CustomerRateLineDescription;
      return $this;
    }

    /**
     * @return string
     */
    public function getPromotionCode()
    {
      return $this->PromotionCode;
    }

    /**
     * @param string $PromotionCode
     * @return \Dingus\SyncroService\CustomerRateLine
     */
    public function setPromotionCode($PromotionCode)
    {
      $this->PromotionCode = $PromotionCode;
      return $this;
    }

    /**
     * @return int
     */
    public function getStay()
    {
      return $this->Stay;
    }

    /**
     * @param int $Stay
     * @return \Dingus\SyncroService\CustomerRateLine
     */
    public function setStay($Stay)
    {
      $this->Stay = $Stay;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getIsBase()
    {
      return $this->IsBase;
    }

    /**
     * @param boolean $IsBase
     * @return \Dingus\SyncroService\CustomerRateLine
     */
    public function setIsBase($IsBase)
    {
      $this->IsBase = $IsBase;
      return $this;
    }

    /**
     * @return int
     */
    public function getMinStay()
    {
      return $this->MinStay;
    }

    /**
     * @param int $MinStay
     * @return \Dingus\SyncroService\CustomerRateLine
     */
    public function setMinStay($MinStay)
    {
      $this->MinStay = $MinStay;
      return $this;
    }

    /**
     * @return int
     */
    public function getMaxStay()
    {
      return $this->MaxStay;
    }

    /**
     * @param int $MaxStay
     * @return \Dingus\SyncroService\CustomerRateLine
     */
    public function setMaxStay($MaxStay)
    {
      $this->MaxStay = $MaxStay;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getNoSendOffers()
    {
      return $this->NoSendOffers;
    }

    /**
     * @param boolean $NoSendOffers
     * @return \Dingus\SyncroService\CustomerRateLine
     */
    public function setNoSendOffers($NoSendOffers)
    {
      $this->NoSendOffers = $NoSendOffers;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getSendCheapestPrice()
    {
      return $this->SendCheapestPrice;
    }

    /**
     * @param boolean $SendCheapestPrice
     * @return \Dingus\SyncroService\CustomerRateLine
     */
    public function setSendCheapestPrice($SendCheapestPrice)
    {
      $this->SendCheapestPrice = $SendCheapestPrice;
      return $this;
    }

    /**
     * @return string
     */
    public function getBookingAllotmentGroupCode()
    {
      return $this->BookingAllotmentGroupCode;
    }

    /**
     * @param string $BookingAllotmentGroupCode
     * @return \Dingus\SyncroService\CustomerRateLine
     */
    public function setBookingAllotmentGroupCode($BookingAllotmentGroupCode)
    {
      $this->BookingAllotmentGroupCode = $BookingAllotmentGroupCode;
      return $this;
    }

    /**
     * @return int
     */
    public function getRelease()
    {
      return $this->Release;
    }

    /**
     * @param int $Release
     * @return \Dingus\SyncroService\CustomerRateLine
     */
    public function setRelease($Release)
    {
      $this->Release = $Release;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getReleaseIndicated()
    {
      return $this->ReleaseIndicated;
    }

    /**
     * @param boolean $ReleaseIndicated
     * @return \Dingus\SyncroService\CustomerRateLine
     */
    public function setReleaseIndicated($ReleaseIndicated)
    {
      $this->ReleaseIndicated = $ReleaseIndicated;
      return $this;
    }

    /**
     * @return string
     */
    public function getAllotmentGroupStay()
    {
      return $this->AllotmentGroupStay;
    }

    /**
     * @param string $AllotmentGroupStay
     * @return \Dingus\SyncroService\CustomerRateLine
     */
    public function setAllotmentGroupStay($AllotmentGroupStay)
    {
      $this->AllotmentGroupStay = $AllotmentGroupStay;
      return $this;
    }

    /**
     * @return string
     */
    public function getMinAllotmentGroupStay()
    {
      return $this->MinAllotmentGroupStay;
    }

    /**
     * @param string $MinAllotmentGroupStay
     * @return \Dingus\SyncroService\CustomerRateLine
     */
    public function setMinAllotmentGroupStay($MinAllotmentGroupStay)
    {
      $this->MinAllotmentGroupStay = $MinAllotmentGroupStay;
      return $this;
    }

}
