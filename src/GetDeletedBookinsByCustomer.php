<?php

namespace Dingus\SyncroService;

class GetDeletedBookinsByCustomer implements \JsonSerializable
{

    /**
     * @var Credentials $Credentials
     */
    protected $Credentials = null;

    /**
     * @var string $CustomerCode
     */
    protected $CustomerCode = null;

    /**
     * @param Credentials $Credentials
     * @param string $CustomerCode
     */
    public function __construct($Credentials, $CustomerCode)
    {
      $this->Credentials = $Credentials;
      $this->CustomerCode = $CustomerCode;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'Credentials' => $this->getCredentials(),
        'CustomerCode' => $this->getCustomerCode(),
      );
    }

    /**
     * @return Credentials
     */
    public function getCredentials()
    {
      return $this->Credentials;
    }

    /**
     * @param Credentials $Credentials
     * @return \Dingus\SyncroService\GetDeletedBookinsByCustomer
     */
    public function setCredentials($Credentials)
    {
      $this->Credentials = $Credentials;
      return $this;
    }

    /**
     * @return string
     */
    public function getCustomerCode()
    {
      return $this->CustomerCode;
    }

    /**
     * @param string $CustomerCode
     * @return \Dingus\SyncroService\GetDeletedBookinsByCustomer
     */
    public function setCustomerCode($CustomerCode)
    {
      $this->CustomerCode = $CustomerCode;
      return $this;
    }

}
