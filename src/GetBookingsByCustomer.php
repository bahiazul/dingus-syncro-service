<?php

namespace Dingus\SyncroService;

class GetBookingsByCustomer implements \JsonSerializable
{

    /**
     * @var Credentials $Credentials
     */
    protected $Credentials = null;

    /**
     * @var string $CustomerCode
     */
    protected $CustomerCode = null;

    /**
     * @var \DateTime $DateFrom
     */
    protected $DateFrom = null;

    /**
     * @var \DateTime $DateTo
     */
    protected $DateTo = null;

    /**
     * @var boolean $NotFullBooking
     */
    protected $NotFullBooking = null;

    /**
     * @param Credentials $Credentials
     * @param string $CustomerCode
     * @param \DateTime $DateFrom
     * @param \DateTime $DateTo
     * @param boolean $NotFullBooking
     */
    public function __construct($Credentials, $CustomerCode, \DateTime $DateFrom, \DateTime $DateTo, $NotFullBooking)
    {
      $this->Credentials = $Credentials;
      $this->CustomerCode = $CustomerCode;
      $this->DateFrom = $DateFrom->format(\DateTime::ATOM);
      $this->DateTo = $DateTo->format(\DateTime::ATOM);
      $this->NotFullBooking = $NotFullBooking;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'Credentials' => $this->getCredentials(),
        'CustomerCode' => $this->getCustomerCode(),
        'DateFrom' => $this->getDateFrom(),
        'DateTo' => $this->getDateTo(),
        'NotFullBooking' => $this->getNotFullBooking(),
      );
    }

    /**
     * @return Credentials
     */
    public function getCredentials()
    {
      return $this->Credentials;
    }

    /**
     * @param Credentials $Credentials
     * @return \Dingus\SyncroService\GetBookingsByCustomer
     */
    public function setCredentials($Credentials)
    {
      $this->Credentials = $Credentials;
      return $this;
    }

    /**
     * @return string
     */
    public function getCustomerCode()
    {
      return $this->CustomerCode;
    }

    /**
     * @param string $CustomerCode
     * @return \Dingus\SyncroService\GetBookingsByCustomer
     */
    public function setCustomerCode($CustomerCode)
    {
      $this->CustomerCode = $CustomerCode;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateFrom()
    {
      if ($this->DateFrom == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->DateFrom);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $DateFrom
     * @return \Dingus\SyncroService\GetBookingsByCustomer
     */
    public function setDateFrom(\DateTime $DateFrom)
    {
      $this->DateFrom = $DateFrom->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateTo()
    {
      if ($this->DateTo == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->DateTo);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $DateTo
     * @return \Dingus\SyncroService\GetBookingsByCustomer
     */
    public function setDateTo(\DateTime $DateTo)
    {
      $this->DateTo = $DateTo->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return boolean
     */
    public function getNotFullBooking()
    {
      return $this->NotFullBooking;
    }

    /**
     * @param boolean $NotFullBooking
     * @return \Dingus\SyncroService\GetBookingsByCustomer
     */
    public function setNotFullBooking($NotFullBooking)
    {
      $this->NotFullBooking = $NotFullBooking;
      return $this;
    }

}
