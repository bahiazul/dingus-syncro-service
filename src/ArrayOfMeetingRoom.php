<?php

namespace Dingus\SyncroService;

class ArrayOfMeetingRoom implements \JsonSerializable, \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var MeetingRoom[] $MeetingRoom
     */
    protected $MeetingRoom = null;

    
    public function __construct()
    {
    
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'MeetingRoom' => $this->getMeetingRoom(),
      );
    }

    /**
     * @return MeetingRoom[]
     */
    public function getMeetingRoom()
    {
      return $this->MeetingRoom;
    }

    /**
     * @param MeetingRoom[] $MeetingRoom
     * @return \Dingus\SyncroService\ArrayOfMeetingRoom
     */
    public function setMeetingRoom(array $MeetingRoom = null)
    {
      $this->MeetingRoom = $MeetingRoom;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->MeetingRoom[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return MeetingRoom
     */
    public function offsetGet($offset)
    {
      return $this->MeetingRoom[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param MeetingRoom $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      $this->MeetingRoom[$offset] = $value;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->MeetingRoom[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return MeetingRoom Return the current element
     */
    public function current()
    {
      return current($this->MeetingRoom);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->MeetingRoom);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->MeetingRoom);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->MeetingRoom);
    }

    /**
     * Countable implementation
     *
     * @return MeetingRoom Return count of elements
     */
    public function count()
    {
      return count($this->MeetingRoom);
    }

}
