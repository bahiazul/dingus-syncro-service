<?php

namespace Dingus\SyncroService;

class ArrayOfCriterionRec implements \JsonSerializable, \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var CriterionRec[] $CriterionRec
     */
    protected $CriterionRec = null;

    
    public function __construct()
    {
    
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'CriterionRec' => $this->getCriterionRec(),
      );
    }

    /**
     * @return CriterionRec[]
     */
    public function getCriterionRec()
    {
      return $this->CriterionRec;
    }

    /**
     * @param CriterionRec[] $CriterionRec
     * @return \Dingus\SyncroService\ArrayOfCriterionRec
     */
    public function setCriterionRec(array $CriterionRec = null)
    {
      $this->CriterionRec = $CriterionRec;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->CriterionRec[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return CriterionRec
     */
    public function offsetGet($offset)
    {
      return $this->CriterionRec[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param CriterionRec $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      $this->CriterionRec[$offset] = $value;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->CriterionRec[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return CriterionRec Return the current element
     */
    public function current()
    {
      return current($this->CriterionRec);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->CriterionRec);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->CriterionRec);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->CriterionRec);
    }

    /**
     * Countable implementation
     *
     * @return CriterionRec Return count of elements
     */
    public function count()
    {
      return count($this->CriterionRec);
    }

}
