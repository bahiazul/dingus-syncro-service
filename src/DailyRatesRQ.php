<?php

namespace Dingus\SyncroService;

class DailyRatesRQ implements \JsonSerializable
{

    /**
     * @var string $RateCode
     */
    protected $RateCode = null;

    /**
     * @var string $RateName
     */
    protected $RateName = null;

    /**
     * @var string $CustomerRatesGroupCode
     */
    protected $CustomerRatesGroupCode = null;

    /**
     * @var string $CustomerRatesGroupName
     */
    protected $CustomerRatesGroupName = null;

    /**
     * @var string $HotelCode
     */
    protected $HotelCode = null;

    /**
     * @var RateType $RateType
     */
    protected $RateType = null;

    /**
     * @var boolean $Active
     */
    protected $Active = null;

    /**
     * @var boolean $ByDefault
     */
    protected $ByDefault = null;

    /**
     * @var string $CurrencyCode
     */
    protected $CurrencyCode = null;

    /**
     * @var string $CurrencyName
     */
    protected $CurrencyName = null;

    /**
     * @var ArrayOfDailyPriceRec $DailyPrices
     */
    protected $DailyPrices = null;

    /**
     * @var ArrayOfDailyPriceOcupationRec $Ocupations
     */
    protected $Ocupations = null;

    /**
     * @var ArrayOfDailyPriceRangeRec $Ranges
     */
    protected $Ranges = null;

    /**
     * @var RateParentInfo $rateParentInfo
     */
    protected $rateParentInfo = null;

    /**
     * @param RateType $RateType
     * @param boolean $Active
     * @param boolean $ByDefault
     */
    public function __construct($RateType, $Active, $ByDefault)
    {
      $this->RateType = $RateType;
      $this->Active = $Active;
      $this->ByDefault = $ByDefault;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'RateCode' => $this->getRateCode(),
        'RateName' => $this->getRateName(),
        'CustomerRatesGroupCode' => $this->getCustomerRatesGroupCode(),
        'CustomerRatesGroupName' => $this->getCustomerRatesGroupName(),
        'HotelCode' => $this->getHotelCode(),
        'RateType' => $this->getRateType(),
        'Active' => $this->getActive(),
        'ByDefault' => $this->getByDefault(),
        'CurrencyCode' => $this->getCurrencyCode(),
        'CurrencyName' => $this->getCurrencyName(),
        'DailyPrices' => $this->getDailyPrices(),
        'Ocupations' => $this->getOcupations(),
        'Ranges' => $this->getRanges(),
        'rateParentInfo' => $this->getRateParentInfo(),
      );
    }

    /**
     * @return string
     */
    public function getRateCode()
    {
      return $this->RateCode;
    }

    /**
     * @param string $RateCode
     * @return \Dingus\SyncroService\DailyRatesRQ
     */
    public function setRateCode($RateCode)
    {
      $this->RateCode = $RateCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getRateName()
    {
      return $this->RateName;
    }

    /**
     * @param string $RateName
     * @return \Dingus\SyncroService\DailyRatesRQ
     */
    public function setRateName($RateName)
    {
      $this->RateName = $RateName;
      return $this;
    }

    /**
     * @return string
     */
    public function getCustomerRatesGroupCode()
    {
      return $this->CustomerRatesGroupCode;
    }

    /**
     * @param string $CustomerRatesGroupCode
     * @return \Dingus\SyncroService\DailyRatesRQ
     */
    public function setCustomerRatesGroupCode($CustomerRatesGroupCode)
    {
      $this->CustomerRatesGroupCode = $CustomerRatesGroupCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getCustomerRatesGroupName()
    {
      return $this->CustomerRatesGroupName;
    }

    /**
     * @param string $CustomerRatesGroupName
     * @return \Dingus\SyncroService\DailyRatesRQ
     */
    public function setCustomerRatesGroupName($CustomerRatesGroupName)
    {
      $this->CustomerRatesGroupName = $CustomerRatesGroupName;
      return $this;
    }

    /**
     * @return string
     */
    public function getHotelCode()
    {
      return $this->HotelCode;
    }

    /**
     * @param string $HotelCode
     * @return \Dingus\SyncroService\DailyRatesRQ
     */
    public function setHotelCode($HotelCode)
    {
      $this->HotelCode = $HotelCode;
      return $this;
    }

    /**
     * @return RateType
     */
    public function getRateType()
    {
      return $this->RateType;
    }

    /**
     * @param RateType $RateType
     * @return \Dingus\SyncroService\DailyRatesRQ
     */
    public function setRateType($RateType)
    {
      $this->RateType = $RateType;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getActive()
    {
      return $this->Active;
    }

    /**
     * @param boolean $Active
     * @return \Dingus\SyncroService\DailyRatesRQ
     */
    public function setActive($Active)
    {
      $this->Active = $Active;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getByDefault()
    {
      return $this->ByDefault;
    }

    /**
     * @param boolean $ByDefault
     * @return \Dingus\SyncroService\DailyRatesRQ
     */
    public function setByDefault($ByDefault)
    {
      $this->ByDefault = $ByDefault;
      return $this;
    }

    /**
     * @return string
     */
    public function getCurrencyCode()
    {
      return $this->CurrencyCode;
    }

    /**
     * @param string $CurrencyCode
     * @return \Dingus\SyncroService\DailyRatesRQ
     */
    public function setCurrencyCode($CurrencyCode)
    {
      $this->CurrencyCode = $CurrencyCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getCurrencyName()
    {
      return $this->CurrencyName;
    }

    /**
     * @param string $CurrencyName
     * @return \Dingus\SyncroService\DailyRatesRQ
     */
    public function setCurrencyName($CurrencyName)
    {
      $this->CurrencyName = $CurrencyName;
      return $this;
    }

    /**
     * @return ArrayOfDailyPriceRec
     */
    public function getDailyPrices()
    {
      return $this->DailyPrices;
    }

    /**
     * @param ArrayOfDailyPriceRec $DailyPrices
     * @return \Dingus\SyncroService\DailyRatesRQ
     */
    public function setDailyPrices($DailyPrices)
    {
      $this->DailyPrices = $DailyPrices;
      return $this;
    }

    /**
     * @return ArrayOfDailyPriceOcupationRec
     */
    public function getOcupations()
    {
      return $this->Ocupations;
    }

    /**
     * @param ArrayOfDailyPriceOcupationRec $Ocupations
     * @return \Dingus\SyncroService\DailyRatesRQ
     */
    public function setOcupations($Ocupations)
    {
      $this->Ocupations = $Ocupations;
      return $this;
    }

    /**
     * @return ArrayOfDailyPriceRangeRec
     */
    public function getRanges()
    {
      return $this->Ranges;
    }

    /**
     * @param ArrayOfDailyPriceRangeRec $Ranges
     * @return \Dingus\SyncroService\DailyRatesRQ
     */
    public function setRanges($Ranges)
    {
      $this->Ranges = $Ranges;
      return $this;
    }

    /**
     * @return RateParentInfo
     */
    public function getRateParentInfo()
    {
      return $this->rateParentInfo;
    }

    /**
     * @param RateParentInfo $rateParentInfo
     * @return \Dingus\SyncroService\DailyRatesRQ
     */
    public function setRateParentInfo($rateParentInfo)
    {
      $this->rateParentInfo = $rateParentInfo;
      return $this;
    }

}
