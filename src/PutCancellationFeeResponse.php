<?php

namespace Dingus\SyncroService;

class PutCancellationFeeResponse implements \JsonSerializable
{

    /**
     * @var SyncroRS $PutCancellationFeeResult
     */
    protected $PutCancellationFeeResult = null;

    /**
     * @param SyncroRS $PutCancellationFeeResult
     */
    public function __construct($PutCancellationFeeResult)
    {
      $this->PutCancellationFeeResult = $PutCancellationFeeResult;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'PutCancellationFeeResult' => $this->getPutCancellationFeeResult(),
      );
    }

    /**
     * @return SyncroRS
     */
    public function getPutCancellationFeeResult()
    {
      return $this->PutCancellationFeeResult;
    }

    /**
     * @param SyncroRS $PutCancellationFeeResult
     * @return \Dingus\SyncroService\PutCancellationFeeResponse
     */
    public function setPutCancellationFeeResult($PutCancellationFeeResult)
    {
      $this->PutCancellationFeeResult = $PutCancellationFeeResult;
      return $this;
    }

}
