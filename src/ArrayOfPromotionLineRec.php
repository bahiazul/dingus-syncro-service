<?php

namespace Dingus\SyncroService;

class ArrayOfPromotionLineRec implements \JsonSerializable, \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var PromotionLineRec[] $PromotionLineRec
     */
    protected $PromotionLineRec = null;

    
    public function __construct()
    {
    
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'PromotionLineRec' => $this->getPromotionLineRec(),
      );
    }

    /**
     * @return PromotionLineRec[]
     */
    public function getPromotionLineRec()
    {
      return $this->PromotionLineRec;
    }

    /**
     * @param PromotionLineRec[] $PromotionLineRec
     * @return \Dingus\SyncroService\ArrayOfPromotionLineRec
     */
    public function setPromotionLineRec(array $PromotionLineRec = null)
    {
      $this->PromotionLineRec = $PromotionLineRec;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->PromotionLineRec[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return PromotionLineRec
     */
    public function offsetGet($offset)
    {
      return $this->PromotionLineRec[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param PromotionLineRec $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      $this->PromotionLineRec[$offset] = $value;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->PromotionLineRec[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return PromotionLineRec Return the current element
     */
    public function current()
    {
      return current($this->PromotionLineRec);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->PromotionLineRec);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->PromotionLineRec);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->PromotionLineRec);
    }

    /**
     * Countable implementation
     *
     * @return PromotionLineRec Return count of elements
     */
    public function count()
    {
      return count($this->PromotionLineRec);
    }

}
