<?php

namespace Dingus\SyncroService;

class PutDailyRatesResponse implements \JsonSerializable
{

    /**
     * @var SyncroRS $PutDailyRatesResult
     */
    protected $PutDailyRatesResult = null;

    /**
     * @param SyncroRS $PutDailyRatesResult
     */
    public function __construct($PutDailyRatesResult)
    {
      $this->PutDailyRatesResult = $PutDailyRatesResult;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'PutDailyRatesResult' => $this->getPutDailyRatesResult(),
      );
    }

    /**
     * @return SyncroRS
     */
    public function getPutDailyRatesResult()
    {
      return $this->PutDailyRatesResult;
    }

    /**
     * @param SyncroRS $PutDailyRatesResult
     * @return \Dingus\SyncroService\PutDailyRatesResponse
     */
    public function setPutDailyRatesResult($PutDailyRatesResult)
    {
      $this->PutDailyRatesResult = $PutDailyRatesResult;
      return $this;
    }

}
