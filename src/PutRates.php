<?php

namespace Dingus\SyncroService;

class PutRates implements \JsonSerializable
{

    /**
     * @var Credentials $Credentials
     */
    protected $Credentials = null;

    /**
     * @var PutRatesRQ $PutRatesRQ
     */
    protected $PutRatesRQ = null;

    /**
     * @param Credentials $Credentials
     * @param PutRatesRQ $PutRatesRQ
     */
    public function __construct($Credentials, $PutRatesRQ)
    {
      $this->Credentials = $Credentials;
      $this->PutRatesRQ = $PutRatesRQ;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'Credentials' => $this->getCredentials(),
        'PutRatesRQ' => $this->getPutRatesRQ(),
      );
    }

    /**
     * @return Credentials
     */
    public function getCredentials()
    {
      return $this->Credentials;
    }

    /**
     * @param Credentials $Credentials
     * @return \Dingus\SyncroService\PutRates
     */
    public function setCredentials($Credentials)
    {
      $this->Credentials = $Credentials;
      return $this;
    }

    /**
     * @return PutRatesRQ
     */
    public function getPutRatesRQ()
    {
      return $this->PutRatesRQ;
    }

    /**
     * @param PutRatesRQ $PutRatesRQ
     * @return \Dingus\SyncroService\PutRates
     */
    public function setPutRatesRQ($PutRatesRQ)
    {
      $this->PutRatesRQ = $PutRatesRQ;
      return $this;
    }

}
