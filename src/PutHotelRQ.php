<?php

namespace Dingus\SyncroService;

class PutHotelRQ implements \JsonSerializable
{

    /**
     * @var ArrayOfMlText $HotelDescriptionMlList
     */
    protected $HotelDescriptionMlList = null;

    /**
     * @var ArrayOfMlText $ShortDescriptionMlList
     */
    protected $ShortDescriptionMlList = null;

    /**
     * @var ArrayOfMlText $CategoryNameMlList
     */
    protected $CategoryNameMlList = null;

    /**
     * @var ArrayOfMlText $ChainNameMlList
     */
    protected $ChainNameMlList = null;

    /**
     * @var ArrayOfRoom $Rooms
     */
    protected $Rooms = null;

    /**
     * @var ArrayOfBoard $Boards
     */
    protected $Boards = null;

    /**
     * @var ArrayOfExtraHotel $Extras
     */
    protected $Extras = null;

    /**
     * @var ArrayOfClose $Closes
     */
    protected $Closes = null;

    /**
     * @var ArrayOfInstalaciones $Instalaciones
     */
    protected $Instalaciones = null;

    /**
     * @var ArrayOfFacilities $Facilities
     */
    protected $Facilities = null;

    /**
     * @var ArrayOfContacts $Contacts
     */
    protected $Contacts = null;

    /**
     * @var ArrayOfDistance $Distances
     */
    protected $Distances = null;

    /**
     * @var ArrayOfMeetingRoom $MeetingRoom
     */
    protected $MeetingRoom = null;

    /**
     * @var ArrayOfRestaurant $Restaurants
     */
    protected $Restaurants = null;

    /**
     * @var ArrayOfService $Services
     */
    protected $Services = null;

    /**
     * @var ArrayOfControlPrice $ControlPrices
     */
    protected $ControlPrices = null;

    /**
     * @var ArrayOfPicture $Pictures
     */
    protected $Pictures = null;

    /**
     * @var ArrayOfCriterion $Criteria
     */
    protected $Criteria = null;

    /**
     * @var Action $Action
     */
    protected $Action = null;

    /**
     * @var string $HotelCode
     */
    protected $HotelCode = null;

    /**
     * @var string $HotelName
     */
    protected $HotelName = null;

    /**
     * @var string $HotelDescription
     */
    protected $HotelDescription = null;

    /**
     * @var string $ShortDescription
     */
    protected $ShortDescription = null;

    /**
     * @var string $CategoryCode
     */
    protected $CategoryCode = null;

    /**
     * @var string $CategoryName
     */
    protected $CategoryName = null;

    /**
     * @var string $ChainCode
     */
    protected $ChainCode = null;

    /**
     * @var string $ChainName
     */
    protected $ChainName = null;

    /**
     * @var string $VendorCode
     */
    protected $VendorCode = null;

    /**
     * @var string $VendorName
     */
    protected $VendorName = null;

    /**
     * @var float $Latitude
     */
    protected $Latitude = null;

    /**
     * @var float $Longitude
     */
    protected $Longitude = null;

    /**
     * @var string $Address
     */
    protected $Address = null;

    /**
     * @var string $ZIPCode
     */
    protected $ZIPCode = null;

    /**
     * @var string $Web
     */
    protected $Web = null;

    /**
     * @var string $Telephone
     */
    protected $Telephone = null;

    /**
     * @var string $Fax
     */
    protected $Fax = null;

    /**
     * @var \DateTime $BuildDate
     */
    protected $BuildDate = null;

    /**
     * @var \DateTime $RenewDate
     */
    protected $RenewDate = null;

    /**
     * @var \DateTime $UpdateRegisters
     */
    protected $UpdateRegisters = null;

    /**
     * @var float $MinRoomPrice
     */
    protected $MinRoomPrice = null;

    /**
     * @var int $NoOfRooms
     */
    protected $NoOfRooms = null;

    /**
     * @var int $MinChildAge
     */
    protected $MinChildAge = null;

    /**
     * @var int $MaxChildAge
     */
    protected $MaxChildAge = null;

    /**
     * @var int $NoOfBuildings
     */
    protected $NoOfBuildings = null;

    /**
     * @var int $NoOfFloors
     */
    protected $NoOfFloors = null;

    /**
     * @var int $NoOfElevators
     */
    protected $NoOfElevators = null;

    /**
     * @var boolean $NoShowNotPaxInGetAvailAndRates
     */
    protected $NoShowNotPaxInGetAvailAndRates = null;

    /**
     * @var boolean $DailyRate
     */
    protected $DailyRate = null;

    /**
     * @var boolean $OrderCalcSuplByPriority
     */
    protected $OrderCalcSuplByPriority = null;

    /**
     * @var boolean $NoConvertPaxInOccupation
     */
    protected $NoConvertPaxInOccupation = null;

    /**
     * @var boolean $StayDetermineMinStay
     */
    protected $StayDetermineMinStay = null;

    /**
     * @var boolean $NetPriceRuleInHotelGroups
     */
    protected $NetPriceRuleInHotelGroups = null;

    /**
     * @var Comission $Comission
     */
    protected $Comission = null;

    /**
     * @param Action $Action
     * @param float $Latitude
     * @param float $Longitude
     * @param \DateTime $BuildDate
     * @param \DateTime $RenewDate
     * @param \DateTime $UpdateRegisters
     * @param float $MinRoomPrice
     * @param int $NoOfRooms
     * @param int $MinChildAge
     * @param int $MaxChildAge
     * @param int $NoOfBuildings
     * @param int $NoOfFloors
     * @param int $NoOfElevators
     * @param boolean $NoShowNotPaxInGetAvailAndRates
     * @param boolean $DailyRate
     * @param boolean $OrderCalcSuplByPriority
     * @param boolean $NoConvertPaxInOccupation
     * @param boolean $StayDetermineMinStay
     * @param boolean $NetPriceRuleInHotelGroups
     */
    public function __construct($Action, $Latitude, $Longitude, \DateTime $BuildDate, \DateTime $RenewDate, \DateTime $UpdateRegisters, $MinRoomPrice, $NoOfRooms, $MinChildAge, $MaxChildAge, $NoOfBuildings, $NoOfFloors, $NoOfElevators, $NoShowNotPaxInGetAvailAndRates, $DailyRate, $OrderCalcSuplByPriority, $NoConvertPaxInOccupation, $StayDetermineMinStay, $NetPriceRuleInHotelGroups)
    {
      $this->Action = $Action;
      $this->Latitude = $Latitude;
      $this->Longitude = $Longitude;
      $this->BuildDate = $BuildDate->format(\DateTime::ATOM);
      $this->RenewDate = $RenewDate->format(\DateTime::ATOM);
      $this->UpdateRegisters = $UpdateRegisters->format(\DateTime::ATOM);
      $this->MinRoomPrice = $MinRoomPrice;
      $this->NoOfRooms = $NoOfRooms;
      $this->MinChildAge = $MinChildAge;
      $this->MaxChildAge = $MaxChildAge;
      $this->NoOfBuildings = $NoOfBuildings;
      $this->NoOfFloors = $NoOfFloors;
      $this->NoOfElevators = $NoOfElevators;
      $this->NoShowNotPaxInGetAvailAndRates = $NoShowNotPaxInGetAvailAndRates;
      $this->DailyRate = $DailyRate;
      $this->OrderCalcSuplByPriority = $OrderCalcSuplByPriority;
      $this->NoConvertPaxInOccupation = $NoConvertPaxInOccupation;
      $this->StayDetermineMinStay = $StayDetermineMinStay;
      $this->NetPriceRuleInHotelGroups = $NetPriceRuleInHotelGroups;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'HotelDescriptionMlList' => $this->getHotelDescriptionMlList(),
        'ShortDescriptionMlList' => $this->getShortDescriptionMlList(),
        'CategoryNameMlList' => $this->getCategoryNameMlList(),
        'ChainNameMlList' => $this->getChainNameMlList(),
        'Rooms' => $this->getRooms(),
        'Boards' => $this->getBoards(),
        'Extras' => $this->getExtras(),
        'Closes' => $this->getCloses(),
        'Instalaciones' => $this->getInstalaciones(),
        'Facilities' => $this->getFacilities(),
        'Contacts' => $this->getContacts(),
        'Distances' => $this->getDistances(),
        'MeetingRoom' => $this->getMeetingRoom(),
        'Restaurants' => $this->getRestaurants(),
        'Services' => $this->getServices(),
        'ControlPrices' => $this->getControlPrices(),
        'Pictures' => $this->getPictures(),
        'Criteria' => $this->getCriteria(),
        'Action' => $this->getAction(),
        'HotelCode' => $this->getHotelCode(),
        'HotelName' => $this->getHotelName(),
        'HotelDescription' => $this->getHotelDescription(),
        'ShortDescription' => $this->getShortDescription(),
        'CategoryCode' => $this->getCategoryCode(),
        'CategoryName' => $this->getCategoryName(),
        'ChainCode' => $this->getChainCode(),
        'ChainName' => $this->getChainName(),
        'VendorCode' => $this->getVendorCode(),
        'VendorName' => $this->getVendorName(),
        'Latitude' => $this->getLatitude(),
        'Longitude' => $this->getLongitude(),
        'Address' => $this->getAddress(),
        'ZIPCode' => $this->getZIPCode(),
        'Web' => $this->getWeb(),
        'Telephone' => $this->getTelephone(),
        'Fax' => $this->getFax(),
        'BuildDate' => $this->getBuildDate(),
        'RenewDate' => $this->getRenewDate(),
        'UpdateRegisters' => $this->getUpdateRegisters(),
        'MinRoomPrice' => $this->getMinRoomPrice(),
        'NoOfRooms' => $this->getNoOfRooms(),
        'MinChildAge' => $this->getMinChildAge(),
        'MaxChildAge' => $this->getMaxChildAge(),
        'NoOfBuildings' => $this->getNoOfBuildings(),
        'NoOfFloors' => $this->getNoOfFloors(),
        'NoOfElevators' => $this->getNoOfElevators(),
        'NoShowNotPaxInGetAvailAndRates' => $this->getNoShowNotPaxInGetAvailAndRates(),
        'DailyRate' => $this->getDailyRate(),
        'OrderCalcSuplByPriority' => $this->getOrderCalcSuplByPriority(),
        'NoConvertPaxInOccupation' => $this->getNoConvertPaxInOccupation(),
        'StayDetermineMinStay' => $this->getStayDetermineMinStay(),
        'NetPriceRuleInHotelGroups' => $this->getNetPriceRuleInHotelGroups(),
        'Comission' => $this->getComission(),
      );
    }

    /**
     * @return ArrayOfMlText
     */
    public function getHotelDescriptionMlList()
    {
      return $this->HotelDescriptionMlList;
    }

    /**
     * @param ArrayOfMlText $HotelDescriptionMlList
     * @return \Dingus\SyncroService\PutHotelRQ
     */
    public function setHotelDescriptionMlList($HotelDescriptionMlList)
    {
      $this->HotelDescriptionMlList = $HotelDescriptionMlList;
      return $this;
    }

    /**
     * @return ArrayOfMlText
     */
    public function getShortDescriptionMlList()
    {
      return $this->ShortDescriptionMlList;
    }

    /**
     * @param ArrayOfMlText $ShortDescriptionMlList
     * @return \Dingus\SyncroService\PutHotelRQ
     */
    public function setShortDescriptionMlList($ShortDescriptionMlList)
    {
      $this->ShortDescriptionMlList = $ShortDescriptionMlList;
      return $this;
    }

    /**
     * @return ArrayOfMlText
     */
    public function getCategoryNameMlList()
    {
      return $this->CategoryNameMlList;
    }

    /**
     * @param ArrayOfMlText $CategoryNameMlList
     * @return \Dingus\SyncroService\PutHotelRQ
     */
    public function setCategoryNameMlList($CategoryNameMlList)
    {
      $this->CategoryNameMlList = $CategoryNameMlList;
      return $this;
    }

    /**
     * @return ArrayOfMlText
     */
    public function getChainNameMlList()
    {
      return $this->ChainNameMlList;
    }

    /**
     * @param ArrayOfMlText $ChainNameMlList
     * @return \Dingus\SyncroService\PutHotelRQ
     */
    public function setChainNameMlList($ChainNameMlList)
    {
      $this->ChainNameMlList = $ChainNameMlList;
      return $this;
    }

    /**
     * @return ArrayOfRoom
     */
    public function getRooms()
    {
      return $this->Rooms;
    }

    /**
     * @param ArrayOfRoom $Rooms
     * @return \Dingus\SyncroService\PutHotelRQ
     */
    public function setRooms($Rooms)
    {
      $this->Rooms = $Rooms;
      return $this;
    }

    /**
     * @return ArrayOfBoard
     */
    public function getBoards()
    {
      return $this->Boards;
    }

    /**
     * @param ArrayOfBoard $Boards
     * @return \Dingus\SyncroService\PutHotelRQ
     */
    public function setBoards($Boards)
    {
      $this->Boards = $Boards;
      return $this;
    }

    /**
     * @return ArrayOfExtraHotel
     */
    public function getExtras()
    {
      return $this->Extras;
    }

    /**
     * @param ArrayOfExtraHotel $Extras
     * @return \Dingus\SyncroService\PutHotelRQ
     */
    public function setExtras($Extras)
    {
      $this->Extras = $Extras;
      return $this;
    }

    /**
     * @return ArrayOfClose
     */
    public function getCloses()
    {
      return $this->Closes;
    }

    /**
     * @param ArrayOfClose $Closes
     * @return \Dingus\SyncroService\PutHotelRQ
     */
    public function setCloses($Closes)
    {
      $this->Closes = $Closes;
      return $this;
    }

    /**
     * @return ArrayOfInstalaciones
     */
    public function getInstalaciones()
    {
      return $this->Instalaciones;
    }

    /**
     * @param ArrayOfInstalaciones $Instalaciones
     * @return \Dingus\SyncroService\PutHotelRQ
     */
    public function setInstalaciones($Instalaciones)
    {
      $this->Instalaciones = $Instalaciones;
      return $this;
    }

    /**
     * @return ArrayOfFacilities
     */
    public function getFacilities()
    {
      return $this->Facilities;
    }

    /**
     * @param ArrayOfFacilities $Facilities
     * @return \Dingus\SyncroService\PutHotelRQ
     */
    public function setFacilities($Facilities)
    {
      $this->Facilities = $Facilities;
      return $this;
    }

    /**
     * @return ArrayOfContacts
     */
    public function getContacts()
    {
      return $this->Contacts;
    }

    /**
     * @param ArrayOfContacts $Contacts
     * @return \Dingus\SyncroService\PutHotelRQ
     */
    public function setContacts($Contacts)
    {
      $this->Contacts = $Contacts;
      return $this;
    }

    /**
     * @return ArrayOfDistance
     */
    public function getDistances()
    {
      return $this->Distances;
    }

    /**
     * @param ArrayOfDistance $Distances
     * @return \Dingus\SyncroService\PutHotelRQ
     */
    public function setDistances($Distances)
    {
      $this->Distances = $Distances;
      return $this;
    }

    /**
     * @return ArrayOfMeetingRoom
     */
    public function getMeetingRoom()
    {
      return $this->MeetingRoom;
    }

    /**
     * @param ArrayOfMeetingRoom $MeetingRoom
     * @return \Dingus\SyncroService\PutHotelRQ
     */
    public function setMeetingRoom($MeetingRoom)
    {
      $this->MeetingRoom = $MeetingRoom;
      return $this;
    }

    /**
     * @return ArrayOfRestaurant
     */
    public function getRestaurants()
    {
      return $this->Restaurants;
    }

    /**
     * @param ArrayOfRestaurant $Restaurants
     * @return \Dingus\SyncroService\PutHotelRQ
     */
    public function setRestaurants($Restaurants)
    {
      $this->Restaurants = $Restaurants;
      return $this;
    }

    /**
     * @return ArrayOfService
     */
    public function getServices()
    {
      return $this->Services;
    }

    /**
     * @param ArrayOfService $Services
     * @return \Dingus\SyncroService\PutHotelRQ
     */
    public function setServices($Services)
    {
      $this->Services = $Services;
      return $this;
    }

    /**
     * @return ArrayOfControlPrice
     */
    public function getControlPrices()
    {
      return $this->ControlPrices;
    }

    /**
     * @param ArrayOfControlPrice $ControlPrices
     * @return \Dingus\SyncroService\PutHotelRQ
     */
    public function setControlPrices($ControlPrices)
    {
      $this->ControlPrices = $ControlPrices;
      return $this;
    }

    /**
     * @return ArrayOfPicture
     */
    public function getPictures()
    {
      return $this->Pictures;
    }

    /**
     * @param ArrayOfPicture $Pictures
     * @return \Dingus\SyncroService\PutHotelRQ
     */
    public function setPictures($Pictures)
    {
      $this->Pictures = $Pictures;
      return $this;
    }

    /**
     * @return ArrayOfCriterion
     */
    public function getCriteria()
    {
      return $this->Criteria;
    }

    /**
     * @param ArrayOfCriterion $Criteria
     * @return \Dingus\SyncroService\PutHotelRQ
     */
    public function setCriteria($Criteria)
    {
      $this->Criteria = $Criteria;
      return $this;
    }

    /**
     * @return Action
     */
    public function getAction()
    {
      return $this->Action;
    }

    /**
     * @param Action $Action
     * @return \Dingus\SyncroService\PutHotelRQ
     */
    public function setAction($Action)
    {
      $this->Action = $Action;
      return $this;
    }

    /**
     * @return string
     */
    public function getHotelCode()
    {
      return $this->HotelCode;
    }

    /**
     * @param string $HotelCode
     * @return \Dingus\SyncroService\PutHotelRQ
     */
    public function setHotelCode($HotelCode)
    {
      $this->HotelCode = $HotelCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getHotelName()
    {
      return $this->HotelName;
    }

    /**
     * @param string $HotelName
     * @return \Dingus\SyncroService\PutHotelRQ
     */
    public function setHotelName($HotelName)
    {
      $this->HotelName = $HotelName;
      return $this;
    }

    /**
     * @return string
     */
    public function getHotelDescription()
    {
      return $this->HotelDescription;
    }

    /**
     * @param string $HotelDescription
     * @return \Dingus\SyncroService\PutHotelRQ
     */
    public function setHotelDescription($HotelDescription)
    {
      $this->HotelDescription = $HotelDescription;
      return $this;
    }

    /**
     * @return string
     */
    public function getShortDescription()
    {
      return $this->ShortDescription;
    }

    /**
     * @param string $ShortDescription
     * @return \Dingus\SyncroService\PutHotelRQ
     */
    public function setShortDescription($ShortDescription)
    {
      $this->ShortDescription = $ShortDescription;
      return $this;
    }

    /**
     * @return string
     */
    public function getCategoryCode()
    {
      return $this->CategoryCode;
    }

    /**
     * @param string $CategoryCode
     * @return \Dingus\SyncroService\PutHotelRQ
     */
    public function setCategoryCode($CategoryCode)
    {
      $this->CategoryCode = $CategoryCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getCategoryName()
    {
      return $this->CategoryName;
    }

    /**
     * @param string $CategoryName
     * @return \Dingus\SyncroService\PutHotelRQ
     */
    public function setCategoryName($CategoryName)
    {
      $this->CategoryName = $CategoryName;
      return $this;
    }

    /**
     * @return string
     */
    public function getChainCode()
    {
      return $this->ChainCode;
    }

    /**
     * @param string $ChainCode
     * @return \Dingus\SyncroService\PutHotelRQ
     */
    public function setChainCode($ChainCode)
    {
      $this->ChainCode = $ChainCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getChainName()
    {
      return $this->ChainName;
    }

    /**
     * @param string $ChainName
     * @return \Dingus\SyncroService\PutHotelRQ
     */
    public function setChainName($ChainName)
    {
      $this->ChainName = $ChainName;
      return $this;
    }

    /**
     * @return string
     */
    public function getVendorCode()
    {
      return $this->VendorCode;
    }

    /**
     * @param string $VendorCode
     * @return \Dingus\SyncroService\PutHotelRQ
     */
    public function setVendorCode($VendorCode)
    {
      $this->VendorCode = $VendorCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getVendorName()
    {
      return $this->VendorName;
    }

    /**
     * @param string $VendorName
     * @return \Dingus\SyncroService\PutHotelRQ
     */
    public function setVendorName($VendorName)
    {
      $this->VendorName = $VendorName;
      return $this;
    }

    /**
     * @return float
     */
    public function getLatitude()
    {
      return $this->Latitude;
    }

    /**
     * @param float $Latitude
     * @return \Dingus\SyncroService\PutHotelRQ
     */
    public function setLatitude($Latitude)
    {
      $this->Latitude = $Latitude;
      return $this;
    }

    /**
     * @return float
     */
    public function getLongitude()
    {
      return $this->Longitude;
    }

    /**
     * @param float $Longitude
     * @return \Dingus\SyncroService\PutHotelRQ
     */
    public function setLongitude($Longitude)
    {
      $this->Longitude = $Longitude;
      return $this;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
      return $this->Address;
    }

    /**
     * @param string $Address
     * @return \Dingus\SyncroService\PutHotelRQ
     */
    public function setAddress($Address)
    {
      $this->Address = $Address;
      return $this;
    }

    /**
     * @return string
     */
    public function getZIPCode()
    {
      return $this->ZIPCode;
    }

    /**
     * @param string $ZIPCode
     * @return \Dingus\SyncroService\PutHotelRQ
     */
    public function setZIPCode($ZIPCode)
    {
      $this->ZIPCode = $ZIPCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getWeb()
    {
      return $this->Web;
    }

    /**
     * @param string $Web
     * @return \Dingus\SyncroService\PutHotelRQ
     */
    public function setWeb($Web)
    {
      $this->Web = $Web;
      return $this;
    }

    /**
     * @return string
     */
    public function getTelephone()
    {
      return $this->Telephone;
    }

    /**
     * @param string $Telephone
     * @return \Dingus\SyncroService\PutHotelRQ
     */
    public function setTelephone($Telephone)
    {
      $this->Telephone = $Telephone;
      return $this;
    }

    /**
     * @return string
     */
    public function getFax()
    {
      return $this->Fax;
    }

    /**
     * @param string $Fax
     * @return \Dingus\SyncroService\PutHotelRQ
     */
    public function setFax($Fax)
    {
      $this->Fax = $Fax;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getBuildDate()
    {
      if ($this->BuildDate == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->BuildDate);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $BuildDate
     * @return \Dingus\SyncroService\PutHotelRQ
     */
    public function setBuildDate(\DateTime $BuildDate)
    {
      $this->BuildDate = $BuildDate->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getRenewDate()
    {
      if ($this->RenewDate == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->RenewDate);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $RenewDate
     * @return \Dingus\SyncroService\PutHotelRQ
     */
    public function setRenewDate(\DateTime $RenewDate)
    {
      $this->RenewDate = $RenewDate->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdateRegisters()
    {
      if ($this->UpdateRegisters == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->UpdateRegisters);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $UpdateRegisters
     * @return \Dingus\SyncroService\PutHotelRQ
     */
    public function setUpdateRegisters(\DateTime $UpdateRegisters)
    {
      $this->UpdateRegisters = $UpdateRegisters->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return float
     */
    public function getMinRoomPrice()
    {
      return $this->MinRoomPrice;
    }

    /**
     * @param float $MinRoomPrice
     * @return \Dingus\SyncroService\PutHotelRQ
     */
    public function setMinRoomPrice($MinRoomPrice)
    {
      $this->MinRoomPrice = $MinRoomPrice;
      return $this;
    }

    /**
     * @return int
     */
    public function getNoOfRooms()
    {
      return $this->NoOfRooms;
    }

    /**
     * @param int $NoOfRooms
     * @return \Dingus\SyncroService\PutHotelRQ
     */
    public function setNoOfRooms($NoOfRooms)
    {
      $this->NoOfRooms = $NoOfRooms;
      return $this;
    }

    /**
     * @return int
     */
    public function getMinChildAge()
    {
      return $this->MinChildAge;
    }

    /**
     * @param int $MinChildAge
     * @return \Dingus\SyncroService\PutHotelRQ
     */
    public function setMinChildAge($MinChildAge)
    {
      $this->MinChildAge = $MinChildAge;
      return $this;
    }

    /**
     * @return int
     */
    public function getMaxChildAge()
    {
      return $this->MaxChildAge;
    }

    /**
     * @param int $MaxChildAge
     * @return \Dingus\SyncroService\PutHotelRQ
     */
    public function setMaxChildAge($MaxChildAge)
    {
      $this->MaxChildAge = $MaxChildAge;
      return $this;
    }

    /**
     * @return int
     */
    public function getNoOfBuildings()
    {
      return $this->NoOfBuildings;
    }

    /**
     * @param int $NoOfBuildings
     * @return \Dingus\SyncroService\PutHotelRQ
     */
    public function setNoOfBuildings($NoOfBuildings)
    {
      $this->NoOfBuildings = $NoOfBuildings;
      return $this;
    }

    /**
     * @return int
     */
    public function getNoOfFloors()
    {
      return $this->NoOfFloors;
    }

    /**
     * @param int $NoOfFloors
     * @return \Dingus\SyncroService\PutHotelRQ
     */
    public function setNoOfFloors($NoOfFloors)
    {
      $this->NoOfFloors = $NoOfFloors;
      return $this;
    }

    /**
     * @return int
     */
    public function getNoOfElevators()
    {
      return $this->NoOfElevators;
    }

    /**
     * @param int $NoOfElevators
     * @return \Dingus\SyncroService\PutHotelRQ
     */
    public function setNoOfElevators($NoOfElevators)
    {
      $this->NoOfElevators = $NoOfElevators;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getNoShowNotPaxInGetAvailAndRates()
    {
      return $this->NoShowNotPaxInGetAvailAndRates;
    }

    /**
     * @param boolean $NoShowNotPaxInGetAvailAndRates
     * @return \Dingus\SyncroService\PutHotelRQ
     */
    public function setNoShowNotPaxInGetAvailAndRates($NoShowNotPaxInGetAvailAndRates)
    {
      $this->NoShowNotPaxInGetAvailAndRates = $NoShowNotPaxInGetAvailAndRates;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getDailyRate()
    {
      return $this->DailyRate;
    }

    /**
     * @param boolean $DailyRate
     * @return \Dingus\SyncroService\PutHotelRQ
     */
    public function setDailyRate($DailyRate)
    {
      $this->DailyRate = $DailyRate;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getOrderCalcSuplByPriority()
    {
      return $this->OrderCalcSuplByPriority;
    }

    /**
     * @param boolean $OrderCalcSuplByPriority
     * @return \Dingus\SyncroService\PutHotelRQ
     */
    public function setOrderCalcSuplByPriority($OrderCalcSuplByPriority)
    {
      $this->OrderCalcSuplByPriority = $OrderCalcSuplByPriority;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getNoConvertPaxInOccupation()
    {
      return $this->NoConvertPaxInOccupation;
    }

    /**
     * @param boolean $NoConvertPaxInOccupation
     * @return \Dingus\SyncroService\PutHotelRQ
     */
    public function setNoConvertPaxInOccupation($NoConvertPaxInOccupation)
    {
      $this->NoConvertPaxInOccupation = $NoConvertPaxInOccupation;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getStayDetermineMinStay()
    {
      return $this->StayDetermineMinStay;
    }

    /**
     * @param boolean $StayDetermineMinStay
     * @return \Dingus\SyncroService\PutHotelRQ
     */
    public function setStayDetermineMinStay($StayDetermineMinStay)
    {
      $this->StayDetermineMinStay = $StayDetermineMinStay;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getNetPriceRuleInHotelGroups()
    {
      return $this->NetPriceRuleInHotelGroups;
    }

    /**
     * @param boolean $NetPriceRuleInHotelGroups
     * @return \Dingus\SyncroService\PutHotelRQ
     */
    public function setNetPriceRuleInHotelGroups($NetPriceRuleInHotelGroups)
    {
      $this->NetPriceRuleInHotelGroups = $NetPriceRuleInHotelGroups;
      return $this;
    }

    /**
     * @return Comission
     */
    public function getComission()
    {
      return $this->Comission;
    }

    /**
     * @param Comission $Comission
     * @return \Dingus\SyncroService\PutHotelRQ
     */
    public function setComission($Comission)
    {
      $this->Comission = $Comission;
      return $this;
    }

}
