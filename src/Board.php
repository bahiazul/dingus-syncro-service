<?php

namespace Dingus\SyncroService;

class Board implements \JsonSerializable
{

    /**
     * @var ArrayOfMlText $BoardRecNameMlList
     */
    protected $BoardRecNameMlList = null;

    /**
     * @var Action $Action
     */
    protected $Action = null;

    /**
     * @var string $BoardCode
     */
    protected $BoardCode = null;

    /**
     * @var string $BoardName
     */
    protected $BoardName = null;

    /**
     * @var boolean $PorDefecto
     */
    protected $PorDefecto = null;

    /**
     * @var boolean $HalfPrice
     */
    protected $HalfPrice = null;

    /**
     * @var \DateTime $ByDefaultDateFrom
     */
    protected $ByDefaultDateFrom = null;

    /**
     * @var \DateTime $ByDefaultDateTo
     */
    protected $ByDefaultDateTo = null;

    /**
     * @var string $PMSCode
     */
    protected $PMSCode = null;

    /**
     * @param Action $Action
     * @param boolean $PorDefecto
     * @param boolean $HalfPrice
     * @param \DateTime $ByDefaultDateFrom
     * @param \DateTime $ByDefaultDateTo
     */
    public function __construct($Action, $PorDefecto, $HalfPrice, \DateTime $ByDefaultDateFrom, \DateTime $ByDefaultDateTo)
    {
      $this->Action = $Action;
      $this->PorDefecto = $PorDefecto;
      $this->HalfPrice = $HalfPrice;
      $this->ByDefaultDateFrom = $ByDefaultDateFrom->format(\DateTime::ATOM);
      $this->ByDefaultDateTo = $ByDefaultDateTo->format(\DateTime::ATOM);
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'BoardRecNameMlList' => $this->getBoardRecNameMlList(),
        'Action' => $this->getAction(),
        'BoardCode' => $this->getBoardCode(),
        'BoardName' => $this->getBoardName(),
        'PorDefecto' => $this->getPorDefecto(),
        'HalfPrice' => $this->getHalfPrice(),
        'ByDefaultDateFrom' => $this->getByDefaultDateFrom(),
        'ByDefaultDateTo' => $this->getByDefaultDateTo(),
        'PMSCode' => $this->getPMSCode(),
      );
    }

    /**
     * @return ArrayOfMlText
     */
    public function getBoardRecNameMlList()
    {
      return $this->BoardRecNameMlList;
    }

    /**
     * @param ArrayOfMlText $BoardRecNameMlList
     * @return \Dingus\SyncroService\Board
     */
    public function setBoardRecNameMlList($BoardRecNameMlList)
    {
      $this->BoardRecNameMlList = $BoardRecNameMlList;
      return $this;
    }

    /**
     * @return Action
     */
    public function getAction()
    {
      return $this->Action;
    }

    /**
     * @param Action $Action
     * @return \Dingus\SyncroService\Board
     */
    public function setAction($Action)
    {
      $this->Action = $Action;
      return $this;
    }

    /**
     * @return string
     */
    public function getBoardCode()
    {
      return $this->BoardCode;
    }

    /**
     * @param string $BoardCode
     * @return \Dingus\SyncroService\Board
     */
    public function setBoardCode($BoardCode)
    {
      $this->BoardCode = $BoardCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getBoardName()
    {
      return $this->BoardName;
    }

    /**
     * @param string $BoardName
     * @return \Dingus\SyncroService\Board
     */
    public function setBoardName($BoardName)
    {
      $this->BoardName = $BoardName;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getPorDefecto()
    {
      return $this->PorDefecto;
    }

    /**
     * @param boolean $PorDefecto
     * @return \Dingus\SyncroService\Board
     */
    public function setPorDefecto($PorDefecto)
    {
      $this->PorDefecto = $PorDefecto;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getHalfPrice()
    {
      return $this->HalfPrice;
    }

    /**
     * @param boolean $HalfPrice
     * @return \Dingus\SyncroService\Board
     */
    public function setHalfPrice($HalfPrice)
    {
      $this->HalfPrice = $HalfPrice;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getByDefaultDateFrom()
    {
      if ($this->ByDefaultDateFrom == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->ByDefaultDateFrom);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $ByDefaultDateFrom
     * @return \Dingus\SyncroService\Board
     */
    public function setByDefaultDateFrom(\DateTime $ByDefaultDateFrom)
    {
      $this->ByDefaultDateFrom = $ByDefaultDateFrom->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getByDefaultDateTo()
    {
      if ($this->ByDefaultDateTo == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->ByDefaultDateTo);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $ByDefaultDateTo
     * @return \Dingus\SyncroService\Board
     */
    public function setByDefaultDateTo(\DateTime $ByDefaultDateTo)
    {
      $this->ByDefaultDateTo = $ByDefaultDateTo->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return string
     */
    public function getPMSCode()
    {
      return $this->PMSCode;
    }

    /**
     * @param string $PMSCode
     * @return \Dingus\SyncroService\Board
     */
    public function setPMSCode($PMSCode)
    {
      $this->PMSCode = $PMSCode;
      return $this;
    }

}
