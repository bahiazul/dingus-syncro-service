<?php

namespace Dingus\SyncroService;

class GetBookingsByBookingDataConverted implements \JsonSerializable
{

    /**
     * @var Credentials $Credentials
     */
    protected $Credentials = null;

    /**
     * @var string $BookingDataCode
     */
    protected $BookingDataCode = null;

    /**
     * @var string $BookingDataValue
     */
    protected $BookingDataValue = null;

    /**
     * @var string $CustomerCode
     */
    protected $CustomerCode = null;

    /**
     * @var string $HotelCode
     */
    protected $HotelCode = null;

    /**
     * @var string $ConversionKey
     */
    protected $ConversionKey = null;

    /**
     * @param Credentials $Credentials
     * @param string $BookingDataCode
     * @param string $BookingDataValue
     * @param string $CustomerCode
     * @param string $HotelCode
     * @param string $ConversionKey
     */
    public function __construct($Credentials, $BookingDataCode, $BookingDataValue, $CustomerCode, $HotelCode, $ConversionKey)
    {
      $this->Credentials = $Credentials;
      $this->BookingDataCode = $BookingDataCode;
      $this->BookingDataValue = $BookingDataValue;
      $this->CustomerCode = $CustomerCode;
      $this->HotelCode = $HotelCode;
      $this->ConversionKey = $ConversionKey;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'Credentials' => $this->getCredentials(),
        'BookingDataCode' => $this->getBookingDataCode(),
        'BookingDataValue' => $this->getBookingDataValue(),
        'CustomerCode' => $this->getCustomerCode(),
        'HotelCode' => $this->getHotelCode(),
        'ConversionKey' => $this->getConversionKey(),
      );
    }

    /**
     * @return Credentials
     */
    public function getCredentials()
    {
      return $this->Credentials;
    }

    /**
     * @param Credentials $Credentials
     * @return \Dingus\SyncroService\GetBookingsByBookingDataConverted
     */
    public function setCredentials($Credentials)
    {
      $this->Credentials = $Credentials;
      return $this;
    }

    /**
     * @return string
     */
    public function getBookingDataCode()
    {
      return $this->BookingDataCode;
    }

    /**
     * @param string $BookingDataCode
     * @return \Dingus\SyncroService\GetBookingsByBookingDataConverted
     */
    public function setBookingDataCode($BookingDataCode)
    {
      $this->BookingDataCode = $BookingDataCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getBookingDataValue()
    {
      return $this->BookingDataValue;
    }

    /**
     * @param string $BookingDataValue
     * @return \Dingus\SyncroService\GetBookingsByBookingDataConverted
     */
    public function setBookingDataValue($BookingDataValue)
    {
      $this->BookingDataValue = $BookingDataValue;
      return $this;
    }

    /**
     * @return string
     */
    public function getCustomerCode()
    {
      return $this->CustomerCode;
    }

    /**
     * @param string $CustomerCode
     * @return \Dingus\SyncroService\GetBookingsByBookingDataConverted
     */
    public function setCustomerCode($CustomerCode)
    {
      $this->CustomerCode = $CustomerCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getHotelCode()
    {
      return $this->HotelCode;
    }

    /**
     * @param string $HotelCode
     * @return \Dingus\SyncroService\GetBookingsByBookingDataConverted
     */
    public function setHotelCode($HotelCode)
    {
      $this->HotelCode = $HotelCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getConversionKey()
    {
      return $this->ConversionKey;
    }

    /**
     * @param string $ConversionKey
     * @return \Dingus\SyncroService\GetBookingsByBookingDataConverted
     */
    public function setConversionKey($ConversionKey)
    {
      $this->ConversionKey = $ConversionKey;
      return $this;
    }

}
