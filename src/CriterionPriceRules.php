<?php

namespace Dingus\SyncroService;

class CriterionPriceRules implements \JsonSerializable
{

    /**
     * @var Action $Action
     */
    protected $Action = null;

    /**
     * @var int $Id
     */
    protected $Id = null;

    /**
     * @var string $Code
     */
    protected $Code = null;

    /**
     * @var string $Value
     */
    protected $Value = null;

    /**
     * @param Action $Action
     * @param int $Id
     */
    public function __construct($Action, $Id)
    {
      $this->Action = $Action;
      $this->Id = $Id;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'Action' => $this->getAction(),
        'Id' => $this->getId(),
        'Code' => $this->getCode(),
        'Value' => $this->getValue(),
      );
    }

    /**
     * @return Action
     */
    public function getAction()
    {
      return $this->Action;
    }

    /**
     * @param Action $Action
     * @return \Dingus\SyncroService\CriterionPriceRules
     */
    public function setAction($Action)
    {
      $this->Action = $Action;
      return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
      return $this->Id;
    }

    /**
     * @param int $Id
     * @return \Dingus\SyncroService\CriterionPriceRules
     */
    public function setId($Id)
    {
      $this->Id = $Id;
      return $this;
    }

    /**
     * @return string
     */
    public function getCode()
    {
      return $this->Code;
    }

    /**
     * @param string $Code
     * @return \Dingus\SyncroService\CriterionPriceRules
     */
    public function setCode($Code)
    {
      $this->Code = $Code;
      return $this;
    }

    /**
     * @return string
     */
    public function getValue()
    {
      return $this->Value;
    }

    /**
     * @param string $Value
     * @return \Dingus\SyncroService\CriterionPriceRules
     */
    public function setValue($Value)
    {
      $this->Value = $Value;
      return $this;
    }

}
