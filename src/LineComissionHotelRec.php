<?php

namespace Dingus\SyncroService;

class LineComissionHotelRec implements \JsonSerializable
{

    /**
     * @var Action $Action
     */
    protected $Action = null;

    /**
     * @var int $Id
     */
    protected $Id = null;

    /**
     * @var \DateTime $DateFrom
     */
    protected $DateFrom = null;

    /**
     * @var \DateTime $DateTo
     */
    protected $DateTo = null;

    /**
     * @var float $Comission
     */
    protected $Comission = null;

    /**
     * @var TipoCalculoTarifaHotel $CalcTypeComissionHotel
     */
    protected $CalcTypeComissionHotel = null;

    /**
     * @param Action $Action
     * @param int $Id
     * @param \DateTime $DateFrom
     * @param \DateTime $DateTo
     * @param float $Comission
     * @param TipoCalculoTarifaHotel $CalcTypeComissionHotel
     */
    public function __construct($Action, $Id, \DateTime $DateFrom, \DateTime $DateTo, $Comission, $CalcTypeComissionHotel)
    {
      $this->Action = $Action;
      $this->Id = $Id;
      $this->DateFrom = $DateFrom->format(\DateTime::ATOM);
      $this->DateTo = $DateTo->format(\DateTime::ATOM);
      $this->Comission = $Comission;
      $this->CalcTypeComissionHotel = $CalcTypeComissionHotel;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'Action' => $this->getAction(),
        'Id' => $this->getId(),
        'DateFrom' => $this->getDateFrom(),
        'DateTo' => $this->getDateTo(),
        'Comission' => $this->getComission(),
        'CalcTypeComissionHotel' => $this->getCalcTypeComissionHotel(),
      );
    }

    /**
     * @return Action
     */
    public function getAction()
    {
      return $this->Action;
    }

    /**
     * @param Action $Action
     * @return \Dingus\SyncroService\LineComissionHotelRec
     */
    public function setAction($Action)
    {
      $this->Action = $Action;
      return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
      return $this->Id;
    }

    /**
     * @param int $Id
     * @return \Dingus\SyncroService\LineComissionHotelRec
     */
    public function setId($Id)
    {
      $this->Id = $Id;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateFrom()
    {
      if ($this->DateFrom == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->DateFrom);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $DateFrom
     * @return \Dingus\SyncroService\LineComissionHotelRec
     */
    public function setDateFrom(\DateTime $DateFrom)
    {
      $this->DateFrom = $DateFrom->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateTo()
    {
      if ($this->DateTo == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->DateTo);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $DateTo
     * @return \Dingus\SyncroService\LineComissionHotelRec
     */
    public function setDateTo(\DateTime $DateTo)
    {
      $this->DateTo = $DateTo->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return float
     */
    public function getComission()
    {
      return $this->Comission;
    }

    /**
     * @param float $Comission
     * @return \Dingus\SyncroService\LineComissionHotelRec
     */
    public function setComission($Comission)
    {
      $this->Comission = $Comission;
      return $this;
    }

    /**
     * @return TipoCalculoTarifaHotel
     */
    public function getCalcTypeComissionHotel()
    {
      return $this->CalcTypeComissionHotel;
    }

    /**
     * @param TipoCalculoTarifaHotel $CalcTypeComissionHotel
     * @return \Dingus\SyncroService\LineComissionHotelRec
     */
    public function setCalcTypeComissionHotel($CalcTypeComissionHotel)
    {
      $this->CalcTypeComissionHotel = $CalcTypeComissionHotel;
      return $this;
    }

}
