<?php

namespace Dingus\SyncroService;

class ComissionHotelRec implements \JsonSerializable
{

    /**
     * @var ArrayOfLineComissionHotelRec $LineComissionHotelList
     */
    protected $LineComissionHotelList = null;

    /**
     * @var Action $Action
     */
    protected $Action = null;

    /**
     * @var string $ComissionCode
     */
    protected $ComissionCode = null;

    /**
     * @var string $ComissionDescription
     */
    protected $ComissionDescription = null;

    /**
     * @var string $CustomerRateGroupCode
     */
    protected $CustomerRateGroupCode = null;

    /**
     * @var string $CustomerRateGroupDescription
     */
    protected $CustomerRateGroupDescription = null;

    /**
     * @var string $PromotionCode
     */
    protected $PromotionCode = null;

    /**
     * @var string $CustomerCode
     */
    protected $CustomerCode = null;

    /**
     * @var string $HotelCode
     */
    protected $HotelCode = null;

    /**
     * @var string $HotelName
     */
    protected $HotelName = null;

    /**
     * @param Action $Action
     */
    public function __construct($Action)
    {
      $this->Action = $Action;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'LineComissionHotelList' => $this->getLineComissionHotelList(),
        'Action' => $this->getAction(),
        'ComissionCode' => $this->getComissionCode(),
        'ComissionDescription' => $this->getComissionDescription(),
        'CustomerRateGroupCode' => $this->getCustomerRateGroupCode(),
        'CustomerRateGroupDescription' => $this->getCustomerRateGroupDescription(),
        'PromotionCode' => $this->getPromotionCode(),
        'CustomerCode' => $this->getCustomerCode(),
        'HotelCode' => $this->getHotelCode(),
        'HotelName' => $this->getHotelName(),
      );
    }

    /**
     * @return ArrayOfLineComissionHotelRec
     */
    public function getLineComissionHotelList()
    {
      return $this->LineComissionHotelList;
    }

    /**
     * @param ArrayOfLineComissionHotelRec $LineComissionHotelList
     * @return \Dingus\SyncroService\ComissionHotelRec
     */
    public function setLineComissionHotelList($LineComissionHotelList)
    {
      $this->LineComissionHotelList = $LineComissionHotelList;
      return $this;
    }

    /**
     * @return Action
     */
    public function getAction()
    {
      return $this->Action;
    }

    /**
     * @param Action $Action
     * @return \Dingus\SyncroService\ComissionHotelRec
     */
    public function setAction($Action)
    {
      $this->Action = $Action;
      return $this;
    }

    /**
     * @return string
     */
    public function getComissionCode()
    {
      return $this->ComissionCode;
    }

    /**
     * @param string $ComissionCode
     * @return \Dingus\SyncroService\ComissionHotelRec
     */
    public function setComissionCode($ComissionCode)
    {
      $this->ComissionCode = $ComissionCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getComissionDescription()
    {
      return $this->ComissionDescription;
    }

    /**
     * @param string $ComissionDescription
     * @return \Dingus\SyncroService\ComissionHotelRec
     */
    public function setComissionDescription($ComissionDescription)
    {
      $this->ComissionDescription = $ComissionDescription;
      return $this;
    }

    /**
     * @return string
     */
    public function getCustomerRateGroupCode()
    {
      return $this->CustomerRateGroupCode;
    }

    /**
     * @param string $CustomerRateGroupCode
     * @return \Dingus\SyncroService\ComissionHotelRec
     */
    public function setCustomerRateGroupCode($CustomerRateGroupCode)
    {
      $this->CustomerRateGroupCode = $CustomerRateGroupCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getCustomerRateGroupDescription()
    {
      return $this->CustomerRateGroupDescription;
    }

    /**
     * @param string $CustomerRateGroupDescription
     * @return \Dingus\SyncroService\ComissionHotelRec
     */
    public function setCustomerRateGroupDescription($CustomerRateGroupDescription)
    {
      $this->CustomerRateGroupDescription = $CustomerRateGroupDescription;
      return $this;
    }

    /**
     * @return string
     */
    public function getPromotionCode()
    {
      return $this->PromotionCode;
    }

    /**
     * @param string $PromotionCode
     * @return \Dingus\SyncroService\ComissionHotelRec
     */
    public function setPromotionCode($PromotionCode)
    {
      $this->PromotionCode = $PromotionCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getCustomerCode()
    {
      return $this->CustomerCode;
    }

    /**
     * @param string $CustomerCode
     * @return \Dingus\SyncroService\ComissionHotelRec
     */
    public function setCustomerCode($CustomerCode)
    {
      $this->CustomerCode = $CustomerCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getHotelCode()
    {
      return $this->HotelCode;
    }

    /**
     * @param string $HotelCode
     * @return \Dingus\SyncroService\ComissionHotelRec
     */
    public function setHotelCode($HotelCode)
    {
      $this->HotelCode = $HotelCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getHotelName()
    {
      return $this->HotelName;
    }

    /**
     * @param string $HotelName
     * @return \Dingus\SyncroService\ComissionHotelRec
     */
    public function setHotelName($HotelName)
    {
      $this->HotelName = $HotelName;
      return $this;
    }

}
