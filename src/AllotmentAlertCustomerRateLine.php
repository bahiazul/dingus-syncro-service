<?php

namespace Dingus\SyncroService;

class AllotmentAlertCustomerRateLine implements \JsonSerializable
{

    /**
     * @var int $IdCustomerRateLine
     */
    protected $IdCustomerRateLine = null;

    /**
     * @var int $IdAllotmentAlert
     */
    protected $IdAllotmentAlert = null;

    /**
     * @var \DateTime $DateAllotmentAlert
     */
    protected $DateAllotmentAlert = null;

    /**
     * @var string $RoomCodeAllotmentAlert
     */
    protected $RoomCodeAllotmentAlert = null;

    /**
     * @var string $CustomerRateAllotmentAlert
     */
    protected $CustomerRateAllotmentAlert = null;

    /**
     * @var string $CustomerRoomAllotmentAlert
     */
    protected $CustomerRoomAllotmentAlert = null;

    /**
     * @var string $CustomerBoardAllotmentAlert
     */
    protected $CustomerBoardAllotmentAlert = null;

    /**
     * @var int $RealAllotmentAllotmentAlert
     */
    protected $RealAllotmentAllotmentAlert = null;

    /**
     * @var int $MinAllotmentAllotmentAlert
     */
    protected $MinAllotmentAllotmentAlert = null;

    /**
     * @var boolean $StopSalesAllotmentAlert
     */
    protected $StopSalesAllotmentAlert = null;

    /**
     * @param int $IdCustomerRateLine
     * @param int $IdAllotmentAlert
     * @param \DateTime $DateAllotmentAlert
     * @param int $RealAllotmentAllotmentAlert
     * @param int $MinAllotmentAllotmentAlert
     * @param boolean $StopSalesAllotmentAlert
     */
    public function __construct($IdCustomerRateLine, $IdAllotmentAlert, \DateTime $DateAllotmentAlert, $RealAllotmentAllotmentAlert, $MinAllotmentAllotmentAlert, $StopSalesAllotmentAlert)
    {
      $this->IdCustomerRateLine = $IdCustomerRateLine;
      $this->IdAllotmentAlert = $IdAllotmentAlert;
      $this->DateAllotmentAlert = $DateAllotmentAlert->format(\DateTime::ATOM);
      $this->RealAllotmentAllotmentAlert = $RealAllotmentAllotmentAlert;
      $this->MinAllotmentAllotmentAlert = $MinAllotmentAllotmentAlert;
      $this->StopSalesAllotmentAlert = $StopSalesAllotmentAlert;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'IdCustomerRateLine' => $this->getIdCustomerRateLine(),
        'IdAllotmentAlert' => $this->getIdAllotmentAlert(),
        'DateAllotmentAlert' => $this->getDateAllotmentAlert(),
        'RoomCodeAllotmentAlert' => $this->getRoomCodeAllotmentAlert(),
        'CustomerRateAllotmentAlert' => $this->getCustomerRateAllotmentAlert(),
        'CustomerRoomAllotmentAlert' => $this->getCustomerRoomAllotmentAlert(),
        'CustomerBoardAllotmentAlert' => $this->getCustomerBoardAllotmentAlert(),
        'RealAllotmentAllotmentAlert' => $this->getRealAllotmentAllotmentAlert(),
        'MinAllotmentAllotmentAlert' => $this->getMinAllotmentAllotmentAlert(),
        'StopSalesAllotmentAlert' => $this->getStopSalesAllotmentAlert(),
      );
    }

    /**
     * @return int
     */
    public function getIdCustomerRateLine()
    {
      return $this->IdCustomerRateLine;
    }

    /**
     * @param int $IdCustomerRateLine
     * @return \Dingus\SyncroService\AllotmentAlertCustomerRateLine
     */
    public function setIdCustomerRateLine($IdCustomerRateLine)
    {
      $this->IdCustomerRateLine = $IdCustomerRateLine;
      return $this;
    }

    /**
     * @return int
     */
    public function getIdAllotmentAlert()
    {
      return $this->IdAllotmentAlert;
    }

    /**
     * @param int $IdAllotmentAlert
     * @return \Dingus\SyncroService\AllotmentAlertCustomerRateLine
     */
    public function setIdAllotmentAlert($IdAllotmentAlert)
    {
      $this->IdAllotmentAlert = $IdAllotmentAlert;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateAllotmentAlert()
    {
      if ($this->DateAllotmentAlert == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->DateAllotmentAlert);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $DateAllotmentAlert
     * @return \Dingus\SyncroService\AllotmentAlertCustomerRateLine
     */
    public function setDateAllotmentAlert(\DateTime $DateAllotmentAlert)
    {
      $this->DateAllotmentAlert = $DateAllotmentAlert->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return string
     */
    public function getRoomCodeAllotmentAlert()
    {
      return $this->RoomCodeAllotmentAlert;
    }

    /**
     * @param string $RoomCodeAllotmentAlert
     * @return \Dingus\SyncroService\AllotmentAlertCustomerRateLine
     */
    public function setRoomCodeAllotmentAlert($RoomCodeAllotmentAlert)
    {
      $this->RoomCodeAllotmentAlert = $RoomCodeAllotmentAlert;
      return $this;
    }

    /**
     * @return string
     */
    public function getCustomerRateAllotmentAlert()
    {
      return $this->CustomerRateAllotmentAlert;
    }

    /**
     * @param string $CustomerRateAllotmentAlert
     * @return \Dingus\SyncroService\AllotmentAlertCustomerRateLine
     */
    public function setCustomerRateAllotmentAlert($CustomerRateAllotmentAlert)
    {
      $this->CustomerRateAllotmentAlert = $CustomerRateAllotmentAlert;
      return $this;
    }

    /**
     * @return string
     */
    public function getCustomerRoomAllotmentAlert()
    {
      return $this->CustomerRoomAllotmentAlert;
    }

    /**
     * @param string $CustomerRoomAllotmentAlert
     * @return \Dingus\SyncroService\AllotmentAlertCustomerRateLine
     */
    public function setCustomerRoomAllotmentAlert($CustomerRoomAllotmentAlert)
    {
      $this->CustomerRoomAllotmentAlert = $CustomerRoomAllotmentAlert;
      return $this;
    }

    /**
     * @return string
     */
    public function getCustomerBoardAllotmentAlert()
    {
      return $this->CustomerBoardAllotmentAlert;
    }

    /**
     * @param string $CustomerBoardAllotmentAlert
     * @return \Dingus\SyncroService\AllotmentAlertCustomerRateLine
     */
    public function setCustomerBoardAllotmentAlert($CustomerBoardAllotmentAlert)
    {
      $this->CustomerBoardAllotmentAlert = $CustomerBoardAllotmentAlert;
      return $this;
    }

    /**
     * @return int
     */
    public function getRealAllotmentAllotmentAlert()
    {
      return $this->RealAllotmentAllotmentAlert;
    }

    /**
     * @param int $RealAllotmentAllotmentAlert
     * @return \Dingus\SyncroService\AllotmentAlertCustomerRateLine
     */
    public function setRealAllotmentAllotmentAlert($RealAllotmentAllotmentAlert)
    {
      $this->RealAllotmentAllotmentAlert = $RealAllotmentAllotmentAlert;
      return $this;
    }

    /**
     * @return int
     */
    public function getMinAllotmentAllotmentAlert()
    {
      return $this->MinAllotmentAllotmentAlert;
    }

    /**
     * @param int $MinAllotmentAllotmentAlert
     * @return \Dingus\SyncroService\AllotmentAlertCustomerRateLine
     */
    public function setMinAllotmentAllotmentAlert($MinAllotmentAllotmentAlert)
    {
      $this->MinAllotmentAllotmentAlert = $MinAllotmentAllotmentAlert;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getStopSalesAllotmentAlert()
    {
      return $this->StopSalesAllotmentAlert;
    }

    /**
     * @param boolean $StopSalesAllotmentAlert
     * @return \Dingus\SyncroService\AllotmentAlertCustomerRateLine
     */
    public function setStopSalesAllotmentAlert($StopSalesAllotmentAlert)
    {
      $this->StopSalesAllotmentAlert = $StopSalesAllotmentAlert;
      return $this;
    }

}
