<?php

namespace Dingus\SyncroService;

class PutStopSalesRQ implements \JsonSerializable
{

    /**
     * @var ArrayOfStopSalesRec $StopSales
     */
    protected $StopSales = null;

    
    public function __construct()
    {
    
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'StopSales' => $this->getStopSales(),
      );
    }

    /**
     * @return ArrayOfStopSalesRec
     */
    public function getStopSales()
    {
      return $this->StopSales;
    }

    /**
     * @param ArrayOfStopSalesRec $StopSales
     * @return \Dingus\SyncroService\PutStopSalesRQ
     */
    public function setStopSales($StopSales)
    {
      $this->StopSales = $StopSales;
      return $this;
    }

}
