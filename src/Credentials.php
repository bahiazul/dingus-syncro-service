<?php

namespace Dingus\SyncroService;

class Credentials implements \JsonSerializable
{

    /**
     * @var string $Languaje
     */
    protected $Languaje = null;

    /**
     * @var string $Password
     */
    protected $Password = null;

    /**
     * @var string $UserName
     */
    protected $UserName = null;

    
    public function __construct()
    {
    
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'Languaje' => $this->getLanguaje(),
        'Password' => $this->getPassword(),
        'UserName' => $this->getUserName(),
      );
    }

    /**
     * @return string
     */
    public function getLanguaje()
    {
      return $this->Languaje;
    }

    /**
     * @param string $Languaje
     * @return \Dingus\SyncroService\Credentials
     */
    public function setLanguaje($Languaje)
    {
      $this->Languaje = $Languaje;
      return $this;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
      return $this->Password;
    }

    /**
     * @param string $Password
     * @return \Dingus\SyncroService\Credentials
     */
    public function setPassword($Password)
    {
      $this->Password = $Password;
      return $this;
    }

    /**
     * @return string
     */
    public function getUserName()
    {
      return $this->UserName;
    }

    /**
     * @param string $UserName
     * @return \Dingus\SyncroService\Credentials
     */
    public function setUserName($UserName)
    {
      $this->UserName = $UserName;
      return $this;
    }

}
