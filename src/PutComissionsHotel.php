<?php

namespace Dingus\SyncroService;

class PutComissionsHotel implements \JsonSerializable
{

    /**
     * @var Credentials $Credentials
     */
    protected $Credentials = null;

    /**
     * @var PutComissionHotelRQ $RQ
     */
    protected $RQ = null;

    /**
     * @param Credentials $Credentials
     * @param PutComissionHotelRQ $RQ
     */
    public function __construct($Credentials, $RQ)
    {
      $this->Credentials = $Credentials;
      $this->RQ = $RQ;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'Credentials' => $this->getCredentials(),
        'RQ' => $this->getRQ(),
      );
    }

    /**
     * @return Credentials
     */
    public function getCredentials()
    {
      return $this->Credentials;
    }

    /**
     * @param Credentials $Credentials
     * @return \Dingus\SyncroService\PutComissionsHotel
     */
    public function setCredentials($Credentials)
    {
      $this->Credentials = $Credentials;
      return $this;
    }

    /**
     * @return PutComissionHotelRQ
     */
    public function getRQ()
    {
      return $this->RQ;
    }

    /**
     * @param PutComissionHotelRQ $RQ
     * @return \Dingus\SyncroService\PutComissionsHotel
     */
    public function setRQ($RQ)
    {
      $this->RQ = $RQ;
      return $this;
    }

}
