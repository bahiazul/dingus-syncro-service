<?php

namespace Dingus\SyncroService;

class PutRatePromotionResponse implements \JsonSerializable
{

    /**
     * @var SyncroRS $PutRatePromotionResult
     */
    protected $PutRatePromotionResult = null;

    /**
     * @param SyncroRS $PutRatePromotionResult
     */
    public function __construct($PutRatePromotionResult)
    {
      $this->PutRatePromotionResult = $PutRatePromotionResult;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'PutRatePromotionResult' => $this->getPutRatePromotionResult(),
      );
    }

    /**
     * @return SyncroRS
     */
    public function getPutRatePromotionResult()
    {
      return $this->PutRatePromotionResult;
    }

    /**
     * @param SyncroRS $PutRatePromotionResult
     * @return \Dingus\SyncroService\PutRatePromotionResponse
     */
    public function setPutRatePromotionResult($PutRatePromotionResult)
    {
      $this->PutRatePromotionResult = $PutRatePromotionResult;
      return $this;
    }

}
