<?php

namespace Dingus\SyncroService;

class ArrayOfStopSalesRec implements \JsonSerializable, \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var StopSalesRec[] $StopSalesRec
     */
    protected $StopSalesRec = null;

    
    public function __construct()
    {
    
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'StopSalesRec' => $this->getStopSalesRec(),
      );
    }

    /**
     * @return StopSalesRec[]
     */
    public function getStopSalesRec()
    {
      return $this->StopSalesRec;
    }

    /**
     * @param StopSalesRec[] $StopSalesRec
     * @return \Dingus\SyncroService\ArrayOfStopSalesRec
     */
    public function setStopSalesRec(array $StopSalesRec = null)
    {
      $this->StopSalesRec = $StopSalesRec;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->StopSalesRec[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return StopSalesRec
     */
    public function offsetGet($offset)
    {
      return $this->StopSalesRec[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param StopSalesRec $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      $this->StopSalesRec[$offset] = $value;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->StopSalesRec[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return StopSalesRec Return the current element
     */
    public function current()
    {
      return current($this->StopSalesRec);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->StopSalesRec);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->StopSalesRec);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->StopSalesRec);
    }

    /**
     * Countable implementation
     *
     * @return StopSalesRec Return count of elements
     */
    public function count()
    {
      return count($this->StopSalesRec);
    }

}
