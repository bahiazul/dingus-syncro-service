<?php

namespace Dingus\SyncroService;

class MarkBookingsAsReceived2Response implements \JsonSerializable
{

    /**
     * @var SyncroRS $MarkBookingsAsReceived2Result
     */
    protected $MarkBookingsAsReceived2Result = null;

    /**
     * @param SyncroRS $MarkBookingsAsReceived2Result
     */
    public function __construct($MarkBookingsAsReceived2Result)
    {
      $this->MarkBookingsAsReceived2Result = $MarkBookingsAsReceived2Result;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'MarkBookingsAsReceived2Result' => $this->getMarkBookingsAsReceived2Result(),
      );
    }

    /**
     * @return SyncroRS
     */
    public function getMarkBookingsAsReceived2Result()
    {
      return $this->MarkBookingsAsReceived2Result;
    }

    /**
     * @param SyncroRS $MarkBookingsAsReceived2Result
     * @return \Dingus\SyncroService\MarkBookingsAsReceived2Response
     */
    public function setMarkBookingsAsReceived2Result($MarkBookingsAsReceived2Result)
    {
      $this->MarkBookingsAsReceived2Result = $MarkBookingsAsReceived2Result;
      return $this;
    }

}
