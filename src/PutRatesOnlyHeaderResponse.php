<?php

namespace Dingus\SyncroService;

class PutRatesOnlyHeaderResponse implements \JsonSerializable
{

    /**
     * @var SyncroRS $PutRatesOnlyHeaderResult
     */
    protected $PutRatesOnlyHeaderResult = null;

    /**
     * @param SyncroRS $PutRatesOnlyHeaderResult
     */
    public function __construct($PutRatesOnlyHeaderResult)
    {
      $this->PutRatesOnlyHeaderResult = $PutRatesOnlyHeaderResult;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'PutRatesOnlyHeaderResult' => $this->getPutRatesOnlyHeaderResult(),
      );
    }

    /**
     * @return SyncroRS
     */
    public function getPutRatesOnlyHeaderResult()
    {
      return $this->PutRatesOnlyHeaderResult;
    }

    /**
     * @param SyncroRS $PutRatesOnlyHeaderResult
     * @return \Dingus\SyncroService\PutRatesOnlyHeaderResponse
     */
    public function setPutRatesOnlyHeaderResult($PutRatesOnlyHeaderResult)
    {
      $this->PutRatesOnlyHeaderResult = $PutRatesOnlyHeaderResult;
      return $this;
    }

}
