<?php

namespace Dingus\SyncroService;

class ArrayOfDataTask implements \JsonSerializable, \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var DataTask[] $DataTask
     */
    protected $DataTask = null;

    
    public function __construct()
    {
    
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'DataTask' => $this->getDataTask(),
      );
    }

    /**
     * @return DataTask[]
     */
    public function getDataTask()
    {
      return $this->DataTask;
    }

    /**
     * @param DataTask[] $DataTask
     * @return \Dingus\SyncroService\ArrayOfDataTask
     */
    public function setDataTask(array $DataTask = null)
    {
      $this->DataTask = $DataTask;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->DataTask[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return DataTask
     */
    public function offsetGet($offset)
    {
      return $this->DataTask[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param DataTask $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      $this->DataTask[$offset] = $value;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->DataTask[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return DataTask Return the current element
     */
    public function current()
    {
      return current($this->DataTask);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->DataTask);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->DataTask);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->DataTask);
    }

    /**
     * Countable implementation
     *
     * @return DataTask Return count of elements
     */
    public function count()
    {
      return count($this->DataTask);
    }

}
