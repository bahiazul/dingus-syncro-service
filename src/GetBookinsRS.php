<?php

namespace Dingus\SyncroService;

class GetBookinsRS implements \JsonSerializable
{

    /**
     * @var ArrayOfBookingRS $Bookins
     */
    protected $Bookins = null;

    /**
     * @var string $ConfirmCode
     */
    protected $ConfirmCode = null;

    /**
     * @var ArrayOfString $ErrorList
     */
    protected $ErrorList = null;

    /**
     * @var boolean $Success
     */
    protected $Success = null;

    /**
     * @param boolean $Success
     */
    public function __construct($Success)
    {
      $this->Success = $Success;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'Bookins' => $this->getBookins(),
        'ConfirmCode' => $this->getConfirmCode(),
        'ErrorList' => $this->getErrorList(),
        'Success' => $this->getSuccess(),
      );
    }

    /**
     * @return ArrayOfBookingRS
     */
    public function getBookins()
    {
      return $this->Bookins;
    }

    /**
     * @param ArrayOfBookingRS $Bookins
     * @return \Dingus\SyncroService\GetBookinsRS
     */
    public function setBookins($Bookins)
    {
      $this->Bookins = $Bookins;
      return $this;
    }

    /**
     * @return string
     */
    public function getConfirmCode()
    {
      return $this->ConfirmCode;
    }

    /**
     * @param string $ConfirmCode
     * @return \Dingus\SyncroService\GetBookinsRS
     */
    public function setConfirmCode($ConfirmCode)
    {
      $this->ConfirmCode = $ConfirmCode;
      return $this;
    }

    /**
     * @return ArrayOfString
     */
    public function getErrorList()
    {
      return $this->ErrorList;
    }

    /**
     * @param ArrayOfString $ErrorList
     * @return \Dingus\SyncroService\GetBookinsRS
     */
    public function setErrorList($ErrorList)
    {
      $this->ErrorList = $ErrorList;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getSuccess()
    {
      return $this->Success;
    }

    /**
     * @param boolean $Success
     * @return \Dingus\SyncroService\GetBookinsRS
     */
    public function setSuccess($Success)
    {
      $this->Success = $Success;
      return $this;
    }

}
