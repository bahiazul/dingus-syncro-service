<?php

namespace Dingus\SyncroService;

class ArrayOfUserConfigurationRec implements \JsonSerializable, \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var UserConfigurationRec[] $UserConfigurationRec
     */
    protected $UserConfigurationRec = null;

    
    public function __construct()
    {
    
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'UserConfigurationRec' => $this->getUserConfigurationRec(),
      );
    }

    /**
     * @return UserConfigurationRec[]
     */
    public function getUserConfigurationRec()
    {
      return $this->UserConfigurationRec;
    }

    /**
     * @param UserConfigurationRec[] $UserConfigurationRec
     * @return \Dingus\SyncroService\ArrayOfUserConfigurationRec
     */
    public function setUserConfigurationRec(array $UserConfigurationRec = null)
    {
      $this->UserConfigurationRec = $UserConfigurationRec;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->UserConfigurationRec[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return UserConfigurationRec
     */
    public function offsetGet($offset)
    {
      return $this->UserConfigurationRec[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param UserConfigurationRec $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      $this->UserConfigurationRec[$offset] = $value;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->UserConfigurationRec[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return UserConfigurationRec Return the current element
     */
    public function current()
    {
      return current($this->UserConfigurationRec);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->UserConfigurationRec);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->UserConfigurationRec);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->UserConfigurationRec);
    }

    /**
     * Countable implementation
     *
     * @return UserConfigurationRec Return count of elements
     */
    public function count()
    {
      return count($this->UserConfigurationRec);
    }

}
