<?php

namespace Dingus\SyncroService;

class ArrayOfCustomerRateSetUpRS implements \JsonSerializable, \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var CustomerRateSetUpRS[] $CustomerRateSetUpRS
     */
    protected $CustomerRateSetUpRS = null;

    
    public function __construct()
    {
    
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'CustomerRateSetUpRS' => $this->getCustomerRateSetUpRS(),
      );
    }

    /**
     * @return CustomerRateSetUpRS[]
     */
    public function getCustomerRateSetUpRS()
    {
      return $this->CustomerRateSetUpRS;
    }

    /**
     * @param CustomerRateSetUpRS[] $CustomerRateSetUpRS
     * @return \Dingus\SyncroService\ArrayOfCustomerRateSetUpRS
     */
    public function setCustomerRateSetUpRS(array $CustomerRateSetUpRS = null)
    {
      $this->CustomerRateSetUpRS = $CustomerRateSetUpRS;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->CustomerRateSetUpRS[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return CustomerRateSetUpRS
     */
    public function offsetGet($offset)
    {
      return $this->CustomerRateSetUpRS[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param CustomerRateSetUpRS $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      $this->CustomerRateSetUpRS[$offset] = $value;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->CustomerRateSetUpRS[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return CustomerRateSetUpRS Return the current element
     */
    public function current()
    {
      return current($this->CustomerRateSetUpRS);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->CustomerRateSetUpRS);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->CustomerRateSetUpRS);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->CustomerRateSetUpRS);
    }

    /**
     * Countable implementation
     *
     * @return CustomerRateSetUpRS Return count of elements
     */
    public function count()
    {
      return count($this->CustomerRateSetUpRS);
    }

}
