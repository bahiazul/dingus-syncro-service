<?php

namespace Dingus\SyncroService;

class MarkBookingsAsNoReceived implements \JsonSerializable
{

    /**
     * @var Credentials $Credentials
     */
    protected $Credentials = null;

    /**
     * @var string $CustomerCode
     */
    protected $CustomerCode = null;

    /**
     * @var string $HotelCode
     */
    protected $HotelCode = null;

    /**
     * @var ArrayOfString $LocalizerCol
     */
    protected $LocalizerCol = null;

    /**
     * @var BookingFindType $bookingFindType
     */
    protected $bookingFindType = null;

    /**
     * @param Credentials $Credentials
     * @param string $CustomerCode
     * @param string $HotelCode
     * @param ArrayOfString $LocalizerCol
     * @param BookingFindType $bookingFindType
     */
    public function __construct($Credentials, $CustomerCode, $HotelCode, $LocalizerCol, $bookingFindType)
    {
      $this->Credentials = $Credentials;
      $this->CustomerCode = $CustomerCode;
      $this->HotelCode = $HotelCode;
      $this->LocalizerCol = $LocalizerCol;
      $this->bookingFindType = $bookingFindType;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'Credentials' => $this->getCredentials(),
        'CustomerCode' => $this->getCustomerCode(),
        'HotelCode' => $this->getHotelCode(),
        'LocalizerCol' => $this->getLocalizerCol(),
        'bookingFindType' => $this->getBookingFindType(),
      );
    }

    /**
     * @return Credentials
     */
    public function getCredentials()
    {
      return $this->Credentials;
    }

    /**
     * @param Credentials $Credentials
     * @return \Dingus\SyncroService\MarkBookingsAsNoReceived
     */
    public function setCredentials($Credentials)
    {
      $this->Credentials = $Credentials;
      return $this;
    }

    /**
     * @return string
     */
    public function getCustomerCode()
    {
      return $this->CustomerCode;
    }

    /**
     * @param string $CustomerCode
     * @return \Dingus\SyncroService\MarkBookingsAsNoReceived
     */
    public function setCustomerCode($CustomerCode)
    {
      $this->CustomerCode = $CustomerCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getHotelCode()
    {
      return $this->HotelCode;
    }

    /**
     * @param string $HotelCode
     * @return \Dingus\SyncroService\MarkBookingsAsNoReceived
     */
    public function setHotelCode($HotelCode)
    {
      $this->HotelCode = $HotelCode;
      return $this;
    }

    /**
     * @return ArrayOfString
     */
    public function getLocalizerCol()
    {
      return $this->LocalizerCol;
    }

    /**
     * @param ArrayOfString $LocalizerCol
     * @return \Dingus\SyncroService\MarkBookingsAsNoReceived
     */
    public function setLocalizerCol($LocalizerCol)
    {
      $this->LocalizerCol = $LocalizerCol;
      return $this;
    }

    /**
     * @return BookingFindType
     */
    public function getBookingFindType()
    {
      return $this->bookingFindType;
    }

    /**
     * @param BookingFindType $bookingFindType
     * @return \Dingus\SyncroService\MarkBookingsAsNoReceived
     */
    public function setBookingFindType($bookingFindType)
    {
      $this->bookingFindType = $bookingFindType;
      return $this;
    }

}
