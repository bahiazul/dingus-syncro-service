<?php

namespace Dingus\SyncroService;

class PutCancellationFee implements \JsonSerializable
{

    /**
     * @var Credentials $Credentials
     */
    protected $Credentials = null;

    /**
     * @var PutCancellationFeeRQ $putCancellationFeeRQ
     */
    protected $putCancellationFeeRQ = null;

    /**
     * @param Credentials $Credentials
     * @param PutCancellationFeeRQ $putCancellationFeeRQ
     */
    public function __construct($Credentials, $putCancellationFeeRQ)
    {
      $this->Credentials = $Credentials;
      $this->putCancellationFeeRQ = $putCancellationFeeRQ;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'Credentials' => $this->getCredentials(),
        'putCancellationFeeRQ' => $this->getPutCancellationFeeRQ(),
      );
    }

    /**
     * @return Credentials
     */
    public function getCredentials()
    {
      return $this->Credentials;
    }

    /**
     * @param Credentials $Credentials
     * @return \Dingus\SyncroService\PutCancellationFee
     */
    public function setCredentials($Credentials)
    {
      $this->Credentials = $Credentials;
      return $this;
    }

    /**
     * @return PutCancellationFeeRQ
     */
    public function getPutCancellationFeeRQ()
    {
      return $this->putCancellationFeeRQ;
    }

    /**
     * @param PutCancellationFeeRQ $putCancellationFeeRQ
     * @return \Dingus\SyncroService\PutCancellationFee
     */
    public function setPutCancellationFeeRQ($putCancellationFeeRQ)
    {
      $this->putCancellationFeeRQ = $putCancellationFeeRQ;
      return $this;
    }

}
