<?php

namespace Dingus\SyncroService;

class Stay implements \JsonSerializable
{

    /**
     * @var int $RoomNo
     */
    protected $RoomNo = null;

    /**
     * @var string $RoomCode
     */
    protected $RoomCode = null;

    /**
     * @var string $SalesRoomCode
     */
    protected $SalesRoomCode = null;

    /**
     * @var boolean $DiscountAllotment
     */
    protected $DiscountAllotment = null;

    /**
     * @var \DateTime $Date
     */
    protected $Date = null;

    /**
     * @param int $RoomNo
     * @param boolean $DiscountAllotment
     * @param \DateTime $Date
     */
    public function __construct($RoomNo, $DiscountAllotment, \DateTime $Date)
    {
      $this->RoomNo = $RoomNo;
      $this->DiscountAllotment = $DiscountAllotment;
      $this->Date = $Date->format(\DateTime::ATOM);
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'RoomNo' => $this->getRoomNo(),
        'RoomCode' => $this->getRoomCode(),
        'SalesRoomCode' => $this->getSalesRoomCode(),
        'DiscountAllotment' => $this->getDiscountAllotment(),
        'Date' => $this->getDate(),
      );
    }

    /**
     * @return int
     */
    public function getRoomNo()
    {
      return $this->RoomNo;
    }

    /**
     * @param int $RoomNo
     * @return \Dingus\SyncroService\Stay
     */
    public function setRoomNo($RoomNo)
    {
      $this->RoomNo = $RoomNo;
      return $this;
    }

    /**
     * @return string
     */
    public function getRoomCode()
    {
      return $this->RoomCode;
    }

    /**
     * @param string $RoomCode
     * @return \Dingus\SyncroService\Stay
     */
    public function setRoomCode($RoomCode)
    {
      $this->RoomCode = $RoomCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getSalesRoomCode()
    {
      return $this->SalesRoomCode;
    }

    /**
     * @param string $SalesRoomCode
     * @return \Dingus\SyncroService\Stay
     */
    public function setSalesRoomCode($SalesRoomCode)
    {
      $this->SalesRoomCode = $SalesRoomCode;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getDiscountAllotment()
    {
      return $this->DiscountAllotment;
    }

    /**
     * @param boolean $DiscountAllotment
     * @return \Dingus\SyncroService\Stay
     */
    public function setDiscountAllotment($DiscountAllotment)
    {
      $this->DiscountAllotment = $DiscountAllotment;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
      if ($this->Date == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->Date);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $Date
     * @return \Dingus\SyncroService\Stay
     */
    public function setDate(\DateTime $Date)
    {
      $this->Date = $Date->format(\DateTime::ATOM);
      return $this;
    }

}
