<?php

namespace Dingus\SyncroService;

class ArrayOfCancellationFeeRec implements \JsonSerializable, \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var CancellationFeeRec[] $CancellationFeeRec
     */
    protected $CancellationFeeRec = null;

    
    public function __construct()
    {
    
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'CancellationFeeRec' => $this->getCancellationFeeRec(),
      );
    }

    /**
     * @return CancellationFeeRec[]
     */
    public function getCancellationFeeRec()
    {
      return $this->CancellationFeeRec;
    }

    /**
     * @param CancellationFeeRec[] $CancellationFeeRec
     * @return \Dingus\SyncroService\ArrayOfCancellationFeeRec
     */
    public function setCancellationFeeRec(array $CancellationFeeRec = null)
    {
      $this->CancellationFeeRec = $CancellationFeeRec;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->CancellationFeeRec[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return CancellationFeeRec
     */
    public function offsetGet($offset)
    {
      return $this->CancellationFeeRec[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param CancellationFeeRec $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      $this->CancellationFeeRec[$offset] = $value;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->CancellationFeeRec[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return CancellationFeeRec Return the current element
     */
    public function current()
    {
      return current($this->CancellationFeeRec);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->CancellationFeeRec);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->CancellationFeeRec);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->CancellationFeeRec);
    }

    /**
     * Countable implementation
     *
     * @return CancellationFeeRec Return count of elements
     */
    public function count()
    {
      return count($this->CancellationFeeRec);
    }

}
