<?php

namespace Dingus\SyncroService;

class CancellationInfo implements \JsonSerializable
{

    /**
     * @var int $RoomNo
     */
    protected $RoomNo = null;

    /**
     * @var float $CancellationFee
     */
    protected $CancellationFee = null;

    /**
     * @var \DateTime $CancellationLimit
     */
    protected $CancellationLimit = null;

    /**
     * @var string $CancellationText
     */
    protected $CancellationText = null;

    /**
     * @var boolean $NoRefundable
     */
    protected $NoRefundable = null;

    /**
     * @var boolean $ModifyCancellationFee
     */
    protected $ModifyCancellationFee = null;

    /**
     * @var float $ImportModifyCancellationFee
     */
    protected $ImportModifyCancellationFee = null;

    /**
     * @param int $RoomNo
     * @param float $CancellationFee
     * @param \DateTime $CancellationLimit
     * @param boolean $NoRefundable
     * @param boolean $ModifyCancellationFee
     * @param float $ImportModifyCancellationFee
     */
    public function __construct($RoomNo, $CancellationFee, \DateTime $CancellationLimit, $NoRefundable, $ModifyCancellationFee, $ImportModifyCancellationFee)
    {
      $this->RoomNo = $RoomNo;
      $this->CancellationFee = $CancellationFee;
      $this->CancellationLimit = $CancellationLimit->format(\DateTime::ATOM);
      $this->NoRefundable = $NoRefundable;
      $this->ModifyCancellationFee = $ModifyCancellationFee;
      $this->ImportModifyCancellationFee = $ImportModifyCancellationFee;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'RoomNo' => $this->getRoomNo(),
        'CancellationFee' => $this->getCancellationFee(),
        'CancellationLimit' => $this->getCancellationLimit(),
        'CancellationText' => $this->getCancellationText(),
        'NoRefundable' => $this->getNoRefundable(),
        'ModifyCancellationFee' => $this->getModifyCancellationFee(),
        'ImportModifyCancellationFee' => $this->getImportModifyCancellationFee(),
      );
    }

    /**
     * @return int
     */
    public function getRoomNo()
    {
      return $this->RoomNo;
    }

    /**
     * @param int $RoomNo
     * @return \Dingus\SyncroService\CancellationInfo
     */
    public function setRoomNo($RoomNo)
    {
      $this->RoomNo = $RoomNo;
      return $this;
    }

    /**
     * @return float
     */
    public function getCancellationFee()
    {
      return $this->CancellationFee;
    }

    /**
     * @param float $CancellationFee
     * @return \Dingus\SyncroService\CancellationInfo
     */
    public function setCancellationFee($CancellationFee)
    {
      $this->CancellationFee = $CancellationFee;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCancellationLimit()
    {
      if ($this->CancellationLimit == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->CancellationLimit);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $CancellationLimit
     * @return \Dingus\SyncroService\CancellationInfo
     */
    public function setCancellationLimit(\DateTime $CancellationLimit)
    {
      $this->CancellationLimit = $CancellationLimit->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return string
     */
    public function getCancellationText()
    {
      return $this->CancellationText;
    }

    /**
     * @param string $CancellationText
     * @return \Dingus\SyncroService\CancellationInfo
     */
    public function setCancellationText($CancellationText)
    {
      $this->CancellationText = $CancellationText;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getNoRefundable()
    {
      return $this->NoRefundable;
    }

    /**
     * @param boolean $NoRefundable
     * @return \Dingus\SyncroService\CancellationInfo
     */
    public function setNoRefundable($NoRefundable)
    {
      $this->NoRefundable = $NoRefundable;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getModifyCancellationFee()
    {
      return $this->ModifyCancellationFee;
    }

    /**
     * @param boolean $ModifyCancellationFee
     * @return \Dingus\SyncroService\CancellationInfo
     */
    public function setModifyCancellationFee($ModifyCancellationFee)
    {
      $this->ModifyCancellationFee = $ModifyCancellationFee;
      return $this;
    }

    /**
     * @return float
     */
    public function getImportModifyCancellationFee()
    {
      return $this->ImportModifyCancellationFee;
    }

    /**
     * @param float $ImportModifyCancellationFee
     * @return \Dingus\SyncroService\CancellationInfo
     */
    public function setImportModifyCancellationFee($ImportModifyCancellationFee)
    {
      $this->ImportModifyCancellationFee = $ImportModifyCancellationFee;
      return $this;
    }

}
