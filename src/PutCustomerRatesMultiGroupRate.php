<?php

namespace Dingus\SyncroService;

class PutCustomerRatesMultiGroupRate implements \JsonSerializable
{

    /**
     * @var Credentials $Credentials
     */
    protected $Credentials = null;

    /**
     * @var PutCustomerDailyRateMultiGroupRateRQ $PutCustomerRateRQ
     */
    protected $PutCustomerRateRQ = null;

    /**
     * @param Credentials $Credentials
     * @param PutCustomerDailyRateMultiGroupRateRQ $PutCustomerRateRQ
     */
    public function __construct($Credentials, $PutCustomerRateRQ)
    {
      $this->Credentials = $Credentials;
      $this->PutCustomerRateRQ = $PutCustomerRateRQ;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'Credentials' => $this->getCredentials(),
        'PutCustomerRateRQ' => $this->getPutCustomerRateRQ(),
      );
    }

    /**
     * @return Credentials
     */
    public function getCredentials()
    {
      return $this->Credentials;
    }

    /**
     * @param Credentials $Credentials
     * @return \Dingus\SyncroService\PutCustomerRatesMultiGroupRate
     */
    public function setCredentials($Credentials)
    {
      $this->Credentials = $Credentials;
      return $this;
    }

    /**
     * @return PutCustomerDailyRateMultiGroupRateRQ
     */
    public function getPutCustomerRateRQ()
    {
      return $this->PutCustomerRateRQ;
    }

    /**
     * @param PutCustomerDailyRateMultiGroupRateRQ $PutCustomerRateRQ
     * @return \Dingus\SyncroService\PutCustomerRatesMultiGroupRate
     */
    public function setPutCustomerRateRQ($PutCustomerRateRQ)
    {
      $this->PutCustomerRateRQ = $PutCustomerRateRQ;
      return $this;
    }

}
