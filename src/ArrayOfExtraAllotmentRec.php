<?php

namespace Dingus\SyncroService;

class ArrayOfExtraAllotmentRec implements \JsonSerializable, \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var ExtraAllotmentRec[] $ExtraAllotmentRec
     */
    protected $ExtraAllotmentRec = null;

    
    public function __construct()
    {
    
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'ExtraAllotmentRec' => $this->getExtraAllotmentRec(),
      );
    }

    /**
     * @return ExtraAllotmentRec[]
     */
    public function getExtraAllotmentRec()
    {
      return $this->ExtraAllotmentRec;
    }

    /**
     * @param ExtraAllotmentRec[] $ExtraAllotmentRec
     * @return \Dingus\SyncroService\ArrayOfExtraAllotmentRec
     */
    public function setExtraAllotmentRec(array $ExtraAllotmentRec = null)
    {
      $this->ExtraAllotmentRec = $ExtraAllotmentRec;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->ExtraAllotmentRec[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return ExtraAllotmentRec
     */
    public function offsetGet($offset)
    {
      return $this->ExtraAllotmentRec[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param ExtraAllotmentRec $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      $this->ExtraAllotmentRec[$offset] = $value;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->ExtraAllotmentRec[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return ExtraAllotmentRec Return the current element
     */
    public function current()
    {
      return current($this->ExtraAllotmentRec);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->ExtraAllotmentRec);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->ExtraAllotmentRec);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->ExtraAllotmentRec);
    }

    /**
     * Countable implementation
     *
     * @return ExtraAllotmentRec Return count of elements
     */
    public function count()
    {
      return count($this->ExtraAllotmentRec);
    }

}
