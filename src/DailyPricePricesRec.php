<?php

namespace Dingus\SyncroService;

class DailyPricePricesRec implements \JsonSerializable
{

    /**
     * @var Action $Action
     */
    protected $Action = null;

    /**
     * @var int $PaxNo
     */
    protected $PaxNo = null;

    /**
     * @var int $Id
     */
    protected $Id = null;

    /**
     * @var PaxType $PaxType
     */
    protected $PaxType = null;

    /**
     * @var float $Price
     */
    protected $Price = null;

    /**
     * @var string $Tag
     */
    protected $Tag = null;

    /**
     * @var int $Range
     */
    protected $Range = null;

    /**
     * @param Action $Action
     * @param int $PaxNo
     * @param int $Id
     * @param PaxType $PaxType
     * @param float $Price
     * @param int $Range
     */
    public function __construct($Action, $PaxNo, $Id, $PaxType, $Price, $Range)
    {
      $this->Action = $Action;
      $this->PaxNo = $PaxNo;
      $this->Id = $Id;
      $this->PaxType = $PaxType;
      $this->Price = $Price;
      $this->Range = $Range;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'Action' => $this->getAction(),
        'PaxNo' => $this->getPaxNo(),
        'Id' => $this->getId(),
        'PaxType' => $this->getPaxType(),
        'Price' => $this->getPrice(),
        'Tag' => $this->getTag(),
        'Range' => $this->getRange(),
      );
    }

    /**
     * @return Action
     */
    public function getAction()
    {
      return $this->Action;
    }

    /**
     * @param Action $Action
     * @return \Dingus\SyncroService\DailyPricePricesRec
     */
    public function setAction($Action)
    {
      $this->Action = $Action;
      return $this;
    }

    /**
     * @return int
     */
    public function getPaxNo()
    {
      return $this->PaxNo;
    }

    /**
     * @param int $PaxNo
     * @return \Dingus\SyncroService\DailyPricePricesRec
     */
    public function setPaxNo($PaxNo)
    {
      $this->PaxNo = $PaxNo;
      return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
      return $this->Id;
    }

    /**
     * @param int $Id
     * @return \Dingus\SyncroService\DailyPricePricesRec
     */
    public function setId($Id)
    {
      $this->Id = $Id;
      return $this;
    }

    /**
     * @return PaxType
     */
    public function getPaxType()
    {
      return $this->PaxType;
    }

    /**
     * @param PaxType $PaxType
     * @return \Dingus\SyncroService\DailyPricePricesRec
     */
    public function setPaxType($PaxType)
    {
      $this->PaxType = $PaxType;
      return $this;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
      return $this->Price;
    }

    /**
     * @param float $Price
     * @return \Dingus\SyncroService\DailyPricePricesRec
     */
    public function setPrice($Price)
    {
      $this->Price = $Price;
      return $this;
    }

    /**
     * @return string
     */
    public function getTag()
    {
      return $this->Tag;
    }

    /**
     * @param string $Tag
     * @return \Dingus\SyncroService\DailyPricePricesRec
     */
    public function setTag($Tag)
    {
      $this->Tag = $Tag;
      return $this;
    }

    /**
     * @return int
     */
    public function getRange()
    {
      return $this->Range;
    }

    /**
     * @param int $Range
     * @return \Dingus\SyncroService\DailyPricePricesRec
     */
    public function setRange($Range)
    {
      $this->Range = $Range;
      return $this;
    }

}
