<?php

namespace Dingus\SyncroService;

class HotelsTask implements \JsonSerializable
{

    /**
     * @var Action $Action
     */
    protected $Action = null;

    /**
     * @var int $HotelTaskID
     */
    protected $HotelTaskID = null;

    /**
     * @var string $HotelCode
     */
    protected $HotelCode = null;

    /**
     * @var string $HotelName
     */
    protected $HotelName = null;

    /**
     * @var string $CustomerHotelCode
     */
    protected $CustomerHotelCode = null;

    /**
     * @param Action $Action
     * @param int $HotelTaskID
     */
    public function __construct($Action, $HotelTaskID)
    {
      $this->Action = $Action;
      $this->HotelTaskID = $HotelTaskID;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'Action' => $this->getAction(),
        'HotelTaskID' => $this->getHotelTaskID(),
        'HotelCode' => $this->getHotelCode(),
        'HotelName' => $this->getHotelName(),
        'CustomerHotelCode' => $this->getCustomerHotelCode(),
      );
    }

    /**
     * @return Action
     */
    public function getAction()
    {
      return $this->Action;
    }

    /**
     * @param Action $Action
     * @return \Dingus\SyncroService\HotelsTask
     */
    public function setAction($Action)
    {
      $this->Action = $Action;
      return $this;
    }

    /**
     * @return int
     */
    public function getHotelTaskID()
    {
      return $this->HotelTaskID;
    }

    /**
     * @param int $HotelTaskID
     * @return \Dingus\SyncroService\HotelsTask
     */
    public function setHotelTaskID($HotelTaskID)
    {
      $this->HotelTaskID = $HotelTaskID;
      return $this;
    }

    /**
     * @return string
     */
    public function getHotelCode()
    {
      return $this->HotelCode;
    }

    /**
     * @param string $HotelCode
     * @return \Dingus\SyncroService\HotelsTask
     */
    public function setHotelCode($HotelCode)
    {
      $this->HotelCode = $HotelCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getHotelName()
    {
      return $this->HotelName;
    }

    /**
     * @param string $HotelName
     * @return \Dingus\SyncroService\HotelsTask
     */
    public function setHotelName($HotelName)
    {
      $this->HotelName = $HotelName;
      return $this;
    }

    /**
     * @return string
     */
    public function getCustomerHotelCode()
    {
      return $this->CustomerHotelCode;
    }

    /**
     * @param string $CustomerHotelCode
     * @return \Dingus\SyncroService\HotelsTask
     */
    public function setCustomerHotelCode($CustomerHotelCode)
    {
      $this->CustomerHotelCode = $CustomerHotelCode;
      return $this;
    }

}
