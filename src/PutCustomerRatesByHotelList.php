<?php

namespace Dingus\SyncroService;

class PutCustomerRatesByHotelList implements \JsonSerializable
{

    /**
     * @var Credentials $Credentials
     */
    protected $Credentials = null;

    /**
     * @var PutCustomerDailyRatesByHotelListRQ $PutCustomerRateByHotelListRQ
     */
    protected $PutCustomerRateByHotelListRQ = null;

    /**
     * @param Credentials $Credentials
     * @param PutCustomerDailyRatesByHotelListRQ $PutCustomerRateByHotelListRQ
     */
    public function __construct($Credentials, $PutCustomerRateByHotelListRQ)
    {
      $this->Credentials = $Credentials;
      $this->PutCustomerRateByHotelListRQ = $PutCustomerRateByHotelListRQ;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'Credentials' => $this->getCredentials(),
        'PutCustomerRateByHotelListRQ' => $this->getPutCustomerRateByHotelListRQ(),
      );
    }

    /**
     * @return Credentials
     */
    public function getCredentials()
    {
      return $this->Credentials;
    }

    /**
     * @param Credentials $Credentials
     * @return \Dingus\SyncroService\PutCustomerRatesByHotelList
     */
    public function setCredentials($Credentials)
    {
      $this->Credentials = $Credentials;
      return $this;
    }

    /**
     * @return PutCustomerDailyRatesByHotelListRQ
     */
    public function getPutCustomerRateByHotelListRQ()
    {
      return $this->PutCustomerRateByHotelListRQ;
    }

    /**
     * @param PutCustomerDailyRatesByHotelListRQ $PutCustomerRateByHotelListRQ
     * @return \Dingus\SyncroService\PutCustomerRatesByHotelList
     */
    public function setPutCustomerRateByHotelListRQ($PutCustomerRateByHotelListRQ)
    {
      $this->PutCustomerRateByHotelListRQ = $PutCustomerRateByHotelListRQ;
      return $this;
    }

}
