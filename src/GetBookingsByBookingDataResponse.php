<?php

namespace Dingus\SyncroService;

class GetBookingsByBookingDataResponse implements \JsonSerializable
{

    /**
     * @var ArrayOfBookingRS $GetBookingsByBookingDataResult
     */
    protected $GetBookingsByBookingDataResult = null;

    /**
     * @param ArrayOfBookingRS $GetBookingsByBookingDataResult
     */
    public function __construct($GetBookingsByBookingDataResult)
    {
      $this->GetBookingsByBookingDataResult = $GetBookingsByBookingDataResult;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'GetBookingsByBookingDataResult' => $this->getGetBookingsByBookingDataResult(),
      );
    }

    /**
     * @return ArrayOfBookingRS
     */
    public function getGetBookingsByBookingDataResult()
    {
      return $this->GetBookingsByBookingDataResult;
    }

    /**
     * @param ArrayOfBookingRS $GetBookingsByBookingDataResult
     * @return \Dingus\SyncroService\GetBookingsByBookingDataResponse
     */
    public function setGetBookingsByBookingDataResult($GetBookingsByBookingDataResult)
    {
      $this->GetBookingsByBookingDataResult = $GetBookingsByBookingDataResult;
      return $this;
    }

}
