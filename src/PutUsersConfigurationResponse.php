<?php

namespace Dingus\SyncroService;

class PutUsersConfigurationResponse implements \JsonSerializable
{

    /**
     * @var SyncroRS $PutUsersConfigurationResult
     */
    protected $PutUsersConfigurationResult = null;

    /**
     * @param SyncroRS $PutUsersConfigurationResult
     */
    public function __construct($PutUsersConfigurationResult)
    {
      $this->PutUsersConfigurationResult = $PutUsersConfigurationResult;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'PutUsersConfigurationResult' => $this->getPutUsersConfigurationResult(),
      );
    }

    /**
     * @return SyncroRS
     */
    public function getPutUsersConfigurationResult()
    {
      return $this->PutUsersConfigurationResult;
    }

    /**
     * @param SyncroRS $PutUsersConfigurationResult
     * @return \Dingus\SyncroService\PutUsersConfigurationResponse
     */
    public function setPutUsersConfigurationResult($PutUsersConfigurationResult)
    {
      $this->PutUsersConfigurationResult = $PutUsersConfigurationResult;
      return $this;
    }

}
