<?php

namespace Dingus\SyncroService;

class ArrayOfCustomerPromotionRec implements \JsonSerializable, \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var CustomerPromotionRec[] $CustomerPromotionRec
     */
    protected $CustomerPromotionRec = null;

    
    public function __construct()
    {
    
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'CustomerPromotionRec' => $this->getCustomerPromotionRec(),
      );
    }

    /**
     * @return CustomerPromotionRec[]
     */
    public function getCustomerPromotionRec()
    {
      return $this->CustomerPromotionRec;
    }

    /**
     * @param CustomerPromotionRec[] $CustomerPromotionRec
     * @return \Dingus\SyncroService\ArrayOfCustomerPromotionRec
     */
    public function setCustomerPromotionRec(array $CustomerPromotionRec = null)
    {
      $this->CustomerPromotionRec = $CustomerPromotionRec;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->CustomerPromotionRec[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return CustomerPromotionRec
     */
    public function offsetGet($offset)
    {
      return $this->CustomerPromotionRec[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param CustomerPromotionRec $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      $this->CustomerPromotionRec[$offset] = $value;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->CustomerPromotionRec[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return CustomerPromotionRec Return the current element
     */
    public function current()
    {
      return current($this->CustomerPromotionRec);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->CustomerPromotionRec);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->CustomerPromotionRec);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->CustomerPromotionRec);
    }

    /**
     * Countable implementation
     *
     * @return CustomerPromotionRec Return count of elements
     */
    public function count()
    {
      return count($this->CustomerPromotionRec);
    }

}
