<?php

namespace Dingus\SyncroService;

class PutHotelGroupsByHotelList implements \JsonSerializable
{

    /**
     * @var Credentials $Credentials
     */
    protected $Credentials = null;

    /**
     * @var PutHotelGroupsByHotelListRQ $RQ
     */
    protected $RQ = null;

    /**
     * @param Credentials $Credentials
     * @param PutHotelGroupsByHotelListRQ $RQ
     */
    public function __construct($Credentials, $RQ)
    {
      $this->Credentials = $Credentials;
      $this->RQ = $RQ;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'Credentials' => $this->getCredentials(),
        'RQ' => $this->getRQ(),
      );
    }

    /**
     * @return Credentials
     */
    public function getCredentials()
    {
      return $this->Credentials;
    }

    /**
     * @param Credentials $Credentials
     * @return \Dingus\SyncroService\PutHotelGroupsByHotelList
     */
    public function setCredentials($Credentials)
    {
      $this->Credentials = $Credentials;
      return $this;
    }

    /**
     * @return PutHotelGroupsByHotelListRQ
     */
    public function getRQ()
    {
      return $this->RQ;
    }

    /**
     * @param PutHotelGroupsByHotelListRQ $RQ
     * @return \Dingus\SyncroService\PutHotelGroupsByHotelList
     */
    public function setRQ($RQ)
    {
      $this->RQ = $RQ;
      return $this;
    }

}
