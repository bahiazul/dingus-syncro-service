<?php

namespace Dingus\SyncroService;

class PutTasks implements \JsonSerializable
{

    /**
     * @var Credentials $Credentials
     */
    protected $Credentials = null;

    /**
     * @var ArrayOfTaskRS $ltaskRS
     */
    protected $ltaskRS = null;

    /**
     * @param Credentials $Credentials
     * @param ArrayOfTaskRS $ltaskRS
     */
    public function __construct($Credentials, $ltaskRS)
    {
      $this->Credentials = $Credentials;
      $this->ltaskRS = $ltaskRS;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'Credentials' => $this->getCredentials(),
        'ltaskRS' => $this->getLtaskRS(),
      );
    }

    /**
     * @return Credentials
     */
    public function getCredentials()
    {
      return $this->Credentials;
    }

    /**
     * @param Credentials $Credentials
     * @return \Dingus\SyncroService\PutTasks
     */
    public function setCredentials($Credentials)
    {
      $this->Credentials = $Credentials;
      return $this;
    }

    /**
     * @return ArrayOfTaskRS
     */
    public function getLtaskRS()
    {
      return $this->ltaskRS;
    }

    /**
     * @param ArrayOfTaskRS $ltaskRS
     * @return \Dingus\SyncroService\PutTasks
     */
    public function setLtaskRS($ltaskRS)
    {
      $this->ltaskRS = $ltaskRS;
      return $this;
    }

}
