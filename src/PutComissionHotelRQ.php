<?php

namespace Dingus\SyncroService;

class PutComissionHotelRQ implements \JsonSerializable
{

    /**
     * @var ArrayOfComissionHotelRec $ComissionHotelRQ
     */
    protected $ComissionHotelRQ = null;

    
    public function __construct()
    {
    
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'ComissionHotelRQ' => $this->getComissionHotelRQ(),
      );
    }

    /**
     * @return ArrayOfComissionHotelRec
     */
    public function getComissionHotelRQ()
    {
      return $this->ComissionHotelRQ;
    }

    /**
     * @param ArrayOfComissionHotelRec $ComissionHotelRQ
     * @return \Dingus\SyncroService\PutComissionHotelRQ
     */
    public function setComissionHotelRQ($ComissionHotelRQ)
    {
      $this->ComissionHotelRQ = $ComissionHotelRQ;
      return $this;
    }

}
