<?php

namespace Dingus\SyncroService;

class UserConfigurationRec implements \JsonSerializable
{

    /**
     * @var Action $Action
     */
    protected $Action = null;

    /**
     * @var string $FicheroCSSWebBooking
     */
    protected $FicheroCSSWebBooking = null;

    /**
     * @var string $ImagenCabeceraBono
     */
    protected $ImagenCabeceraBono = null;

    /**
     * @var string $ImagenPieBono
     */
    protected $ImagenPieBono = null;

    /**
     * @var ModoWebBooking $ModoWebBooking
     */
    protected $ModoWebBooking = null;

    /**
     * @var string $PlantillaBonoCancel
     */
    protected $PlantillaBonoCancel = null;

    /**
     * @var string $PlantillaBonoNew
     */
    protected $PlantillaBonoNew = null;

    /**
     * @var string $PlantillaBonoHtmlCancel
     */
    protected $PlantillaBonoHtmlCancel = null;

    /**
     * @var string $PlantillaBonoHtmlNew
     */
    protected $PlantillaBonoHtmlNew = null;

    /**
     * @var string $PlantillaHtmlProForma
     */
    protected $PlantillaHtmlProForma = null;

    /**
     * @var int $TipoBuscador
     */
    protected $TipoBuscador = null;

    /**
     * @var string $TipoCriterio1
     */
    protected $TipoCriterio1 = null;

    /**
     * @var string $TipoCriterio2
     */
    protected $TipoCriterio2 = null;

    /**
     * @var string $UrlRetornoWebBooking
     */
    protected $UrlRetornoWebBooking = null;

    /**
     * @var int $UserId
     */
    protected $UserId = null;

    /**
     * @var string $UserName
     */
    protected $UserName = null;

    /**
     * @param Action $Action
     * @param ModoWebBooking $ModoWebBooking
     * @param int $TipoBuscador
     * @param int $UserId
     */
    public function __construct($Action, $ModoWebBooking, $TipoBuscador, $UserId)
    {
      $this->Action = $Action;
      $this->ModoWebBooking = $ModoWebBooking;
      $this->TipoBuscador = $TipoBuscador;
      $this->UserId = $UserId;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'Action' => $this->getAction(),
        'FicheroCSSWebBooking' => $this->getFicheroCSSWebBooking(),
        'ImagenCabeceraBono' => $this->getImagenCabeceraBono(),
        'ImagenPieBono' => $this->getImagenPieBono(),
        'ModoWebBooking' => $this->getModoWebBooking(),
        'PlantillaBonoCancel' => $this->getPlantillaBonoCancel(),
        'PlantillaBonoNew' => $this->getPlantillaBonoNew(),
        'PlantillaBonoHtmlCancel' => $this->getPlantillaBonoHtmlCancel(),
        'PlantillaBonoHtmlNew' => $this->getPlantillaBonoHtmlNew(),
        'PlantillaHtmlProForma' => $this->getPlantillaHtmlProForma(),
        'TipoBuscador' => $this->getTipoBuscador(),
        'TipoCriterio1' => $this->getTipoCriterio1(),
        'TipoCriterio2' => $this->getTipoCriterio2(),
        'UrlRetornoWebBooking' => $this->getUrlRetornoWebBooking(),
        'UserId' => $this->getUserId(),
        'UserName' => $this->getUserName(),
      );
    }

    /**
     * @return Action
     */
    public function getAction()
    {
      return $this->Action;
    }

    /**
     * @param Action $Action
     * @return \Dingus\SyncroService\UserConfigurationRec
     */
    public function setAction($Action)
    {
      $this->Action = $Action;
      return $this;
    }

    /**
     * @return string
     */
    public function getFicheroCSSWebBooking()
    {
      return $this->FicheroCSSWebBooking;
    }

    /**
     * @param string $FicheroCSSWebBooking
     * @return \Dingus\SyncroService\UserConfigurationRec
     */
    public function setFicheroCSSWebBooking($FicheroCSSWebBooking)
    {
      $this->FicheroCSSWebBooking = $FicheroCSSWebBooking;
      return $this;
    }

    /**
     * @return string
     */
    public function getImagenCabeceraBono()
    {
      return $this->ImagenCabeceraBono;
    }

    /**
     * @param string $ImagenCabeceraBono
     * @return \Dingus\SyncroService\UserConfigurationRec
     */
    public function setImagenCabeceraBono($ImagenCabeceraBono)
    {
      $this->ImagenCabeceraBono = $ImagenCabeceraBono;
      return $this;
    }

    /**
     * @return string
     */
    public function getImagenPieBono()
    {
      return $this->ImagenPieBono;
    }

    /**
     * @param string $ImagenPieBono
     * @return \Dingus\SyncroService\UserConfigurationRec
     */
    public function setImagenPieBono($ImagenPieBono)
    {
      $this->ImagenPieBono = $ImagenPieBono;
      return $this;
    }

    /**
     * @return ModoWebBooking
     */
    public function getModoWebBooking()
    {
      return $this->ModoWebBooking;
    }

    /**
     * @param ModoWebBooking $ModoWebBooking
     * @return \Dingus\SyncroService\UserConfigurationRec
     */
    public function setModoWebBooking($ModoWebBooking)
    {
      $this->ModoWebBooking = $ModoWebBooking;
      return $this;
    }

    /**
     * @return string
     */
    public function getPlantillaBonoCancel()
    {
      return $this->PlantillaBonoCancel;
    }

    /**
     * @param string $PlantillaBonoCancel
     * @return \Dingus\SyncroService\UserConfigurationRec
     */
    public function setPlantillaBonoCancel($PlantillaBonoCancel)
    {
      $this->PlantillaBonoCancel = $PlantillaBonoCancel;
      return $this;
    }

    /**
     * @return string
     */
    public function getPlantillaBonoNew()
    {
      return $this->PlantillaBonoNew;
    }

    /**
     * @param string $PlantillaBonoNew
     * @return \Dingus\SyncroService\UserConfigurationRec
     */
    public function setPlantillaBonoNew($PlantillaBonoNew)
    {
      $this->PlantillaBonoNew = $PlantillaBonoNew;
      return $this;
    }

    /**
     * @return string
     */
    public function getPlantillaBonoHtmlCancel()
    {
      return $this->PlantillaBonoHtmlCancel;
    }

    /**
     * @param string $PlantillaBonoHtmlCancel
     * @return \Dingus\SyncroService\UserConfigurationRec
     */
    public function setPlantillaBonoHtmlCancel($PlantillaBonoHtmlCancel)
    {
      $this->PlantillaBonoHtmlCancel = $PlantillaBonoHtmlCancel;
      return $this;
    }

    /**
     * @return string
     */
    public function getPlantillaBonoHtmlNew()
    {
      return $this->PlantillaBonoHtmlNew;
    }

    /**
     * @param string $PlantillaBonoHtmlNew
     * @return \Dingus\SyncroService\UserConfigurationRec
     */
    public function setPlantillaBonoHtmlNew($PlantillaBonoHtmlNew)
    {
      $this->PlantillaBonoHtmlNew = $PlantillaBonoHtmlNew;
      return $this;
    }

    /**
     * @return string
     */
    public function getPlantillaHtmlProForma()
    {
      return $this->PlantillaHtmlProForma;
    }

    /**
     * @param string $PlantillaHtmlProForma
     * @return \Dingus\SyncroService\UserConfigurationRec
     */
    public function setPlantillaHtmlProForma($PlantillaHtmlProForma)
    {
      $this->PlantillaHtmlProForma = $PlantillaHtmlProForma;
      return $this;
    }

    /**
     * @return int
     */
    public function getTipoBuscador()
    {
      return $this->TipoBuscador;
    }

    /**
     * @param int $TipoBuscador
     * @return \Dingus\SyncroService\UserConfigurationRec
     */
    public function setTipoBuscador($TipoBuscador)
    {
      $this->TipoBuscador = $TipoBuscador;
      return $this;
    }

    /**
     * @return string
     */
    public function getTipoCriterio1()
    {
      return $this->TipoCriterio1;
    }

    /**
     * @param string $TipoCriterio1
     * @return \Dingus\SyncroService\UserConfigurationRec
     */
    public function setTipoCriterio1($TipoCriterio1)
    {
      $this->TipoCriterio1 = $TipoCriterio1;
      return $this;
    }

    /**
     * @return string
     */
    public function getTipoCriterio2()
    {
      return $this->TipoCriterio2;
    }

    /**
     * @param string $TipoCriterio2
     * @return \Dingus\SyncroService\UserConfigurationRec
     */
    public function setTipoCriterio2($TipoCriterio2)
    {
      $this->TipoCriterio2 = $TipoCriterio2;
      return $this;
    }

    /**
     * @return string
     */
    public function getUrlRetornoWebBooking()
    {
      return $this->UrlRetornoWebBooking;
    }

    /**
     * @param string $UrlRetornoWebBooking
     * @return \Dingus\SyncroService\UserConfigurationRec
     */
    public function setUrlRetornoWebBooking($UrlRetornoWebBooking)
    {
      $this->UrlRetornoWebBooking = $UrlRetornoWebBooking;
      return $this;
    }

    /**
     * @return int
     */
    public function getUserId()
    {
      return $this->UserId;
    }

    /**
     * @param int $UserId
     * @return \Dingus\SyncroService\UserConfigurationRec
     */
    public function setUserId($UserId)
    {
      $this->UserId = $UserId;
      return $this;
    }

    /**
     * @return string
     */
    public function getUserName()
    {
      return $this->UserName;
    }

    /**
     * @param string $UserName
     * @return \Dingus\SyncroService\UserConfigurationRec
     */
    public function setUserName($UserName)
    {
      $this->UserName = $UserName;
      return $this;
    }

}
