<?php

namespace Dingus\SyncroService;

class GetBookingsByHotel2Response implements \JsonSerializable
{

    /**
     * @var GetBookinsRS $GetBookingsByHotel2Result
     */
    protected $GetBookingsByHotel2Result = null;

    /**
     * @param GetBookinsRS $GetBookingsByHotel2Result
     */
    public function __construct($GetBookingsByHotel2Result)
    {
      $this->GetBookingsByHotel2Result = $GetBookingsByHotel2Result;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'GetBookingsByHotel2Result' => $this->getGetBookingsByHotel2Result(),
      );
    }

    /**
     * @return GetBookinsRS
     */
    public function getGetBookingsByHotel2Result()
    {
      return $this->GetBookingsByHotel2Result;
    }

    /**
     * @param GetBookinsRS $GetBookingsByHotel2Result
     * @return \Dingus\SyncroService\GetBookingsByHotel2Response
     */
    public function setGetBookingsByHotel2Result($GetBookingsByHotel2Result)
    {
      $this->GetBookingsByHotel2Result = $GetBookingsByHotel2Result;
      return $this;
    }

}
