<?php

namespace Dingus\SyncroService;

class ArrayOfLineCancellationFeeRS implements \JsonSerializable, \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var LineCancellationFeeRS[] $LineCancellationFeeRS
     */
    protected $LineCancellationFeeRS = null;

    
    public function __construct()
    {
    
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'LineCancellationFeeRS' => $this->getLineCancellationFeeRS(),
      );
    }

    /**
     * @return LineCancellationFeeRS[]
     */
    public function getLineCancellationFeeRS()
    {
      return $this->LineCancellationFeeRS;
    }

    /**
     * @param LineCancellationFeeRS[] $LineCancellationFeeRS
     * @return \Dingus\SyncroService\ArrayOfLineCancellationFeeRS
     */
    public function setLineCancellationFeeRS(array $LineCancellationFeeRS = null)
    {
      $this->LineCancellationFeeRS = $LineCancellationFeeRS;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->LineCancellationFeeRS[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return LineCancellationFeeRS
     */
    public function offsetGet($offset)
    {
      return $this->LineCancellationFeeRS[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param LineCancellationFeeRS $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      $this->LineCancellationFeeRS[$offset] = $value;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->LineCancellationFeeRS[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return LineCancellationFeeRS Return the current element
     */
    public function current()
    {
      return current($this->LineCancellationFeeRS);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->LineCancellationFeeRS);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->LineCancellationFeeRS);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->LineCancellationFeeRS);
    }

    /**
     * Countable implementation
     *
     * @return LineCancellationFeeRS Return count of elements
     */
    public function count()
    {
      return count($this->LineCancellationFeeRS);
    }

}
