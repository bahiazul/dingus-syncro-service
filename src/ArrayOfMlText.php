<?php

namespace Dingus\SyncroService;

class ArrayOfMlText implements \JsonSerializable, \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var MlText[] $MlText
     */
    protected $MlText = null;

    
    public function __construct()
    {
    
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'MlText' => $this->getMlText(),
      );
    }

    /**
     * @return MlText[]
     */
    public function getMlText()
    {
      return $this->MlText;
    }

    /**
     * @param MlText[] $MlText
     * @return \Dingus\SyncroService\ArrayOfMlText
     */
    public function setMlText(array $MlText = null)
    {
      $this->MlText = $MlText;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->MlText[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return MlText
     */
    public function offsetGet($offset)
    {
      return $this->MlText[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param MlText $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      $this->MlText[$offset] = $value;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->MlText[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return MlText Return the current element
     */
    public function current()
    {
      return current($this->MlText);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->MlText);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->MlText);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->MlText);
    }

    /**
     * Countable implementation
     *
     * @return MlText Return count of elements
     */
    public function count()
    {
      return count($this->MlText);
    }

}
