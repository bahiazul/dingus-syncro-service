<?php

namespace Dingus\SyncroService;

class ArrayOfExtraHotel implements \JsonSerializable, \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var ExtraHotel[] $ExtraHotel
     */
    protected $ExtraHotel = null;

    
    public function __construct()
    {
    
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'ExtraHotel' => $this->getExtraHotel(),
      );
    }

    /**
     * @return ExtraHotel[]
     */
    public function getExtraHotel()
    {
      return $this->ExtraHotel;
    }

    /**
     * @param ExtraHotel[] $ExtraHotel
     * @return \Dingus\SyncroService\ArrayOfExtraHotel
     */
    public function setExtraHotel(array $ExtraHotel = null)
    {
      $this->ExtraHotel = $ExtraHotel;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->ExtraHotel[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return ExtraHotel
     */
    public function offsetGet($offset)
    {
      return $this->ExtraHotel[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param ExtraHotel $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      $this->ExtraHotel[$offset] = $value;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->ExtraHotel[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return ExtraHotel Return the current element
     */
    public function current()
    {
      return current($this->ExtraHotel);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->ExtraHotel);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->ExtraHotel);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->ExtraHotel);
    }

    /**
     * Countable implementation
     *
     * @return ExtraHotel Return count of elements
     */
    public function count()
    {
      return count($this->ExtraHotel);
    }

}
