<?php

namespace Dingus\SyncroService;

class PutCustomerRateSetUpList implements \JsonSerializable
{

    /**
     * @var Credentials $Credentials
     */
    protected $Credentials = null;

    /**
     * @var PutCustomerRateSetUpRQ $PutCustomerRateSetUpRQ
     */
    protected $PutCustomerRateSetUpRQ = null;

    /**
     * @param Credentials $Credentials
     * @param PutCustomerRateSetUpRQ $PutCustomerRateSetUpRQ
     */
    public function __construct($Credentials, $PutCustomerRateSetUpRQ)
    {
      $this->Credentials = $Credentials;
      $this->PutCustomerRateSetUpRQ = $PutCustomerRateSetUpRQ;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'Credentials' => $this->getCredentials(),
        'PutCustomerRateSetUpRQ' => $this->getPutCustomerRateSetUpRQ(),
      );
    }

    /**
     * @return Credentials
     */
    public function getCredentials()
    {
      return $this->Credentials;
    }

    /**
     * @param Credentials $Credentials
     * @return \Dingus\SyncroService\PutCustomerRateSetUpList
     */
    public function setCredentials($Credentials)
    {
      $this->Credentials = $Credentials;
      return $this;
    }

    /**
     * @return PutCustomerRateSetUpRQ
     */
    public function getPutCustomerRateSetUpRQ()
    {
      return $this->PutCustomerRateSetUpRQ;
    }

    /**
     * @param PutCustomerRateSetUpRQ $PutCustomerRateSetUpRQ
     * @return \Dingus\SyncroService\PutCustomerRateSetUpList
     */
    public function setPutCustomerRateSetUpRQ($PutCustomerRateSetUpRQ)
    {
      $this->PutCustomerRateSetUpRQ = $PutCustomerRateSetUpRQ;
      return $this;
    }

}
