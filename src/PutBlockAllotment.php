<?php

namespace Dingus\SyncroService;

class PutBlockAllotment implements \JsonSerializable
{

    /**
     * @var Credentials $Credentials
     */
    protected $Credentials = null;

    /**
     * @var string $CustomerCode
     */
    protected $CustomerCode = null;

    /**
     * @var string $HotelCode
     */
    protected $HotelCode = null;

    /**
     * @var ArrayOfCriterion $Criterios
     */
    protected $Criterios = null;

    /**
     * @var ArrayOfBlockAllotmentRQ $rq
     */
    protected $rq = null;

    /**
     * @param Credentials $Credentials
     * @param string $CustomerCode
     * @param string $HotelCode
     * @param ArrayOfCriterion $Criterios
     * @param ArrayOfBlockAllotmentRQ $rq
     */
    public function __construct($Credentials, $CustomerCode, $HotelCode, $Criterios, $rq)
    {
      $this->Credentials = $Credentials;
      $this->CustomerCode = $CustomerCode;
      $this->HotelCode = $HotelCode;
      $this->Criterios = $Criterios;
      $this->rq = $rq;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'Credentials' => $this->getCredentials(),
        'CustomerCode' => $this->getCustomerCode(),
        'HotelCode' => $this->getHotelCode(),
        'Criterios' => $this->getCriterios(),
        'rq' => $this->getRq(),
      );
    }

    /**
     * @return Credentials
     */
    public function getCredentials()
    {
      return $this->Credentials;
    }

    /**
     * @param Credentials $Credentials
     * @return \Dingus\SyncroService\PutBlockAllotment
     */
    public function setCredentials($Credentials)
    {
      $this->Credentials = $Credentials;
      return $this;
    }

    /**
     * @return string
     */
    public function getCustomerCode()
    {
      return $this->CustomerCode;
    }

    /**
     * @param string $CustomerCode
     * @return \Dingus\SyncroService\PutBlockAllotment
     */
    public function setCustomerCode($CustomerCode)
    {
      $this->CustomerCode = $CustomerCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getHotelCode()
    {
      return $this->HotelCode;
    }

    /**
     * @param string $HotelCode
     * @return \Dingus\SyncroService\PutBlockAllotment
     */
    public function setHotelCode($HotelCode)
    {
      $this->HotelCode = $HotelCode;
      return $this;
    }

    /**
     * @return ArrayOfCriterion
     */
    public function getCriterios()
    {
      return $this->Criterios;
    }

    /**
     * @param ArrayOfCriterion $Criterios
     * @return \Dingus\SyncroService\PutBlockAllotment
     */
    public function setCriterios($Criterios)
    {
      $this->Criterios = $Criterios;
      return $this;
    }

    /**
     * @return ArrayOfBlockAllotmentRQ
     */
    public function getRq()
    {
      return $this->rq;
    }

    /**
     * @param ArrayOfBlockAllotmentRQ $rq
     * @return \Dingus\SyncroService\PutBlockAllotment
     */
    public function setRq($rq)
    {
      $this->rq = $rq;
      return $this;
    }

}
