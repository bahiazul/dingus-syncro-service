<?php

namespace Dingus\SyncroService;

class ArrayOfDailyPriceRec implements \JsonSerializable, \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var DailyPriceRec[] $DailyPriceRec
     */
    protected $DailyPriceRec = null;

    
    public function __construct()
    {
    
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'DailyPriceRec' => $this->getDailyPriceRec(),
      );
    }

    /**
     * @return DailyPriceRec[]
     */
    public function getDailyPriceRec()
    {
      return $this->DailyPriceRec;
    }

    /**
     * @param DailyPriceRec[] $DailyPriceRec
     * @return \Dingus\SyncroService\ArrayOfDailyPriceRec
     */
    public function setDailyPriceRec(array $DailyPriceRec = null)
    {
      $this->DailyPriceRec = $DailyPriceRec;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->DailyPriceRec[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return DailyPriceRec
     */
    public function offsetGet($offset)
    {
      return $this->DailyPriceRec[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param DailyPriceRec $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      $this->DailyPriceRec[$offset] = $value;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->DailyPriceRec[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return DailyPriceRec Return the current element
     */
    public function current()
    {
      return current($this->DailyPriceRec);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->DailyPriceRec);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->DailyPriceRec);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->DailyPriceRec);
    }

    /**
     * Countable implementation
     *
     * @return DailyPriceRec Return count of elements
     */
    public function count()
    {
      return count($this->DailyPriceRec);
    }

}
