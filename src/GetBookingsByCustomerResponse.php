<?php

namespace Dingus\SyncroService;

class GetBookingsByCustomerResponse implements \JsonSerializable
{

    /**
     * @var GetBookinsRS $GetBookingsByCustomerResult
     */
    protected $GetBookingsByCustomerResult = null;

    /**
     * @param GetBookinsRS $GetBookingsByCustomerResult
     */
    public function __construct($GetBookingsByCustomerResult)
    {
      $this->GetBookingsByCustomerResult = $GetBookingsByCustomerResult;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'GetBookingsByCustomerResult' => $this->getGetBookingsByCustomerResult(),
      );
    }

    /**
     * @return GetBookinsRS
     */
    public function getGetBookingsByCustomerResult()
    {
      return $this->GetBookingsByCustomerResult;
    }

    /**
     * @param GetBookinsRS $GetBookingsByCustomerResult
     * @return \Dingus\SyncroService\GetBookingsByCustomerResponse
     */
    public function setGetBookingsByCustomerResult($GetBookingsByCustomerResult)
    {
      $this->GetBookingsByCustomerResult = $GetBookingsByCustomerResult;
      return $this;
    }

}
