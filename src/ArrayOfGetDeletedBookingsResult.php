<?php

namespace Dingus\SyncroService;

class ArrayOfGetDeletedBookingsResult implements \JsonSerializable, \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var GetDeletedBookingsResult[] $GetDeletedBookingsResult
     */
    protected $GetDeletedBookingsResult = null;

    
    public function __construct()
    {
    
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'GetDeletedBookingsResult' => $this->getGetDeletedBookingsResult(),
      );
    }

    /**
     * @return GetDeletedBookingsResult[]
     */
    public function getGetDeletedBookingsResult()
    {
      return $this->GetDeletedBookingsResult;
    }

    /**
     * @param GetDeletedBookingsResult[] $GetDeletedBookingsResult
     * @return \Dingus\SyncroService\ArrayOfGetDeletedBookingsResult
     */
    public function setGetDeletedBookingsResult(array $GetDeletedBookingsResult = null)
    {
      $this->GetDeletedBookingsResult = $GetDeletedBookingsResult;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->GetDeletedBookingsResult[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return GetDeletedBookingsResult
     */
    public function offsetGet($offset)
    {
      return $this->GetDeletedBookingsResult[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param GetDeletedBookingsResult $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      $this->GetDeletedBookingsResult[$offset] = $value;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->GetDeletedBookingsResult[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return GetDeletedBookingsResult Return the current element
     */
    public function current()
    {
      return current($this->GetDeletedBookingsResult);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->GetDeletedBookingsResult);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->GetDeletedBookingsResult);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->GetDeletedBookingsResult);
    }

    /**
     * Countable implementation
     *
     * @return GetDeletedBookingsResult Return count of elements
     */
    public function count()
    {
      return count($this->GetDeletedBookingsResult);
    }

}
