<?php

namespace Dingus\SyncroService;

class ArrayOfInstalaciones implements \JsonSerializable, \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var Instalaciones[] $Instalaciones
     */
    protected $Instalaciones = null;

    
    public function __construct()
    {
    
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'Instalaciones' => $this->getInstalaciones(),
      );
    }

    /**
     * @return Instalaciones[]
     */
    public function getInstalaciones()
    {
      return $this->Instalaciones;
    }

    /**
     * @param Instalaciones[] $Instalaciones
     * @return \Dingus\SyncroService\ArrayOfInstalaciones
     */
    public function setInstalaciones(array $Instalaciones = null)
    {
      $this->Instalaciones = $Instalaciones;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->Instalaciones[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return Instalaciones
     */
    public function offsetGet($offset)
    {
      return $this->Instalaciones[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param Instalaciones $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      $this->Instalaciones[$offset] = $value;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->Instalaciones[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return Instalaciones Return the current element
     */
    public function current()
    {
      return current($this->Instalaciones);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->Instalaciones);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->Instalaciones);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->Instalaciones);
    }

    /**
     * Countable implementation
     *
     * @return Instalaciones Return count of elements
     */
    public function count()
    {
      return count($this->Instalaciones);
    }

}
