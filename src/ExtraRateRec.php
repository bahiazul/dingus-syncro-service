<?php

namespace Dingus\SyncroService;

class ExtraRateRec implements \JsonSerializable
{

    /**
     * @var Action $Action
     */
    protected $Action = null;

    /**
     * @var string $Code
     */
    protected $Code = null;

    /**
     * @var string $RoomCode
     */
    protected $RoomCode = null;

    /**
     * @var string $BoardCode
     */
    protected $BoardCode = null;

    /**
     * @var \DateTime $EffectiveDate
     */
    protected $EffectiveDate = null;

    /**
     * @var \DateTime $DateFrom
     */
    protected $DateFrom = null;

    /**
     * @var \DateTime $DateFrom2
     */
    protected $DateFrom2 = null;

    /**
     * @var DatesType $DatesType
     */
    protected $DatesType = null;

    /**
     * @var \DateTime $DateTo
     */
    protected $DateTo = null;

    /**
     * @var \DateTime $DateTo2
     */
    protected $DateTo2 = null;

    /**
     * @var string $PMSCode
     */
    protected $PMSCode = null;

    /**
     * @var float $Price
     */
    protected $Price = null;

    /**
     * @var float $ThirdPaxPrice
     */
    protected $ThirdPaxPrice = null;

    /**
     * @var float $FourthPaxPrice
     */
    protected $FourthPaxPrice = null;

    /**
     * @var float $ChildPrice
     */
    protected $ChildPrice = null;

    /**
     * @var float $SecondChildPrice
     */
    protected $SecondChildPrice = null;

    /**
     * @var float $ThirdChildPrice
     */
    protected $ThirdChildPrice = null;

    /**
     * @var float $FourthChildPrice
     */
    protected $FourthChildPrice = null;

    /**
     * @var float $EnfantPrice
     */
    protected $EnfantPrice = null;

    /**
     * @var int $DaysFromBeforeChecking
     */
    protected $DaysFromBeforeChecking = null;

    /**
     * @var int $DaysToBeforeChecking
     */
    protected $DaysToBeforeChecking = null;

    /**
     * @var TipoPax $PaxType
     */
    protected $PaxType = null;

    /**
     * @var boolean $PaxTypeIndicated
     */
    protected $PaxTypeIndicated = null;

    /**
     * @var int $PaxFrom
     */
    protected $PaxFrom = null;

    /**
     * @var int $PaxTo
     */
    protected $PaxTo = null;

    /**
     * @var int $AdultFrom
     */
    protected $AdultFrom = null;

    /**
     * @var int $AdultTo
     */
    protected $AdultTo = null;

    /**
     * @var int $ChildrenFrom
     */
    protected $ChildrenFrom = null;

    /**
     * @var int $ChildrenTo
     */
    protected $ChildrenTo = null;

    /**
     * @var int $AgeFrom
     */
    protected $AgeFrom = null;

    /**
     * @var int $AgeTo
     */
    protected $AgeTo = null;

    /**
     * @var int $PaxNumber
     */
    protected $PaxNumber = null;

    /**
     * @var int $NightsFrom
     */
    protected $NightsFrom = null;

    /**
     * @var int $NightsTo
     */
    protected $NightsTo = null;

    /**
     * @var boolean $Monday
     */
    protected $Monday = null;

    /**
     * @var boolean $Tuesday
     */
    protected $Tuesday = null;

    /**
     * @var boolean $Wednesday
     */
    protected $Wednesday = null;

    /**
     * @var boolean $Thursday
     */
    protected $Thursday = null;

    /**
     * @var boolean $Friday
     */
    protected $Friday = null;

    /**
     * @var boolean $Saturday
     */
    protected $Saturday = null;

    /**
     * @var boolean $Sunday
     */
    protected $Sunday = null;

    /**
     * @var boolean $UniqueApplicationFirstDay
     */
    protected $UniqueApplicationFirstDay = null;

    /**
     * @var boolean $UniqueApplicationLastDay
     */
    protected $UniqueApplicationLastDay = null;

    /**
     * @var boolean $IsPercent
     */
    protected $IsPercent = null;

    /**
     * @var boolean $AplicationRoom
     */
    protected $AplicationRoom = null;

    /**
     * @var boolean $AplicatonBoard
     */
    protected $AplicatonBoard = null;

    /**
     * @var boolean $AplicationSupl
     */
    protected $AplicationSupl = null;

    /**
     * @var boolean $Guaranteed
     */
    protected $Guaranteed = null;

    /**
     * @param Action $Action
     * @param \DateTime $EffectiveDate
     * @param \DateTime $DateFrom
     * @param \DateTime $DateFrom2
     * @param DatesType $DatesType
     * @param \DateTime $DateTo
     * @param \DateTime $DateTo2
     * @param float $Price
     * @param float $ThirdPaxPrice
     * @param float $FourthPaxPrice
     * @param float $ChildPrice
     * @param float $SecondChildPrice
     * @param float $ThirdChildPrice
     * @param float $FourthChildPrice
     * @param float $EnfantPrice
     * @param int $DaysFromBeforeChecking
     * @param int $DaysToBeforeChecking
     * @param TipoPax $PaxType
     * @param boolean $PaxTypeIndicated
     * @param int $PaxFrom
     * @param int $PaxTo
     * @param int $AdultFrom
     * @param int $AdultTo
     * @param int $ChildrenFrom
     * @param int $ChildrenTo
     * @param int $AgeFrom
     * @param int $AgeTo
     * @param int $PaxNumber
     * @param int $NightsFrom
     * @param int $NightsTo
     * @param boolean $Monday
     * @param boolean $Tuesday
     * @param boolean $Wednesday
     * @param boolean $Thursday
     * @param boolean $Friday
     * @param boolean $Saturday
     * @param boolean $Sunday
     * @param boolean $UniqueApplicationFirstDay
     * @param boolean $UniqueApplicationLastDay
     * @param boolean $IsPercent
     * @param boolean $AplicationRoom
     * @param boolean $AplicatonBoard
     * @param boolean $AplicationSupl
     * @param boolean $Guaranteed
     */
    public function __construct($Action, \DateTime $EffectiveDate, \DateTime $DateFrom, \DateTime $DateFrom2, $DatesType, \DateTime $DateTo, \DateTime $DateTo2, $Price, $ThirdPaxPrice, $FourthPaxPrice, $ChildPrice, $SecondChildPrice, $ThirdChildPrice, $FourthChildPrice, $EnfantPrice, $DaysFromBeforeChecking, $DaysToBeforeChecking, $PaxType, $PaxTypeIndicated, $PaxFrom, $PaxTo, $AdultFrom, $AdultTo, $ChildrenFrom, $ChildrenTo, $AgeFrom, $AgeTo, $PaxNumber, $NightsFrom, $NightsTo, $Monday, $Tuesday, $Wednesday, $Thursday, $Friday, $Saturday, $Sunday, $UniqueApplicationFirstDay, $UniqueApplicationLastDay, $IsPercent, $AplicationRoom, $AplicatonBoard, $AplicationSupl, $Guaranteed)
    {
      $this->Action = $Action;
      $this->EffectiveDate = $EffectiveDate->format(\DateTime::ATOM);
      $this->DateFrom = $DateFrom->format(\DateTime::ATOM);
      $this->DateFrom2 = $DateFrom2->format(\DateTime::ATOM);
      $this->DatesType = $DatesType;
      $this->DateTo = $DateTo->format(\DateTime::ATOM);
      $this->DateTo2 = $DateTo2->format(\DateTime::ATOM);
      $this->Price = $Price;
      $this->ThirdPaxPrice = $ThirdPaxPrice;
      $this->FourthPaxPrice = $FourthPaxPrice;
      $this->ChildPrice = $ChildPrice;
      $this->SecondChildPrice = $SecondChildPrice;
      $this->ThirdChildPrice = $ThirdChildPrice;
      $this->FourthChildPrice = $FourthChildPrice;
      $this->EnfantPrice = $EnfantPrice;
      $this->DaysFromBeforeChecking = $DaysFromBeforeChecking;
      $this->DaysToBeforeChecking = $DaysToBeforeChecking;
      $this->PaxType = $PaxType;
      $this->PaxTypeIndicated = $PaxTypeIndicated;
      $this->PaxFrom = $PaxFrom;
      $this->PaxTo = $PaxTo;
      $this->AdultFrom = $AdultFrom;
      $this->AdultTo = $AdultTo;
      $this->ChildrenFrom = $ChildrenFrom;
      $this->ChildrenTo = $ChildrenTo;
      $this->AgeFrom = $AgeFrom;
      $this->AgeTo = $AgeTo;
      $this->PaxNumber = $PaxNumber;
      $this->NightsFrom = $NightsFrom;
      $this->NightsTo = $NightsTo;
      $this->Monday = $Monday;
      $this->Tuesday = $Tuesday;
      $this->Wednesday = $Wednesday;
      $this->Thursday = $Thursday;
      $this->Friday = $Friday;
      $this->Saturday = $Saturday;
      $this->Sunday = $Sunday;
      $this->UniqueApplicationFirstDay = $UniqueApplicationFirstDay;
      $this->UniqueApplicationLastDay = $UniqueApplicationLastDay;
      $this->IsPercent = $IsPercent;
      $this->AplicationRoom = $AplicationRoom;
      $this->AplicatonBoard = $AplicatonBoard;
      $this->AplicationSupl = $AplicationSupl;
      $this->Guaranteed = $Guaranteed;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'Action' => $this->getAction(),
        'Code' => $this->getCode(),
        'RoomCode' => $this->getRoomCode(),
        'BoardCode' => $this->getBoardCode(),
        'EffectiveDate' => $this->getEffectiveDate(),
        'DateFrom' => $this->getDateFrom(),
        'DateFrom2' => $this->getDateFrom2(),
        'DatesType' => $this->getDatesType(),
        'DateTo' => $this->getDateTo(),
        'DateTo2' => $this->getDateTo2(),
        'PMSCode' => $this->getPMSCode(),
        'Price' => $this->getPrice(),
        'ThirdPaxPrice' => $this->getThirdPaxPrice(),
        'FourthPaxPrice' => $this->getFourthPaxPrice(),
        'ChildPrice' => $this->getChildPrice(),
        'SecondChildPrice' => $this->getSecondChildPrice(),
        'ThirdChildPrice' => $this->getThirdChildPrice(),
        'FourthChildPrice' => $this->getFourthChildPrice(),
        'EnfantPrice' => $this->getEnfantPrice(),
        'DaysFromBeforeChecking' => $this->getDaysFromBeforeChecking(),
        'DaysToBeforeChecking' => $this->getDaysToBeforeChecking(),
        'PaxType' => $this->getPaxType(),
        'PaxTypeIndicated' => $this->getPaxTypeIndicated(),
        'PaxFrom' => $this->getPaxFrom(),
        'PaxTo' => $this->getPaxTo(),
        'AdultFrom' => $this->getAdultFrom(),
        'AdultTo' => $this->getAdultTo(),
        'ChildrenFrom' => $this->getChildrenFrom(),
        'ChildrenTo' => $this->getChildrenTo(),
        'AgeFrom' => $this->getAgeFrom(),
        'AgeTo' => $this->getAgeTo(),
        'PaxNumber' => $this->getPaxNumber(),
        'NightsFrom' => $this->getNightsFrom(),
        'NightsTo' => $this->getNightsTo(),
        'Monday' => $this->getMonday(),
        'Tuesday' => $this->getTuesday(),
        'Wednesday' => $this->getWednesday(),
        'Thursday' => $this->getThursday(),
        'Friday' => $this->getFriday(),
        'Saturday' => $this->getSaturday(),
        'Sunday' => $this->getSunday(),
        'UniqueApplicationFirstDay' => $this->getUniqueApplicationFirstDay(),
        'UniqueApplicationLastDay' => $this->getUniqueApplicationLastDay(),
        'IsPercent' => $this->getIsPercent(),
        'AplicationRoom' => $this->getAplicationRoom(),
        'AplicatonBoard' => $this->getAplicatonBoard(),
        'AplicationSupl' => $this->getAplicationSupl(),
        'Guaranteed' => $this->getGuaranteed(),
      );
    }

    /**
     * @return Action
     */
    public function getAction()
    {
      return $this->Action;
    }

    /**
     * @param Action $Action
     * @return \Dingus\SyncroService\ExtraRateRec
     */
    public function setAction($Action)
    {
      $this->Action = $Action;
      return $this;
    }

    /**
     * @return string
     */
    public function getCode()
    {
      return $this->Code;
    }

    /**
     * @param string $Code
     * @return \Dingus\SyncroService\ExtraRateRec
     */
    public function setCode($Code)
    {
      $this->Code = $Code;
      return $this;
    }

    /**
     * @return string
     */
    public function getRoomCode()
    {
      return $this->RoomCode;
    }

    /**
     * @param string $RoomCode
     * @return \Dingus\SyncroService\ExtraRateRec
     */
    public function setRoomCode($RoomCode)
    {
      $this->RoomCode = $RoomCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getBoardCode()
    {
      return $this->BoardCode;
    }

    /**
     * @param string $BoardCode
     * @return \Dingus\SyncroService\ExtraRateRec
     */
    public function setBoardCode($BoardCode)
    {
      $this->BoardCode = $BoardCode;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getEffectiveDate()
    {
      if ($this->EffectiveDate == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->EffectiveDate);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $EffectiveDate
     * @return \Dingus\SyncroService\ExtraRateRec
     */
    public function setEffectiveDate(\DateTime $EffectiveDate)
    {
      $this->EffectiveDate = $EffectiveDate->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateFrom()
    {
      if ($this->DateFrom == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->DateFrom);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $DateFrom
     * @return \Dingus\SyncroService\ExtraRateRec
     */
    public function setDateFrom(\DateTime $DateFrom)
    {
      $this->DateFrom = $DateFrom->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateFrom2()
    {
      if ($this->DateFrom2 == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->DateFrom2);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $DateFrom2
     * @return \Dingus\SyncroService\ExtraRateRec
     */
    public function setDateFrom2(\DateTime $DateFrom2)
    {
      $this->DateFrom2 = $DateFrom2->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return DatesType
     */
    public function getDatesType()
    {
      return $this->DatesType;
    }

    /**
     * @param DatesType $DatesType
     * @return \Dingus\SyncroService\ExtraRateRec
     */
    public function setDatesType($DatesType)
    {
      $this->DatesType = $DatesType;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateTo()
    {
      if ($this->DateTo == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->DateTo);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $DateTo
     * @return \Dingus\SyncroService\ExtraRateRec
     */
    public function setDateTo(\DateTime $DateTo)
    {
      $this->DateTo = $DateTo->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateTo2()
    {
      if ($this->DateTo2 == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->DateTo2);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $DateTo2
     * @return \Dingus\SyncroService\ExtraRateRec
     */
    public function setDateTo2(\DateTime $DateTo2)
    {
      $this->DateTo2 = $DateTo2->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return string
     */
    public function getPMSCode()
    {
      return $this->PMSCode;
    }

    /**
     * @param string $PMSCode
     * @return \Dingus\SyncroService\ExtraRateRec
     */
    public function setPMSCode($PMSCode)
    {
      $this->PMSCode = $PMSCode;
      return $this;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
      return $this->Price;
    }

    /**
     * @param float $Price
     * @return \Dingus\SyncroService\ExtraRateRec
     */
    public function setPrice($Price)
    {
      $this->Price = $Price;
      return $this;
    }

    /**
     * @return float
     */
    public function getThirdPaxPrice()
    {
      return $this->ThirdPaxPrice;
    }

    /**
     * @param float $ThirdPaxPrice
     * @return \Dingus\SyncroService\ExtraRateRec
     */
    public function setThirdPaxPrice($ThirdPaxPrice)
    {
      $this->ThirdPaxPrice = $ThirdPaxPrice;
      return $this;
    }

    /**
     * @return float
     */
    public function getFourthPaxPrice()
    {
      return $this->FourthPaxPrice;
    }

    /**
     * @param float $FourthPaxPrice
     * @return \Dingus\SyncroService\ExtraRateRec
     */
    public function setFourthPaxPrice($FourthPaxPrice)
    {
      $this->FourthPaxPrice = $FourthPaxPrice;
      return $this;
    }

    /**
     * @return float
     */
    public function getChildPrice()
    {
      return $this->ChildPrice;
    }

    /**
     * @param float $ChildPrice
     * @return \Dingus\SyncroService\ExtraRateRec
     */
    public function setChildPrice($ChildPrice)
    {
      $this->ChildPrice = $ChildPrice;
      return $this;
    }

    /**
     * @return float
     */
    public function getSecondChildPrice()
    {
      return $this->SecondChildPrice;
    }

    /**
     * @param float $SecondChildPrice
     * @return \Dingus\SyncroService\ExtraRateRec
     */
    public function setSecondChildPrice($SecondChildPrice)
    {
      $this->SecondChildPrice = $SecondChildPrice;
      return $this;
    }

    /**
     * @return float
     */
    public function getThirdChildPrice()
    {
      return $this->ThirdChildPrice;
    }

    /**
     * @param float $ThirdChildPrice
     * @return \Dingus\SyncroService\ExtraRateRec
     */
    public function setThirdChildPrice($ThirdChildPrice)
    {
      $this->ThirdChildPrice = $ThirdChildPrice;
      return $this;
    }

    /**
     * @return float
     */
    public function getFourthChildPrice()
    {
      return $this->FourthChildPrice;
    }

    /**
     * @param float $FourthChildPrice
     * @return \Dingus\SyncroService\ExtraRateRec
     */
    public function setFourthChildPrice($FourthChildPrice)
    {
      $this->FourthChildPrice = $FourthChildPrice;
      return $this;
    }

    /**
     * @return float
     */
    public function getEnfantPrice()
    {
      return $this->EnfantPrice;
    }

    /**
     * @param float $EnfantPrice
     * @return \Dingus\SyncroService\ExtraRateRec
     */
    public function setEnfantPrice($EnfantPrice)
    {
      $this->EnfantPrice = $EnfantPrice;
      return $this;
    }

    /**
     * @return int
     */
    public function getDaysFromBeforeChecking()
    {
      return $this->DaysFromBeforeChecking;
    }

    /**
     * @param int $DaysFromBeforeChecking
     * @return \Dingus\SyncroService\ExtraRateRec
     */
    public function setDaysFromBeforeChecking($DaysFromBeforeChecking)
    {
      $this->DaysFromBeforeChecking = $DaysFromBeforeChecking;
      return $this;
    }

    /**
     * @return int
     */
    public function getDaysToBeforeChecking()
    {
      return $this->DaysToBeforeChecking;
    }

    /**
     * @param int $DaysToBeforeChecking
     * @return \Dingus\SyncroService\ExtraRateRec
     */
    public function setDaysToBeforeChecking($DaysToBeforeChecking)
    {
      $this->DaysToBeforeChecking = $DaysToBeforeChecking;
      return $this;
    }

    /**
     * @return TipoPax
     */
    public function getPaxType()
    {
      return $this->PaxType;
    }

    /**
     * @param TipoPax $PaxType
     * @return \Dingus\SyncroService\ExtraRateRec
     */
    public function setPaxType($PaxType)
    {
      $this->PaxType = $PaxType;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getPaxTypeIndicated()
    {
      return $this->PaxTypeIndicated;
    }

    /**
     * @param boolean $PaxTypeIndicated
     * @return \Dingus\SyncroService\ExtraRateRec
     */
    public function setPaxTypeIndicated($PaxTypeIndicated)
    {
      $this->PaxTypeIndicated = $PaxTypeIndicated;
      return $this;
    }

    /**
     * @return int
     */
    public function getPaxFrom()
    {
      return $this->PaxFrom;
    }

    /**
     * @param int $PaxFrom
     * @return \Dingus\SyncroService\ExtraRateRec
     */
    public function setPaxFrom($PaxFrom)
    {
      $this->PaxFrom = $PaxFrom;
      return $this;
    }

    /**
     * @return int
     */
    public function getPaxTo()
    {
      return $this->PaxTo;
    }

    /**
     * @param int $PaxTo
     * @return \Dingus\SyncroService\ExtraRateRec
     */
    public function setPaxTo($PaxTo)
    {
      $this->PaxTo = $PaxTo;
      return $this;
    }

    /**
     * @return int
     */
    public function getAdultFrom()
    {
      return $this->AdultFrom;
    }

    /**
     * @param int $AdultFrom
     * @return \Dingus\SyncroService\ExtraRateRec
     */
    public function setAdultFrom($AdultFrom)
    {
      $this->AdultFrom = $AdultFrom;
      return $this;
    }

    /**
     * @return int
     */
    public function getAdultTo()
    {
      return $this->AdultTo;
    }

    /**
     * @param int $AdultTo
     * @return \Dingus\SyncroService\ExtraRateRec
     */
    public function setAdultTo($AdultTo)
    {
      $this->AdultTo = $AdultTo;
      return $this;
    }

    /**
     * @return int
     */
    public function getChildrenFrom()
    {
      return $this->ChildrenFrom;
    }

    /**
     * @param int $ChildrenFrom
     * @return \Dingus\SyncroService\ExtraRateRec
     */
    public function setChildrenFrom($ChildrenFrom)
    {
      $this->ChildrenFrom = $ChildrenFrom;
      return $this;
    }

    /**
     * @return int
     */
    public function getChildrenTo()
    {
      return $this->ChildrenTo;
    }

    /**
     * @param int $ChildrenTo
     * @return \Dingus\SyncroService\ExtraRateRec
     */
    public function setChildrenTo($ChildrenTo)
    {
      $this->ChildrenTo = $ChildrenTo;
      return $this;
    }

    /**
     * @return int
     */
    public function getAgeFrom()
    {
      return $this->AgeFrom;
    }

    /**
     * @param int $AgeFrom
     * @return \Dingus\SyncroService\ExtraRateRec
     */
    public function setAgeFrom($AgeFrom)
    {
      $this->AgeFrom = $AgeFrom;
      return $this;
    }

    /**
     * @return int
     */
    public function getAgeTo()
    {
      return $this->AgeTo;
    }

    /**
     * @param int $AgeTo
     * @return \Dingus\SyncroService\ExtraRateRec
     */
    public function setAgeTo($AgeTo)
    {
      $this->AgeTo = $AgeTo;
      return $this;
    }

    /**
     * @return int
     */
    public function getPaxNumber()
    {
      return $this->PaxNumber;
    }

    /**
     * @param int $PaxNumber
     * @return \Dingus\SyncroService\ExtraRateRec
     */
    public function setPaxNumber($PaxNumber)
    {
      $this->PaxNumber = $PaxNumber;
      return $this;
    }

    /**
     * @return int
     */
    public function getNightsFrom()
    {
      return $this->NightsFrom;
    }

    /**
     * @param int $NightsFrom
     * @return \Dingus\SyncroService\ExtraRateRec
     */
    public function setNightsFrom($NightsFrom)
    {
      $this->NightsFrom = $NightsFrom;
      return $this;
    }

    /**
     * @return int
     */
    public function getNightsTo()
    {
      return $this->NightsTo;
    }

    /**
     * @param int $NightsTo
     * @return \Dingus\SyncroService\ExtraRateRec
     */
    public function setNightsTo($NightsTo)
    {
      $this->NightsTo = $NightsTo;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getMonday()
    {
      return $this->Monday;
    }

    /**
     * @param boolean $Monday
     * @return \Dingus\SyncroService\ExtraRateRec
     */
    public function setMonday($Monday)
    {
      $this->Monday = $Monday;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getTuesday()
    {
      return $this->Tuesday;
    }

    /**
     * @param boolean $Tuesday
     * @return \Dingus\SyncroService\ExtraRateRec
     */
    public function setTuesday($Tuesday)
    {
      $this->Tuesday = $Tuesday;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getWednesday()
    {
      return $this->Wednesday;
    }

    /**
     * @param boolean $Wednesday
     * @return \Dingus\SyncroService\ExtraRateRec
     */
    public function setWednesday($Wednesday)
    {
      $this->Wednesday = $Wednesday;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getThursday()
    {
      return $this->Thursday;
    }

    /**
     * @param boolean $Thursday
     * @return \Dingus\SyncroService\ExtraRateRec
     */
    public function setThursday($Thursday)
    {
      $this->Thursday = $Thursday;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getFriday()
    {
      return $this->Friday;
    }

    /**
     * @param boolean $Friday
     * @return \Dingus\SyncroService\ExtraRateRec
     */
    public function setFriday($Friday)
    {
      $this->Friday = $Friday;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getSaturday()
    {
      return $this->Saturday;
    }

    /**
     * @param boolean $Saturday
     * @return \Dingus\SyncroService\ExtraRateRec
     */
    public function setSaturday($Saturday)
    {
      $this->Saturday = $Saturday;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getSunday()
    {
      return $this->Sunday;
    }

    /**
     * @param boolean $Sunday
     * @return \Dingus\SyncroService\ExtraRateRec
     */
    public function setSunday($Sunday)
    {
      $this->Sunday = $Sunday;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getUniqueApplicationFirstDay()
    {
      return $this->UniqueApplicationFirstDay;
    }

    /**
     * @param boolean $UniqueApplicationFirstDay
     * @return \Dingus\SyncroService\ExtraRateRec
     */
    public function setUniqueApplicationFirstDay($UniqueApplicationFirstDay)
    {
      $this->UniqueApplicationFirstDay = $UniqueApplicationFirstDay;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getUniqueApplicationLastDay()
    {
      return $this->UniqueApplicationLastDay;
    }

    /**
     * @param boolean $UniqueApplicationLastDay
     * @return \Dingus\SyncroService\ExtraRateRec
     */
    public function setUniqueApplicationLastDay($UniqueApplicationLastDay)
    {
      $this->UniqueApplicationLastDay = $UniqueApplicationLastDay;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getIsPercent()
    {
      return $this->IsPercent;
    }

    /**
     * @param boolean $IsPercent
     * @return \Dingus\SyncroService\ExtraRateRec
     */
    public function setIsPercent($IsPercent)
    {
      $this->IsPercent = $IsPercent;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getAplicationRoom()
    {
      return $this->AplicationRoom;
    }

    /**
     * @param boolean $AplicationRoom
     * @return \Dingus\SyncroService\ExtraRateRec
     */
    public function setAplicationRoom($AplicationRoom)
    {
      $this->AplicationRoom = $AplicationRoom;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getAplicatonBoard()
    {
      return $this->AplicatonBoard;
    }

    /**
     * @param boolean $AplicatonBoard
     * @return \Dingus\SyncroService\ExtraRateRec
     */
    public function setAplicatonBoard($AplicatonBoard)
    {
      $this->AplicatonBoard = $AplicatonBoard;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getAplicationSupl()
    {
      return $this->AplicationSupl;
    }

    /**
     * @param boolean $AplicationSupl
     * @return \Dingus\SyncroService\ExtraRateRec
     */
    public function setAplicationSupl($AplicationSupl)
    {
      $this->AplicationSupl = $AplicationSupl;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getGuaranteed()
    {
      return $this->Guaranteed;
    }

    /**
     * @param boolean $Guaranteed
     * @return \Dingus\SyncroService\ExtraRateRec
     */
    public function setGuaranteed($Guaranteed)
    {
      $this->Guaranteed = $Guaranteed;
      return $this;
    }

}
