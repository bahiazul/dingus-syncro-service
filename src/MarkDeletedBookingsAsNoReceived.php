<?php

namespace Dingus\SyncroService;

class MarkDeletedBookingsAsNoReceived implements \JsonSerializable
{

    /**
     * @var Credentials $Credentials
     */
    protected $Credentials = null;

    /**
     * @var string $CustomerCode
     */
    protected $CustomerCode = null;

    /**
     * @var string $HotelCode
     */
    protected $HotelCode = null;

    /**
     * @var ArrayOfString $LocalizerCol
     */
    protected $LocalizerCol = null;

    /**
     * @var string $PMSCode
     */
    protected $PMSCode = null;

    /**
     * @var string $RoomReference
     */
    protected $RoomReference = null;

    /**
     * @param Credentials $Credentials
     * @param string $CustomerCode
     * @param string $HotelCode
     * @param ArrayOfString $LocalizerCol
     * @param string $PMSCode
     * @param string $RoomReference
     */
    public function __construct($Credentials, $CustomerCode, $HotelCode, $LocalizerCol, $PMSCode, $RoomReference)
    {
      $this->Credentials = $Credentials;
      $this->CustomerCode = $CustomerCode;
      $this->HotelCode = $HotelCode;
      $this->LocalizerCol = $LocalizerCol;
      $this->PMSCode = $PMSCode;
      $this->RoomReference = $RoomReference;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'Credentials' => $this->getCredentials(),
        'CustomerCode' => $this->getCustomerCode(),
        'HotelCode' => $this->getHotelCode(),
        'LocalizerCol' => $this->getLocalizerCol(),
        'PMSCode' => $this->getPMSCode(),
        'RoomReference' => $this->getRoomReference(),
      );
    }

    /**
     * @return Credentials
     */
    public function getCredentials()
    {
      return $this->Credentials;
    }

    /**
     * @param Credentials $Credentials
     * @return \Dingus\SyncroService\MarkDeletedBookingsAsNoReceived
     */
    public function setCredentials($Credentials)
    {
      $this->Credentials = $Credentials;
      return $this;
    }

    /**
     * @return string
     */
    public function getCustomerCode()
    {
      return $this->CustomerCode;
    }

    /**
     * @param string $CustomerCode
     * @return \Dingus\SyncroService\MarkDeletedBookingsAsNoReceived
     */
    public function setCustomerCode($CustomerCode)
    {
      $this->CustomerCode = $CustomerCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getHotelCode()
    {
      return $this->HotelCode;
    }

    /**
     * @param string $HotelCode
     * @return \Dingus\SyncroService\MarkDeletedBookingsAsNoReceived
     */
    public function setHotelCode($HotelCode)
    {
      $this->HotelCode = $HotelCode;
      return $this;
    }

    /**
     * @return ArrayOfString
     */
    public function getLocalizerCol()
    {
      return $this->LocalizerCol;
    }

    /**
     * @param ArrayOfString $LocalizerCol
     * @return \Dingus\SyncroService\MarkDeletedBookingsAsNoReceived
     */
    public function setLocalizerCol($LocalizerCol)
    {
      $this->LocalizerCol = $LocalizerCol;
      return $this;
    }

    /**
     * @return string
     */
    public function getPMSCode()
    {
      return $this->PMSCode;
    }

    /**
     * @param string $PMSCode
     * @return \Dingus\SyncroService\MarkDeletedBookingsAsNoReceived
     */
    public function setPMSCode($PMSCode)
    {
      $this->PMSCode = $PMSCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getRoomReference()
    {
      return $this->RoomReference;
    }

    /**
     * @param string $RoomReference
     * @return \Dingus\SyncroService\MarkDeletedBookingsAsNoReceived
     */
    public function setRoomReference($RoomReference)
    {
      $this->RoomReference = $RoomReference;
      return $this;
    }

}
