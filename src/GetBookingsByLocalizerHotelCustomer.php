<?php

namespace Dingus\SyncroService;

class GetBookingsByLocalizerHotelCustomer implements \JsonSerializable
{

    /**
     * @var Credentials $Credentials
     */
    protected $Credentials = null;

    /**
     * @var string $Localizer
     */
    protected $Localizer = null;

    /**
     * @var string $HotelCode
     */
    protected $HotelCode = null;

    /**
     * @var string $CustomerCode
     */
    protected $CustomerCode = null;

    /**
     * @var \DateTime $DateFrom
     */
    protected $DateFrom = null;

    /**
     * @var \DateTime $DateTo
     */
    protected $DateTo = null;

    /**
     * @var BookingFindType $FindBookingType
     */
    protected $FindBookingType = null;

    /**
     * @var boolean $NotFullBooking
     */
    protected $NotFullBooking = null;

    /**
     * @param Credentials $Credentials
     * @param string $Localizer
     * @param string $HotelCode
     * @param string $CustomerCode
     * @param \DateTime $DateFrom
     * @param \DateTime $DateTo
     * @param BookingFindType $FindBookingType
     * @param boolean $NotFullBooking
     */
    public function __construct($Credentials, $Localizer, $HotelCode, $CustomerCode, \DateTime $DateFrom, \DateTime $DateTo, $FindBookingType, $NotFullBooking)
    {
      $this->Credentials = $Credentials;
      $this->Localizer = $Localizer;
      $this->HotelCode = $HotelCode;
      $this->CustomerCode = $CustomerCode;
      $this->DateFrom = $DateFrom->format(\DateTime::ATOM);
      $this->DateTo = $DateTo->format(\DateTime::ATOM);
      $this->FindBookingType = $FindBookingType;
      $this->NotFullBooking = $NotFullBooking;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'Credentials' => $this->getCredentials(),
        'Localizer' => $this->getLocalizer(),
        'HotelCode' => $this->getHotelCode(),
        'CustomerCode' => $this->getCustomerCode(),
        'DateFrom' => $this->getDateFrom(),
        'DateTo' => $this->getDateTo(),
        'FindBookingType' => $this->getFindBookingType(),
        'NotFullBooking' => $this->getNotFullBooking(),
      );
    }

    /**
     * @return Credentials
     */
    public function getCredentials()
    {
      return $this->Credentials;
    }

    /**
     * @param Credentials $Credentials
     * @return \Dingus\SyncroService\GetBookingsByLocalizerHotelCustomer
     */
    public function setCredentials($Credentials)
    {
      $this->Credentials = $Credentials;
      return $this;
    }

    /**
     * @return string
     */
    public function getLocalizer()
    {
      return $this->Localizer;
    }

    /**
     * @param string $Localizer
     * @return \Dingus\SyncroService\GetBookingsByLocalizerHotelCustomer
     */
    public function setLocalizer($Localizer)
    {
      $this->Localizer = $Localizer;
      return $this;
    }

    /**
     * @return string
     */
    public function getHotelCode()
    {
      return $this->HotelCode;
    }

    /**
     * @param string $HotelCode
     * @return \Dingus\SyncroService\GetBookingsByLocalizerHotelCustomer
     */
    public function setHotelCode($HotelCode)
    {
      $this->HotelCode = $HotelCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getCustomerCode()
    {
      return $this->CustomerCode;
    }

    /**
     * @param string $CustomerCode
     * @return \Dingus\SyncroService\GetBookingsByLocalizerHotelCustomer
     */
    public function setCustomerCode($CustomerCode)
    {
      $this->CustomerCode = $CustomerCode;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateFrom()
    {
      if ($this->DateFrom == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->DateFrom);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $DateFrom
     * @return \Dingus\SyncroService\GetBookingsByLocalizerHotelCustomer
     */
    public function setDateFrom(\DateTime $DateFrom)
    {
      $this->DateFrom = $DateFrom->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateTo()
    {
      if ($this->DateTo == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->DateTo);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $DateTo
     * @return \Dingus\SyncroService\GetBookingsByLocalizerHotelCustomer
     */
    public function setDateTo(\DateTime $DateTo)
    {
      $this->DateTo = $DateTo->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return BookingFindType
     */
    public function getFindBookingType()
    {
      return $this->FindBookingType;
    }

    /**
     * @param BookingFindType $FindBookingType
     * @return \Dingus\SyncroService\GetBookingsByLocalizerHotelCustomer
     */
    public function setFindBookingType($FindBookingType)
    {
      $this->FindBookingType = $FindBookingType;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getNotFullBooking()
    {
      return $this->NotFullBooking;
    }

    /**
     * @param boolean $NotFullBooking
     * @return \Dingus\SyncroService\GetBookingsByLocalizerHotelCustomer
     */
    public function setNotFullBooking($NotFullBooking)
    {
      $this->NotFullBooking = $NotFullBooking;
      return $this;
    }

}
