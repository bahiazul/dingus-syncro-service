<?php

namespace Dingus\SyncroService;

class AllotmentRec implements \JsonSerializable
{

    /**
     * @var Action $Action
     */
    protected $Action = null;

    /**
     * @var \DateTime $DateFrom
     */
    protected $DateFrom = null;

    /**
     * @var \DateTime $DateTo
     */
    protected $DateTo = null;

    /**
     * @var int $MaximunStay
     */
    protected $MaximunStay = null;

    /**
     * @var int $MinimunStay
     */
    protected $MinimunStay = null;

    /**
     * @var string $PMSCode
     */
    protected $PMSCode = null;

    /**
     * @var int $Contracted
     */
    protected $Contracted = null;

    /**
     * @var int $Quantity
     */
    protected $Quantity = null;

    /**
     * @var int $Release
     */
    protected $Release = null;

    /**
     * @var string $RoomCode
     */
    protected $RoomCode = null;

    /**
     * @var boolean $Closed
     */
    protected $Closed = null;

    /**
     * @param Action $Action
     * @param \DateTime $DateFrom
     * @param \DateTime $DateTo
     * @param int $MaximunStay
     * @param int $MinimunStay
     * @param int $Contracted
     * @param int $Quantity
     * @param int $Release
     * @param boolean $Closed
     */
    public function __construct($Action, \DateTime $DateFrom, \DateTime $DateTo, $MaximunStay, $MinimunStay, $Contracted, $Quantity, $Release, $Closed)
    {
      $this->Action = $Action;
      $this->DateFrom = $DateFrom->format(\DateTime::ATOM);
      $this->DateTo = $DateTo->format(\DateTime::ATOM);
      $this->MaximunStay = $MaximunStay;
      $this->MinimunStay = $MinimunStay;
      $this->Contracted = $Contracted;
      $this->Quantity = $Quantity;
      $this->Release = $Release;
      $this->Closed = $Closed;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'Action' => $this->getAction(),
        'DateFrom' => $this->getDateFrom(),
        'DateTo' => $this->getDateTo(),
        'MaximunStay' => $this->getMaximunStay(),
        'MinimunStay' => $this->getMinimunStay(),
        'PMSCode' => $this->getPMSCode(),
        'Contracted' => $this->getContracted(),
        'Quantity' => $this->getQuantity(),
        'Release' => $this->getRelease(),
        'RoomCode' => $this->getRoomCode(),
        'Closed' => $this->getClosed(),
      );
    }

    /**
     * @return Action
     */
    public function getAction()
    {
      return $this->Action;
    }

    /**
     * @param Action $Action
     * @return \Dingus\SyncroService\AllotmentRec
     */
    public function setAction($Action)
    {
      $this->Action = $Action;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateFrom()
    {
      if ($this->DateFrom == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->DateFrom);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $DateFrom
     * @return \Dingus\SyncroService\AllotmentRec
     */
    public function setDateFrom(\DateTime $DateFrom)
    {
      $this->DateFrom = $DateFrom->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateTo()
    {
      if ($this->DateTo == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->DateTo);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $DateTo
     * @return \Dingus\SyncroService\AllotmentRec
     */
    public function setDateTo(\DateTime $DateTo)
    {
      $this->DateTo = $DateTo->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return int
     */
    public function getMaximunStay()
    {
      return $this->MaximunStay;
    }

    /**
     * @param int $MaximunStay
     * @return \Dingus\SyncroService\AllotmentRec
     */
    public function setMaximunStay($MaximunStay)
    {
      $this->MaximunStay = $MaximunStay;
      return $this;
    }

    /**
     * @return int
     */
    public function getMinimunStay()
    {
      return $this->MinimunStay;
    }

    /**
     * @param int $MinimunStay
     * @return \Dingus\SyncroService\AllotmentRec
     */
    public function setMinimunStay($MinimunStay)
    {
      $this->MinimunStay = $MinimunStay;
      return $this;
    }

    /**
     * @return string
     */
    public function getPMSCode()
    {
      return $this->PMSCode;
    }

    /**
     * @param string $PMSCode
     * @return \Dingus\SyncroService\AllotmentRec
     */
    public function setPMSCode($PMSCode)
    {
      $this->PMSCode = $PMSCode;
      return $this;
    }

    /**
     * @return int
     */
    public function getContracted()
    {
      return $this->Contracted;
    }

    /**
     * @param int $Contracted
     * @return \Dingus\SyncroService\AllotmentRec
     */
    public function setContracted($Contracted)
    {
      $this->Contracted = $Contracted;
      return $this;
    }

    /**
     * @return int
     */
    public function getQuantity()
    {
      return $this->Quantity;
    }

    /**
     * @param int $Quantity
     * @return \Dingus\SyncroService\AllotmentRec
     */
    public function setQuantity($Quantity)
    {
      $this->Quantity = $Quantity;
      return $this;
    }

    /**
     * @return int
     */
    public function getRelease()
    {
      return $this->Release;
    }

    /**
     * @param int $Release
     * @return \Dingus\SyncroService\AllotmentRec
     */
    public function setRelease($Release)
    {
      $this->Release = $Release;
      return $this;
    }

    /**
     * @return string
     */
    public function getRoomCode()
    {
      return $this->RoomCode;
    }

    /**
     * @param string $RoomCode
     * @return \Dingus\SyncroService\AllotmentRec
     */
    public function setRoomCode($RoomCode)
    {
      $this->RoomCode = $RoomCode;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getClosed()
    {
      return $this->Closed;
    }

    /**
     * @param boolean $Closed
     * @return \Dingus\SyncroService\AllotmentRec
     */
    public function setClosed($Closed)
    {
      $this->Closed = $Closed;
      return $this;
    }

}
