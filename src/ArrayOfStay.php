<?php

namespace Dingus\SyncroService;

class ArrayOfStay implements \JsonSerializable, \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var Stay[] $Stay
     */
    protected $Stay = null;

    
    public function __construct()
    {
    
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'Stay' => $this->getStay(),
      );
    }

    /**
     * @return Stay[]
     */
    public function getStay()
    {
      return $this->Stay;
    }

    /**
     * @param Stay[] $Stay
     * @return \Dingus\SyncroService\ArrayOfStay
     */
    public function setStay(array $Stay = null)
    {
      $this->Stay = $Stay;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->Stay[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return Stay
     */
    public function offsetGet($offset)
    {
      return $this->Stay[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param Stay $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      $this->Stay[$offset] = $value;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->Stay[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return Stay Return the current element
     */
    public function current()
    {
      return current($this->Stay);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->Stay);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->Stay);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->Stay);
    }

    /**
     * Countable implementation
     *
     * @return Stay Return count of elements
     */
    public function count()
    {
      return count($this->Stay);
    }

}
