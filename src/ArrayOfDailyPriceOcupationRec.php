<?php

namespace Dingus\SyncroService;

class ArrayOfDailyPriceOcupationRec implements \JsonSerializable, \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var DailyPriceOcupationRec[] $DailyPriceOcupationRec
     */
    protected $DailyPriceOcupationRec = null;

    
    public function __construct()
    {
    
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'DailyPriceOcupationRec' => $this->getDailyPriceOcupationRec(),
      );
    }

    /**
     * @return DailyPriceOcupationRec[]
     */
    public function getDailyPriceOcupationRec()
    {
      return $this->DailyPriceOcupationRec;
    }

    /**
     * @param DailyPriceOcupationRec[] $DailyPriceOcupationRec
     * @return \Dingus\SyncroService\ArrayOfDailyPriceOcupationRec
     */
    public function setDailyPriceOcupationRec(array $DailyPriceOcupationRec = null)
    {
      $this->DailyPriceOcupationRec = $DailyPriceOcupationRec;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->DailyPriceOcupationRec[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return DailyPriceOcupationRec
     */
    public function offsetGet($offset)
    {
      return $this->DailyPriceOcupationRec[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param DailyPriceOcupationRec $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      $this->DailyPriceOcupationRec[$offset] = $value;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->DailyPriceOcupationRec[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return DailyPriceOcupationRec Return the current element
     */
    public function current()
    {
      return current($this->DailyPriceOcupationRec);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->DailyPriceOcupationRec);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->DailyPriceOcupationRec);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->DailyPriceOcupationRec);
    }

    /**
     * Countable implementation
     *
     * @return DailyPriceOcupationRec Return count of elements
     */
    public function count()
    {
      return count($this->DailyPriceOcupationRec);
    }

}
