<?php

namespace Dingus\SyncroService;

class ArrayOfExtra implements \JsonSerializable, \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var Extra[] $Extra
     */
    protected $Extra = null;

    
    public function __construct()
    {
    
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'Extra' => $this->getExtra(),
      );
    }

    /**
     * @return Extra[]
     */
    public function getExtra()
    {
      return $this->Extra;
    }

    /**
     * @param Extra[] $Extra
     * @return \Dingus\SyncroService\ArrayOfExtra
     */
    public function setExtra(array $Extra = null)
    {
      $this->Extra = $Extra;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->Extra[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return Extra
     */
    public function offsetGet($offset)
    {
      return $this->Extra[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param Extra $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      $this->Extra[$offset] = $value;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->Extra[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return Extra Return the current element
     */
    public function current()
    {
      return current($this->Extra);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->Extra);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->Extra);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->Extra);
    }

    /**
     * Countable implementation
     *
     * @return Extra Return count of elements
     */
    public function count()
    {
      return count($this->Extra);
    }

}
