<?php

namespace Dingus\SyncroService;

class PutCustomerRates implements \JsonSerializable
{

    /**
     * @var Credentials $Credentials
     */
    protected $Credentials = null;

    /**
     * @var PutCustomerDailyRatesRQ $PutCustomerRateRQ
     */
    protected $PutCustomerRateRQ = null;

    /**
     * @param Credentials $Credentials
     * @param PutCustomerDailyRatesRQ $PutCustomerRateRQ
     */
    public function __construct($Credentials, $PutCustomerRateRQ)
    {
      $this->Credentials = $Credentials;
      $this->PutCustomerRateRQ = $PutCustomerRateRQ;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'Credentials' => $this->getCredentials(),
        'PutCustomerRateRQ' => $this->getPutCustomerRateRQ(),
      );
    }

    /**
     * @return Credentials
     */
    public function getCredentials()
    {
      return $this->Credentials;
    }

    /**
     * @param Credentials $Credentials
     * @return \Dingus\SyncroService\PutCustomerRates
     */
    public function setCredentials($Credentials)
    {
      $this->Credentials = $Credentials;
      return $this;
    }

    /**
     * @return PutCustomerDailyRatesRQ
     */
    public function getPutCustomerRateRQ()
    {
      return $this->PutCustomerRateRQ;
    }

    /**
     * @param PutCustomerDailyRatesRQ $PutCustomerRateRQ
     * @return \Dingus\SyncroService\PutCustomerRates
     */
    public function setPutCustomerRateRQ($PutCustomerRateRQ)
    {
      $this->PutCustomerRateRQ = $PutCustomerRateRQ;
      return $this;
    }

}
