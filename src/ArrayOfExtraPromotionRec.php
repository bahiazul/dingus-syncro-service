<?php

namespace Dingus\SyncroService;

class ArrayOfExtraPromotionRec implements \JsonSerializable, \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var ExtraPromotionRec[] $ExtraPromotionRec
     */
    protected $ExtraPromotionRec = null;

    
    public function __construct()
    {
    
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'ExtraPromotionRec' => $this->getExtraPromotionRec(),
      );
    }

    /**
     * @return ExtraPromotionRec[]
     */
    public function getExtraPromotionRec()
    {
      return $this->ExtraPromotionRec;
    }

    /**
     * @param ExtraPromotionRec[] $ExtraPromotionRec
     * @return \Dingus\SyncroService\ArrayOfExtraPromotionRec
     */
    public function setExtraPromotionRec(array $ExtraPromotionRec = null)
    {
      $this->ExtraPromotionRec = $ExtraPromotionRec;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->ExtraPromotionRec[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return ExtraPromotionRec
     */
    public function offsetGet($offset)
    {
      return $this->ExtraPromotionRec[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param ExtraPromotionRec $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      $this->ExtraPromotionRec[$offset] = $value;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->ExtraPromotionRec[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return ExtraPromotionRec Return the current element
     */
    public function current()
    {
      return current($this->ExtraPromotionRec);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->ExtraPromotionRec);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->ExtraPromotionRec);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->ExtraPromotionRec);
    }

    /**
     * Countable implementation
     *
     * @return ExtraPromotionRec Return count of elements
     */
    public function count()
    {
      return count($this->ExtraPromotionRec);
    }

}
