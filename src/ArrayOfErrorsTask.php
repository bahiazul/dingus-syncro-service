<?php

namespace Dingus\SyncroService;

class ArrayOfErrorsTask implements \JsonSerializable, \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var ErrorsTask[] $ErrorsTask
     */
    protected $ErrorsTask = null;

    
    public function __construct()
    {
    
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'ErrorsTask' => $this->getErrorsTask(),
      );
    }

    /**
     * @return ErrorsTask[]
     */
    public function getErrorsTask()
    {
      return $this->ErrorsTask;
    }

    /**
     * @param ErrorsTask[] $ErrorsTask
     * @return \Dingus\SyncroService\ArrayOfErrorsTask
     */
    public function setErrorsTask(array $ErrorsTask = null)
    {
      $this->ErrorsTask = $ErrorsTask;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->ErrorsTask[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return ErrorsTask
     */
    public function offsetGet($offset)
    {
      return $this->ErrorsTask[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param ErrorsTask $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      $this->ErrorsTask[$offset] = $value;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->ErrorsTask[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return ErrorsTask Return the current element
     */
    public function current()
    {
      return current($this->ErrorsTask);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->ErrorsTask);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->ErrorsTask);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->ErrorsTask);
    }

    /**
     * Countable implementation
     *
     * @return ErrorsTask Return count of elements
     */
    public function count()
    {
      return count($this->ErrorsTask);
    }

}
