<?php

namespace Dingus\SyncroService;

class ArrayOfExtraRateRec implements \JsonSerializable, \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var ExtraRateRec[] $ExtraRateRec
     */
    protected $ExtraRateRec = null;

    
    public function __construct()
    {
    
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'ExtraRateRec' => $this->getExtraRateRec(),
      );
    }

    /**
     * @return ExtraRateRec[]
     */
    public function getExtraRateRec()
    {
      return $this->ExtraRateRec;
    }

    /**
     * @param ExtraRateRec[] $ExtraRateRec
     * @return \Dingus\SyncroService\ArrayOfExtraRateRec
     */
    public function setExtraRateRec(array $ExtraRateRec = null)
    {
      $this->ExtraRateRec = $ExtraRateRec;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->ExtraRateRec[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return ExtraRateRec
     */
    public function offsetGet($offset)
    {
      return $this->ExtraRateRec[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param ExtraRateRec $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      $this->ExtraRateRec[$offset] = $value;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->ExtraRateRec[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return ExtraRateRec Return the current element
     */
    public function current()
    {
      return current($this->ExtraRateRec);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->ExtraRateRec);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->ExtraRateRec);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->ExtraRateRec);
    }

    /**
     * Countable implementation
     *
     * @return ExtraRateRec Return count of elements
     */
    public function count()
    {
      return count($this->ExtraRateRec);
    }

}
