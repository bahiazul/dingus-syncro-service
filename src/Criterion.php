<?php

namespace Dingus\SyncroService;

class Criterion implements \JsonSerializable
{

    /**
     * @var Action $Action
     */
    protected $Action = null;

    /**
     * @var int $CriterionId
     */
    protected $CriterionId = null;

    /**
     * @var string $CriterionDescription
     */
    protected $CriterionDescription = null;

    /**
     * @var string $CriterionType
     */
    protected $CriterionType = null;

    /**
     * @var string $CriterionValue
     */
    protected $CriterionValue = null;

    /**
     * @param Action $Action
     * @param int $CriterionId
     */
    public function __construct($Action, $CriterionId)
    {
      $this->Action = $Action;
      $this->CriterionId = $CriterionId;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'Action' => $this->getAction(),
        'CriterionId' => $this->getCriterionId(),
        'CriterionDescription' => $this->getCriterionDescription(),
        'CriterionType' => $this->getCriterionType(),
        'CriterionValue' => $this->getCriterionValue(),
      );
    }

    /**
     * @return Action
     */
    public function getAction()
    {
      return $this->Action;
    }

    /**
     * @param Action $Action
     * @return \Dingus\SyncroService\Criterion
     */
    public function setAction($Action)
    {
      $this->Action = $Action;
      return $this;
    }

    /**
     * @return int
     */
    public function getCriterionId()
    {
      return $this->CriterionId;
    }

    /**
     * @param int $CriterionId
     * @return \Dingus\SyncroService\Criterion
     */
    public function setCriterionId($CriterionId)
    {
      $this->CriterionId = $CriterionId;
      return $this;
    }

    /**
     * @return string
     */
    public function getCriterionDescription()
    {
      return $this->CriterionDescription;
    }

    /**
     * @param string $CriterionDescription
     * @return \Dingus\SyncroService\Criterion
     */
    public function setCriterionDescription($CriterionDescription)
    {
      $this->CriterionDescription = $CriterionDescription;
      return $this;
    }

    /**
     * @return string
     */
    public function getCriterionType()
    {
      return $this->CriterionType;
    }

    /**
     * @param string $CriterionType
     * @return \Dingus\SyncroService\Criterion
     */
    public function setCriterionType($CriterionType)
    {
      $this->CriterionType = $CriterionType;
      return $this;
    }

    /**
     * @return string
     */
    public function getCriterionValue()
    {
      return $this->CriterionValue;
    }

    /**
     * @param string $CriterionValue
     * @return \Dingus\SyncroService\Criterion
     */
    public function setCriterionValue($CriterionValue)
    {
      $this->CriterionValue = $CriterionValue;
      return $this;
    }

}
