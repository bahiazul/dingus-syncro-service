<?php

namespace Dingus\SyncroService;

class StopSalesPromotionRec implements \JsonSerializable
{

    /**
     * @var Action $Action
     */
    protected $Action = null;

    /**
     * @var int $IdStopSalesPromotion
     */
    protected $IdStopSalesPromotion = null;

    /**
     * @var \DateTime $DateFrom
     */
    protected $DateFrom = null;

    /**
     * @var \DateTime $DateTo
     */
    protected $DateTo = null;

    /**
     * @param Action $Action
     * @param int $IdStopSalesPromotion
     * @param \DateTime $DateFrom
     * @param \DateTime $DateTo
     */
    public function __construct($Action, $IdStopSalesPromotion, \DateTime $DateFrom, \DateTime $DateTo)
    {
      $this->Action = $Action;
      $this->IdStopSalesPromotion = $IdStopSalesPromotion;
      $this->DateFrom = $DateFrom->format(\DateTime::ATOM);
      $this->DateTo = $DateTo->format(\DateTime::ATOM);
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'Action' => $this->getAction(),
        'IdStopSalesPromotion' => $this->getIdStopSalesPromotion(),
        'DateFrom' => $this->getDateFrom(),
        'DateTo' => $this->getDateTo(),
      );
    }

    /**
     * @return Action
     */
    public function getAction()
    {
      return $this->Action;
    }

    /**
     * @param Action $Action
     * @return \Dingus\SyncroService\StopSalesPromotionRec
     */
    public function setAction($Action)
    {
      $this->Action = $Action;
      return $this;
    }

    /**
     * @return int
     */
    public function getIdStopSalesPromotion()
    {
      return $this->IdStopSalesPromotion;
    }

    /**
     * @param int $IdStopSalesPromotion
     * @return \Dingus\SyncroService\StopSalesPromotionRec
     */
    public function setIdStopSalesPromotion($IdStopSalesPromotion)
    {
      $this->IdStopSalesPromotion = $IdStopSalesPromotion;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateFrom()
    {
      if ($this->DateFrom == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->DateFrom);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $DateFrom
     * @return \Dingus\SyncroService\StopSalesPromotionRec
     */
    public function setDateFrom(\DateTime $DateFrom)
    {
      $this->DateFrom = $DateFrom->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateTo()
    {
      if ($this->DateTo == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->DateTo);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $DateTo
     * @return \Dingus\SyncroService\StopSalesPromotionRec
     */
    public function setDateTo(\DateTime $DateTo)
    {
      $this->DateTo = $DateTo->format(\DateTime::ATOM);
      return $this;
    }

}
