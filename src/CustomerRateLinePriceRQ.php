<?php

namespace Dingus\SyncroService;

class CustomerRateLinePriceRQ implements \JsonSerializable
{

    /**
     * @var int $IdCustomerRate
     */
    protected $IdCustomerRate = null;

    /**
     * @var int $IdCustomerRateLine
     */
    protected $IdCustomerRateLine = null;

    /**
     * @var \DateTime $DateFrom
     */
    protected $DateFrom = null;

    /**
     * @var \DateTime $DateTo
     */
    protected $DateTo = null;

    /**
     * @var boolean $FilterIdCustomerRateLine
     */
    protected $FilterIdCustomerRateLine = null;

    /**
     * @var boolean $FilterDates
     */
    protected $FilterDates = null;

    /**
     * @param int $IdCustomerRate
     * @param int $IdCustomerRateLine
     * @param \DateTime $DateFrom
     * @param \DateTime $DateTo
     * @param boolean $FilterIdCustomerRateLine
     * @param boolean $FilterDates
     */
    public function __construct($IdCustomerRate, $IdCustomerRateLine, \DateTime $DateFrom, \DateTime $DateTo, $FilterIdCustomerRateLine, $FilterDates)
    {
      $this->IdCustomerRate = $IdCustomerRate;
      $this->IdCustomerRateLine = $IdCustomerRateLine;
      $this->DateFrom = $DateFrom->format(\DateTime::ATOM);
      $this->DateTo = $DateTo->format(\DateTime::ATOM);
      $this->FilterIdCustomerRateLine = $FilterIdCustomerRateLine;
      $this->FilterDates = $FilterDates;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'IdCustomerRate' => $this->getIdCustomerRate(),
        'IdCustomerRateLine' => $this->getIdCustomerRateLine(),
        'DateFrom' => $this->getDateFrom(),
        'DateTo' => $this->getDateTo(),
        'FilterIdCustomerRateLine' => $this->getFilterIdCustomerRateLine(),
        'FilterDates' => $this->getFilterDates(),
      );
    }

    /**
     * @return int
     */
    public function getIdCustomerRate()
    {
      return $this->IdCustomerRate;
    }

    /**
     * @param int $IdCustomerRate
     * @return \Dingus\SyncroService\CustomerRateLinePriceRQ
     */
    public function setIdCustomerRate($IdCustomerRate)
    {
      $this->IdCustomerRate = $IdCustomerRate;
      return $this;
    }

    /**
     * @return int
     */
    public function getIdCustomerRateLine()
    {
      return $this->IdCustomerRateLine;
    }

    /**
     * @param int $IdCustomerRateLine
     * @return \Dingus\SyncroService\CustomerRateLinePriceRQ
     */
    public function setIdCustomerRateLine($IdCustomerRateLine)
    {
      $this->IdCustomerRateLine = $IdCustomerRateLine;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateFrom()
    {
      if ($this->DateFrom == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->DateFrom);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $DateFrom
     * @return \Dingus\SyncroService\CustomerRateLinePriceRQ
     */
    public function setDateFrom(\DateTime $DateFrom)
    {
      $this->DateFrom = $DateFrom->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateTo()
    {
      if ($this->DateTo == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->DateTo);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $DateTo
     * @return \Dingus\SyncroService\CustomerRateLinePriceRQ
     */
    public function setDateTo(\DateTime $DateTo)
    {
      $this->DateTo = $DateTo->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return boolean
     */
    public function getFilterIdCustomerRateLine()
    {
      return $this->FilterIdCustomerRateLine;
    }

    /**
     * @param boolean $FilterIdCustomerRateLine
     * @return \Dingus\SyncroService\CustomerRateLinePriceRQ
     */
    public function setFilterIdCustomerRateLine($FilterIdCustomerRateLine)
    {
      $this->FilterIdCustomerRateLine = $FilterIdCustomerRateLine;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getFilterDates()
    {
      return $this->FilterDates;
    }

    /**
     * @param boolean $FilterDates
     * @return \Dingus\SyncroService\CustomerRateLinePriceRQ
     */
    public function setFilterDates($FilterDates)
    {
      $this->FilterDates = $FilterDates;
      return $this;
    }

}
