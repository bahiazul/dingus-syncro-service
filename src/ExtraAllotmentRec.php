<?php

namespace Dingus\SyncroService;

class ExtraAllotmentRec implements \JsonSerializable
{

    /**
     * @var Action $Action
     */
    protected $Action = null;

    /**
     * @var \DateTime $DateFrom
     */
    protected $DateFrom = null;

    /**
     * @var \DateTime $DateTo
     */
    protected $DateTo = null;

    /**
     * @var string $PMSCode
     */
    protected $PMSCode = null;

    /**
     * @var int $Quantity
     */
    protected $Quantity = null;

    /**
     * @var string $ExtraCode
     */
    protected $ExtraCode = null;

    /**
     * @var boolean $Closed
     */
    protected $Closed = null;

    /**
     * @param Action $Action
     * @param \DateTime $DateFrom
     * @param \DateTime $DateTo
     * @param int $Quantity
     * @param boolean $Closed
     */
    public function __construct($Action, \DateTime $DateFrom, \DateTime $DateTo, $Quantity, $Closed)
    {
      $this->Action = $Action;
      $this->DateFrom = $DateFrom->format(\DateTime::ATOM);
      $this->DateTo = $DateTo->format(\DateTime::ATOM);
      $this->Quantity = $Quantity;
      $this->Closed = $Closed;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'Action' => $this->getAction(),
        'DateFrom' => $this->getDateFrom(),
        'DateTo' => $this->getDateTo(),
        'PMSCode' => $this->getPMSCode(),
        'Quantity' => $this->getQuantity(),
        'ExtraCode' => $this->getExtraCode(),
        'Closed' => $this->getClosed(),
      );
    }

    /**
     * @return Action
     */
    public function getAction()
    {
      return $this->Action;
    }

    /**
     * @param Action $Action
     * @return \Dingus\SyncroService\ExtraAllotmentRec
     */
    public function setAction($Action)
    {
      $this->Action = $Action;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateFrom()
    {
      if ($this->DateFrom == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->DateFrom);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $DateFrom
     * @return \Dingus\SyncroService\ExtraAllotmentRec
     */
    public function setDateFrom(\DateTime $DateFrom)
    {
      $this->DateFrom = $DateFrom->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateTo()
    {
      if ($this->DateTo == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->DateTo);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $DateTo
     * @return \Dingus\SyncroService\ExtraAllotmentRec
     */
    public function setDateTo(\DateTime $DateTo)
    {
      $this->DateTo = $DateTo->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return string
     */
    public function getPMSCode()
    {
      return $this->PMSCode;
    }

    /**
     * @param string $PMSCode
     * @return \Dingus\SyncroService\ExtraAllotmentRec
     */
    public function setPMSCode($PMSCode)
    {
      $this->PMSCode = $PMSCode;
      return $this;
    }

    /**
     * @return int
     */
    public function getQuantity()
    {
      return $this->Quantity;
    }

    /**
     * @param int $Quantity
     * @return \Dingus\SyncroService\ExtraAllotmentRec
     */
    public function setQuantity($Quantity)
    {
      $this->Quantity = $Quantity;
      return $this;
    }

    /**
     * @return string
     */
    public function getExtraCode()
    {
      return $this->ExtraCode;
    }

    /**
     * @param string $ExtraCode
     * @return \Dingus\SyncroService\ExtraAllotmentRec
     */
    public function setExtraCode($ExtraCode)
    {
      $this->ExtraCode = $ExtraCode;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getClosed()
    {
      return $this->Closed;
    }

    /**
     * @param boolean $Closed
     * @return \Dingus\SyncroService\ExtraAllotmentRec
     */
    public function setClosed($Closed)
    {
      $this->Closed = $Closed;
      return $this;
    }

}
