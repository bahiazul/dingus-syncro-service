<?php

namespace Dingus\SyncroService;

class Payment implements \JsonSerializable
{

    /**
     * @var int $PaymentID
     */
    protected $PaymentID = null;

    /**
     * @var \DateTime $PaymentDate
     */
    protected $PaymentDate = null;

    /**
     * @var float $PaymentAmount
     */
    protected $PaymentAmount = null;

    /**
     * @var string $PaymentCurrency
     */
    protected $PaymentCurrency = null;

    /**
     * @var string $PaymentConfirmation
     */
    protected $PaymentConfirmation = null;

    /**
     * @var string $PaymentForm
     */
    protected $PaymentForm = null;

    /**
     * @param int $PaymentID
     * @param \DateTime $PaymentDate
     * @param float $PaymentAmount
     */
    public function __construct($PaymentID, \DateTime $PaymentDate, $PaymentAmount)
    {
      $this->PaymentID = $PaymentID;
      $this->PaymentDate = $PaymentDate->format(\DateTime::ATOM);
      $this->PaymentAmount = $PaymentAmount;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'PaymentID' => $this->getPaymentID(),
        'PaymentDate' => $this->getPaymentDate(),
        'PaymentAmount' => $this->getPaymentAmount(),
        'PaymentCurrency' => $this->getPaymentCurrency(),
        'PaymentConfirmation' => $this->getPaymentConfirmation(),
        'PaymentForm' => $this->getPaymentForm(),
      );
    }

    /**
     * @return int
     */
    public function getPaymentID()
    {
      return $this->PaymentID;
    }

    /**
     * @param int $PaymentID
     * @return \Dingus\SyncroService\Payment
     */
    public function setPaymentID($PaymentID)
    {
      $this->PaymentID = $PaymentID;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getPaymentDate()
    {
      if ($this->PaymentDate == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->PaymentDate);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $PaymentDate
     * @return \Dingus\SyncroService\Payment
     */
    public function setPaymentDate(\DateTime $PaymentDate)
    {
      $this->PaymentDate = $PaymentDate->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return float
     */
    public function getPaymentAmount()
    {
      return $this->PaymentAmount;
    }

    /**
     * @param float $PaymentAmount
     * @return \Dingus\SyncroService\Payment
     */
    public function setPaymentAmount($PaymentAmount)
    {
      $this->PaymentAmount = $PaymentAmount;
      return $this;
    }

    /**
     * @return string
     */
    public function getPaymentCurrency()
    {
      return $this->PaymentCurrency;
    }

    /**
     * @param string $PaymentCurrency
     * @return \Dingus\SyncroService\Payment
     */
    public function setPaymentCurrency($PaymentCurrency)
    {
      $this->PaymentCurrency = $PaymentCurrency;
      return $this;
    }

    /**
     * @return string
     */
    public function getPaymentConfirmation()
    {
      return $this->PaymentConfirmation;
    }

    /**
     * @param string $PaymentConfirmation
     * @return \Dingus\SyncroService\Payment
     */
    public function setPaymentConfirmation($PaymentConfirmation)
    {
      $this->PaymentConfirmation = $PaymentConfirmation;
      return $this;
    }

    /**
     * @return string
     */
    public function getPaymentForm()
    {
      return $this->PaymentForm;
    }

    /**
     * @param string $PaymentForm
     * @return \Dingus\SyncroService\Payment
     */
    public function setPaymentForm($PaymentForm)
    {
      $this->PaymentForm = $PaymentForm;
      return $this;
    }

}
