<?php

namespace Dingus\SyncroService;

class Comission implements \JsonSerializable
{

    /**
     * @var TipoCalculoTarifaHotel $ComissionType
     */
    protected $ComissionType = null;

    /**
     * @var float $Percent
     */
    protected $Percent = null;

    /**
     * @param TipoCalculoTarifaHotel $ComissionType
     * @param float $Percent
     */
    public function __construct($ComissionType, $Percent)
    {
      $this->ComissionType = $ComissionType;
      $this->Percent = $Percent;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'ComissionType' => $this->getComissionType(),
        'Percent' => $this->getPercent(),
      );
    }

    /**
     * @return TipoCalculoTarifaHotel
     */
    public function getComissionType()
    {
      return $this->ComissionType;
    }

    /**
     * @param TipoCalculoTarifaHotel $ComissionType
     * @return \Dingus\SyncroService\Comission
     */
    public function setComissionType($ComissionType)
    {
      $this->ComissionType = $ComissionType;
      return $this;
    }

    /**
     * @return float
     */
    public function getPercent()
    {
      return $this->Percent;
    }

    /**
     * @param float $Percent
     * @return \Dingus\SyncroService\Comission
     */
    public function setPercent($Percent)
    {
      $this->Percent = $Percent;
      return $this;
    }

}
