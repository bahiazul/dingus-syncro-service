<?php

namespace Dingus\SyncroService;

class PutPromotionalCodesByQuantityResponse implements \JsonSerializable
{

    /**
     * @var SyncroRS $PutPromotionalCodesByQuantityResult
     */
    protected $PutPromotionalCodesByQuantityResult = null;

    /**
     * @param SyncroRS $PutPromotionalCodesByQuantityResult
     */
    public function __construct($PutPromotionalCodesByQuantityResult)
    {
      $this->PutPromotionalCodesByQuantityResult = $PutPromotionalCodesByQuantityResult;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'PutPromotionalCodesByQuantityResult' => $this->getPutPromotionalCodesByQuantityResult(),
      );
    }

    /**
     * @return SyncroRS
     */
    public function getPutPromotionalCodesByQuantityResult()
    {
      return $this->PutPromotionalCodesByQuantityResult;
    }

    /**
     * @param SyncroRS $PutPromotionalCodesByQuantityResult
     * @return \Dingus\SyncroService\PutPromotionalCodesByQuantityResponse
     */
    public function setPutPromotionalCodesByQuantityResult($PutPromotionalCodesByQuantityResult)
    {
      $this->PutPromotionalCodesByQuantityResult = $PutPromotionalCodesByQuantityResult;
      return $this;
    }

}
