<?php

namespace Dingus\SyncroService;

class PutAllotmentResponse implements \JsonSerializable
{

    /**
     * @var SyncroRS $PutAllotmentResult
     */
    protected $PutAllotmentResult = null;

    /**
     * @param SyncroRS $PutAllotmentResult
     */
    public function __construct($PutAllotmentResult)
    {
      $this->PutAllotmentResult = $PutAllotmentResult;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'PutAllotmentResult' => $this->getPutAllotmentResult(),
      );
    }

    /**
     * @return SyncroRS
     */
    public function getPutAllotmentResult()
    {
      return $this->PutAllotmentResult;
    }

    /**
     * @param SyncroRS $PutAllotmentResult
     * @return \Dingus\SyncroService\PutAllotmentResponse
     */
    public function setPutAllotmentResult($PutAllotmentResult)
    {
      $this->PutAllotmentResult = $PutAllotmentResult;
      return $this;
    }

}
