<?php

namespace Dingus\SyncroService;

class Data implements \JsonSerializable
{

    /**
     * @var string $Code
     */
    protected $Code = null;

    /**
     * @var string $Value
     */
    protected $Value = null;

    /**
     * @var boolean $IsEnc
     */
    protected $IsEnc = null;

    /**
     * @var boolean $EncBookingData
     */
    protected $EncBookingData = null;

    /**
     * @var TipoEncriptacion $EncType
     */
    protected $EncType = null;

    /**
     * @param boolean $IsEnc
     * @param boolean $EncBookingData
     * @param TipoEncriptacion $EncType
     */
    public function __construct($IsEnc, $EncBookingData, $EncType)
    {
      $this->IsEnc = $IsEnc;
      $this->EncBookingData = $EncBookingData;
      $this->EncType = $EncType;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'Code' => $this->getCode(),
        'Value' => $this->getValue(),
        'IsEnc' => $this->getIsEnc(),
        'EncBookingData' => $this->getEncBookingData(),
        'EncType' => $this->getEncType(),
      );
    }

    /**
     * @return string
     */
    public function getCode()
    {
      return $this->Code;
    }

    /**
     * @param string $Code
     * @return \Dingus\SyncroService\Data
     */
    public function setCode($Code)
    {
      $this->Code = $Code;
      return $this;
    }

    /**
     * @return string
     */
    public function getValue()
    {
      return $this->Value;
    }

    /**
     * @param string $Value
     * @return \Dingus\SyncroService\Data
     */
    public function setValue($Value)
    {
      $this->Value = $Value;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getIsEnc()
    {
      return $this->IsEnc;
    }

    /**
     * @param boolean $IsEnc
     * @return \Dingus\SyncroService\Data
     */
    public function setIsEnc($IsEnc)
    {
      $this->IsEnc = $IsEnc;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getEncBookingData()
    {
      return $this->EncBookingData;
    }

    /**
     * @param boolean $EncBookingData
     * @return \Dingus\SyncroService\Data
     */
    public function setEncBookingData($EncBookingData)
    {
      $this->EncBookingData = $EncBookingData;
      return $this;
    }

    /**
     * @return TipoEncriptacion
     */
    public function getEncType()
    {
      return $this->EncType;
    }

    /**
     * @param TipoEncriptacion $EncType
     * @return \Dingus\SyncroService\Data
     */
    public function setEncType($EncType)
    {
      $this->EncType = $EncType;
      return $this;
    }

}
