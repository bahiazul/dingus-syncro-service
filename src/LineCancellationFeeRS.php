<?php

namespace Dingus\SyncroService;

class LineCancellationFeeRS implements \JsonSerializable
{

    /**
     * @var ArrayOfMlText $CancellationFeeLineDescriptionMlList
     */
    protected $CancellationFeeLineDescriptionMlList = null;

    /**
     * @var Action $Action
     */
    protected $Action = null;

    /**
     * @var int $Id
     */
    protected $Id = null;

    /**
     * @var \DateTime $DateFrom
     */
    protected $DateFrom = null;

    /**
     * @var \DateTime $DateTo
     */
    protected $DateTo = null;

    /**
     * @var int $HoursLimitCancellationFee
     */
    protected $HoursLimitCancellationFee = null;

    /**
     * @var float $ImportCancellationFee
     */
    protected $ImportCancellationFee = null;

    /**
     * @var TipoCalculoGastoCancelacion $CalcTypeCancellationFee
     */
    protected $CalcTypeCancellationFee = null;

    /**
     * @var boolean $NoRefundable
     */
    protected $NoRefundable = null;

    /**
     * @var boolean $ModifyCancellationFee
     */
    protected $ModifyCancellationFee = null;

    /**
     * @var float $ImportModifyCancellationFee
     */
    protected $ImportModifyCancellationFee = null;

    /**
     * @var string $CancellationFeeLineDescription
     */
    protected $CancellationFeeLineDescription = null;

    /**
     * @param Action $Action
     * @param int $Id
     * @param \DateTime $DateFrom
     * @param \DateTime $DateTo
     * @param int $HoursLimitCancellationFee
     * @param float $ImportCancellationFee
     * @param TipoCalculoGastoCancelacion $CalcTypeCancellationFee
     * @param boolean $NoRefundable
     * @param boolean $ModifyCancellationFee
     * @param float $ImportModifyCancellationFee
     */
    public function __construct($Action, $Id, \DateTime $DateFrom, \DateTime $DateTo, $HoursLimitCancellationFee, $ImportCancellationFee, $CalcTypeCancellationFee, $NoRefundable, $ModifyCancellationFee, $ImportModifyCancellationFee)
    {
      $this->Action = $Action;
      $this->Id = $Id;
      $this->DateFrom = $DateFrom->format(\DateTime::ATOM);
      $this->DateTo = $DateTo->format(\DateTime::ATOM);
      $this->HoursLimitCancellationFee = $HoursLimitCancellationFee;
      $this->ImportCancellationFee = $ImportCancellationFee;
      $this->CalcTypeCancellationFee = $CalcTypeCancellationFee;
      $this->NoRefundable = $NoRefundable;
      $this->ModifyCancellationFee = $ModifyCancellationFee;
      $this->ImportModifyCancellationFee = $ImportModifyCancellationFee;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'CancellationFeeLineDescriptionMlList' => $this->getCancellationFeeLineDescriptionMlList(),
        'Action' => $this->getAction(),
        'Id' => $this->getId(),
        'DateFrom' => $this->getDateFrom(),
        'DateTo' => $this->getDateTo(),
        'HoursLimitCancellationFee' => $this->getHoursLimitCancellationFee(),
        'ImportCancellationFee' => $this->getImportCancellationFee(),
        'CalcTypeCancellationFee' => $this->getCalcTypeCancellationFee(),
        'NoRefundable' => $this->getNoRefundable(),
        'ModifyCancellationFee' => $this->getModifyCancellationFee(),
        'ImportModifyCancellationFee' => $this->getImportModifyCancellationFee(),
        'CancellationFeeLineDescription' => $this->getCancellationFeeLineDescription(),
      );
    }

    /**
     * @return ArrayOfMlText
     */
    public function getCancellationFeeLineDescriptionMlList()
    {
      return $this->CancellationFeeLineDescriptionMlList;
    }

    /**
     * @param ArrayOfMlText $CancellationFeeLineDescriptionMlList
     * @return \Dingus\SyncroService\LineCancellationFeeRS
     */
    public function setCancellationFeeLineDescriptionMlList($CancellationFeeLineDescriptionMlList)
    {
      $this->CancellationFeeLineDescriptionMlList = $CancellationFeeLineDescriptionMlList;
      return $this;
    }

    /**
     * @return Action
     */
    public function getAction()
    {
      return $this->Action;
    }

    /**
     * @param Action $Action
     * @return \Dingus\SyncroService\LineCancellationFeeRS
     */
    public function setAction($Action)
    {
      $this->Action = $Action;
      return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
      return $this->Id;
    }

    /**
     * @param int $Id
     * @return \Dingus\SyncroService\LineCancellationFeeRS
     */
    public function setId($Id)
    {
      $this->Id = $Id;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateFrom()
    {
      if ($this->DateFrom == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->DateFrom);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $DateFrom
     * @return \Dingus\SyncroService\LineCancellationFeeRS
     */
    public function setDateFrom(\DateTime $DateFrom)
    {
      $this->DateFrom = $DateFrom->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateTo()
    {
      if ($this->DateTo == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->DateTo);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $DateTo
     * @return \Dingus\SyncroService\LineCancellationFeeRS
     */
    public function setDateTo(\DateTime $DateTo)
    {
      $this->DateTo = $DateTo->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return int
     */
    public function getHoursLimitCancellationFee()
    {
      return $this->HoursLimitCancellationFee;
    }

    /**
     * @param int $HoursLimitCancellationFee
     * @return \Dingus\SyncroService\LineCancellationFeeRS
     */
    public function setHoursLimitCancellationFee($HoursLimitCancellationFee)
    {
      $this->HoursLimitCancellationFee = $HoursLimitCancellationFee;
      return $this;
    }

    /**
     * @return float
     */
    public function getImportCancellationFee()
    {
      return $this->ImportCancellationFee;
    }

    /**
     * @param float $ImportCancellationFee
     * @return \Dingus\SyncroService\LineCancellationFeeRS
     */
    public function setImportCancellationFee($ImportCancellationFee)
    {
      $this->ImportCancellationFee = $ImportCancellationFee;
      return $this;
    }

    /**
     * @return TipoCalculoGastoCancelacion
     */
    public function getCalcTypeCancellationFee()
    {
      return $this->CalcTypeCancellationFee;
    }

    /**
     * @param TipoCalculoGastoCancelacion $CalcTypeCancellationFee
     * @return \Dingus\SyncroService\LineCancellationFeeRS
     */
    public function setCalcTypeCancellationFee($CalcTypeCancellationFee)
    {
      $this->CalcTypeCancellationFee = $CalcTypeCancellationFee;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getNoRefundable()
    {
      return $this->NoRefundable;
    }

    /**
     * @param boolean $NoRefundable
     * @return \Dingus\SyncroService\LineCancellationFeeRS
     */
    public function setNoRefundable($NoRefundable)
    {
      $this->NoRefundable = $NoRefundable;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getModifyCancellationFee()
    {
      return $this->ModifyCancellationFee;
    }

    /**
     * @param boolean $ModifyCancellationFee
     * @return \Dingus\SyncroService\LineCancellationFeeRS
     */
    public function setModifyCancellationFee($ModifyCancellationFee)
    {
      $this->ModifyCancellationFee = $ModifyCancellationFee;
      return $this;
    }

    /**
     * @return float
     */
    public function getImportModifyCancellationFee()
    {
      return $this->ImportModifyCancellationFee;
    }

    /**
     * @param float $ImportModifyCancellationFee
     * @return \Dingus\SyncroService\LineCancellationFeeRS
     */
    public function setImportModifyCancellationFee($ImportModifyCancellationFee)
    {
      $this->ImportModifyCancellationFee = $ImportModifyCancellationFee;
      return $this;
    }

    /**
     * @return string
     */
    public function getCancellationFeeLineDescription()
    {
      return $this->CancellationFeeLineDescription;
    }

    /**
     * @param string $CancellationFeeLineDescription
     * @return \Dingus\SyncroService\LineCancellationFeeRS
     */
    public function setCancellationFeeLineDescription($CancellationFeeLineDescription)
    {
      $this->CancellationFeeLineDescription = $CancellationFeeLineDescription;
      return $this;
    }

}
