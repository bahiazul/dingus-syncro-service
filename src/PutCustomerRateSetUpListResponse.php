<?php

namespace Dingus\SyncroService;

class PutCustomerRateSetUpListResponse implements \JsonSerializable
{

    /**
     * @var SyncroRS $PutCustomerRateSetUpListResult
     */
    protected $PutCustomerRateSetUpListResult = null;

    /**
     * @param SyncroRS $PutCustomerRateSetUpListResult
     */
    public function __construct($PutCustomerRateSetUpListResult)
    {
      $this->PutCustomerRateSetUpListResult = $PutCustomerRateSetUpListResult;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'PutCustomerRateSetUpListResult' => $this->getPutCustomerRateSetUpListResult(),
      );
    }

    /**
     * @return SyncroRS
     */
    public function getPutCustomerRateSetUpListResult()
    {
      return $this->PutCustomerRateSetUpListResult;
    }

    /**
     * @param SyncroRS $PutCustomerRateSetUpListResult
     * @return \Dingus\SyncroService\PutCustomerRateSetUpListResponse
     */
    public function setPutCustomerRateSetUpListResult($PutCustomerRateSetUpListResult)
    {
      $this->PutCustomerRateSetUpListResult = $PutCustomerRateSetUpListResult;
      return $this;
    }

}
