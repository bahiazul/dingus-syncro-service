<?php

namespace Dingus\SyncroService;

class CustomersPriceRules implements \JsonSerializable
{

    /**
     * @var Action $Action
     */
    protected $Action = null;

    /**
     * @var int $Id
     */
    protected $Id = null;

    /**
     * @var string $CustomerGroupCode
     */
    protected $CustomerGroupCode = null;

    /**
     * @var string $CustomerGroupName
     */
    protected $CustomerGroupName = null;

    /**
     * @param Action $Action
     * @param int $Id
     */
    public function __construct($Action, $Id)
    {
      $this->Action = $Action;
      $this->Id = $Id;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'Action' => $this->getAction(),
        'Id' => $this->getId(),
        'CustomerGroupCode' => $this->getCustomerGroupCode(),
        'CustomerGroupName' => $this->getCustomerGroupName(),
      );
    }

    /**
     * @return Action
     */
    public function getAction()
    {
      return $this->Action;
    }

    /**
     * @param Action $Action
     * @return \Dingus\SyncroService\CustomersPriceRules
     */
    public function setAction($Action)
    {
      $this->Action = $Action;
      return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
      return $this->Id;
    }

    /**
     * @param int $Id
     * @return \Dingus\SyncroService\CustomersPriceRules
     */
    public function setId($Id)
    {
      $this->Id = $Id;
      return $this;
    }

    /**
     * @return string
     */
    public function getCustomerGroupCode()
    {
      return $this->CustomerGroupCode;
    }

    /**
     * @param string $CustomerGroupCode
     * @return \Dingus\SyncroService\CustomersPriceRules
     */
    public function setCustomerGroupCode($CustomerGroupCode)
    {
      $this->CustomerGroupCode = $CustomerGroupCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getCustomerGroupName()
    {
      return $this->CustomerGroupName;
    }

    /**
     * @param string $CustomerGroupName
     * @return \Dingus\SyncroService\CustomersPriceRules
     */
    public function setCustomerGroupName($CustomerGroupName)
    {
      $this->CustomerGroupName = $CustomerGroupName;
      return $this;
    }

}
