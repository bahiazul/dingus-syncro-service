<?php

namespace Dingus\SyncroService;

class PutStopSales implements \JsonSerializable
{

    /**
     * @var Credentials $Credentials
     */
    protected $Credentials = null;

    /**
     * @var PutStopSalesRQ $PutStopSalesRQ
     */
    protected $PutStopSalesRQ = null;

    /**
     * @param Credentials $Credentials
     * @param PutStopSalesRQ $PutStopSalesRQ
     */
    public function __construct($Credentials, $PutStopSalesRQ)
    {
      $this->Credentials = $Credentials;
      $this->PutStopSalesRQ = $PutStopSalesRQ;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'Credentials' => $this->getCredentials(),
        'PutStopSalesRQ' => $this->getPutStopSalesRQ(),
      );
    }

    /**
     * @return Credentials
     */
    public function getCredentials()
    {
      return $this->Credentials;
    }

    /**
     * @param Credentials $Credentials
     * @return \Dingus\SyncroService\PutStopSales
     */
    public function setCredentials($Credentials)
    {
      $this->Credentials = $Credentials;
      return $this;
    }

    /**
     * @return PutStopSalesRQ
     */
    public function getPutStopSalesRQ()
    {
      return $this->PutStopSalesRQ;
    }

    /**
     * @param PutStopSalesRQ $PutStopSalesRQ
     * @return \Dingus\SyncroService\PutStopSales
     */
    public function setPutStopSalesRQ($PutStopSalesRQ)
    {
      $this->PutStopSalesRQ = $PutStopSalesRQ;
      return $this;
    }

}
