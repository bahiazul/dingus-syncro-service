<?php

namespace Dingus\SyncroService;

class SyncroBufferRS implements \JsonSerializable
{

    /**
     * @var Action $Action
     */
    protected $Action = null;

    /**
     * @var int $IdBuffer
     */
    protected $IdBuffer = null;

    /**
     * @var TipoBuffer $BufferType
     */
    protected $BufferType = null;

    /**
     * @var string $CustomerCodeBuffer
     */
    protected $CustomerCodeBuffer = null;

    /**
     * @var string $HotelCodeBuffer
     */
    protected $HotelCodeBuffer = null;

    /**
     * @var \DateTime $DateBuffer
     */
    protected $DateBuffer = null;

    /**
     * @var \DateTime $HourBuffer
     */
    protected $HourBuffer = null;

    /**
     * @var string $LocalizerBuffer
     */
    protected $LocalizerBuffer = null;

    /**
     * @var string $XmlBuffer
     */
    protected $XmlBuffer = null;

    /**
     * @var string $ErrorBuffer
     */
    protected $ErrorBuffer = null;

    /**
     * @var boolean $ProcessedBuffer
     */
    protected $ProcessedBuffer = null;

    /**
     * @var boolean $PreConectBuffer
     */
    protected $PreConectBuffer = null;

    /**
     * @var boolean $SendBuffer
     */
    protected $SendBuffer = null;

    /**
     * @var boolean $ConfirmedBuffer
     */
    protected $ConfirmedBuffer = null;

    /**
     * @var string $TaskBuffer
     */
    protected $TaskBuffer = null;

    /**
     * @var boolean $InProcessBuffer
     */
    protected $InProcessBuffer = null;

    /**
     * @var string $ErrorConfirmBuffer
     */
    protected $ErrorConfirmBuffer = null;

    /**
     * @var \DateTime $DateEntryBuffer
     */
    protected $DateEntryBuffer = null;

    /**
     * @var \DateTime $DateErrorBuffer
     */
    protected $DateErrorBuffer = null;

    /**
     * @var string $ResponsibleBuffer
     */
    protected $ResponsibleBuffer = null;

    /**
     * @var boolean $CheckedBuffer
     */
    protected $CheckedBuffer = null;

    /**
     * @var boolean $PendingToCustomerBuffer
     */
    protected $PendingToCustomerBuffer = null;

    /**
     * @var boolean $NotifOKBuffer
     */
    protected $NotifOKBuffer = null;

    /**
     * @param Action $Action
     * @param int $IdBuffer
     * @param TipoBuffer $BufferType
     * @param \DateTime $DateBuffer
     * @param \DateTime $HourBuffer
     * @param boolean $ProcessedBuffer
     * @param boolean $PreConectBuffer
     * @param boolean $SendBuffer
     * @param boolean $ConfirmedBuffer
     * @param boolean $InProcessBuffer
     * @param \DateTime $DateEntryBuffer
     * @param \DateTime $DateErrorBuffer
     * @param boolean $CheckedBuffer
     * @param boolean $PendingToCustomerBuffer
     * @param boolean $NotifOKBuffer
     */
    public function __construct($Action, $IdBuffer, $BufferType, \DateTime $DateBuffer, \DateTime $HourBuffer, $ProcessedBuffer, $PreConectBuffer, $SendBuffer, $ConfirmedBuffer, $InProcessBuffer, \DateTime $DateEntryBuffer, \DateTime $DateErrorBuffer, $CheckedBuffer, $PendingToCustomerBuffer, $NotifOKBuffer)
    {
      $this->Action = $Action;
      $this->IdBuffer = $IdBuffer;
      $this->BufferType = $BufferType;
      $this->DateBuffer = $DateBuffer->format(\DateTime::ATOM);
      $this->HourBuffer = $HourBuffer->format(\DateTime::ATOM);
      $this->ProcessedBuffer = $ProcessedBuffer;
      $this->PreConectBuffer = $PreConectBuffer;
      $this->SendBuffer = $SendBuffer;
      $this->ConfirmedBuffer = $ConfirmedBuffer;
      $this->InProcessBuffer = $InProcessBuffer;
      $this->DateEntryBuffer = $DateEntryBuffer->format(\DateTime::ATOM);
      $this->DateErrorBuffer = $DateErrorBuffer->format(\DateTime::ATOM);
      $this->CheckedBuffer = $CheckedBuffer;
      $this->PendingToCustomerBuffer = $PendingToCustomerBuffer;
      $this->NotifOKBuffer = $NotifOKBuffer;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'Action' => $this->getAction(),
        'IdBuffer' => $this->getIdBuffer(),
        'BufferType' => $this->getBufferType(),
        'CustomerCodeBuffer' => $this->getCustomerCodeBuffer(),
        'HotelCodeBuffer' => $this->getHotelCodeBuffer(),
        'DateBuffer' => $this->getDateBuffer(),
        'HourBuffer' => $this->getHourBuffer(),
        'LocalizerBuffer' => $this->getLocalizerBuffer(),
        'XmlBuffer' => $this->getXmlBuffer(),
        'ErrorBuffer' => $this->getErrorBuffer(),
        'ProcessedBuffer' => $this->getProcessedBuffer(),
        'PreConectBuffer' => $this->getPreConectBuffer(),
        'SendBuffer' => $this->getSendBuffer(),
        'ConfirmedBuffer' => $this->getConfirmedBuffer(),
        'TaskBuffer' => $this->getTaskBuffer(),
        'InProcessBuffer' => $this->getInProcessBuffer(),
        'ErrorConfirmBuffer' => $this->getErrorConfirmBuffer(),
        'DateEntryBuffer' => $this->getDateEntryBuffer(),
        'DateErrorBuffer' => $this->getDateErrorBuffer(),
        'ResponsibleBuffer' => $this->getResponsibleBuffer(),
        'CheckedBuffer' => $this->getCheckedBuffer(),
        'PendingToCustomerBuffer' => $this->getPendingToCustomerBuffer(),
        'NotifOKBuffer' => $this->getNotifOKBuffer(),
      );
    }

    /**
     * @return Action
     */
    public function getAction()
    {
      return $this->Action;
    }

    /**
     * @param Action $Action
     * @return \Dingus\SyncroService\SyncroBufferRS
     */
    public function setAction($Action)
    {
      $this->Action = $Action;
      return $this;
    }

    /**
     * @return int
     */
    public function getIdBuffer()
    {
      return $this->IdBuffer;
    }

    /**
     * @param int $IdBuffer
     * @return \Dingus\SyncroService\SyncroBufferRS
     */
    public function setIdBuffer($IdBuffer)
    {
      $this->IdBuffer = $IdBuffer;
      return $this;
    }

    /**
     * @return TipoBuffer
     */
    public function getBufferType()
    {
      return $this->BufferType;
    }

    /**
     * @param TipoBuffer $BufferType
     * @return \Dingus\SyncroService\SyncroBufferRS
     */
    public function setBufferType($BufferType)
    {
      $this->BufferType = $BufferType;
      return $this;
    }

    /**
     * @return string
     */
    public function getCustomerCodeBuffer()
    {
      return $this->CustomerCodeBuffer;
    }

    /**
     * @param string $CustomerCodeBuffer
     * @return \Dingus\SyncroService\SyncroBufferRS
     */
    public function setCustomerCodeBuffer($CustomerCodeBuffer)
    {
      $this->CustomerCodeBuffer = $CustomerCodeBuffer;
      return $this;
    }

    /**
     * @return string
     */
    public function getHotelCodeBuffer()
    {
      return $this->HotelCodeBuffer;
    }

    /**
     * @param string $HotelCodeBuffer
     * @return \Dingus\SyncroService\SyncroBufferRS
     */
    public function setHotelCodeBuffer($HotelCodeBuffer)
    {
      $this->HotelCodeBuffer = $HotelCodeBuffer;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateBuffer()
    {
      if ($this->DateBuffer == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->DateBuffer);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $DateBuffer
     * @return \Dingus\SyncroService\SyncroBufferRS
     */
    public function setDateBuffer(\DateTime $DateBuffer)
    {
      $this->DateBuffer = $DateBuffer->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getHourBuffer()
    {
      if ($this->HourBuffer == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->HourBuffer);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $HourBuffer
     * @return \Dingus\SyncroService\SyncroBufferRS
     */
    public function setHourBuffer(\DateTime $HourBuffer)
    {
      $this->HourBuffer = $HourBuffer->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return string
     */
    public function getLocalizerBuffer()
    {
      return $this->LocalizerBuffer;
    }

    /**
     * @param string $LocalizerBuffer
     * @return \Dingus\SyncroService\SyncroBufferRS
     */
    public function setLocalizerBuffer($LocalizerBuffer)
    {
      $this->LocalizerBuffer = $LocalizerBuffer;
      return $this;
    }

    /**
     * @return string
     */
    public function getXmlBuffer()
    {
      return $this->XmlBuffer;
    }

    /**
     * @param string $XmlBuffer
     * @return \Dingus\SyncroService\SyncroBufferRS
     */
    public function setXmlBuffer($XmlBuffer)
    {
      $this->XmlBuffer = $XmlBuffer;
      return $this;
    }

    /**
     * @return string
     */
    public function getErrorBuffer()
    {
      return $this->ErrorBuffer;
    }

    /**
     * @param string $ErrorBuffer
     * @return \Dingus\SyncroService\SyncroBufferRS
     */
    public function setErrorBuffer($ErrorBuffer)
    {
      $this->ErrorBuffer = $ErrorBuffer;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getProcessedBuffer()
    {
      return $this->ProcessedBuffer;
    }

    /**
     * @param boolean $ProcessedBuffer
     * @return \Dingus\SyncroService\SyncroBufferRS
     */
    public function setProcessedBuffer($ProcessedBuffer)
    {
      $this->ProcessedBuffer = $ProcessedBuffer;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getPreConectBuffer()
    {
      return $this->PreConectBuffer;
    }

    /**
     * @param boolean $PreConectBuffer
     * @return \Dingus\SyncroService\SyncroBufferRS
     */
    public function setPreConectBuffer($PreConectBuffer)
    {
      $this->PreConectBuffer = $PreConectBuffer;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getSendBuffer()
    {
      return $this->SendBuffer;
    }

    /**
     * @param boolean $SendBuffer
     * @return \Dingus\SyncroService\SyncroBufferRS
     */
    public function setSendBuffer($SendBuffer)
    {
      $this->SendBuffer = $SendBuffer;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getConfirmedBuffer()
    {
      return $this->ConfirmedBuffer;
    }

    /**
     * @param boolean $ConfirmedBuffer
     * @return \Dingus\SyncroService\SyncroBufferRS
     */
    public function setConfirmedBuffer($ConfirmedBuffer)
    {
      $this->ConfirmedBuffer = $ConfirmedBuffer;
      return $this;
    }

    /**
     * @return string
     */
    public function getTaskBuffer()
    {
      return $this->TaskBuffer;
    }

    /**
     * @param string $TaskBuffer
     * @return \Dingus\SyncroService\SyncroBufferRS
     */
    public function setTaskBuffer($TaskBuffer)
    {
      $this->TaskBuffer = $TaskBuffer;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getInProcessBuffer()
    {
      return $this->InProcessBuffer;
    }

    /**
     * @param boolean $InProcessBuffer
     * @return \Dingus\SyncroService\SyncroBufferRS
     */
    public function setInProcessBuffer($InProcessBuffer)
    {
      $this->InProcessBuffer = $InProcessBuffer;
      return $this;
    }

    /**
     * @return string
     */
    public function getErrorConfirmBuffer()
    {
      return $this->ErrorConfirmBuffer;
    }

    /**
     * @param string $ErrorConfirmBuffer
     * @return \Dingus\SyncroService\SyncroBufferRS
     */
    public function setErrorConfirmBuffer($ErrorConfirmBuffer)
    {
      $this->ErrorConfirmBuffer = $ErrorConfirmBuffer;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateEntryBuffer()
    {
      if ($this->DateEntryBuffer == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->DateEntryBuffer);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $DateEntryBuffer
     * @return \Dingus\SyncroService\SyncroBufferRS
     */
    public function setDateEntryBuffer(\DateTime $DateEntryBuffer)
    {
      $this->DateEntryBuffer = $DateEntryBuffer->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateErrorBuffer()
    {
      if ($this->DateErrorBuffer == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->DateErrorBuffer);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $DateErrorBuffer
     * @return \Dingus\SyncroService\SyncroBufferRS
     */
    public function setDateErrorBuffer(\DateTime $DateErrorBuffer)
    {
      $this->DateErrorBuffer = $DateErrorBuffer->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return string
     */
    public function getResponsibleBuffer()
    {
      return $this->ResponsibleBuffer;
    }

    /**
     * @param string $ResponsibleBuffer
     * @return \Dingus\SyncroService\SyncroBufferRS
     */
    public function setResponsibleBuffer($ResponsibleBuffer)
    {
      $this->ResponsibleBuffer = $ResponsibleBuffer;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getCheckedBuffer()
    {
      return $this->CheckedBuffer;
    }

    /**
     * @param boolean $CheckedBuffer
     * @return \Dingus\SyncroService\SyncroBufferRS
     */
    public function setCheckedBuffer($CheckedBuffer)
    {
      $this->CheckedBuffer = $CheckedBuffer;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getPendingToCustomerBuffer()
    {
      return $this->PendingToCustomerBuffer;
    }

    /**
     * @param boolean $PendingToCustomerBuffer
     * @return \Dingus\SyncroService\SyncroBufferRS
     */
    public function setPendingToCustomerBuffer($PendingToCustomerBuffer)
    {
      $this->PendingToCustomerBuffer = $PendingToCustomerBuffer;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getNotifOKBuffer()
    {
      return $this->NotifOKBuffer;
    }

    /**
     * @param boolean $NotifOKBuffer
     * @return \Dingus\SyncroService\SyncroBufferRS
     */
    public function setNotifOKBuffer($NotifOKBuffer)
    {
      $this->NotifOKBuffer = $NotifOKBuffer;
      return $this;
    }

}
