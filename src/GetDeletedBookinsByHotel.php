<?php

namespace Dingus\SyncroService;

class GetDeletedBookinsByHotel implements \JsonSerializable
{

    /**
     * @var Credentials $Credentials
     */
    protected $Credentials = null;

    /**
     * @var string $HotelCode
     */
    protected $HotelCode = null;

    /**
     * @param Credentials $Credentials
     * @param string $HotelCode
     */
    public function __construct($Credentials, $HotelCode)
    {
      $this->Credentials = $Credentials;
      $this->HotelCode = $HotelCode;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'Credentials' => $this->getCredentials(),
        'HotelCode' => $this->getHotelCode(),
      );
    }

    /**
     * @return Credentials
     */
    public function getCredentials()
    {
      return $this->Credentials;
    }

    /**
     * @param Credentials $Credentials
     * @return \Dingus\SyncroService\GetDeletedBookinsByHotel
     */
    public function setCredentials($Credentials)
    {
      $this->Credentials = $Credentials;
      return $this;
    }

    /**
     * @return string
     */
    public function getHotelCode()
    {
      return $this->HotelCode;
    }

    /**
     * @param string $HotelCode
     * @return \Dingus\SyncroService\GetDeletedBookinsByHotel
     */
    public function setHotelCode($HotelCode)
    {
      $this->HotelCode = $HotelCode;
      return $this;
    }

}
