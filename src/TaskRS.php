<?php

namespace Dingus\SyncroService;

class TaskRS implements \JsonSerializable
{

    /**
     * @var ArrayOfErrorsTask $lErrorTask
     */
    protected $lErrorTask = null;

    /**
     * @var ArrayOfDataTask $lDataTask
     */
    protected $lDataTask = null;

    /**
     * @var ArrayOfHotelsTask $lHotelsTask
     */
    protected $lHotelsTask = null;

    /**
     * @var ArrayOfCustomersTask $lCustomersTask
     */
    protected $lCustomersTask = null;

    /**
     * @var ArrayOfUsersTask $lUsersTask
     */
    protected $lUsersTask = null;

    /**
     * @var Action $Action
     */
    protected $Action = null;

    /**
     * @var string $TaskCode
     */
    protected $TaskCode = null;

    /**
     * @var string $TaskDescription
     */
    protected $TaskDescription = null;

    /**
     * @var TipoTarea $TaskType
     */
    protected $TaskType = null;

    /**
     * @var TipoEjecucionTarea $TaskExecutionType
     */
    protected $TaskExecutionType = null;

    /**
     * @var string $GroupTask
     */
    protected $GroupTask = null;

    /**
     * @var string $GroupTaskDescription
     */
    protected $GroupTaskDescription = null;

    /**
     * @var int $SecondIntervals
     */
    protected $SecondIntervals = null;

    /**
     * @var \DateTime $HourExecution
     */
    protected $HourExecution = null;

    /**
     * @var \DateTime $HourNextExecution
     */
    protected $HourNextExecution = null;

    /**
     * @var \DateTime $HourLastExecution
     */
    protected $HourLastExecution = null;

    /**
     * @var boolean $MondayTask
     */
    protected $MondayTask = null;

    /**
     * @var boolean $TuesdayTask
     */
    protected $TuesdayTask = null;

    /**
     * @var boolean $WednesdayTask
     */
    protected $WednesdayTask = null;

    /**
     * @var boolean $ThurdsdayTask
     */
    protected $ThurdsdayTask = null;

    /**
     * @var boolean $FridayTask
     */
    protected $FridayTask = null;

    /**
     * @var boolean $SaturdayTask
     */
    protected $SaturdayTask = null;

    /**
     * @var boolean $SundayTask
     */
    protected $SundayTask = null;

    /**
     * @var string $LibraryTask
     */
    protected $LibraryTask = null;

    /**
     * @var string $ClassTask
     */
    protected $ClassTask = null;

    /**
     * @var string $MethodTask
     */
    protected $MethodTask = null;

    /**
     * @var boolean $StaticTask
     */
    protected $StaticTask = null;

    /**
     * @var boolean $ActivateTask
     */
    protected $ActivateTask = null;

    /**
     * @var string $XmlSetUp
     */
    protected $XmlSetUp = null;

    /**
     * @var string $LastExecutionText
     */
    protected $LastExecutionText = null;

    /**
     * @var boolean $InmediateTask
     */
    protected $InmediateTask = null;

    /**
     * @var string $ResponsibleTask
     */
    protected $ResponsibleTask = null;

    /**
     * @var boolean $CheckedTask
     */
    protected $CheckedTask = null;

    /**
     * @param Action $Action
     * @param TipoTarea $TaskType
     * @param TipoEjecucionTarea $TaskExecutionType
     * @param int $SecondIntervals
     * @param \DateTime $HourExecution
     * @param \DateTime $HourNextExecution
     * @param \DateTime $HourLastExecution
     * @param boolean $MondayTask
     * @param boolean $TuesdayTask
     * @param boolean $WednesdayTask
     * @param boolean $ThurdsdayTask
     * @param boolean $FridayTask
     * @param boolean $SaturdayTask
     * @param boolean $SundayTask
     * @param boolean $StaticTask
     * @param boolean $ActivateTask
     * @param boolean $InmediateTask
     * @param boolean $CheckedTask
     */
    public function __construct($Action, $TaskType, $TaskExecutionType, $SecondIntervals, \DateTime $HourExecution, \DateTime $HourNextExecution, \DateTime $HourLastExecution, $MondayTask, $TuesdayTask, $WednesdayTask, $ThurdsdayTask, $FridayTask, $SaturdayTask, $SundayTask, $StaticTask, $ActivateTask, $InmediateTask, $CheckedTask)
    {
      $this->Action = $Action;
      $this->TaskType = $TaskType;
      $this->TaskExecutionType = $TaskExecutionType;
      $this->SecondIntervals = $SecondIntervals;
      $this->HourExecution = $HourExecution->format(\DateTime::ATOM);
      $this->HourNextExecution = $HourNextExecution->format(\DateTime::ATOM);
      $this->HourLastExecution = $HourLastExecution->format(\DateTime::ATOM);
      $this->MondayTask = $MondayTask;
      $this->TuesdayTask = $TuesdayTask;
      $this->WednesdayTask = $WednesdayTask;
      $this->ThurdsdayTask = $ThurdsdayTask;
      $this->FridayTask = $FridayTask;
      $this->SaturdayTask = $SaturdayTask;
      $this->SundayTask = $SundayTask;
      $this->StaticTask = $StaticTask;
      $this->ActivateTask = $ActivateTask;
      $this->InmediateTask = $InmediateTask;
      $this->CheckedTask = $CheckedTask;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'lErrorTask' => $this->getLErrorTask(),
        'lDataTask' => $this->getLDataTask(),
        'lHotelsTask' => $this->getLHotelsTask(),
        'lCustomersTask' => $this->getLCustomersTask(),
        'lUsersTask' => $this->getLUsersTask(),
        'Action' => $this->getAction(),
        'TaskCode' => $this->getTaskCode(),
        'TaskDescription' => $this->getTaskDescription(),
        'TaskType' => $this->getTaskType(),
        'TaskExecutionType' => $this->getTaskExecutionType(),
        'GroupTask' => $this->getGroupTask(),
        'GroupTaskDescription' => $this->getGroupTaskDescription(),
        'SecondIntervals' => $this->getSecondIntervals(),
        'HourExecution' => $this->getHourExecution(),
        'HourNextExecution' => $this->getHourNextExecution(),
        'HourLastExecution' => $this->getHourLastExecution(),
        'MondayTask' => $this->getMondayTask(),
        'TuesdayTask' => $this->getTuesdayTask(),
        'WednesdayTask' => $this->getWednesdayTask(),
        'ThurdsdayTask' => $this->getThurdsdayTask(),
        'FridayTask' => $this->getFridayTask(),
        'SaturdayTask' => $this->getSaturdayTask(),
        'SundayTask' => $this->getSundayTask(),
        'LibraryTask' => $this->getLibraryTask(),
        'ClassTask' => $this->getClassTask(),
        'MethodTask' => $this->getMethodTask(),
        'StaticTask' => $this->getStaticTask(),
        'ActivateTask' => $this->getActivateTask(),
        'XmlSetUp' => $this->getXmlSetUp(),
        'LastExecutionText' => $this->getLastExecutionText(),
        'InmediateTask' => $this->getInmediateTask(),
        'ResponsibleTask' => $this->getResponsibleTask(),
        'CheckedTask' => $this->getCheckedTask(),
      );
    }

    /**
     * @return ArrayOfErrorsTask
     */
    public function getLErrorTask()
    {
      return $this->lErrorTask;
    }

    /**
     * @param ArrayOfErrorsTask $lErrorTask
     * @return \Dingus\SyncroService\TaskRS
     */
    public function setLErrorTask($lErrorTask)
    {
      $this->lErrorTask = $lErrorTask;
      return $this;
    }

    /**
     * @return ArrayOfDataTask
     */
    public function getLDataTask()
    {
      return $this->lDataTask;
    }

    /**
     * @param ArrayOfDataTask $lDataTask
     * @return \Dingus\SyncroService\TaskRS
     */
    public function setLDataTask($lDataTask)
    {
      $this->lDataTask = $lDataTask;
      return $this;
    }

    /**
     * @return ArrayOfHotelsTask
     */
    public function getLHotelsTask()
    {
      return $this->lHotelsTask;
    }

    /**
     * @param ArrayOfHotelsTask $lHotelsTask
     * @return \Dingus\SyncroService\TaskRS
     */
    public function setLHotelsTask($lHotelsTask)
    {
      $this->lHotelsTask = $lHotelsTask;
      return $this;
    }

    /**
     * @return ArrayOfCustomersTask
     */
    public function getLCustomersTask()
    {
      return $this->lCustomersTask;
    }

    /**
     * @param ArrayOfCustomersTask $lCustomersTask
     * @return \Dingus\SyncroService\TaskRS
     */
    public function setLCustomersTask($lCustomersTask)
    {
      $this->lCustomersTask = $lCustomersTask;
      return $this;
    }

    /**
     * @return ArrayOfUsersTask
     */
    public function getLUsersTask()
    {
      return $this->lUsersTask;
    }

    /**
     * @param ArrayOfUsersTask $lUsersTask
     * @return \Dingus\SyncroService\TaskRS
     */
    public function setLUsersTask($lUsersTask)
    {
      $this->lUsersTask = $lUsersTask;
      return $this;
    }

    /**
     * @return Action
     */
    public function getAction()
    {
      return $this->Action;
    }

    /**
     * @param Action $Action
     * @return \Dingus\SyncroService\TaskRS
     */
    public function setAction($Action)
    {
      $this->Action = $Action;
      return $this;
    }

    /**
     * @return string
     */
    public function getTaskCode()
    {
      return $this->TaskCode;
    }

    /**
     * @param string $TaskCode
     * @return \Dingus\SyncroService\TaskRS
     */
    public function setTaskCode($TaskCode)
    {
      $this->TaskCode = $TaskCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getTaskDescription()
    {
      return $this->TaskDescription;
    }

    /**
     * @param string $TaskDescription
     * @return \Dingus\SyncroService\TaskRS
     */
    public function setTaskDescription($TaskDescription)
    {
      $this->TaskDescription = $TaskDescription;
      return $this;
    }

    /**
     * @return TipoTarea
     */
    public function getTaskType()
    {
      return $this->TaskType;
    }

    /**
     * @param TipoTarea $TaskType
     * @return \Dingus\SyncroService\TaskRS
     */
    public function setTaskType($TaskType)
    {
      $this->TaskType = $TaskType;
      return $this;
    }

    /**
     * @return TipoEjecucionTarea
     */
    public function getTaskExecutionType()
    {
      return $this->TaskExecutionType;
    }

    /**
     * @param TipoEjecucionTarea $TaskExecutionType
     * @return \Dingus\SyncroService\TaskRS
     */
    public function setTaskExecutionType($TaskExecutionType)
    {
      $this->TaskExecutionType = $TaskExecutionType;
      return $this;
    }

    /**
     * @return string
     */
    public function getGroupTask()
    {
      return $this->GroupTask;
    }

    /**
     * @param string $GroupTask
     * @return \Dingus\SyncroService\TaskRS
     */
    public function setGroupTask($GroupTask)
    {
      $this->GroupTask = $GroupTask;
      return $this;
    }

    /**
     * @return string
     */
    public function getGroupTaskDescription()
    {
      return $this->GroupTaskDescription;
    }

    /**
     * @param string $GroupTaskDescription
     * @return \Dingus\SyncroService\TaskRS
     */
    public function setGroupTaskDescription($GroupTaskDescription)
    {
      $this->GroupTaskDescription = $GroupTaskDescription;
      return $this;
    }

    /**
     * @return int
     */
    public function getSecondIntervals()
    {
      return $this->SecondIntervals;
    }

    /**
     * @param int $SecondIntervals
     * @return \Dingus\SyncroService\TaskRS
     */
    public function setSecondIntervals($SecondIntervals)
    {
      $this->SecondIntervals = $SecondIntervals;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getHourExecution()
    {
      if ($this->HourExecution == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->HourExecution);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $HourExecution
     * @return \Dingus\SyncroService\TaskRS
     */
    public function setHourExecution(\DateTime $HourExecution)
    {
      $this->HourExecution = $HourExecution->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getHourNextExecution()
    {
      if ($this->HourNextExecution == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->HourNextExecution);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $HourNextExecution
     * @return \Dingus\SyncroService\TaskRS
     */
    public function setHourNextExecution(\DateTime $HourNextExecution)
    {
      $this->HourNextExecution = $HourNextExecution->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getHourLastExecution()
    {
      if ($this->HourLastExecution == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->HourLastExecution);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $HourLastExecution
     * @return \Dingus\SyncroService\TaskRS
     */
    public function setHourLastExecution(\DateTime $HourLastExecution)
    {
      $this->HourLastExecution = $HourLastExecution->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return boolean
     */
    public function getMondayTask()
    {
      return $this->MondayTask;
    }

    /**
     * @param boolean $MondayTask
     * @return \Dingus\SyncroService\TaskRS
     */
    public function setMondayTask($MondayTask)
    {
      $this->MondayTask = $MondayTask;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getTuesdayTask()
    {
      return $this->TuesdayTask;
    }

    /**
     * @param boolean $TuesdayTask
     * @return \Dingus\SyncroService\TaskRS
     */
    public function setTuesdayTask($TuesdayTask)
    {
      $this->TuesdayTask = $TuesdayTask;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getWednesdayTask()
    {
      return $this->WednesdayTask;
    }

    /**
     * @param boolean $WednesdayTask
     * @return \Dingus\SyncroService\TaskRS
     */
    public function setWednesdayTask($WednesdayTask)
    {
      $this->WednesdayTask = $WednesdayTask;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getThurdsdayTask()
    {
      return $this->ThurdsdayTask;
    }

    /**
     * @param boolean $ThurdsdayTask
     * @return \Dingus\SyncroService\TaskRS
     */
    public function setThurdsdayTask($ThurdsdayTask)
    {
      $this->ThurdsdayTask = $ThurdsdayTask;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getFridayTask()
    {
      return $this->FridayTask;
    }

    /**
     * @param boolean $FridayTask
     * @return \Dingus\SyncroService\TaskRS
     */
    public function setFridayTask($FridayTask)
    {
      $this->FridayTask = $FridayTask;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getSaturdayTask()
    {
      return $this->SaturdayTask;
    }

    /**
     * @param boolean $SaturdayTask
     * @return \Dingus\SyncroService\TaskRS
     */
    public function setSaturdayTask($SaturdayTask)
    {
      $this->SaturdayTask = $SaturdayTask;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getSundayTask()
    {
      return $this->SundayTask;
    }

    /**
     * @param boolean $SundayTask
     * @return \Dingus\SyncroService\TaskRS
     */
    public function setSundayTask($SundayTask)
    {
      $this->SundayTask = $SundayTask;
      return $this;
    }

    /**
     * @return string
     */
    public function getLibraryTask()
    {
      return $this->LibraryTask;
    }

    /**
     * @param string $LibraryTask
     * @return \Dingus\SyncroService\TaskRS
     */
    public function setLibraryTask($LibraryTask)
    {
      $this->LibraryTask = $LibraryTask;
      return $this;
    }

    /**
     * @return string
     */
    public function getClassTask()
    {
      return $this->ClassTask;
    }

    /**
     * @param string $ClassTask
     * @return \Dingus\SyncroService\TaskRS
     */
    public function setClassTask($ClassTask)
    {
      $this->ClassTask = $ClassTask;
      return $this;
    }

    /**
     * @return string
     */
    public function getMethodTask()
    {
      return $this->MethodTask;
    }

    /**
     * @param string $MethodTask
     * @return \Dingus\SyncroService\TaskRS
     */
    public function setMethodTask($MethodTask)
    {
      $this->MethodTask = $MethodTask;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getStaticTask()
    {
      return $this->StaticTask;
    }

    /**
     * @param boolean $StaticTask
     * @return \Dingus\SyncroService\TaskRS
     */
    public function setStaticTask($StaticTask)
    {
      $this->StaticTask = $StaticTask;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getActivateTask()
    {
      return $this->ActivateTask;
    }

    /**
     * @param boolean $ActivateTask
     * @return \Dingus\SyncroService\TaskRS
     */
    public function setActivateTask($ActivateTask)
    {
      $this->ActivateTask = $ActivateTask;
      return $this;
    }

    /**
     * @return string
     */
    public function getXmlSetUp()
    {
      return $this->XmlSetUp;
    }

    /**
     * @param string $XmlSetUp
     * @return \Dingus\SyncroService\TaskRS
     */
    public function setXmlSetUp($XmlSetUp)
    {
      $this->XmlSetUp = $XmlSetUp;
      return $this;
    }

    /**
     * @return string
     */
    public function getLastExecutionText()
    {
      return $this->LastExecutionText;
    }

    /**
     * @param string $LastExecutionText
     * @return \Dingus\SyncroService\TaskRS
     */
    public function setLastExecutionText($LastExecutionText)
    {
      $this->LastExecutionText = $LastExecutionText;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getInmediateTask()
    {
      return $this->InmediateTask;
    }

    /**
     * @param boolean $InmediateTask
     * @return \Dingus\SyncroService\TaskRS
     */
    public function setInmediateTask($InmediateTask)
    {
      $this->InmediateTask = $InmediateTask;
      return $this;
    }

    /**
     * @return string
     */
    public function getResponsibleTask()
    {
      return $this->ResponsibleTask;
    }

    /**
     * @param string $ResponsibleTask
     * @return \Dingus\SyncroService\TaskRS
     */
    public function setResponsibleTask($ResponsibleTask)
    {
      $this->ResponsibleTask = $ResponsibleTask;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getCheckedTask()
    {
      return $this->CheckedTask;
    }

    /**
     * @param boolean $CheckedTask
     * @return \Dingus\SyncroService\TaskRS
     */
    public function setCheckedTask($CheckedTask)
    {
      $this->CheckedTask = $CheckedTask;
      return $this;
    }

}
