<?php

namespace Dingus\SyncroService;

class Restaurant implements \JsonSerializable
{

    /**
     * @var ArrayOfMlText $DescriptionMlList
     */
    protected $DescriptionMlList = null;

    /**
     * @var ArrayOfPicture $Pictures
     */
    protected $Pictures = null;

    /**
     * @var Action $Action
     */
    protected $Action = null;

    /**
     * @var int $Id
     */
    protected $Id = null;

    /**
     * @var string $Name
     */
    protected $Name = null;

    /**
     * @var string $Description
     */
    protected $Description = null;

    /**
     * @var TipoRestauranteHotel $Type
     */
    protected $Type = null;

    /**
     * @var int $Capacity
     */
    protected $Capacity = null;

    /**
     * @var \DateTime $OpenTime
     */
    protected $OpenTime = null;

    /**
     * @var \DateTime $CloseTime
     */
    protected $CloseTime = null;

    /**
     * @var boolean $SpecialGown
     */
    protected $SpecialGown = null;

    /**
     * @var boolean $Breakfast
     */
    protected $Breakfast = null;

    /**
     * @var boolean $Luncheon
     */
    protected $Luncheon = null;

    /**
     * @var boolean $Dinner
     */
    protected $Dinner = null;

    /**
     * @param Action $Action
     * @param int $Id
     * @param TipoRestauranteHotel $Type
     * @param int $Capacity
     * @param \DateTime $OpenTime
     * @param \DateTime $CloseTime
     * @param boolean $SpecialGown
     * @param boolean $Breakfast
     * @param boolean $Luncheon
     * @param boolean $Dinner
     */
    public function __construct($Action, $Id, $Type, $Capacity, \DateTime $OpenTime, \DateTime $CloseTime, $SpecialGown, $Breakfast, $Luncheon, $Dinner)
    {
      $this->Action = $Action;
      $this->Id = $Id;
      $this->Type = $Type;
      $this->Capacity = $Capacity;
      $this->OpenTime = $OpenTime->format(\DateTime::ATOM);
      $this->CloseTime = $CloseTime->format(\DateTime::ATOM);
      $this->SpecialGown = $SpecialGown;
      $this->Breakfast = $Breakfast;
      $this->Luncheon = $Luncheon;
      $this->Dinner = $Dinner;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'DescriptionMlList' => $this->getDescriptionMlList(),
        'Pictures' => $this->getPictures(),
        'Action' => $this->getAction(),
        'Id' => $this->getId(),
        'Name' => $this->getName(),
        'Description' => $this->getDescription(),
        'Type' => $this->getType(),
        'Capacity' => $this->getCapacity(),
        'OpenTime' => $this->getOpenTime(),
        'CloseTime' => $this->getCloseTime(),
        'SpecialGown' => $this->getSpecialGown(),
        'Breakfast' => $this->getBreakfast(),
        'Luncheon' => $this->getLuncheon(),
        'Dinner' => $this->getDinner(),
      );
    }

    /**
     * @return ArrayOfMlText
     */
    public function getDescriptionMlList()
    {
      return $this->DescriptionMlList;
    }

    /**
     * @param ArrayOfMlText $DescriptionMlList
     * @return \Dingus\SyncroService\Restaurant
     */
    public function setDescriptionMlList($DescriptionMlList)
    {
      $this->DescriptionMlList = $DescriptionMlList;
      return $this;
    }

    /**
     * @return ArrayOfPicture
     */
    public function getPictures()
    {
      return $this->Pictures;
    }

    /**
     * @param ArrayOfPicture $Pictures
     * @return \Dingus\SyncroService\Restaurant
     */
    public function setPictures($Pictures)
    {
      $this->Pictures = $Pictures;
      return $this;
    }

    /**
     * @return Action
     */
    public function getAction()
    {
      return $this->Action;
    }

    /**
     * @param Action $Action
     * @return \Dingus\SyncroService\Restaurant
     */
    public function setAction($Action)
    {
      $this->Action = $Action;
      return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
      return $this->Id;
    }

    /**
     * @param int $Id
     * @return \Dingus\SyncroService\Restaurant
     */
    public function setId($Id)
    {
      $this->Id = $Id;
      return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
      return $this->Name;
    }

    /**
     * @param string $Name
     * @return \Dingus\SyncroService\Restaurant
     */
    public function setName($Name)
    {
      $this->Name = $Name;
      return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
      return $this->Description;
    }

    /**
     * @param string $Description
     * @return \Dingus\SyncroService\Restaurant
     */
    public function setDescription($Description)
    {
      $this->Description = $Description;
      return $this;
    }

    /**
     * @return TipoRestauranteHotel
     */
    public function getType()
    {
      return $this->Type;
    }

    /**
     * @param TipoRestauranteHotel $Type
     * @return \Dingus\SyncroService\Restaurant
     */
    public function setType($Type)
    {
      $this->Type = $Type;
      return $this;
    }

    /**
     * @return int
     */
    public function getCapacity()
    {
      return $this->Capacity;
    }

    /**
     * @param int $Capacity
     * @return \Dingus\SyncroService\Restaurant
     */
    public function setCapacity($Capacity)
    {
      $this->Capacity = $Capacity;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getOpenTime()
    {
      if ($this->OpenTime == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->OpenTime);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $OpenTime
     * @return \Dingus\SyncroService\Restaurant
     */
    public function setOpenTime(\DateTime $OpenTime)
    {
      $this->OpenTime = $OpenTime->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCloseTime()
    {
      if ($this->CloseTime == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->CloseTime);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $CloseTime
     * @return \Dingus\SyncroService\Restaurant
     */
    public function setCloseTime(\DateTime $CloseTime)
    {
      $this->CloseTime = $CloseTime->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return boolean
     */
    public function getSpecialGown()
    {
      return $this->SpecialGown;
    }

    /**
     * @param boolean $SpecialGown
     * @return \Dingus\SyncroService\Restaurant
     */
    public function setSpecialGown($SpecialGown)
    {
      $this->SpecialGown = $SpecialGown;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getBreakfast()
    {
      return $this->Breakfast;
    }

    /**
     * @param boolean $Breakfast
     * @return \Dingus\SyncroService\Restaurant
     */
    public function setBreakfast($Breakfast)
    {
      $this->Breakfast = $Breakfast;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getLuncheon()
    {
      return $this->Luncheon;
    }

    /**
     * @param boolean $Luncheon
     * @return \Dingus\SyncroService\Restaurant
     */
    public function setLuncheon($Luncheon)
    {
      $this->Luncheon = $Luncheon;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getDinner()
    {
      return $this->Dinner;
    }

    /**
     * @param boolean $Dinner
     * @return \Dingus\SyncroService\Restaurant
     */
    public function setDinner($Dinner)
    {
      $this->Dinner = $Dinner;
      return $this;
    }

}
