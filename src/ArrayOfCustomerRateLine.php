<?php

namespace Dingus\SyncroService;

class ArrayOfCustomerRateLine implements \JsonSerializable, \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var CustomerRateLine[] $CustomerRateLine
     */
    protected $CustomerRateLine = null;

    
    public function __construct()
    {
    
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'CustomerRateLine' => $this->getCustomerRateLine(),
      );
    }

    /**
     * @return CustomerRateLine[]
     */
    public function getCustomerRateLine()
    {
      return $this->CustomerRateLine;
    }

    /**
     * @param CustomerRateLine[] $CustomerRateLine
     * @return \Dingus\SyncroService\ArrayOfCustomerRateLine
     */
    public function setCustomerRateLine(array $CustomerRateLine = null)
    {
      $this->CustomerRateLine = $CustomerRateLine;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->CustomerRateLine[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return CustomerRateLine
     */
    public function offsetGet($offset)
    {
      return $this->CustomerRateLine[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param CustomerRateLine $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      $this->CustomerRateLine[$offset] = $value;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->CustomerRateLine[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return CustomerRateLine Return the current element
     */
    public function current()
    {
      return current($this->CustomerRateLine);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->CustomerRateLine);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->CustomerRateLine);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->CustomerRateLine);
    }

    /**
     * Countable implementation
     *
     * @return CustomerRateLine Return count of elements
     */
    public function count()
    {
      return count($this->CustomerRateLine);
    }

}
