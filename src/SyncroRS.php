<?php

namespace Dingus\SyncroService;

class SyncroRS implements \JsonSerializable
{

    /**
     * @var ArrayOfString $ErrorList
     */
    protected $ErrorList = null;

    /**
     * @var boolean $Success
     */
    protected $Success = null;

    /**
     * @param boolean $Success
     */
    public function __construct($Success)
    {
      $this->Success = $Success;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'ErrorList' => $this->getErrorList(),
        'Success' => $this->getSuccess(),
      );
    }

    /**
     * @return ArrayOfString
     */
    public function getErrorList()
    {
      return $this->ErrorList;
    }

    /**
     * @param ArrayOfString $ErrorList
     * @return \Dingus\SyncroService\SyncroRS
     */
    public function setErrorList($ErrorList)
    {
      $this->ErrorList = $ErrorList;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getSuccess()
    {
      return $this->Success;
    }

    /**
     * @param boolean $Success
     * @return \Dingus\SyncroService\SyncroRS
     */
    public function setSuccess($Success)
    {
      $this->Success = $Success;
      return $this;
    }

}
