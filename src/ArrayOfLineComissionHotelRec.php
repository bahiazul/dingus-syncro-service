<?php

namespace Dingus\SyncroService;

class ArrayOfLineComissionHotelRec implements \JsonSerializable, \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var LineComissionHotelRec[] $LineComissionHotelRec
     */
    protected $LineComissionHotelRec = null;

    
    public function __construct()
    {
    
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'LineComissionHotelRec' => $this->getLineComissionHotelRec(),
      );
    }

    /**
     * @return LineComissionHotelRec[]
     */
    public function getLineComissionHotelRec()
    {
      return $this->LineComissionHotelRec;
    }

    /**
     * @param LineComissionHotelRec[] $LineComissionHotelRec
     * @return \Dingus\SyncroService\ArrayOfLineComissionHotelRec
     */
    public function setLineComissionHotelRec(array $LineComissionHotelRec = null)
    {
      $this->LineComissionHotelRec = $LineComissionHotelRec;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->LineComissionHotelRec[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return LineComissionHotelRec
     */
    public function offsetGet($offset)
    {
      return $this->LineComissionHotelRec[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param LineComissionHotelRec $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      $this->LineComissionHotelRec[$offset] = $value;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->LineComissionHotelRec[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return LineComissionHotelRec Return the current element
     */
    public function current()
    {
      return current($this->LineComissionHotelRec);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->LineComissionHotelRec);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->LineComissionHotelRec);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->LineComissionHotelRec);
    }

    /**
     * Countable implementation
     *
     * @return LineComissionHotelRec Return count of elements
     */
    public function count()
    {
      return count($this->LineComissionHotelRec);
    }

}
