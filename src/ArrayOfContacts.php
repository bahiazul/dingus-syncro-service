<?php

namespace Dingus\SyncroService;

class ArrayOfContacts implements \JsonSerializable, \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var Contacts[] $Contacts
     */
    protected $Contacts = null;

    
    public function __construct()
    {
    
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'Contacts' => $this->getContacts(),
      );
    }

    /**
     * @return Contacts[]
     */
    public function getContacts()
    {
      return $this->Contacts;
    }

    /**
     * @param Contacts[] $Contacts
     * @return \Dingus\SyncroService\ArrayOfContacts
     */
    public function setContacts(array $Contacts = null)
    {
      $this->Contacts = $Contacts;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->Contacts[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return Contacts
     */
    public function offsetGet($offset)
    {
      return $this->Contacts[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param Contacts $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      $this->Contacts[$offset] = $value;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->Contacts[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return Contacts Return the current element
     */
    public function current()
    {
      return current($this->Contacts);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->Contacts);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->Contacts);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->Contacts);
    }

    /**
     * Countable implementation
     *
     * @return Contacts Return count of elements
     */
    public function count()
    {
      return count($this->Contacts);
    }

}
