<?php

namespace Dingus\SyncroService;

class BoardRateRec implements \JsonSerializable
{

    /**
     * @var Action $Action
     */
    protected $Action = null;

    /**
     * @var float $AdultPrice
     */
    protected $AdultPrice = null;

    /**
     * @var float $ChidlPrice
     */
    protected $ChidlPrice = null;

    /**
     * @var string $Code
     */
    protected $Code = null;

    /**
     * @var \DateTime $EffectiveDate
     */
    protected $EffectiveDate = null;

    /**
     * @var \DateTime $DateFrom
     */
    protected $DateFrom = null;

    /**
     * @var \DateTime $DateFrom2
     */
    protected $DateFrom2 = null;

    /**
     * @var DatesType $DatesType
     */
    protected $DatesType = null;

    /**
     * @var \DateTime $DateTo
     */
    protected $DateTo = null;

    /**
     * @var \DateTime $DateTo2
     */
    protected $DateTo2 = null;

    /**
     * @var float $EnfandPrice
     */
    protected $EnfandPrice = null;

    /**
     * @var float $FourthChildPrice
     */
    protected $FourthChildPrice = null;

    /**
     * @var float $FourthPaxPrice
     */
    protected $FourthPaxPrice = null;

    /**
     * @var string $PMSCode
     */
    protected $PMSCode = null;

    /**
     * @var string $RoomCode
     */
    protected $RoomCode = null;

    /**
     * @var float $SecondChidlPrice
     */
    protected $SecondChidlPrice = null;

    /**
     * @var float $ThirdChidlPrice
     */
    protected $ThirdChidlPrice = null;

    /**
     * @var float $ThirdPaxPrice
     */
    protected $ThirdPaxPrice = null;

    /**
     * @var boolean $UniqueApplication
     */
    protected $UniqueApplication = null;

    /**
     * @var boolean $Monday
     */
    protected $Monday = null;

    /**
     * @var boolean $Tuesday
     */
    protected $Tuesday = null;

    /**
     * @var boolean $Wednesday
     */
    protected $Wednesday = null;

    /**
     * @var boolean $Thursday
     */
    protected $Thursday = null;

    /**
     * @var boolean $Friday
     */
    protected $Friday = null;

    /**
     * @var boolean $Saturday
     */
    protected $Saturday = null;

    /**
     * @var boolean $Sunday
     */
    protected $Sunday = null;

    /**
     * @var boolean $Guaranteed
     */
    protected $Guaranteed = null;

    /**
     * @param Action $Action
     * @param float $AdultPrice
     * @param float $ChidlPrice
     * @param \DateTime $EffectiveDate
     * @param \DateTime $DateFrom
     * @param \DateTime $DateFrom2
     * @param DatesType $DatesType
     * @param \DateTime $DateTo
     * @param \DateTime $DateTo2
     * @param float $EnfandPrice
     * @param float $FourthChildPrice
     * @param float $FourthPaxPrice
     * @param float $SecondChidlPrice
     * @param float $ThirdChidlPrice
     * @param float $ThirdPaxPrice
     * @param boolean $UniqueApplication
     * @param boolean $Monday
     * @param boolean $Tuesday
     * @param boolean $Wednesday
     * @param boolean $Thursday
     * @param boolean $Friday
     * @param boolean $Saturday
     * @param boolean $Sunday
     * @param boolean $Guaranteed
     */
    public function __construct($Action, $AdultPrice, $ChidlPrice, \DateTime $EffectiveDate, \DateTime $DateFrom, \DateTime $DateFrom2, $DatesType, \DateTime $DateTo, \DateTime $DateTo2, $EnfandPrice, $FourthChildPrice, $FourthPaxPrice, $SecondChidlPrice, $ThirdChidlPrice, $ThirdPaxPrice, $UniqueApplication, $Monday, $Tuesday, $Wednesday, $Thursday, $Friday, $Saturday, $Sunday, $Guaranteed)
    {
      $this->Action = $Action;
      $this->AdultPrice = $AdultPrice;
      $this->ChidlPrice = $ChidlPrice;
      $this->EffectiveDate = $EffectiveDate->format(\DateTime::ATOM);
      $this->DateFrom = $DateFrom->format(\DateTime::ATOM);
      $this->DateFrom2 = $DateFrom2->format(\DateTime::ATOM);
      $this->DatesType = $DatesType;
      $this->DateTo = $DateTo->format(\DateTime::ATOM);
      $this->DateTo2 = $DateTo2->format(\DateTime::ATOM);
      $this->EnfandPrice = $EnfandPrice;
      $this->FourthChildPrice = $FourthChildPrice;
      $this->FourthPaxPrice = $FourthPaxPrice;
      $this->SecondChidlPrice = $SecondChidlPrice;
      $this->ThirdChidlPrice = $ThirdChidlPrice;
      $this->ThirdPaxPrice = $ThirdPaxPrice;
      $this->UniqueApplication = $UniqueApplication;
      $this->Monday = $Monday;
      $this->Tuesday = $Tuesday;
      $this->Wednesday = $Wednesday;
      $this->Thursday = $Thursday;
      $this->Friday = $Friday;
      $this->Saturday = $Saturday;
      $this->Sunday = $Sunday;
      $this->Guaranteed = $Guaranteed;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'Action' => $this->getAction(),
        'AdultPrice' => $this->getAdultPrice(),
        'ChidlPrice' => $this->getChidlPrice(),
        'Code' => $this->getCode(),
        'EffectiveDate' => $this->getEffectiveDate(),
        'DateFrom' => $this->getDateFrom(),
        'DateFrom2' => $this->getDateFrom2(),
        'DatesType' => $this->getDatesType(),
        'DateTo' => $this->getDateTo(),
        'DateTo2' => $this->getDateTo2(),
        'EnfandPrice' => $this->getEnfandPrice(),
        'FourthChildPrice' => $this->getFourthChildPrice(),
        'FourthPaxPrice' => $this->getFourthPaxPrice(),
        'PMSCode' => $this->getPMSCode(),
        'RoomCode' => $this->getRoomCode(),
        'SecondChidlPrice' => $this->getSecondChidlPrice(),
        'ThirdChidlPrice' => $this->getThirdChidlPrice(),
        'ThirdPaxPrice' => $this->getThirdPaxPrice(),
        'UniqueApplication' => $this->getUniqueApplication(),
        'Monday' => $this->getMonday(),
        'Tuesday' => $this->getTuesday(),
        'Wednesday' => $this->getWednesday(),
        'Thursday' => $this->getThursday(),
        'Friday' => $this->getFriday(),
        'Saturday' => $this->getSaturday(),
        'Sunday' => $this->getSunday(),
        'Guaranteed' => $this->getGuaranteed(),
      );
    }

    /**
     * @return Action
     */
    public function getAction()
    {
      return $this->Action;
    }

    /**
     * @param Action $Action
     * @return \Dingus\SyncroService\BoardRateRec
     */
    public function setAction($Action)
    {
      $this->Action = $Action;
      return $this;
    }

    /**
     * @return float
     */
    public function getAdultPrice()
    {
      return $this->AdultPrice;
    }

    /**
     * @param float $AdultPrice
     * @return \Dingus\SyncroService\BoardRateRec
     */
    public function setAdultPrice($AdultPrice)
    {
      $this->AdultPrice = $AdultPrice;
      return $this;
    }

    /**
     * @return float
     */
    public function getChidlPrice()
    {
      return $this->ChidlPrice;
    }

    /**
     * @param float $ChidlPrice
     * @return \Dingus\SyncroService\BoardRateRec
     */
    public function setChidlPrice($ChidlPrice)
    {
      $this->ChidlPrice = $ChidlPrice;
      return $this;
    }

    /**
     * @return string
     */
    public function getCode()
    {
      return $this->Code;
    }

    /**
     * @param string $Code
     * @return \Dingus\SyncroService\BoardRateRec
     */
    public function setCode($Code)
    {
      $this->Code = $Code;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getEffectiveDate()
    {
      if ($this->EffectiveDate == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->EffectiveDate);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $EffectiveDate
     * @return \Dingus\SyncroService\BoardRateRec
     */
    public function setEffectiveDate(\DateTime $EffectiveDate)
    {
      $this->EffectiveDate = $EffectiveDate->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateFrom()
    {
      if ($this->DateFrom == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->DateFrom);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $DateFrom
     * @return \Dingus\SyncroService\BoardRateRec
     */
    public function setDateFrom(\DateTime $DateFrom)
    {
      $this->DateFrom = $DateFrom->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateFrom2()
    {
      if ($this->DateFrom2 == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->DateFrom2);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $DateFrom2
     * @return \Dingus\SyncroService\BoardRateRec
     */
    public function setDateFrom2(\DateTime $DateFrom2)
    {
      $this->DateFrom2 = $DateFrom2->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return DatesType
     */
    public function getDatesType()
    {
      return $this->DatesType;
    }

    /**
     * @param DatesType $DatesType
     * @return \Dingus\SyncroService\BoardRateRec
     */
    public function setDatesType($DatesType)
    {
      $this->DatesType = $DatesType;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateTo()
    {
      if ($this->DateTo == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->DateTo);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $DateTo
     * @return \Dingus\SyncroService\BoardRateRec
     */
    public function setDateTo(\DateTime $DateTo)
    {
      $this->DateTo = $DateTo->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateTo2()
    {
      if ($this->DateTo2 == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->DateTo2);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $DateTo2
     * @return \Dingus\SyncroService\BoardRateRec
     */
    public function setDateTo2(\DateTime $DateTo2)
    {
      $this->DateTo2 = $DateTo2->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return float
     */
    public function getEnfandPrice()
    {
      return $this->EnfandPrice;
    }

    /**
     * @param float $EnfandPrice
     * @return \Dingus\SyncroService\BoardRateRec
     */
    public function setEnfandPrice($EnfandPrice)
    {
      $this->EnfandPrice = $EnfandPrice;
      return $this;
    }

    /**
     * @return float
     */
    public function getFourthChildPrice()
    {
      return $this->FourthChildPrice;
    }

    /**
     * @param float $FourthChildPrice
     * @return \Dingus\SyncroService\BoardRateRec
     */
    public function setFourthChildPrice($FourthChildPrice)
    {
      $this->FourthChildPrice = $FourthChildPrice;
      return $this;
    }

    /**
     * @return float
     */
    public function getFourthPaxPrice()
    {
      return $this->FourthPaxPrice;
    }

    /**
     * @param float $FourthPaxPrice
     * @return \Dingus\SyncroService\BoardRateRec
     */
    public function setFourthPaxPrice($FourthPaxPrice)
    {
      $this->FourthPaxPrice = $FourthPaxPrice;
      return $this;
    }

    /**
     * @return string
     */
    public function getPMSCode()
    {
      return $this->PMSCode;
    }

    /**
     * @param string $PMSCode
     * @return \Dingus\SyncroService\BoardRateRec
     */
    public function setPMSCode($PMSCode)
    {
      $this->PMSCode = $PMSCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getRoomCode()
    {
      return $this->RoomCode;
    }

    /**
     * @param string $RoomCode
     * @return \Dingus\SyncroService\BoardRateRec
     */
    public function setRoomCode($RoomCode)
    {
      $this->RoomCode = $RoomCode;
      return $this;
    }

    /**
     * @return float
     */
    public function getSecondChidlPrice()
    {
      return $this->SecondChidlPrice;
    }

    /**
     * @param float $SecondChidlPrice
     * @return \Dingus\SyncroService\BoardRateRec
     */
    public function setSecondChidlPrice($SecondChidlPrice)
    {
      $this->SecondChidlPrice = $SecondChidlPrice;
      return $this;
    }

    /**
     * @return float
     */
    public function getThirdChidlPrice()
    {
      return $this->ThirdChidlPrice;
    }

    /**
     * @param float $ThirdChidlPrice
     * @return \Dingus\SyncroService\BoardRateRec
     */
    public function setThirdChidlPrice($ThirdChidlPrice)
    {
      $this->ThirdChidlPrice = $ThirdChidlPrice;
      return $this;
    }

    /**
     * @return float
     */
    public function getThirdPaxPrice()
    {
      return $this->ThirdPaxPrice;
    }

    /**
     * @param float $ThirdPaxPrice
     * @return \Dingus\SyncroService\BoardRateRec
     */
    public function setThirdPaxPrice($ThirdPaxPrice)
    {
      $this->ThirdPaxPrice = $ThirdPaxPrice;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getUniqueApplication()
    {
      return $this->UniqueApplication;
    }

    /**
     * @param boolean $UniqueApplication
     * @return \Dingus\SyncroService\BoardRateRec
     */
    public function setUniqueApplication($UniqueApplication)
    {
      $this->UniqueApplication = $UniqueApplication;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getMonday()
    {
      return $this->Monday;
    }

    /**
     * @param boolean $Monday
     * @return \Dingus\SyncroService\BoardRateRec
     */
    public function setMonday($Monday)
    {
      $this->Monday = $Monday;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getTuesday()
    {
      return $this->Tuesday;
    }

    /**
     * @param boolean $Tuesday
     * @return \Dingus\SyncroService\BoardRateRec
     */
    public function setTuesday($Tuesday)
    {
      $this->Tuesday = $Tuesday;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getWednesday()
    {
      return $this->Wednesday;
    }

    /**
     * @param boolean $Wednesday
     * @return \Dingus\SyncroService\BoardRateRec
     */
    public function setWednesday($Wednesday)
    {
      $this->Wednesday = $Wednesday;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getThursday()
    {
      return $this->Thursday;
    }

    /**
     * @param boolean $Thursday
     * @return \Dingus\SyncroService\BoardRateRec
     */
    public function setThursday($Thursday)
    {
      $this->Thursday = $Thursday;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getFriday()
    {
      return $this->Friday;
    }

    /**
     * @param boolean $Friday
     * @return \Dingus\SyncroService\BoardRateRec
     */
    public function setFriday($Friday)
    {
      $this->Friday = $Friday;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getSaturday()
    {
      return $this->Saturday;
    }

    /**
     * @param boolean $Saturday
     * @return \Dingus\SyncroService\BoardRateRec
     */
    public function setSaturday($Saturday)
    {
      $this->Saturday = $Saturday;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getSunday()
    {
      return $this->Sunday;
    }

    /**
     * @param boolean $Sunday
     * @return \Dingus\SyncroService\BoardRateRec
     */
    public function setSunday($Sunday)
    {
      $this->Sunday = $Sunday;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getGuaranteed()
    {
      return $this->Guaranteed;
    }

    /**
     * @param boolean $Guaranteed
     * @return \Dingus\SyncroService\BoardRateRec
     */
    public function setGuaranteed($Guaranteed)
    {
      $this->Guaranteed = $Guaranteed;
      return $this;
    }

}
