<?php

namespace Dingus\SyncroService;

class MlText implements \JsonSerializable
{

    /**
     * @var string $LanguajeCode
     */
    protected $LanguajeCode = null;

    /**
     * @var string $Text
     */
    protected $Text = null;

    
    public function __construct()
    {
    
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'LanguajeCode' => $this->getLanguajeCode(),
        'Text' => $this->getText(),
      );
    }

    /**
     * @return string
     */
    public function getLanguajeCode()
    {
      return $this->LanguajeCode;
    }

    /**
     * @param string $LanguajeCode
     * @return \Dingus\SyncroService\MlText
     */
    public function setLanguajeCode($LanguajeCode)
    {
      $this->LanguajeCode = $LanguajeCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getText()
    {
      return $this->Text;
    }

    /**
     * @param string $Text
     * @return \Dingus\SyncroService\MlText
     */
    public function setText($Text)
    {
      $this->Text = $Text;
      return $this;
    }

}
