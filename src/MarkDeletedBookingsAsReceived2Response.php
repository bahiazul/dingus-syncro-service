<?php

namespace Dingus\SyncroService;

class MarkDeletedBookingsAsReceived2Response implements \JsonSerializable
{

    /**
     * @var SyncroRS $MarkDeletedBookingsAsReceived2Result
     */
    protected $MarkDeletedBookingsAsReceived2Result = null;

    /**
     * @param SyncroRS $MarkDeletedBookingsAsReceived2Result
     */
    public function __construct($MarkDeletedBookingsAsReceived2Result)
    {
      $this->MarkDeletedBookingsAsReceived2Result = $MarkDeletedBookingsAsReceived2Result;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'MarkDeletedBookingsAsReceived2Result' => $this->getMarkDeletedBookingsAsReceived2Result(),
      );
    }

    /**
     * @return SyncroRS
     */
    public function getMarkDeletedBookingsAsReceived2Result()
    {
      return $this->MarkDeletedBookingsAsReceived2Result;
    }

    /**
     * @param SyncroRS $MarkDeletedBookingsAsReceived2Result
     * @return \Dingus\SyncroService\MarkDeletedBookingsAsReceived2Response
     */
    public function setMarkDeletedBookingsAsReceived2Result($MarkDeletedBookingsAsReceived2Result)
    {
      $this->MarkDeletedBookingsAsReceived2Result = $MarkDeletedBookingsAsReceived2Result;
      return $this;
    }

}
