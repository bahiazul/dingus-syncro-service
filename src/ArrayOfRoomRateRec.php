<?php

namespace Dingus\SyncroService;

class ArrayOfRoomRateRec implements \JsonSerializable, \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var RoomRateRec[] $RoomRateRec
     */
    protected $RoomRateRec = null;

    
    public function __construct()
    {
    
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'RoomRateRec' => $this->getRoomRateRec(),
      );
    }

    /**
     * @return RoomRateRec[]
     */
    public function getRoomRateRec()
    {
      return $this->RoomRateRec;
    }

    /**
     * @param RoomRateRec[] $RoomRateRec
     * @return \Dingus\SyncroService\ArrayOfRoomRateRec
     */
    public function setRoomRateRec(array $RoomRateRec = null)
    {
      $this->RoomRateRec = $RoomRateRec;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->RoomRateRec[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return RoomRateRec
     */
    public function offsetGet($offset)
    {
      return $this->RoomRateRec[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param RoomRateRec $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      $this->RoomRateRec[$offset] = $value;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->RoomRateRec[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return RoomRateRec Return the current element
     */
    public function current()
    {
      return current($this->RoomRateRec);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->RoomRateRec);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->RoomRateRec);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->RoomRateRec);
    }

    /**
     * Countable implementation
     *
     * @return RoomRateRec Return count of elements
     */
    public function count()
    {
      return count($this->RoomRateRec);
    }

}
