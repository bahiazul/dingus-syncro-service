<?php

namespace Dingus\SyncroService;

class ErrorsTask implements \JsonSerializable
{

    /**
     * @var Action $Action
     */
    protected $Action = null;

    /**
     * @var int $ErrorID
     */
    protected $ErrorID = null;

    /**
     * @var \DateTime $ErrorDate
     */
    protected $ErrorDate = null;

    /**
     * @var string $ErrorText
     */
    protected $ErrorText = null;

    /**
     * @param Action $Action
     * @param int $ErrorID
     * @param \DateTime $ErrorDate
     */
    public function __construct($Action, $ErrorID, \DateTime $ErrorDate)
    {
      $this->Action = $Action;
      $this->ErrorID = $ErrorID;
      $this->ErrorDate = $ErrorDate->format(\DateTime::ATOM);
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'Action' => $this->getAction(),
        'ErrorID' => $this->getErrorID(),
        'ErrorDate' => $this->getErrorDate(),
        'ErrorText' => $this->getErrorText(),
      );
    }

    /**
     * @return Action
     */
    public function getAction()
    {
      return $this->Action;
    }

    /**
     * @param Action $Action
     * @return \Dingus\SyncroService\ErrorsTask
     */
    public function setAction($Action)
    {
      $this->Action = $Action;
      return $this;
    }

    /**
     * @return int
     */
    public function getErrorID()
    {
      return $this->ErrorID;
    }

    /**
     * @param int $ErrorID
     * @return \Dingus\SyncroService\ErrorsTask
     */
    public function setErrorID($ErrorID)
    {
      $this->ErrorID = $ErrorID;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getErrorDate()
    {
      if ($this->ErrorDate == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->ErrorDate);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $ErrorDate
     * @return \Dingus\SyncroService\ErrorsTask
     */
    public function setErrorDate(\DateTime $ErrorDate)
    {
      $this->ErrorDate = $ErrorDate->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return string
     */
    public function getErrorText()
    {
      return $this->ErrorText;
    }

    /**
     * @param string $ErrorText
     * @return \Dingus\SyncroService\ErrorsTask
     */
    public function setErrorText($ErrorText)
    {
      $this->ErrorText = $ErrorText;
      return $this;
    }

}
