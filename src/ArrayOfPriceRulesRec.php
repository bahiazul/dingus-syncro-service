<?php

namespace Dingus\SyncroService;

class ArrayOfPriceRulesRec implements \JsonSerializable, \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var PriceRulesRec[] $PriceRulesRec
     */
    protected $PriceRulesRec = null;

    
    public function __construct()
    {
    
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'PriceRulesRec' => $this->getPriceRulesRec(),
      );
    }

    /**
     * @return PriceRulesRec[]
     */
    public function getPriceRulesRec()
    {
      return $this->PriceRulesRec;
    }

    /**
     * @param PriceRulesRec[] $PriceRulesRec
     * @return \Dingus\SyncroService\ArrayOfPriceRulesRec
     */
    public function setPriceRulesRec(array $PriceRulesRec = null)
    {
      $this->PriceRulesRec = $PriceRulesRec;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->PriceRulesRec[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return PriceRulesRec
     */
    public function offsetGet($offset)
    {
      return $this->PriceRulesRec[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param PriceRulesRec $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      $this->PriceRulesRec[$offset] = $value;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->PriceRulesRec[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return PriceRulesRec Return the current element
     */
    public function current()
    {
      return current($this->PriceRulesRec);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->PriceRulesRec);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->PriceRulesRec);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->PriceRulesRec);
    }

    /**
     * Countable implementation
     *
     * @return PriceRulesRec Return count of elements
     */
    public function count()
    {
      return count($this->PriceRulesRec);
    }

}
