<?php

namespace Dingus\SyncroService;

class PutUserConfigurationRQ implements \JsonSerializable
{

    /**
     * @var ArrayOfUserConfigurationRec $UserConfigurationRQ
     */
    protected $UserConfigurationRQ = null;

    
    public function __construct()
    {
    
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'UserConfigurationRQ' => $this->getUserConfigurationRQ(),
      );
    }

    /**
     * @return ArrayOfUserConfigurationRec
     */
    public function getUserConfigurationRQ()
    {
      return $this->UserConfigurationRQ;
    }

    /**
     * @param ArrayOfUserConfigurationRec $UserConfigurationRQ
     * @return \Dingus\SyncroService\PutUserConfigurationRQ
     */
    public function setUserConfigurationRQ($UserConfigurationRQ)
    {
      $this->UserConfigurationRQ = $UserConfigurationRQ;
      return $this;
    }

}
