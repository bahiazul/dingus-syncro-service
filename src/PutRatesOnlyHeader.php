<?php

namespace Dingus\SyncroService;

class PutRatesOnlyHeader implements \JsonSerializable
{

    /**
     * @var Credentials $Credentials
     */
    protected $Credentials = null;

    /**
     * @var PutRatesOnlyHeaderRQ $PutRatesRQ
     */
    protected $PutRatesRQ = null;

    /**
     * @param Credentials $Credentials
     * @param PutRatesOnlyHeaderRQ $PutRatesRQ
     */
    public function __construct($Credentials, $PutRatesRQ)
    {
      $this->Credentials = $Credentials;
      $this->PutRatesRQ = $PutRatesRQ;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'Credentials' => $this->getCredentials(),
        'PutRatesRQ' => $this->getPutRatesRQ(),
      );
    }

    /**
     * @return Credentials
     */
    public function getCredentials()
    {
      return $this->Credentials;
    }

    /**
     * @param Credentials $Credentials
     * @return \Dingus\SyncroService\PutRatesOnlyHeader
     */
    public function setCredentials($Credentials)
    {
      $this->Credentials = $Credentials;
      return $this;
    }

    /**
     * @return PutRatesOnlyHeaderRQ
     */
    public function getPutRatesRQ()
    {
      return $this->PutRatesRQ;
    }

    /**
     * @param PutRatesOnlyHeaderRQ $PutRatesRQ
     * @return \Dingus\SyncroService\PutRatesOnlyHeader
     */
    public function setPutRatesRQ($PutRatesRQ)
    {
      $this->PutRatesRQ = $PutRatesRQ;
      return $this;
    }

}
