<?php

namespace Dingus\SyncroService;

class Close implements \JsonSerializable
{

    /**
     * @var Action $Action
     */
    protected $Action = null;

    /**
     * @var int $Id
     */
    protected $Id = null;

    /**
     * @var \DateTime $CloseBegin
     */
    protected $CloseBegin = null;

    /**
     * @var \DateTime $CloseEnd
     */
    protected $CloseEnd = null;

    /**
     * @param Action $Action
     * @param int $Id
     * @param \DateTime $CloseBegin
     * @param \DateTime $CloseEnd
     */
    public function __construct($Action, $Id, \DateTime $CloseBegin, \DateTime $CloseEnd)
    {
      $this->Action = $Action;
      $this->Id = $Id;
      $this->CloseBegin = $CloseBegin->format(\DateTime::ATOM);
      $this->CloseEnd = $CloseEnd->format(\DateTime::ATOM);
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'Action' => $this->getAction(),
        'Id' => $this->getId(),
        'CloseBegin' => $this->getCloseBegin(),
        'CloseEnd' => $this->getCloseEnd(),
      );
    }

    /**
     * @return Action
     */
    public function getAction()
    {
      return $this->Action;
    }

    /**
     * @param Action $Action
     * @return \Dingus\SyncroService\Close
     */
    public function setAction($Action)
    {
      $this->Action = $Action;
      return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
      return $this->Id;
    }

    /**
     * @param int $Id
     * @return \Dingus\SyncroService\Close
     */
    public function setId($Id)
    {
      $this->Id = $Id;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCloseBegin()
    {
      if ($this->CloseBegin == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->CloseBegin);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $CloseBegin
     * @return \Dingus\SyncroService\Close
     */
    public function setCloseBegin(\DateTime $CloseBegin)
    {
      $this->CloseBegin = $CloseBegin->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCloseEnd()
    {
      if ($this->CloseEnd == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->CloseEnd);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $CloseEnd
     * @return \Dingus\SyncroService\Close
     */
    public function setCloseEnd(\DateTime $CloseEnd)
    {
      $this->CloseEnd = $CloseEnd->format(\DateTime::ATOM);
      return $this;
    }

}
