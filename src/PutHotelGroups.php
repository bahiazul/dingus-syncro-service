<?php

namespace Dingus\SyncroService;

class PutHotelGroups implements \JsonSerializable
{

    /**
     * @var Credentials $Credentials
     */
    protected $Credentials = null;

    /**
     * @var PutHotelGroupsRQ $RQ
     */
    protected $RQ = null;

    /**
     * @param Credentials $Credentials
     * @param PutHotelGroupsRQ $RQ
     */
    public function __construct($Credentials, $RQ)
    {
      $this->Credentials = $Credentials;
      $this->RQ = $RQ;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'Credentials' => $this->getCredentials(),
        'RQ' => $this->getRQ(),
      );
    }

    /**
     * @return Credentials
     */
    public function getCredentials()
    {
      return $this->Credentials;
    }

    /**
     * @param Credentials $Credentials
     * @return \Dingus\SyncroService\PutHotelGroups
     */
    public function setCredentials($Credentials)
    {
      $this->Credentials = $Credentials;
      return $this;
    }

    /**
     * @return PutHotelGroupsRQ
     */
    public function getRQ()
    {
      return $this->RQ;
    }

    /**
     * @param PutHotelGroupsRQ $RQ
     * @return \Dingus\SyncroService\PutHotelGroups
     */
    public function setRQ($RQ)
    {
      $this->RQ = $RQ;
      return $this;
    }

}
