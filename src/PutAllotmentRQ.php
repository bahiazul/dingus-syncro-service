<?php

namespace Dingus\SyncroService;

class PutAllotmentRQ implements \JsonSerializable
{

    /**
     * @var ArrayOfCriterion $Criteria
     */
    protected $Criteria = null;

    /**
     * @var string $CustomerAllotmentGroupCode
     */
    protected $CustomerAllotmentGroupCode = null;

    /**
     * @var string $CustomerAllotmentGroupName
     */
    protected $CustomerAllotmentGroupName = null;

    /**
     * @var ArrayOfCustomer $CustomerList
     */
    protected $CustomerList = null;

    /**
     * @var string $HotelCode
     */
    protected $HotelCode = null;

    /**
     * @var ArrayOfAllotmentRec $Recs
     */
    protected $Recs = null;

    /**
     * @var ArrayOfExtraAllotmentRec $ExtraRecs
     */
    protected $ExtraRecs = null;

    /**
     * @var ArrayOfStayAllotmentRec $StayRecs
     */
    protected $StayRecs = null;

    
    public function __construct()
    {
    
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'Criteria' => $this->getCriteria(),
        'CustomerAllotmentGroupCode' => $this->getCustomerAllotmentGroupCode(),
        'CustomerAllotmentGroupName' => $this->getCustomerAllotmentGroupName(),
        'CustomerList' => $this->getCustomerList(),
        'HotelCode' => $this->getHotelCode(),
        'Recs' => $this->getRecs(),
        'ExtraRecs' => $this->getExtraRecs(),
        'StayRecs' => $this->getStayRecs(),
      );
    }

    /**
     * @return ArrayOfCriterion
     */
    public function getCriteria()
    {
      return $this->Criteria;
    }

    /**
     * @param ArrayOfCriterion $Criteria
     * @return \Dingus\SyncroService\PutAllotmentRQ
     */
    public function setCriteria($Criteria)
    {
      $this->Criteria = $Criteria;
      return $this;
    }

    /**
     * @return string
     */
    public function getCustomerAllotmentGroupCode()
    {
      return $this->CustomerAllotmentGroupCode;
    }

    /**
     * @param string $CustomerAllotmentGroupCode
     * @return \Dingus\SyncroService\PutAllotmentRQ
     */
    public function setCustomerAllotmentGroupCode($CustomerAllotmentGroupCode)
    {
      $this->CustomerAllotmentGroupCode = $CustomerAllotmentGroupCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getCustomerAllotmentGroupName()
    {
      return $this->CustomerAllotmentGroupName;
    }

    /**
     * @param string $CustomerAllotmentGroupName
     * @return \Dingus\SyncroService\PutAllotmentRQ
     */
    public function setCustomerAllotmentGroupName($CustomerAllotmentGroupName)
    {
      $this->CustomerAllotmentGroupName = $CustomerAllotmentGroupName;
      return $this;
    }

    /**
     * @return ArrayOfCustomer
     */
    public function getCustomerList()
    {
      return $this->CustomerList;
    }

    /**
     * @param ArrayOfCustomer $CustomerList
     * @return \Dingus\SyncroService\PutAllotmentRQ
     */
    public function setCustomerList($CustomerList)
    {
      $this->CustomerList = $CustomerList;
      return $this;
    }

    /**
     * @return string
     */
    public function getHotelCode()
    {
      return $this->HotelCode;
    }

    /**
     * @param string $HotelCode
     * @return \Dingus\SyncroService\PutAllotmentRQ
     */
    public function setHotelCode($HotelCode)
    {
      $this->HotelCode = $HotelCode;
      return $this;
    }

    /**
     * @return ArrayOfAllotmentRec
     */
    public function getRecs()
    {
      return $this->Recs;
    }

    /**
     * @param ArrayOfAllotmentRec $Recs
     * @return \Dingus\SyncroService\PutAllotmentRQ
     */
    public function setRecs($Recs)
    {
      $this->Recs = $Recs;
      return $this;
    }

    /**
     * @return ArrayOfExtraAllotmentRec
     */
    public function getExtraRecs()
    {
      return $this->ExtraRecs;
    }

    /**
     * @param ArrayOfExtraAllotmentRec $ExtraRecs
     * @return \Dingus\SyncroService\PutAllotmentRQ
     */
    public function setExtraRecs($ExtraRecs)
    {
      $this->ExtraRecs = $ExtraRecs;
      return $this;
    }

    /**
     * @return ArrayOfStayAllotmentRec
     */
    public function getStayRecs()
    {
      return $this->StayRecs;
    }

    /**
     * @param ArrayOfStayAllotmentRec $StayRecs
     * @return \Dingus\SyncroService\PutAllotmentRQ
     */
    public function setStayRecs($StayRecs)
    {
      $this->StayRecs = $StayRecs;
      return $this;
    }

}
