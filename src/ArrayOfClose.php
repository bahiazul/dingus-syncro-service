<?php

namespace Dingus\SyncroService;

class ArrayOfClose implements \JsonSerializable, \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var Close[] $Close
     */
    protected $Close = null;

    
    public function __construct()
    {
    
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'Close' => $this->getClose(),
      );
    }

    /**
     * @return Close[]
     */
    public function getClose()
    {
      return $this->Close;
    }

    /**
     * @param Close[] $Close
     * @return \Dingus\SyncroService\ArrayOfClose
     */
    public function setClose(array $Close = null)
    {
      $this->Close = $Close;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->Close[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return Close
     */
    public function offsetGet($offset)
    {
      return $this->Close[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param Close $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      $this->Close[$offset] = $value;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->Close[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return Close Return the current element
     */
    public function current()
    {
      return current($this->Close);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->Close);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->Close);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->Close);
    }

    /**
     * Countable implementation
     *
     * @return Close Return count of elements
     */
    public function count()
    {
      return count($this->Close);
    }

}
