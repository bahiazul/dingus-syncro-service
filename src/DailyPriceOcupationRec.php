<?php

namespace Dingus\SyncroService;

class DailyPriceOcupationRec implements \JsonSerializable
{

    /**
     * @var Action $Action
     */
    protected $Action = null;

    /**
     * @var int $Id
     */
    protected $Id = null;

    /**
     * @var string $RoomCode
     */
    protected $RoomCode = null;

    /**
     * @var int $Adults
     */
    protected $Adults = null;

    /**
     * @var int $Childs
     */
    protected $Childs = null;

    /**
     * @var string $Reference
     */
    protected $Reference = null;

    /**
     * @var ArrayOfDailyPriceOcupationPositionRec $Positions
     */
    protected $Positions = null;

    /**
     * @param Action $Action
     * @param int $Id
     * @param int $Adults
     * @param int $Childs
     */
    public function __construct($Action, $Id, $Adults, $Childs)
    {
      $this->Action = $Action;
      $this->Id = $Id;
      $this->Adults = $Adults;
      $this->Childs = $Childs;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'Action' => $this->getAction(),
        'Id' => $this->getId(),
        'RoomCode' => $this->getRoomCode(),
        'Adults' => $this->getAdults(),
        'Childs' => $this->getChilds(),
        'Reference' => $this->getReference(),
        'Positions' => $this->getPositions(),
      );
    }

    /**
     * @return Action
     */
    public function getAction()
    {
      return $this->Action;
    }

    /**
     * @param Action $Action
     * @return \Dingus\SyncroService\DailyPriceOcupationRec
     */
    public function setAction($Action)
    {
      $this->Action = $Action;
      return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
      return $this->Id;
    }

    /**
     * @param int $Id
     * @return \Dingus\SyncroService\DailyPriceOcupationRec
     */
    public function setId($Id)
    {
      $this->Id = $Id;
      return $this;
    }

    /**
     * @return string
     */
    public function getRoomCode()
    {
      return $this->RoomCode;
    }

    /**
     * @param string $RoomCode
     * @return \Dingus\SyncroService\DailyPriceOcupationRec
     */
    public function setRoomCode($RoomCode)
    {
      $this->RoomCode = $RoomCode;
      return $this;
    }

    /**
     * @return int
     */
    public function getAdults()
    {
      return $this->Adults;
    }

    /**
     * @param int $Adults
     * @return \Dingus\SyncroService\DailyPriceOcupationRec
     */
    public function setAdults($Adults)
    {
      $this->Adults = $Adults;
      return $this;
    }

    /**
     * @return int
     */
    public function getChilds()
    {
      return $this->Childs;
    }

    /**
     * @param int $Childs
     * @return \Dingus\SyncroService\DailyPriceOcupationRec
     */
    public function setChilds($Childs)
    {
      $this->Childs = $Childs;
      return $this;
    }

    /**
     * @return string
     */
    public function getReference()
    {
      return $this->Reference;
    }

    /**
     * @param string $Reference
     * @return \Dingus\SyncroService\DailyPriceOcupationRec
     */
    public function setReference($Reference)
    {
      $this->Reference = $Reference;
      return $this;
    }

    /**
     * @return ArrayOfDailyPriceOcupationPositionRec
     */
    public function getPositions()
    {
      return $this->Positions;
    }

    /**
     * @param ArrayOfDailyPriceOcupationPositionRec $Positions
     * @return \Dingus\SyncroService\DailyPriceOcupationRec
     */
    public function setPositions($Positions)
    {
      $this->Positions = $Positions;
      return $this;
    }

}
