<?php

namespace Dingus\SyncroService;

class ArrayOfCustomersTask implements \JsonSerializable, \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var CustomersTask[] $CustomersTask
     */
    protected $CustomersTask = null;

    
    public function __construct()
    {
    
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'CustomersTask' => $this->getCustomersTask(),
      );
    }

    /**
     * @return CustomersTask[]
     */
    public function getCustomersTask()
    {
      return $this->CustomersTask;
    }

    /**
     * @param CustomersTask[] $CustomersTask
     * @return \Dingus\SyncroService\ArrayOfCustomersTask
     */
    public function setCustomersTask(array $CustomersTask = null)
    {
      $this->CustomersTask = $CustomersTask;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->CustomersTask[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return CustomersTask
     */
    public function offsetGet($offset)
    {
      return $this->CustomersTask[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param CustomersTask $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      $this->CustomersTask[$offset] = $value;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->CustomersTask[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return CustomersTask Return the current element
     */
    public function current()
    {
      return current($this->CustomersTask);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->CustomersTask);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->CustomersTask);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->CustomersTask);
    }

    /**
     * Countable implementation
     *
     * @return CustomersTask Return count of elements
     */
    public function count()
    {
      return count($this->CustomersTask);
    }

}
