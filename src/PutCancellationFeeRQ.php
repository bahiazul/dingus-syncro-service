<?php

namespace Dingus\SyncroService;

class PutCancellationFeeRQ implements \JsonSerializable
{

    /**
     * @var ArrayOfCancellationFeeRec $CancellationFeeRQ
     */
    protected $CancellationFeeRQ = null;

    
    public function __construct()
    {
    
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'CancellationFeeRQ' => $this->getCancellationFeeRQ(),
      );
    }

    /**
     * @return ArrayOfCancellationFeeRec
     */
    public function getCancellationFeeRQ()
    {
      return $this->CancellationFeeRQ;
    }

    /**
     * @param ArrayOfCancellationFeeRec $CancellationFeeRQ
     * @return \Dingus\SyncroService\PutCancellationFeeRQ
     */
    public function setCancellationFeeRQ($CancellationFeeRQ)
    {
      $this->CancellationFeeRQ = $CancellationFeeRQ;
      return $this;
    }

}
