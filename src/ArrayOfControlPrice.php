<?php

namespace Dingus\SyncroService;

class ArrayOfControlPrice implements \JsonSerializable, \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var ControlPrice[] $ControlPrice
     */
    protected $ControlPrice = null;

    
    public function __construct()
    {
    
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'ControlPrice' => $this->getControlPrice(),
      );
    }

    /**
     * @return ControlPrice[]
     */
    public function getControlPrice()
    {
      return $this->ControlPrice;
    }

    /**
     * @param ControlPrice[] $ControlPrice
     * @return \Dingus\SyncroService\ArrayOfControlPrice
     */
    public function setControlPrice(array $ControlPrice = null)
    {
      $this->ControlPrice = $ControlPrice;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->ControlPrice[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return ControlPrice
     */
    public function offsetGet($offset)
    {
      return $this->ControlPrice[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param ControlPrice $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      $this->ControlPrice[$offset] = $value;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->ControlPrice[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return ControlPrice Return the current element
     */
    public function current()
    {
      return current($this->ControlPrice);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->ControlPrice);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->ControlPrice);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->ControlPrice);
    }

    /**
     * Countable implementation
     *
     * @return ControlPrice Return count of elements
     */
    public function count()
    {
      return count($this->ControlPrice);
    }

}
