<?php

namespace Dingus\SyncroService;

class PutBlockAllotmentResponse implements \JsonSerializable
{

    /**
     * @var SyncroRS $PutBlockAllotmentResult
     */
    protected $PutBlockAllotmentResult = null;

    /**
     * @param SyncroRS $PutBlockAllotmentResult
     */
    public function __construct($PutBlockAllotmentResult)
    {
      $this->PutBlockAllotmentResult = $PutBlockAllotmentResult;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'PutBlockAllotmentResult' => $this->getPutBlockAllotmentResult(),
      );
    }

    /**
     * @return SyncroRS
     */
    public function getPutBlockAllotmentResult()
    {
      return $this->PutBlockAllotmentResult;
    }

    /**
     * @param SyncroRS $PutBlockAllotmentResult
     * @return \Dingus\SyncroService\PutBlockAllotmentResponse
     */
    public function setPutBlockAllotmentResult($PutBlockAllotmentResult)
    {
      $this->PutBlockAllotmentResult = $PutBlockAllotmentResult;
      return $this;
    }

}
