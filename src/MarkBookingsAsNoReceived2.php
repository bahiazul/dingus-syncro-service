<?php

namespace Dingus\SyncroService;

class MarkBookingsAsNoReceived2 implements \JsonSerializable
{

    /**
     * @var Credentials $Credentials
     */
    protected $Credentials = null;

    /**
     * @var string $CustomerCode
     */
    protected $CustomerCode = null;

    /**
     * @var string $HotelCode
     */
    protected $HotelCode = null;

    /**
     * @var ArrayOfString $LocalizerCol
     */
    protected $LocalizerCol = null;

    /**
     * @param Credentials $Credentials
     * @param string $CustomerCode
     * @param string $HotelCode
     * @param ArrayOfString $LocalizerCol
     */
    public function __construct($Credentials, $CustomerCode, $HotelCode, $LocalizerCol)
    {
      $this->Credentials = $Credentials;
      $this->CustomerCode = $CustomerCode;
      $this->HotelCode = $HotelCode;
      $this->LocalizerCol = $LocalizerCol;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'Credentials' => $this->getCredentials(),
        'CustomerCode' => $this->getCustomerCode(),
        'HotelCode' => $this->getHotelCode(),
        'LocalizerCol' => $this->getLocalizerCol(),
      );
    }

    /**
     * @return Credentials
     */
    public function getCredentials()
    {
      return $this->Credentials;
    }

    /**
     * @param Credentials $Credentials
     * @return \Dingus\SyncroService\MarkBookingsAsNoReceived2
     */
    public function setCredentials($Credentials)
    {
      $this->Credentials = $Credentials;
      return $this;
    }

    /**
     * @return string
     */
    public function getCustomerCode()
    {
      return $this->CustomerCode;
    }

    /**
     * @param string $CustomerCode
     * @return \Dingus\SyncroService\MarkBookingsAsNoReceived2
     */
    public function setCustomerCode($CustomerCode)
    {
      $this->CustomerCode = $CustomerCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getHotelCode()
    {
      return $this->HotelCode;
    }

    /**
     * @param string $HotelCode
     * @return \Dingus\SyncroService\MarkBookingsAsNoReceived2
     */
    public function setHotelCode($HotelCode)
    {
      $this->HotelCode = $HotelCode;
      return $this;
    }

    /**
     * @return ArrayOfString
     */
    public function getLocalizerCol()
    {
      return $this->LocalizerCol;
    }

    /**
     * @param ArrayOfString $LocalizerCol
     * @return \Dingus\SyncroService\MarkBookingsAsNoReceived2
     */
    public function setLocalizerCol($LocalizerCol)
    {
      $this->LocalizerCol = $LocalizerCol;
      return $this;
    }

}
