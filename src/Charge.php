<?php

namespace Dingus\SyncroService;

class Charge implements \JsonSerializable
{

    /**
     * @var int $ChargeID
     */
    protected $ChargeID = null;

    /**
     * @var \DateTime $ChargeDate
     */
    protected $ChargeDate = null;

    /**
     * @var float $ChargeAmount
     */
    protected $ChargeAmount = null;

    /**
     * @var boolean $ChargePaid
     */
    protected $ChargePaid = null;

    /**
     * @var int $RoomNo
     */
    protected $RoomNo = null;

    /**
     * @param int $ChargeID
     * @param \DateTime $ChargeDate
     * @param float $ChargeAmount
     * @param boolean $ChargePaid
     * @param int $RoomNo
     */
    public function __construct($ChargeID, \DateTime $ChargeDate, $ChargeAmount, $ChargePaid, $RoomNo)
    {
      $this->ChargeID = $ChargeID;
      $this->ChargeDate = $ChargeDate->format(\DateTime::ATOM);
      $this->ChargeAmount = $ChargeAmount;
      $this->ChargePaid = $ChargePaid;
      $this->RoomNo = $RoomNo;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'ChargeID' => $this->getChargeID(),
        'ChargeDate' => $this->getChargeDate(),
        'ChargeAmount' => $this->getChargeAmount(),
        'ChargePaid' => $this->getChargePaid(),
        'RoomNo' => $this->getRoomNo(),
      );
    }

    /**
     * @return int
     */
    public function getChargeID()
    {
      return $this->ChargeID;
    }

    /**
     * @param int $ChargeID
     * @return \Dingus\SyncroService\Charge
     */
    public function setChargeID($ChargeID)
    {
      $this->ChargeID = $ChargeID;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getChargeDate()
    {
      if ($this->ChargeDate == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->ChargeDate);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $ChargeDate
     * @return \Dingus\SyncroService\Charge
     */
    public function setChargeDate(\DateTime $ChargeDate)
    {
      $this->ChargeDate = $ChargeDate->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return float
     */
    public function getChargeAmount()
    {
      return $this->ChargeAmount;
    }

    /**
     * @param float $ChargeAmount
     * @return \Dingus\SyncroService\Charge
     */
    public function setChargeAmount($ChargeAmount)
    {
      $this->ChargeAmount = $ChargeAmount;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getChargePaid()
    {
      return $this->ChargePaid;
    }

    /**
     * @param boolean $ChargePaid
     * @return \Dingus\SyncroService\Charge
     */
    public function setChargePaid($ChargePaid)
    {
      $this->ChargePaid = $ChargePaid;
      return $this;
    }

    /**
     * @return int
     */
    public function getRoomNo()
    {
      return $this->RoomNo;
    }

    /**
     * @param int $RoomNo
     * @return \Dingus\SyncroService\Charge
     */
    public function setRoomNo($RoomNo)
    {
      $this->RoomNo = $RoomNo;
      return $this;
    }

}
