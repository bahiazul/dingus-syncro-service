<?php

namespace Dingus\SyncroService;

class ArrayOfDailyPriceOcupationPositionRec implements \JsonSerializable, \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var DailyPriceOcupationPositionRec[] $DailyPriceOcupationPositionRec
     */
    protected $DailyPriceOcupationPositionRec = null;

    
    public function __construct()
    {
    
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'DailyPriceOcupationPositionRec' => $this->getDailyPriceOcupationPositionRec(),
      );
    }

    /**
     * @return DailyPriceOcupationPositionRec[]
     */
    public function getDailyPriceOcupationPositionRec()
    {
      return $this->DailyPriceOcupationPositionRec;
    }

    /**
     * @param DailyPriceOcupationPositionRec[] $DailyPriceOcupationPositionRec
     * @return \Dingus\SyncroService\ArrayOfDailyPriceOcupationPositionRec
     */
    public function setDailyPriceOcupationPositionRec(array $DailyPriceOcupationPositionRec = null)
    {
      $this->DailyPriceOcupationPositionRec = $DailyPriceOcupationPositionRec;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->DailyPriceOcupationPositionRec[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return DailyPriceOcupationPositionRec
     */
    public function offsetGet($offset)
    {
      return $this->DailyPriceOcupationPositionRec[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param DailyPriceOcupationPositionRec $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      $this->DailyPriceOcupationPositionRec[$offset] = $value;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->DailyPriceOcupationPositionRec[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return DailyPriceOcupationPositionRec Return the current element
     */
    public function current()
    {
      return current($this->DailyPriceOcupationPositionRec);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->DailyPriceOcupationPositionRec);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->DailyPriceOcupationPositionRec);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->DailyPriceOcupationPositionRec);
    }

    /**
     * Countable implementation
     *
     * @return DailyPriceOcupationPositionRec Return count of elements
     */
    public function count()
    {
      return count($this->DailyPriceOcupationPositionRec);
    }

}
