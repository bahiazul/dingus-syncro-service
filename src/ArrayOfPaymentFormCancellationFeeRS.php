<?php

namespace Dingus\SyncroService;

class ArrayOfPaymentFormCancellationFeeRS implements \JsonSerializable, \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var PaymentFormCancellationFeeRS[] $PaymentFormCancellationFeeRS
     */
    protected $PaymentFormCancellationFeeRS = null;

    
    public function __construct()
    {
    
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'PaymentFormCancellationFeeRS' => $this->getPaymentFormCancellationFeeRS(),
      );
    }

    /**
     * @return PaymentFormCancellationFeeRS[]
     */
    public function getPaymentFormCancellationFeeRS()
    {
      return $this->PaymentFormCancellationFeeRS;
    }

    /**
     * @param PaymentFormCancellationFeeRS[] $PaymentFormCancellationFeeRS
     * @return \Dingus\SyncroService\ArrayOfPaymentFormCancellationFeeRS
     */
    public function setPaymentFormCancellationFeeRS(array $PaymentFormCancellationFeeRS = null)
    {
      $this->PaymentFormCancellationFeeRS = $PaymentFormCancellationFeeRS;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->PaymentFormCancellationFeeRS[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return PaymentFormCancellationFeeRS
     */
    public function offsetGet($offset)
    {
      return $this->PaymentFormCancellationFeeRS[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param PaymentFormCancellationFeeRS $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      $this->PaymentFormCancellationFeeRS[$offset] = $value;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->PaymentFormCancellationFeeRS[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return PaymentFormCancellationFeeRS Return the current element
     */
    public function current()
    {
      return current($this->PaymentFormCancellationFeeRS);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->PaymentFormCancellationFeeRS);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->PaymentFormCancellationFeeRS);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->PaymentFormCancellationFeeRS);
    }

    /**
     * Countable implementation
     *
     * @return PaymentFormCancellationFeeRS Return count of elements
     */
    public function count()
    {
      return count($this->PaymentFormCancellationFeeRS);
    }

}
