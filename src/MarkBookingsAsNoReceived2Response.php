<?php

namespace Dingus\SyncroService;

class MarkBookingsAsNoReceived2Response implements \JsonSerializable
{

    /**
     * @var SyncroRS $MarkBookingsAsNoReceived2Result
     */
    protected $MarkBookingsAsNoReceived2Result = null;

    /**
     * @param SyncroRS $MarkBookingsAsNoReceived2Result
     */
    public function __construct($MarkBookingsAsNoReceived2Result)
    {
      $this->MarkBookingsAsNoReceived2Result = $MarkBookingsAsNoReceived2Result;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'MarkBookingsAsNoReceived2Result' => $this->getMarkBookingsAsNoReceived2Result(),
      );
    }

    /**
     * @return SyncroRS
     */
    public function getMarkBookingsAsNoReceived2Result()
    {
      return $this->MarkBookingsAsNoReceived2Result;
    }

    /**
     * @param SyncroRS $MarkBookingsAsNoReceived2Result
     * @return \Dingus\SyncroService\MarkBookingsAsNoReceived2Response
     */
    public function setMarkBookingsAsNoReceived2Result($MarkBookingsAsNoReceived2Result)
    {
      $this->MarkBookingsAsNoReceived2Result = $MarkBookingsAsNoReceived2Result;
      return $this;
    }

}
