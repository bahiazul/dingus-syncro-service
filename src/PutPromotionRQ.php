<?php

namespace Dingus\SyncroService;

class PutPromotionRQ implements \JsonSerializable
{

    /**
     * @var ArrayOfMlText $PromotionDescriptionMlList
     */
    protected $PromotionDescriptionMlList = null;

    /**
     * @var ArrayOfCustomerPromotionRec $LCustomersPromotionRec
     */
    protected $LCustomersPromotionRec = null;

    /**
     * @var ArrayOfRateGroupPromotionRec $LRateGroupsPromotionRec
     */
    protected $LRateGroupsPromotionRec = null;

    /**
     * @var ArrayOfPromotionLineRec $LPromotionsRec
     */
    protected $LPromotionsRec = null;

    /**
     * @var ArrayOfExtraPromotionRec $LExtrasPromotionRec
     */
    protected $LExtrasPromotionRec = null;

    /**
     * @var ArrayOfStopSalesPromotionRec $LStopSalesPromotionRec
     */
    protected $LStopSalesPromotionRec = null;

    /**
     * @var Action $Action
     */
    protected $Action = null;

    /**
     * @var int $IdPromotion
     */
    protected $IdPromotion = null;

    /**
     * @var string $PromotionCode
     */
    protected $PromotionCode = null;

    /**
     * @var string $PromotionDescription
     */
    protected $PromotionDescription = null;

    /**
     * @var boolean $PublicPromotion
     */
    protected $PublicPromotion = null;

    /**
     * @var ContainedServiceRec $ContainedService
     */
    protected $ContainedService = null;

    /**
     * @var int $PriorityOfPublic
     */
    protected $PriorityOfPublic = null;

    /**
     * @var boolean $CancelSupl
     */
    protected $CancelSupl = null;

    /**
     * @var boolean $CancelOfer
     */
    protected $CancelOfer = null;

    /**
     * @var boolean $CancelAxB
     */
    protected $CancelAxB = null;

    /**
     * @var TipoFechasPrecio $DatesType
     */
    protected $DatesType = null;

    /**
     * @var \DateTime $DateFrom
     */
    protected $DateFrom = null;

    /**
     * @var \DateTime $DateTo
     */
    protected $DateTo = null;

    /**
     * @var \DateTime $DateFrom2
     */
    protected $DateFrom2 = null;

    /**
     * @var \DateTime $DateTo2
     */
    protected $DateTo2 = null;

    /**
     * @var int $DaysBeforeSalesFrom
     */
    protected $DaysBeforeSalesFrom = null;

    /**
     * @var int $DaysBeforeSalesTo
     */
    protected $DaysBeforeSalesTo = null;

    /**
     * @var int $MaxStay
     */
    protected $MaxStay = null;

    /**
     * @var int $MinStay
     */
    protected $MinStay = null;

    /**
     * @var int $PaxFrom
     */
    protected $PaxFrom = null;

    /**
     * @var int $PaxTo
     */
    protected $PaxTo = null;

    /**
     * @var int $AdultFrom
     */
    protected $AdultFrom = null;

    /**
     * @var int $AdultTo
     */
    protected $AdultTo = null;

    /**
     * @var int $ChildrenFrom
     */
    protected $ChildrenFrom = null;

    /**
     * @var int $ChildrenTo
     */
    protected $ChildrenTo = null;

    /**
     * @var int $EnfantFrom
     */
    protected $EnfantFrom = null;

    /**
     * @var int $EnfantTo
     */
    protected $EnfantTo = null;

    /**
     * @var string $HotelCode
     */
    protected $HotelCode = null;

    /**
     * @var string $RoomCode
     */
    protected $RoomCode = null;

    /**
     * @var string $BoardCode
     */
    protected $BoardCode = null;

    /**
     * @var string $PMSCode
     */
    protected $PMSCode = null;

    /**
     * @param Action $Action
     * @param int $IdPromotion
     * @param boolean $PublicPromotion
     * @param int $PriorityOfPublic
     * @param boolean $CancelSupl
     * @param boolean $CancelOfer
     * @param boolean $CancelAxB
     * @param TipoFechasPrecio $DatesType
     * @param \DateTime $DateFrom
     * @param \DateTime $DateTo
     * @param \DateTime $DateFrom2
     * @param \DateTime $DateTo2
     * @param int $DaysBeforeSalesFrom
     * @param int $DaysBeforeSalesTo
     * @param int $MaxStay
     * @param int $MinStay
     * @param int $PaxFrom
     * @param int $PaxTo
     * @param int $AdultFrom
     * @param int $AdultTo
     * @param int $ChildrenFrom
     * @param int $ChildrenTo
     * @param int $EnfantFrom
     * @param int $EnfantTo
     */
    public function __construct($Action, $IdPromotion, $PublicPromotion, $PriorityOfPublic, $CancelSupl, $CancelOfer, $CancelAxB, $DatesType, \DateTime $DateFrom, \DateTime $DateTo, \DateTime $DateFrom2, \DateTime $DateTo2, $DaysBeforeSalesFrom, $DaysBeforeSalesTo, $MaxStay, $MinStay, $PaxFrom, $PaxTo, $AdultFrom, $AdultTo, $ChildrenFrom, $ChildrenTo, $EnfantFrom, $EnfantTo)
    {
      $this->Action = $Action;
      $this->IdPromotion = $IdPromotion;
      $this->PublicPromotion = $PublicPromotion;
      $this->PriorityOfPublic = $PriorityOfPublic;
      $this->CancelSupl = $CancelSupl;
      $this->CancelOfer = $CancelOfer;
      $this->CancelAxB = $CancelAxB;
      $this->DatesType = $DatesType;
      $this->DateFrom = $DateFrom->format(\DateTime::ATOM);
      $this->DateTo = $DateTo->format(\DateTime::ATOM);
      $this->DateFrom2 = $DateFrom2->format(\DateTime::ATOM);
      $this->DateTo2 = $DateTo2->format(\DateTime::ATOM);
      $this->DaysBeforeSalesFrom = $DaysBeforeSalesFrom;
      $this->DaysBeforeSalesTo = $DaysBeforeSalesTo;
      $this->MaxStay = $MaxStay;
      $this->MinStay = $MinStay;
      $this->PaxFrom = $PaxFrom;
      $this->PaxTo = $PaxTo;
      $this->AdultFrom = $AdultFrom;
      $this->AdultTo = $AdultTo;
      $this->ChildrenFrom = $ChildrenFrom;
      $this->ChildrenTo = $ChildrenTo;
      $this->EnfantFrom = $EnfantFrom;
      $this->EnfantTo = $EnfantTo;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'PromotionDescriptionMlList' => $this->getPromotionDescriptionMlList(),
        'LCustomersPromotionRec' => $this->getLCustomersPromotionRec(),
        'LRateGroupsPromotionRec' => $this->getLRateGroupsPromotionRec(),
        'LPromotionsRec' => $this->getLPromotionsRec(),
        'LExtrasPromotionRec' => $this->getLExtrasPromotionRec(),
        'LStopSalesPromotionRec' => $this->getLStopSalesPromotionRec(),
        'Action' => $this->getAction(),
        'IdPromotion' => $this->getIdPromotion(),
        'PromotionCode' => $this->getPromotionCode(),
        'PromotionDescription' => $this->getPromotionDescription(),
        'PublicPromotion' => $this->getPublicPromotion(),
        'ContainedService' => $this->getContainedService(),
        'PriorityOfPublic' => $this->getPriorityOfPublic(),
        'CancelSupl' => $this->getCancelSupl(),
        'CancelOfer' => $this->getCancelOfer(),
        'CancelAxB' => $this->getCancelAxB(),
        'DatesType' => $this->getDatesType(),
        'DateFrom' => $this->getDateFrom(),
        'DateTo' => $this->getDateTo(),
        'DateFrom2' => $this->getDateFrom2(),
        'DateTo2' => $this->getDateTo2(),
        'DaysBeforeSalesFrom' => $this->getDaysBeforeSalesFrom(),
        'DaysBeforeSalesTo' => $this->getDaysBeforeSalesTo(),
        'MaxStay' => $this->getMaxStay(),
        'MinStay' => $this->getMinStay(),
        'PaxFrom' => $this->getPaxFrom(),
        'PaxTo' => $this->getPaxTo(),
        'AdultFrom' => $this->getAdultFrom(),
        'AdultTo' => $this->getAdultTo(),
        'ChildrenFrom' => $this->getChildrenFrom(),
        'ChildrenTo' => $this->getChildrenTo(),
        'EnfantFrom' => $this->getEnfantFrom(),
        'EnfantTo' => $this->getEnfantTo(),
        'HotelCode' => $this->getHotelCode(),
        'RoomCode' => $this->getRoomCode(),
        'BoardCode' => $this->getBoardCode(),
        'PMSCode' => $this->getPMSCode(),
      );
    }

    /**
     * @return ArrayOfMlText
     */
    public function getPromotionDescriptionMlList()
    {
      return $this->PromotionDescriptionMlList;
    }

    /**
     * @param ArrayOfMlText $PromotionDescriptionMlList
     * @return \Dingus\SyncroService\PutPromotionRQ
     */
    public function setPromotionDescriptionMlList($PromotionDescriptionMlList)
    {
      $this->PromotionDescriptionMlList = $PromotionDescriptionMlList;
      return $this;
    }

    /**
     * @return ArrayOfCustomerPromotionRec
     */
    public function getLCustomersPromotionRec()
    {
      return $this->LCustomersPromotionRec;
    }

    /**
     * @param ArrayOfCustomerPromotionRec $LCustomersPromotionRec
     * @return \Dingus\SyncroService\PutPromotionRQ
     */
    public function setLCustomersPromotionRec($LCustomersPromotionRec)
    {
      $this->LCustomersPromotionRec = $LCustomersPromotionRec;
      return $this;
    }

    /**
     * @return ArrayOfRateGroupPromotionRec
     */
    public function getLRateGroupsPromotionRec()
    {
      return $this->LRateGroupsPromotionRec;
    }

    /**
     * @param ArrayOfRateGroupPromotionRec $LRateGroupsPromotionRec
     * @return \Dingus\SyncroService\PutPromotionRQ
     */
    public function setLRateGroupsPromotionRec($LRateGroupsPromotionRec)
    {
      $this->LRateGroupsPromotionRec = $LRateGroupsPromotionRec;
      return $this;
    }

    /**
     * @return ArrayOfPromotionLineRec
     */
    public function getLPromotionsRec()
    {
      return $this->LPromotionsRec;
    }

    /**
     * @param ArrayOfPromotionLineRec $LPromotionsRec
     * @return \Dingus\SyncroService\PutPromotionRQ
     */
    public function setLPromotionsRec($LPromotionsRec)
    {
      $this->LPromotionsRec = $LPromotionsRec;
      return $this;
    }

    /**
     * @return ArrayOfExtraPromotionRec
     */
    public function getLExtrasPromotionRec()
    {
      return $this->LExtrasPromotionRec;
    }

    /**
     * @param ArrayOfExtraPromotionRec $LExtrasPromotionRec
     * @return \Dingus\SyncroService\PutPromotionRQ
     */
    public function setLExtrasPromotionRec($LExtrasPromotionRec)
    {
      $this->LExtrasPromotionRec = $LExtrasPromotionRec;
      return $this;
    }

    /**
     * @return ArrayOfStopSalesPromotionRec
     */
    public function getLStopSalesPromotionRec()
    {
      return $this->LStopSalesPromotionRec;
    }

    /**
     * @param ArrayOfStopSalesPromotionRec $LStopSalesPromotionRec
     * @return \Dingus\SyncroService\PutPromotionRQ
     */
    public function setLStopSalesPromotionRec($LStopSalesPromotionRec)
    {
      $this->LStopSalesPromotionRec = $LStopSalesPromotionRec;
      return $this;
    }

    /**
     * @return Action
     */
    public function getAction()
    {
      return $this->Action;
    }

    /**
     * @param Action $Action
     * @return \Dingus\SyncroService\PutPromotionRQ
     */
    public function setAction($Action)
    {
      $this->Action = $Action;
      return $this;
    }

    /**
     * @return int
     */
    public function getIdPromotion()
    {
      return $this->IdPromotion;
    }

    /**
     * @param int $IdPromotion
     * @return \Dingus\SyncroService\PutPromotionRQ
     */
    public function setIdPromotion($IdPromotion)
    {
      $this->IdPromotion = $IdPromotion;
      return $this;
    }

    /**
     * @return string
     */
    public function getPromotionCode()
    {
      return $this->PromotionCode;
    }

    /**
     * @param string $PromotionCode
     * @return \Dingus\SyncroService\PutPromotionRQ
     */
    public function setPromotionCode($PromotionCode)
    {
      $this->PromotionCode = $PromotionCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getPromotionDescription()
    {
      return $this->PromotionDescription;
    }

    /**
     * @param string $PromotionDescription
     * @return \Dingus\SyncroService\PutPromotionRQ
     */
    public function setPromotionDescription($PromotionDescription)
    {
      $this->PromotionDescription = $PromotionDescription;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getPublicPromotion()
    {
      return $this->PublicPromotion;
    }

    /**
     * @param boolean $PublicPromotion
     * @return \Dingus\SyncroService\PutPromotionRQ
     */
    public function setPublicPromotion($PublicPromotion)
    {
      $this->PublicPromotion = $PublicPromotion;
      return $this;
    }

    /**
     * @return ContainedServiceRec
     */
    public function getContainedService()
    {
      return $this->ContainedService;
    }

    /**
     * @param ContainedServiceRec $ContainedService
     * @return \Dingus\SyncroService\PutPromotionRQ
     */
    public function setContainedService($ContainedService)
    {
      $this->ContainedService = $ContainedService;
      return $this;
    }

    /**
     * @return int
     */
    public function getPriorityOfPublic()
    {
      return $this->PriorityOfPublic;
    }

    /**
     * @param int $PriorityOfPublic
     * @return \Dingus\SyncroService\PutPromotionRQ
     */
    public function setPriorityOfPublic($PriorityOfPublic)
    {
      $this->PriorityOfPublic = $PriorityOfPublic;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getCancelSupl()
    {
      return $this->CancelSupl;
    }

    /**
     * @param boolean $CancelSupl
     * @return \Dingus\SyncroService\PutPromotionRQ
     */
    public function setCancelSupl($CancelSupl)
    {
      $this->CancelSupl = $CancelSupl;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getCancelOfer()
    {
      return $this->CancelOfer;
    }

    /**
     * @param boolean $CancelOfer
     * @return \Dingus\SyncroService\PutPromotionRQ
     */
    public function setCancelOfer($CancelOfer)
    {
      $this->CancelOfer = $CancelOfer;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getCancelAxB()
    {
      return $this->CancelAxB;
    }

    /**
     * @param boolean $CancelAxB
     * @return \Dingus\SyncroService\PutPromotionRQ
     */
    public function setCancelAxB($CancelAxB)
    {
      $this->CancelAxB = $CancelAxB;
      return $this;
    }

    /**
     * @return TipoFechasPrecio
     */
    public function getDatesType()
    {
      return $this->DatesType;
    }

    /**
     * @param TipoFechasPrecio $DatesType
     * @return \Dingus\SyncroService\PutPromotionRQ
     */
    public function setDatesType($DatesType)
    {
      $this->DatesType = $DatesType;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateFrom()
    {
      if ($this->DateFrom == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->DateFrom);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $DateFrom
     * @return \Dingus\SyncroService\PutPromotionRQ
     */
    public function setDateFrom(\DateTime $DateFrom)
    {
      $this->DateFrom = $DateFrom->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateTo()
    {
      if ($this->DateTo == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->DateTo);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $DateTo
     * @return \Dingus\SyncroService\PutPromotionRQ
     */
    public function setDateTo(\DateTime $DateTo)
    {
      $this->DateTo = $DateTo->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateFrom2()
    {
      if ($this->DateFrom2 == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->DateFrom2);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $DateFrom2
     * @return \Dingus\SyncroService\PutPromotionRQ
     */
    public function setDateFrom2(\DateTime $DateFrom2)
    {
      $this->DateFrom2 = $DateFrom2->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateTo2()
    {
      if ($this->DateTo2 == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->DateTo2);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $DateTo2
     * @return \Dingus\SyncroService\PutPromotionRQ
     */
    public function setDateTo2(\DateTime $DateTo2)
    {
      $this->DateTo2 = $DateTo2->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return int
     */
    public function getDaysBeforeSalesFrom()
    {
      return $this->DaysBeforeSalesFrom;
    }

    /**
     * @param int $DaysBeforeSalesFrom
     * @return \Dingus\SyncroService\PutPromotionRQ
     */
    public function setDaysBeforeSalesFrom($DaysBeforeSalesFrom)
    {
      $this->DaysBeforeSalesFrom = $DaysBeforeSalesFrom;
      return $this;
    }

    /**
     * @return int
     */
    public function getDaysBeforeSalesTo()
    {
      return $this->DaysBeforeSalesTo;
    }

    /**
     * @param int $DaysBeforeSalesTo
     * @return \Dingus\SyncroService\PutPromotionRQ
     */
    public function setDaysBeforeSalesTo($DaysBeforeSalesTo)
    {
      $this->DaysBeforeSalesTo = $DaysBeforeSalesTo;
      return $this;
    }

    /**
     * @return int
     */
    public function getMaxStay()
    {
      return $this->MaxStay;
    }

    /**
     * @param int $MaxStay
     * @return \Dingus\SyncroService\PutPromotionRQ
     */
    public function setMaxStay($MaxStay)
    {
      $this->MaxStay = $MaxStay;
      return $this;
    }

    /**
     * @return int
     */
    public function getMinStay()
    {
      return $this->MinStay;
    }

    /**
     * @param int $MinStay
     * @return \Dingus\SyncroService\PutPromotionRQ
     */
    public function setMinStay($MinStay)
    {
      $this->MinStay = $MinStay;
      return $this;
    }

    /**
     * @return int
     */
    public function getPaxFrom()
    {
      return $this->PaxFrom;
    }

    /**
     * @param int $PaxFrom
     * @return \Dingus\SyncroService\PutPromotionRQ
     */
    public function setPaxFrom($PaxFrom)
    {
      $this->PaxFrom = $PaxFrom;
      return $this;
    }

    /**
     * @return int
     */
    public function getPaxTo()
    {
      return $this->PaxTo;
    }

    /**
     * @param int $PaxTo
     * @return \Dingus\SyncroService\PutPromotionRQ
     */
    public function setPaxTo($PaxTo)
    {
      $this->PaxTo = $PaxTo;
      return $this;
    }

    /**
     * @return int
     */
    public function getAdultFrom()
    {
      return $this->AdultFrom;
    }

    /**
     * @param int $AdultFrom
     * @return \Dingus\SyncroService\PutPromotionRQ
     */
    public function setAdultFrom($AdultFrom)
    {
      $this->AdultFrom = $AdultFrom;
      return $this;
    }

    /**
     * @return int
     */
    public function getAdultTo()
    {
      return $this->AdultTo;
    }

    /**
     * @param int $AdultTo
     * @return \Dingus\SyncroService\PutPromotionRQ
     */
    public function setAdultTo($AdultTo)
    {
      $this->AdultTo = $AdultTo;
      return $this;
    }

    /**
     * @return int
     */
    public function getChildrenFrom()
    {
      return $this->ChildrenFrom;
    }

    /**
     * @param int $ChildrenFrom
     * @return \Dingus\SyncroService\PutPromotionRQ
     */
    public function setChildrenFrom($ChildrenFrom)
    {
      $this->ChildrenFrom = $ChildrenFrom;
      return $this;
    }

    /**
     * @return int
     */
    public function getChildrenTo()
    {
      return $this->ChildrenTo;
    }

    /**
     * @param int $ChildrenTo
     * @return \Dingus\SyncroService\PutPromotionRQ
     */
    public function setChildrenTo($ChildrenTo)
    {
      $this->ChildrenTo = $ChildrenTo;
      return $this;
    }

    /**
     * @return int
     */
    public function getEnfantFrom()
    {
      return $this->EnfantFrom;
    }

    /**
     * @param int $EnfantFrom
     * @return \Dingus\SyncroService\PutPromotionRQ
     */
    public function setEnfantFrom($EnfantFrom)
    {
      $this->EnfantFrom = $EnfantFrom;
      return $this;
    }

    /**
     * @return int
     */
    public function getEnfantTo()
    {
      return $this->EnfantTo;
    }

    /**
     * @param int $EnfantTo
     * @return \Dingus\SyncroService\PutPromotionRQ
     */
    public function setEnfantTo($EnfantTo)
    {
      $this->EnfantTo = $EnfantTo;
      return $this;
    }

    /**
     * @return string
     */
    public function getHotelCode()
    {
      return $this->HotelCode;
    }

    /**
     * @param string $HotelCode
     * @return \Dingus\SyncroService\PutPromotionRQ
     */
    public function setHotelCode($HotelCode)
    {
      $this->HotelCode = $HotelCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getRoomCode()
    {
      return $this->RoomCode;
    }

    /**
     * @param string $RoomCode
     * @return \Dingus\SyncroService\PutPromotionRQ
     */
    public function setRoomCode($RoomCode)
    {
      $this->RoomCode = $RoomCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getBoardCode()
    {
      return $this->BoardCode;
    }

    /**
     * @param string $BoardCode
     * @return \Dingus\SyncroService\PutPromotionRQ
     */
    public function setBoardCode($BoardCode)
    {
      $this->BoardCode = $BoardCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getPMSCode()
    {
      return $this->PMSCode;
    }

    /**
     * @param string $PMSCode
     * @return \Dingus\SyncroService\PutPromotionRQ
     */
    public function setPMSCode($PMSCode)
    {
      $this->PMSCode = $PMSCode;
      return $this;
    }

}
