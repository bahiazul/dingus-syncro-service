<?php

namespace Dingus\SyncroService;

class Instalaciones implements \JsonSerializable
{

    /**
     * @var ArrayOfMlText $DescriptionMlList
     */
    protected $DescriptionMlList = null;

    /**
     * @var ArrayOfPicture $Pictures
     */
    protected $Pictures = null;

    /**
     * @var Action $Action
     */
    protected $Action = null;

    /**
     * @var int $Id
     */
    protected $Id = null;

    /**
     * @var string $Name
     */
    protected $Name = null;

    /**
     * @var string $Description
     */
    protected $Description = null;

    /**
     * @var boolean $Available
     */
    protected $Available = null;

    /**
     * @param Action $Action
     * @param int $Id
     * @param boolean $Available
     */
    public function __construct($Action, $Id, $Available)
    {
      $this->Action = $Action;
      $this->Id = $Id;
      $this->Available = $Available;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'DescriptionMlList' => $this->getDescriptionMlList(),
        'Pictures' => $this->getPictures(),
        'Action' => $this->getAction(),
        'Id' => $this->getId(),
        'Name' => $this->getName(),
        'Description' => $this->getDescription(),
        'Available' => $this->getAvailable(),
      );
    }

    /**
     * @return ArrayOfMlText
     */
    public function getDescriptionMlList()
    {
      return $this->DescriptionMlList;
    }

    /**
     * @param ArrayOfMlText $DescriptionMlList
     * @return \Dingus\SyncroService\Instalaciones
     */
    public function setDescriptionMlList($DescriptionMlList)
    {
      $this->DescriptionMlList = $DescriptionMlList;
      return $this;
    }

    /**
     * @return ArrayOfPicture
     */
    public function getPictures()
    {
      return $this->Pictures;
    }

    /**
     * @param ArrayOfPicture $Pictures
     * @return \Dingus\SyncroService\Instalaciones
     */
    public function setPictures($Pictures)
    {
      $this->Pictures = $Pictures;
      return $this;
    }

    /**
     * @return Action
     */
    public function getAction()
    {
      return $this->Action;
    }

    /**
     * @param Action $Action
     * @return \Dingus\SyncroService\Instalaciones
     */
    public function setAction($Action)
    {
      $this->Action = $Action;
      return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
      return $this->Id;
    }

    /**
     * @param int $Id
     * @return \Dingus\SyncroService\Instalaciones
     */
    public function setId($Id)
    {
      $this->Id = $Id;
      return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
      return $this->Name;
    }

    /**
     * @param string $Name
     * @return \Dingus\SyncroService\Instalaciones
     */
    public function setName($Name)
    {
      $this->Name = $Name;
      return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
      return $this->Description;
    }

    /**
     * @param string $Description
     * @return \Dingus\SyncroService\Instalaciones
     */
    public function setDescription($Description)
    {
      $this->Description = $Description;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getAvailable()
    {
      return $this->Available;
    }

    /**
     * @param boolean $Available
     * @return \Dingus\SyncroService\Instalaciones
     */
    public function setAvailable($Available)
    {
      $this->Available = $Available;
      return $this;
    }

}
