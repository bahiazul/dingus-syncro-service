<?php

namespace Dingus\SyncroService;

class HotelGroupsRec implements \JsonSerializable
{

    /**
     * @var Action $Action
     */
    protected $Action = null;

    /**
     * @var int $Id
     */
    protected $Id = null;

    /**
     * @var string $HotelCode
     */
    protected $HotelCode = null;

    /**
     * @var string $HotelName
     */
    protected $HotelName = null;

    /**
     * @var string $CustomerCode
     */
    protected $CustomerCode = null;

    /**
     * @var string $CustomerName
     */
    protected $CustomerName = null;

    /**
     * @var string $FiscalCustomerCode
     */
    protected $FiscalCustomerCode = null;

    /**
     * @var string $FiscalCustomerName
     */
    protected $FiscalCustomerName = null;

    /**
     * @var string $RateGroupCode
     */
    protected $RateGroupCode = null;

    /**
     * @var ContainedServiceRec $ContainedService
     */
    protected $ContainedService = null;

    /**
     * @var string $AllotmentGroupCode
     */
    protected $AllotmentGroupCode = null;

    /**
     * @var string $AllotmentGroupExtraCode
     */
    protected $AllotmentGroupExtraCode = null;

    /**
     * @var string $TypeRounding
     */
    protected $TypeRounding = null;

    /**
     * @var boolean $AcceptsRQ
     */
    protected $AcceptsRQ = null;

    /**
     * @var boolean $ByDefault
     */
    protected $ByDefault = null;

    /**
     * @var TipoCalculoTarifaHotel $RateType
     */
    protected $RateType = null;

    /**
     * @var float $PercentRound
     */
    protected $PercentRound = null;

    /**
     * @var boolean $DailyRate
     */
    protected $DailyRate = null;

    /**
     * @var boolean $PrivateRate
     */
    protected $PrivateRate = null;

    /**
     * @var \DateTime $DateFrom
     */
    protected $DateFrom = null;

    /**
     * @var \DateTime $DateTo
     */
    protected $DateTo = null;

    /**
     * @var float $PercentNetPriceRule
     */
    protected $PercentNetPriceRule = null;

    /**
     * @var boolean $NoApplyRoomNetPriceRule
     */
    protected $NoApplyRoomNetPriceRule = null;

    /**
     * @var boolean $NoApplyBoardNetPriceRule
     */
    protected $NoApplyBoardNetPriceRule = null;

    /**
     * @var boolean $NoApplySuplNetPriceRule
     */
    protected $NoApplySuplNetPriceRule = null;

    /**
     * @var boolean $NoApplyExtraNetPriceRule
     */
    protected $NoApplyExtraNetPriceRule = null;

    /**
     * @param Action $Action
     * @param int $Id
     * @param boolean $AcceptsRQ
     * @param boolean $ByDefault
     * @param TipoCalculoTarifaHotel $RateType
     * @param float $PercentRound
     * @param boolean $DailyRate
     * @param boolean $PrivateRate
     * @param \DateTime $DateFrom
     * @param \DateTime $DateTo
     * @param float $PercentNetPriceRule
     * @param boolean $NoApplyRoomNetPriceRule
     * @param boolean $NoApplyBoardNetPriceRule
     * @param boolean $NoApplySuplNetPriceRule
     * @param boolean $NoApplyExtraNetPriceRule
     */
    public function __construct($Action, $Id, $AcceptsRQ, $ByDefault, $RateType, $PercentRound, $DailyRate, $PrivateRate, \DateTime $DateFrom, \DateTime $DateTo, $PercentNetPriceRule, $NoApplyRoomNetPriceRule, $NoApplyBoardNetPriceRule, $NoApplySuplNetPriceRule, $NoApplyExtraNetPriceRule)
    {
      $this->Action = $Action;
      $this->Id = $Id;
      $this->AcceptsRQ = $AcceptsRQ;
      $this->ByDefault = $ByDefault;
      $this->RateType = $RateType;
      $this->PercentRound = $PercentRound;
      $this->DailyRate = $DailyRate;
      $this->PrivateRate = $PrivateRate;
      $this->DateFrom = $DateFrom->format(\DateTime::ATOM);
      $this->DateTo = $DateTo->format(\DateTime::ATOM);
      $this->PercentNetPriceRule = $PercentNetPriceRule;
      $this->NoApplyRoomNetPriceRule = $NoApplyRoomNetPriceRule;
      $this->NoApplyBoardNetPriceRule = $NoApplyBoardNetPriceRule;
      $this->NoApplySuplNetPriceRule = $NoApplySuplNetPriceRule;
      $this->NoApplyExtraNetPriceRule = $NoApplyExtraNetPriceRule;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'Action' => $this->getAction(),
        'Id' => $this->getId(),
        'HotelCode' => $this->getHotelCode(),
        'HotelName' => $this->getHotelName(),
        'CustomerCode' => $this->getCustomerCode(),
        'CustomerName' => $this->getCustomerName(),
        'FiscalCustomerCode' => $this->getFiscalCustomerCode(),
        'FiscalCustomerName' => $this->getFiscalCustomerName(),
        'RateGroupCode' => $this->getRateGroupCode(),
        'ContainedService' => $this->getContainedService(),
        'AllotmentGroupCode' => $this->getAllotmentGroupCode(),
        'AllotmentGroupExtraCode' => $this->getAllotmentGroupExtraCode(),
        'TypeRounding' => $this->getTypeRounding(),
        'AcceptsRQ' => $this->getAcceptsRQ(),
        'ByDefault' => $this->getByDefault(),
        'RateType' => $this->getRateType(),
        'PercentRound' => $this->getPercentRound(),
        'DailyRate' => $this->getDailyRate(),
        'PrivateRate' => $this->getPrivateRate(),
        'DateFrom' => $this->getDateFrom(),
        'DateTo' => $this->getDateTo(),
        'PercentNetPriceRule' => $this->getPercentNetPriceRule(),
        'NoApplyRoomNetPriceRule' => $this->getNoApplyRoomNetPriceRule(),
        'NoApplyBoardNetPriceRule' => $this->getNoApplyBoardNetPriceRule(),
        'NoApplySuplNetPriceRule' => $this->getNoApplySuplNetPriceRule(),
        'NoApplyExtraNetPriceRule' => $this->getNoApplyExtraNetPriceRule(),
      );
    }

    /**
     * @return Action
     */
    public function getAction()
    {
      return $this->Action;
    }

    /**
     * @param Action $Action
     * @return \Dingus\SyncroService\HotelGroupsRec
     */
    public function setAction($Action)
    {
      $this->Action = $Action;
      return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
      return $this->Id;
    }

    /**
     * @param int $Id
     * @return \Dingus\SyncroService\HotelGroupsRec
     */
    public function setId($Id)
    {
      $this->Id = $Id;
      return $this;
    }

    /**
     * @return string
     */
    public function getHotelCode()
    {
      return $this->HotelCode;
    }

    /**
     * @param string $HotelCode
     * @return \Dingus\SyncroService\HotelGroupsRec
     */
    public function setHotelCode($HotelCode)
    {
      $this->HotelCode = $HotelCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getHotelName()
    {
      return $this->HotelName;
    }

    /**
     * @param string $HotelName
     * @return \Dingus\SyncroService\HotelGroupsRec
     */
    public function setHotelName($HotelName)
    {
      $this->HotelName = $HotelName;
      return $this;
    }

    /**
     * @return string
     */
    public function getCustomerCode()
    {
      return $this->CustomerCode;
    }

    /**
     * @param string $CustomerCode
     * @return \Dingus\SyncroService\HotelGroupsRec
     */
    public function setCustomerCode($CustomerCode)
    {
      $this->CustomerCode = $CustomerCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getCustomerName()
    {
      return $this->CustomerName;
    }

    /**
     * @param string $CustomerName
     * @return \Dingus\SyncroService\HotelGroupsRec
     */
    public function setCustomerName($CustomerName)
    {
      $this->CustomerName = $CustomerName;
      return $this;
    }

    /**
     * @return string
     */
    public function getFiscalCustomerCode()
    {
      return $this->FiscalCustomerCode;
    }

    /**
     * @param string $FiscalCustomerCode
     * @return \Dingus\SyncroService\HotelGroupsRec
     */
    public function setFiscalCustomerCode($FiscalCustomerCode)
    {
      $this->FiscalCustomerCode = $FiscalCustomerCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getFiscalCustomerName()
    {
      return $this->FiscalCustomerName;
    }

    /**
     * @param string $FiscalCustomerName
     * @return \Dingus\SyncroService\HotelGroupsRec
     */
    public function setFiscalCustomerName($FiscalCustomerName)
    {
      $this->FiscalCustomerName = $FiscalCustomerName;
      return $this;
    }

    /**
     * @return string
     */
    public function getRateGroupCode()
    {
      return $this->RateGroupCode;
    }

    /**
     * @param string $RateGroupCode
     * @return \Dingus\SyncroService\HotelGroupsRec
     */
    public function setRateGroupCode($RateGroupCode)
    {
      $this->RateGroupCode = $RateGroupCode;
      return $this;
    }

    /**
     * @return ContainedServiceRec
     */
    public function getContainedService()
    {
      return $this->ContainedService;
    }

    /**
     * @param ContainedServiceRec $ContainedService
     * @return \Dingus\SyncroService\HotelGroupsRec
     */
    public function setContainedService($ContainedService)
    {
      $this->ContainedService = $ContainedService;
      return $this;
    }

    /**
     * @return string
     */
    public function getAllotmentGroupCode()
    {
      return $this->AllotmentGroupCode;
    }

    /**
     * @param string $AllotmentGroupCode
     * @return \Dingus\SyncroService\HotelGroupsRec
     */
    public function setAllotmentGroupCode($AllotmentGroupCode)
    {
      $this->AllotmentGroupCode = $AllotmentGroupCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getAllotmentGroupExtraCode()
    {
      return $this->AllotmentGroupExtraCode;
    }

    /**
     * @param string $AllotmentGroupExtraCode
     * @return \Dingus\SyncroService\HotelGroupsRec
     */
    public function setAllotmentGroupExtraCode($AllotmentGroupExtraCode)
    {
      $this->AllotmentGroupExtraCode = $AllotmentGroupExtraCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getTypeRounding()
    {
      return $this->TypeRounding;
    }

    /**
     * @param string $TypeRounding
     * @return \Dingus\SyncroService\HotelGroupsRec
     */
    public function setTypeRounding($TypeRounding)
    {
      $this->TypeRounding = $TypeRounding;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getAcceptsRQ()
    {
      return $this->AcceptsRQ;
    }

    /**
     * @param boolean $AcceptsRQ
     * @return \Dingus\SyncroService\HotelGroupsRec
     */
    public function setAcceptsRQ($AcceptsRQ)
    {
      $this->AcceptsRQ = $AcceptsRQ;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getByDefault()
    {
      return $this->ByDefault;
    }

    /**
     * @param boolean $ByDefault
     * @return \Dingus\SyncroService\HotelGroupsRec
     */
    public function setByDefault($ByDefault)
    {
      $this->ByDefault = $ByDefault;
      return $this;
    }

    /**
     * @return TipoCalculoTarifaHotel
     */
    public function getRateType()
    {
      return $this->RateType;
    }

    /**
     * @param TipoCalculoTarifaHotel $RateType
     * @return \Dingus\SyncroService\HotelGroupsRec
     */
    public function setRateType($RateType)
    {
      $this->RateType = $RateType;
      return $this;
    }

    /**
     * @return float
     */
    public function getPercentRound()
    {
      return $this->PercentRound;
    }

    /**
     * @param float $PercentRound
     * @return \Dingus\SyncroService\HotelGroupsRec
     */
    public function setPercentRound($PercentRound)
    {
      $this->PercentRound = $PercentRound;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getDailyRate()
    {
      return $this->DailyRate;
    }

    /**
     * @param boolean $DailyRate
     * @return \Dingus\SyncroService\HotelGroupsRec
     */
    public function setDailyRate($DailyRate)
    {
      $this->DailyRate = $DailyRate;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getPrivateRate()
    {
      return $this->PrivateRate;
    }

    /**
     * @param boolean $PrivateRate
     * @return \Dingus\SyncroService\HotelGroupsRec
     */
    public function setPrivateRate($PrivateRate)
    {
      $this->PrivateRate = $PrivateRate;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateFrom()
    {
      if ($this->DateFrom == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->DateFrom);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $DateFrom
     * @return \Dingus\SyncroService\HotelGroupsRec
     */
    public function setDateFrom(\DateTime $DateFrom)
    {
      $this->DateFrom = $DateFrom->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateTo()
    {
      if ($this->DateTo == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->DateTo);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $DateTo
     * @return \Dingus\SyncroService\HotelGroupsRec
     */
    public function setDateTo(\DateTime $DateTo)
    {
      $this->DateTo = $DateTo->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return float
     */
    public function getPercentNetPriceRule()
    {
      return $this->PercentNetPriceRule;
    }

    /**
     * @param float $PercentNetPriceRule
     * @return \Dingus\SyncroService\HotelGroupsRec
     */
    public function setPercentNetPriceRule($PercentNetPriceRule)
    {
      $this->PercentNetPriceRule = $PercentNetPriceRule;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getNoApplyRoomNetPriceRule()
    {
      return $this->NoApplyRoomNetPriceRule;
    }

    /**
     * @param boolean $NoApplyRoomNetPriceRule
     * @return \Dingus\SyncroService\HotelGroupsRec
     */
    public function setNoApplyRoomNetPriceRule($NoApplyRoomNetPriceRule)
    {
      $this->NoApplyRoomNetPriceRule = $NoApplyRoomNetPriceRule;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getNoApplyBoardNetPriceRule()
    {
      return $this->NoApplyBoardNetPriceRule;
    }

    /**
     * @param boolean $NoApplyBoardNetPriceRule
     * @return \Dingus\SyncroService\HotelGroupsRec
     */
    public function setNoApplyBoardNetPriceRule($NoApplyBoardNetPriceRule)
    {
      $this->NoApplyBoardNetPriceRule = $NoApplyBoardNetPriceRule;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getNoApplySuplNetPriceRule()
    {
      return $this->NoApplySuplNetPriceRule;
    }

    /**
     * @param boolean $NoApplySuplNetPriceRule
     * @return \Dingus\SyncroService\HotelGroupsRec
     */
    public function setNoApplySuplNetPriceRule($NoApplySuplNetPriceRule)
    {
      $this->NoApplySuplNetPriceRule = $NoApplySuplNetPriceRule;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getNoApplyExtraNetPriceRule()
    {
      return $this->NoApplyExtraNetPriceRule;
    }

    /**
     * @param boolean $NoApplyExtraNetPriceRule
     * @return \Dingus\SyncroService\HotelGroupsRec
     */
    public function setNoApplyExtraNetPriceRule($NoApplyExtraNetPriceRule)
    {
      $this->NoApplyExtraNetPriceRule = $NoApplyExtraNetPriceRule;
      return $this;
    }

}
