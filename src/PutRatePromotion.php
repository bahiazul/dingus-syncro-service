<?php

namespace Dingus\SyncroService;

class PutRatePromotion implements \JsonSerializable
{

    /**
     * @var Credentials $Credentials
     */
    protected $Credentials = null;

    /**
     * @var PutPromotionRQ $PutPromotionRQ
     */
    protected $PutPromotionRQ = null;

    /**
     * @param Credentials $Credentials
     * @param PutPromotionRQ $PutPromotionRQ
     */
    public function __construct($Credentials, $PutPromotionRQ)
    {
      $this->Credentials = $Credentials;
      $this->PutPromotionRQ = $PutPromotionRQ;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'Credentials' => $this->getCredentials(),
        'PutPromotionRQ' => $this->getPutPromotionRQ(),
      );
    }

    /**
     * @return Credentials
     */
    public function getCredentials()
    {
      return $this->Credentials;
    }

    /**
     * @param Credentials $Credentials
     * @return \Dingus\SyncroService\PutRatePromotion
     */
    public function setCredentials($Credentials)
    {
      $this->Credentials = $Credentials;
      return $this;
    }

    /**
     * @return PutPromotionRQ
     */
    public function getPutPromotionRQ()
    {
      return $this->PutPromotionRQ;
    }

    /**
     * @param PutPromotionRQ $PutPromotionRQ
     * @return \Dingus\SyncroService\PutRatePromotion
     */
    public function setPutPromotionRQ($PutPromotionRQ)
    {
      $this->PutPromotionRQ = $PutPromotionRQ;
      return $this;
    }

}
