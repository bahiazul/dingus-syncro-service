<?php

namespace Dingus\SyncroService;

class DeleteCustomerRateLinePrice implements \JsonSerializable
{

    /**
     * @var Credentials $Credentials
     */
    protected $Credentials = null;

    /**
     * @var CustomerRateLinePriceRQ $RQ
     */
    protected $RQ = null;

    /**
     * @param Credentials $Credentials
     * @param CustomerRateLinePriceRQ $RQ
     */
    public function __construct($Credentials, $RQ)
    {
      $this->Credentials = $Credentials;
      $this->RQ = $RQ;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'Credentials' => $this->getCredentials(),
        'RQ' => $this->getRQ(),
      );
    }

    /**
     * @return Credentials
     */
    public function getCredentials()
    {
      return $this->Credentials;
    }

    /**
     * @param Credentials $Credentials
     * @return \Dingus\SyncroService\DeleteCustomerRateLinePrice
     */
    public function setCredentials($Credentials)
    {
      $this->Credentials = $Credentials;
      return $this;
    }

    /**
     * @return CustomerRateLinePriceRQ
     */
    public function getRQ()
    {
      return $this->RQ;
    }

    /**
     * @param CustomerRateLinePriceRQ $RQ
     * @return \Dingus\SyncroService\DeleteCustomerRateLinePrice
     */
    public function setRQ($RQ)
    {
      $this->RQ = $RQ;
      return $this;
    }

}
