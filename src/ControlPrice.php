<?php

namespace Dingus\SyncroService;

class ControlPrice implements \JsonSerializable
{

    /**
     * @var Action $Action
     */
    protected $Action = null;

    /**
     * @var int $Id
     */
    protected $Id = null;

    /**
     * @var string $RoomCode
     */
    protected $RoomCode = null;

    /**
     * @var string $BoardCode
     */
    protected $BoardCode = null;

    /**
     * @var float $MaxPrice
     */
    protected $MaxPrice = null;

    /**
     * @var float $MinPrice
     */
    protected $MinPrice = null;

    /**
     * @param Action $Action
     * @param int $Id
     * @param float $MaxPrice
     * @param float $MinPrice
     */
    public function __construct($Action, $Id, $MaxPrice, $MinPrice)
    {
      $this->Action = $Action;
      $this->Id = $Id;
      $this->MaxPrice = $MaxPrice;
      $this->MinPrice = $MinPrice;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'Action' => $this->getAction(),
        'Id' => $this->getId(),
        'RoomCode' => $this->getRoomCode(),
        'BoardCode' => $this->getBoardCode(),
        'MaxPrice' => $this->getMaxPrice(),
        'MinPrice' => $this->getMinPrice(),
      );
    }

    /**
     * @return Action
     */
    public function getAction()
    {
      return $this->Action;
    }

    /**
     * @param Action $Action
     * @return \Dingus\SyncroService\ControlPrice
     */
    public function setAction($Action)
    {
      $this->Action = $Action;
      return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
      return $this->Id;
    }

    /**
     * @param int $Id
     * @return \Dingus\SyncroService\ControlPrice
     */
    public function setId($Id)
    {
      $this->Id = $Id;
      return $this;
    }

    /**
     * @return string
     */
    public function getRoomCode()
    {
      return $this->RoomCode;
    }

    /**
     * @param string $RoomCode
     * @return \Dingus\SyncroService\ControlPrice
     */
    public function setRoomCode($RoomCode)
    {
      $this->RoomCode = $RoomCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getBoardCode()
    {
      return $this->BoardCode;
    }

    /**
     * @param string $BoardCode
     * @return \Dingus\SyncroService\ControlPrice
     */
    public function setBoardCode($BoardCode)
    {
      $this->BoardCode = $BoardCode;
      return $this;
    }

    /**
     * @return float
     */
    public function getMaxPrice()
    {
      return $this->MaxPrice;
    }

    /**
     * @param float $MaxPrice
     * @return \Dingus\SyncroService\ControlPrice
     */
    public function setMaxPrice($MaxPrice)
    {
      $this->MaxPrice = $MaxPrice;
      return $this;
    }

    /**
     * @return float
     */
    public function getMinPrice()
    {
      return $this->MinPrice;
    }

    /**
     * @param float $MinPrice
     * @return \Dingus\SyncroService\ControlPrice
     */
    public function setMinPrice($MinPrice)
    {
      $this->MinPrice = $MinPrice;
      return $this;
    }

}
