<?php

namespace Dingus\SyncroService;

class CustomersTask implements \JsonSerializable
{

    /**
     * @var Action $Action
     */
    protected $Action = null;

    /**
     * @var int $CustomerTaskID
     */
    protected $CustomerTaskID = null;

    /**
     * @var string $CustomerCode
     */
    protected $CustomerCode = null;

    /**
     * @var string $CustomerName
     */
    protected $CustomerName = null;

    /**
     * @var string $CustomerConverted
     */
    protected $CustomerConverted = null;

    /**
     * @param Action $Action
     * @param int $CustomerTaskID
     */
    public function __construct($Action, $CustomerTaskID)
    {
      $this->Action = $Action;
      $this->CustomerTaskID = $CustomerTaskID;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'Action' => $this->getAction(),
        'CustomerTaskID' => $this->getCustomerTaskID(),
        'CustomerCode' => $this->getCustomerCode(),
        'CustomerName' => $this->getCustomerName(),
        'CustomerConverted' => $this->getCustomerConverted(),
      );
    }

    /**
     * @return Action
     */
    public function getAction()
    {
      return $this->Action;
    }

    /**
     * @param Action $Action
     * @return \Dingus\SyncroService\CustomersTask
     */
    public function setAction($Action)
    {
      $this->Action = $Action;
      return $this;
    }

    /**
     * @return int
     */
    public function getCustomerTaskID()
    {
      return $this->CustomerTaskID;
    }

    /**
     * @param int $CustomerTaskID
     * @return \Dingus\SyncroService\CustomersTask
     */
    public function setCustomerTaskID($CustomerTaskID)
    {
      $this->CustomerTaskID = $CustomerTaskID;
      return $this;
    }

    /**
     * @return string
     */
    public function getCustomerCode()
    {
      return $this->CustomerCode;
    }

    /**
     * @param string $CustomerCode
     * @return \Dingus\SyncroService\CustomersTask
     */
    public function setCustomerCode($CustomerCode)
    {
      $this->CustomerCode = $CustomerCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getCustomerName()
    {
      return $this->CustomerName;
    }

    /**
     * @param string $CustomerName
     * @return \Dingus\SyncroService\CustomersTask
     */
    public function setCustomerName($CustomerName)
    {
      $this->CustomerName = $CustomerName;
      return $this;
    }

    /**
     * @return string
     */
    public function getCustomerConverted()
    {
      return $this->CustomerConverted;
    }

    /**
     * @param string $CustomerConverted
     * @return \Dingus\SyncroService\CustomersTask
     */
    public function setCustomerConverted($CustomerConverted)
    {
      $this->CustomerConverted = $CustomerConverted;
      return $this;
    }

}
