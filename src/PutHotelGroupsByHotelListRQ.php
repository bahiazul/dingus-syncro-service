<?php

namespace Dingus\SyncroService;

class PutHotelGroupsByHotelListRQ implements \JsonSerializable
{

    /**
     * @var ArrayOfString $HotelCodeList
     */
    protected $HotelCodeList = null;

    /**
     * @var ArrayOfHotelGroupsRec $Recs
     */
    protected $Recs = null;

    
    public function __construct()
    {
    
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'HotelCodeList' => $this->getHotelCodeList(),
        'Recs' => $this->getRecs(),
      );
    }

    /**
     * @return ArrayOfString
     */
    public function getHotelCodeList()
    {
      return $this->HotelCodeList;
    }

    /**
     * @param ArrayOfString $HotelCodeList
     * @return \Dingus\SyncroService\PutHotelGroupsByHotelListRQ
     */
    public function setHotelCodeList($HotelCodeList)
    {
      $this->HotelCodeList = $HotelCodeList;
      return $this;
    }

    /**
     * @return ArrayOfHotelGroupsRec
     */
    public function getRecs()
    {
      return $this->Recs;
    }

    /**
     * @param ArrayOfHotelGroupsRec $Recs
     * @return \Dingus\SyncroService\PutHotelGroupsByHotelListRQ
     */
    public function setRecs($Recs)
    {
      $this->Recs = $Recs;
      return $this;
    }

}
