<?php

namespace Dingus\SyncroService;

class PutCustomerRatesMultiGroupRateResponse implements \JsonSerializable
{

    /**
     * @var SyncroRS $PutCustomerRatesMultiGroupRateResult
     */
    protected $PutCustomerRatesMultiGroupRateResult = null;

    /**
     * @param SyncroRS $PutCustomerRatesMultiGroupRateResult
     */
    public function __construct($PutCustomerRatesMultiGroupRateResult)
    {
      $this->PutCustomerRatesMultiGroupRateResult = $PutCustomerRatesMultiGroupRateResult;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'PutCustomerRatesMultiGroupRateResult' => $this->getPutCustomerRatesMultiGroupRateResult(),
      );
    }

    /**
     * @return SyncroRS
     */
    public function getPutCustomerRatesMultiGroupRateResult()
    {
      return $this->PutCustomerRatesMultiGroupRateResult;
    }

    /**
     * @param SyncroRS $PutCustomerRatesMultiGroupRateResult
     * @return \Dingus\SyncroService\PutCustomerRatesMultiGroupRateResponse
     */
    public function setPutCustomerRatesMultiGroupRateResult($PutCustomerRatesMultiGroupRateResult)
    {
      $this->PutCustomerRatesMultiGroupRateResult = $PutCustomerRatesMultiGroupRateResult;
      return $this;
    }

}
