<?php

namespace Dingus\SyncroService;

class ArrayOfCriterionPriceRules implements \JsonSerializable, \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var CriterionPriceRules[] $CriterionPriceRules
     */
    protected $CriterionPriceRules = null;

    
    public function __construct()
    {
    
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'CriterionPriceRules' => $this->getCriterionPriceRules(),
      );
    }

    /**
     * @return CriterionPriceRules[]
     */
    public function getCriterionPriceRules()
    {
      return $this->CriterionPriceRules;
    }

    /**
     * @param CriterionPriceRules[] $CriterionPriceRules
     * @return \Dingus\SyncroService\ArrayOfCriterionPriceRules
     */
    public function setCriterionPriceRules(array $CriterionPriceRules = null)
    {
      $this->CriterionPriceRules = $CriterionPriceRules;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->CriterionPriceRules[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return CriterionPriceRules
     */
    public function offsetGet($offset)
    {
      return $this->CriterionPriceRules[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param CriterionPriceRules $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      $this->CriterionPriceRules[$offset] = $value;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->CriterionPriceRules[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return CriterionPriceRules Return the current element
     */
    public function current()
    {
      return current($this->CriterionPriceRules);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->CriterionPriceRules);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->CriterionPriceRules);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->CriterionPriceRules);
    }

    /**
     * Countable implementation
     *
     * @return CriterionPriceRules Return count of elements
     */
    public function count()
    {
      return count($this->CriterionPriceRules);
    }

}
