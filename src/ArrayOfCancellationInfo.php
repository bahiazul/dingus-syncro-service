<?php

namespace Dingus\SyncroService;

class ArrayOfCancellationInfo implements \JsonSerializable, \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var CancellationInfo[] $CancellationInfo
     */
    protected $CancellationInfo = null;

    
    public function __construct()
    {
    
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'CancellationInfo' => $this->getCancellationInfo(),
      );
    }

    /**
     * @return CancellationInfo[]
     */
    public function getCancellationInfo()
    {
      return $this->CancellationInfo;
    }

    /**
     * @param CancellationInfo[] $CancellationInfo
     * @return \Dingus\SyncroService\ArrayOfCancellationInfo
     */
    public function setCancellationInfo(array $CancellationInfo = null)
    {
      $this->CancellationInfo = $CancellationInfo;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->CancellationInfo[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return CancellationInfo
     */
    public function offsetGet($offset)
    {
      return $this->CancellationInfo[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param CancellationInfo $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      $this->CancellationInfo[$offset] = $value;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->CancellationInfo[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return CancellationInfo Return the current element
     */
    public function current()
    {
      return current($this->CancellationInfo);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->CancellationInfo);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->CancellationInfo);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->CancellationInfo);
    }

    /**
     * Countable implementation
     *
     * @return CancellationInfo Return count of elements
     */
    public function count()
    {
      return count($this->CancellationInfo);
    }

}
