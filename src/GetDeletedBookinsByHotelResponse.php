<?php

namespace Dingus\SyncroService;

class GetDeletedBookinsByHotelResponse implements \JsonSerializable
{

    /**
     * @var ArrayOfGetDeletedBookingsResult $GetDeletedBookinsByHotelResult
     */
    protected $GetDeletedBookinsByHotelResult = null;

    /**
     * @param ArrayOfGetDeletedBookingsResult $GetDeletedBookinsByHotelResult
     */
    public function __construct($GetDeletedBookinsByHotelResult)
    {
      $this->GetDeletedBookinsByHotelResult = $GetDeletedBookinsByHotelResult;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'GetDeletedBookinsByHotelResult' => $this->getGetDeletedBookinsByHotelResult(),
      );
    }

    /**
     * @return ArrayOfGetDeletedBookingsResult
     */
    public function getGetDeletedBookinsByHotelResult()
    {
      return $this->GetDeletedBookinsByHotelResult;
    }

    /**
     * @param ArrayOfGetDeletedBookingsResult $GetDeletedBookinsByHotelResult
     * @return \Dingus\SyncroService\GetDeletedBookinsByHotelResponse
     */
    public function setGetDeletedBookinsByHotelResult($GetDeletedBookinsByHotelResult)
    {
      $this->GetDeletedBookinsByHotelResult = $GetDeletedBookinsByHotelResult;
      return $this;
    }

}
