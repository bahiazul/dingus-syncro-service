<?php

namespace Dingus\SyncroService;

class CancellationFeeRec implements \JsonSerializable
{

    /**
     * @var ArrayOfPaymentFormCancellationFeeRS $PaymentFormCancellationFeeList
     */
    protected $PaymentFormCancellationFeeList = null;

    /**
     * @var ArrayOfLineCancellationFeeRS $LineCancellationFeeList
     */
    protected $LineCancellationFeeList = null;

    /**
     * @var Action $Action
     */
    protected $Action = null;

    /**
     * @var string $CancellationFeeCode
     */
    protected $CancellationFeeCode = null;

    /**
     * @var string $CancellationFeeDescription
     */
    protected $CancellationFeeDescription = null;

    /**
     * @var string $CustomerRateGroupCode
     */
    protected $CustomerRateGroupCode = null;

    /**
     * @var string $CustomerRateGroupDescription
     */
    protected $CustomerRateGroupDescription = null;

    /**
     * @var string $PromotionCode
     */
    protected $PromotionCode = null;

    /**
     * @var string $CustomerCode
     */
    protected $CustomerCode = null;

    /**
     * @var string $HotelCode
     */
    protected $HotelCode = null;

    /**
     * @var string $HotelName
     */
    protected $HotelName = null;

    /**
     * @param Action $Action
     */
    public function __construct($Action)
    {
      $this->Action = $Action;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'PaymentFormCancellationFeeList' => $this->getPaymentFormCancellationFeeList(),
        'LineCancellationFeeList' => $this->getLineCancellationFeeList(),
        'Action' => $this->getAction(),
        'CancellationFeeCode' => $this->getCancellationFeeCode(),
        'CancellationFeeDescription' => $this->getCancellationFeeDescription(),
        'CustomerRateGroupCode' => $this->getCustomerRateGroupCode(),
        'CustomerRateGroupDescription' => $this->getCustomerRateGroupDescription(),
        'PromotionCode' => $this->getPromotionCode(),
        'CustomerCode' => $this->getCustomerCode(),
        'HotelCode' => $this->getHotelCode(),
        'HotelName' => $this->getHotelName(),
      );
    }

    /**
     * @return ArrayOfPaymentFormCancellationFeeRS
     */
    public function getPaymentFormCancellationFeeList()
    {
      return $this->PaymentFormCancellationFeeList;
    }

    /**
     * @param ArrayOfPaymentFormCancellationFeeRS $PaymentFormCancellationFeeList
     * @return \Dingus\SyncroService\CancellationFeeRec
     */
    public function setPaymentFormCancellationFeeList($PaymentFormCancellationFeeList)
    {
      $this->PaymentFormCancellationFeeList = $PaymentFormCancellationFeeList;
      return $this;
    }

    /**
     * @return ArrayOfLineCancellationFeeRS
     */
    public function getLineCancellationFeeList()
    {
      return $this->LineCancellationFeeList;
    }

    /**
     * @param ArrayOfLineCancellationFeeRS $LineCancellationFeeList
     * @return \Dingus\SyncroService\CancellationFeeRec
     */
    public function setLineCancellationFeeList($LineCancellationFeeList)
    {
      $this->LineCancellationFeeList = $LineCancellationFeeList;
      return $this;
    }

    /**
     * @return Action
     */
    public function getAction()
    {
      return $this->Action;
    }

    /**
     * @param Action $Action
     * @return \Dingus\SyncroService\CancellationFeeRec
     */
    public function setAction($Action)
    {
      $this->Action = $Action;
      return $this;
    }

    /**
     * @return string
     */
    public function getCancellationFeeCode()
    {
      return $this->CancellationFeeCode;
    }

    /**
     * @param string $CancellationFeeCode
     * @return \Dingus\SyncroService\CancellationFeeRec
     */
    public function setCancellationFeeCode($CancellationFeeCode)
    {
      $this->CancellationFeeCode = $CancellationFeeCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getCancellationFeeDescription()
    {
      return $this->CancellationFeeDescription;
    }

    /**
     * @param string $CancellationFeeDescription
     * @return \Dingus\SyncroService\CancellationFeeRec
     */
    public function setCancellationFeeDescription($CancellationFeeDescription)
    {
      $this->CancellationFeeDescription = $CancellationFeeDescription;
      return $this;
    }

    /**
     * @return string
     */
    public function getCustomerRateGroupCode()
    {
      return $this->CustomerRateGroupCode;
    }

    /**
     * @param string $CustomerRateGroupCode
     * @return \Dingus\SyncroService\CancellationFeeRec
     */
    public function setCustomerRateGroupCode($CustomerRateGroupCode)
    {
      $this->CustomerRateGroupCode = $CustomerRateGroupCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getCustomerRateGroupDescription()
    {
      return $this->CustomerRateGroupDescription;
    }

    /**
     * @param string $CustomerRateGroupDescription
     * @return \Dingus\SyncroService\CancellationFeeRec
     */
    public function setCustomerRateGroupDescription($CustomerRateGroupDescription)
    {
      $this->CustomerRateGroupDescription = $CustomerRateGroupDescription;
      return $this;
    }

    /**
     * @return string
     */
    public function getPromotionCode()
    {
      return $this->PromotionCode;
    }

    /**
     * @param string $PromotionCode
     * @return \Dingus\SyncroService\CancellationFeeRec
     */
    public function setPromotionCode($PromotionCode)
    {
      $this->PromotionCode = $PromotionCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getCustomerCode()
    {
      return $this->CustomerCode;
    }

    /**
     * @param string $CustomerCode
     * @return \Dingus\SyncroService\CancellationFeeRec
     */
    public function setCustomerCode($CustomerCode)
    {
      $this->CustomerCode = $CustomerCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getHotelCode()
    {
      return $this->HotelCode;
    }

    /**
     * @param string $HotelCode
     * @return \Dingus\SyncroService\CancellationFeeRec
     */
    public function setHotelCode($HotelCode)
    {
      $this->HotelCode = $HotelCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getHotelName()
    {
      return $this->HotelName;
    }

    /**
     * @param string $HotelName
     * @return \Dingus\SyncroService\CancellationFeeRec
     */
    public function setHotelName($HotelName)
    {
      $this->HotelName = $HotelName;
      return $this;
    }

}
