<?php

namespace Dingus\SyncroService;

class PutCustomerRateSetUp implements \JsonSerializable
{

    /**
     * @var Credentials $Credentials
     */
    protected $Credentials = null;

    /**
     * @var CustomerRateSetUpRS $customerRateSetUpRS
     */
    protected $customerRateSetUpRS = null;

    /**
     * @param Credentials $Credentials
     * @param CustomerRateSetUpRS $customerRateSetUpRS
     */
    public function __construct($Credentials, $customerRateSetUpRS)
    {
      $this->Credentials = $Credentials;
      $this->customerRateSetUpRS = $customerRateSetUpRS;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'Credentials' => $this->getCredentials(),
        'customerRateSetUpRS' => $this->getCustomerRateSetUpRS(),
      );
    }

    /**
     * @return Credentials
     */
    public function getCredentials()
    {
      return $this->Credentials;
    }

    /**
     * @param Credentials $Credentials
     * @return \Dingus\SyncroService\PutCustomerRateSetUp
     */
    public function setCredentials($Credentials)
    {
      $this->Credentials = $Credentials;
      return $this;
    }

    /**
     * @return CustomerRateSetUpRS
     */
    public function getCustomerRateSetUpRS()
    {
      return $this->customerRateSetUpRS;
    }

    /**
     * @param CustomerRateSetUpRS $customerRateSetUpRS
     * @return \Dingus\SyncroService\PutCustomerRateSetUp
     */
    public function setCustomerRateSetUpRS($customerRateSetUpRS)
    {
      $this->customerRateSetUpRS = $customerRateSetUpRS;
      return $this;
    }

}
