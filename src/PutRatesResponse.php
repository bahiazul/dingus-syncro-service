<?php

namespace Dingus\SyncroService;

class PutRatesResponse implements \JsonSerializable
{

    /**
     * @var SyncroRS $PutRatesResult
     */
    protected $PutRatesResult = null;

    /**
     * @param SyncroRS $PutRatesResult
     */
    public function __construct($PutRatesResult)
    {
      $this->PutRatesResult = $PutRatesResult;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'PutRatesResult' => $this->getPutRatesResult(),
      );
    }

    /**
     * @return SyncroRS
     */
    public function getPutRatesResult()
    {
      return $this->PutRatesResult;
    }

    /**
     * @param SyncroRS $PutRatesResult
     * @return \Dingus\SyncroService\PutRatesResponse
     */
    public function setPutRatesResult($PutRatesResult)
    {
      $this->PutRatesResult = $PutRatesResult;
      return $this;
    }

}
