<?php

namespace Dingus\SyncroService;

class StopSalesRec implements \JsonSerializable
{

    /**
     * @var Action $Action
     */
    protected $Action = null;

    /**
     * @var string $AllotmentGroupCode
     */
    protected $AllotmentGroupCode = null;

    /**
     * @var string $RateGroupCode
     */
    protected $RateGroupCode = null;

    /**
     * @var \DateTime $DateFrom
     */
    protected $DateFrom = null;

    /**
     * @var \DateTime $DateTo
     */
    protected $DateTo = null;

    /**
     * @var \DateTime $CreationDate
     */
    protected $CreationDate = null;

    /**
     * @var string $HotelCode
     */
    protected $HotelCode = null;

    /**
     * @var string $BoardCode
     */
    protected $BoardCode = null;

    /**
     * @var string $PMSCode
     */
    protected $PMSCode = null;

    /**
     * @var string $RoomCode
     */
    protected $RoomCode = null;

    /**
     * @var int $MinStay
     */
    protected $MinStay = null;

    /**
     * @var int $MaxStay
     */
    protected $MaxStay = null;

    /**
     * @var boolean $Monday
     */
    protected $Monday = null;

    /**
     * @var boolean $Tuesday
     */
    protected $Tuesday = null;

    /**
     * @var boolean $Wednesday
     */
    protected $Wednesday = null;

    /**
     * @var boolean $Thursday
     */
    protected $Thursday = null;

    /**
     * @var boolean $Friday
     */
    protected $Friday = null;

    /**
     * @var boolean $Saturday
     */
    protected $Saturday = null;

    /**
     * @var boolean $Sunday
     */
    protected $Sunday = null;

    /**
     * @var int $Adults
     */
    protected $Adults = null;

    /**
     * @var int $Children
     */
    protected $Children = null;

    /**
     * @var ArrayOfCustomer $CustomerList
     */
    protected $CustomerList = null;

    /**
     * @param Action $Action
     * @param \DateTime $DateFrom
     * @param \DateTime $DateTo
     * @param \DateTime $CreationDate
     * @param int $MinStay
     * @param int $MaxStay
     * @param boolean $Monday
     * @param boolean $Tuesday
     * @param boolean $Wednesday
     * @param boolean $Thursday
     * @param boolean $Friday
     * @param boolean $Saturday
     * @param boolean $Sunday
     * @param int $Adults
     * @param int $Children
     */
    public function __construct($Action, \DateTime $DateFrom, \DateTime $DateTo, \DateTime $CreationDate, $MinStay, $MaxStay, $Monday, $Tuesday, $Wednesday, $Thursday, $Friday, $Saturday, $Sunday, $Adults, $Children)
    {
      $this->Action = $Action;
      $this->DateFrom = $DateFrom->format(\DateTime::ATOM);
      $this->DateTo = $DateTo->format(\DateTime::ATOM);
      $this->CreationDate = $CreationDate->format(\DateTime::ATOM);
      $this->MinStay = $MinStay;
      $this->MaxStay = $MaxStay;
      $this->Monday = $Monday;
      $this->Tuesday = $Tuesday;
      $this->Wednesday = $Wednesday;
      $this->Thursday = $Thursday;
      $this->Friday = $Friday;
      $this->Saturday = $Saturday;
      $this->Sunday = $Sunday;
      $this->Adults = $Adults;
      $this->Children = $Children;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'Action' => $this->getAction(),
        'AllotmentGroupCode' => $this->getAllotmentGroupCode(),
        'RateGroupCode' => $this->getRateGroupCode(),
        'DateFrom' => $this->getDateFrom(),
        'DateTo' => $this->getDateTo(),
        'CreationDate' => $this->getCreationDate(),
        'HotelCode' => $this->getHotelCode(),
        'BoardCode' => $this->getBoardCode(),
        'PMSCode' => $this->getPMSCode(),
        'RoomCode' => $this->getRoomCode(),
        'MinStay' => $this->getMinStay(),
        'MaxStay' => $this->getMaxStay(),
        'Monday' => $this->getMonday(),
        'Tuesday' => $this->getTuesday(),
        'Wednesday' => $this->getWednesday(),
        'Thursday' => $this->getThursday(),
        'Friday' => $this->getFriday(),
        'Saturday' => $this->getSaturday(),
        'Sunday' => $this->getSunday(),
        'Adults' => $this->getAdults(),
        'Children' => $this->getChildren(),
        'CustomerList' => $this->getCustomerList(),
      );
    }

    /**
     * @return Action
     */
    public function getAction()
    {
      return $this->Action;
    }

    /**
     * @param Action $Action
     * @return \Dingus\SyncroService\StopSalesRec
     */
    public function setAction($Action)
    {
      $this->Action = $Action;
      return $this;
    }

    /**
     * @return string
     */
    public function getAllotmentGroupCode()
    {
      return $this->AllotmentGroupCode;
    }

    /**
     * @param string $AllotmentGroupCode
     * @return \Dingus\SyncroService\StopSalesRec
     */
    public function setAllotmentGroupCode($AllotmentGroupCode)
    {
      $this->AllotmentGroupCode = $AllotmentGroupCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getRateGroupCode()
    {
      return $this->RateGroupCode;
    }

    /**
     * @param string $RateGroupCode
     * @return \Dingus\SyncroService\StopSalesRec
     */
    public function setRateGroupCode($RateGroupCode)
    {
      $this->RateGroupCode = $RateGroupCode;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateFrom()
    {
      if ($this->DateFrom == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->DateFrom);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $DateFrom
     * @return \Dingus\SyncroService\StopSalesRec
     */
    public function setDateFrom(\DateTime $DateFrom)
    {
      $this->DateFrom = $DateFrom->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateTo()
    {
      if ($this->DateTo == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->DateTo);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $DateTo
     * @return \Dingus\SyncroService\StopSalesRec
     */
    public function setDateTo(\DateTime $DateTo)
    {
      $this->DateTo = $DateTo->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreationDate()
    {
      if ($this->CreationDate == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->CreationDate);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $CreationDate
     * @return \Dingus\SyncroService\StopSalesRec
     */
    public function setCreationDate(\DateTime $CreationDate)
    {
      $this->CreationDate = $CreationDate->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return string
     */
    public function getHotelCode()
    {
      return $this->HotelCode;
    }

    /**
     * @param string $HotelCode
     * @return \Dingus\SyncroService\StopSalesRec
     */
    public function setHotelCode($HotelCode)
    {
      $this->HotelCode = $HotelCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getBoardCode()
    {
      return $this->BoardCode;
    }

    /**
     * @param string $BoardCode
     * @return \Dingus\SyncroService\StopSalesRec
     */
    public function setBoardCode($BoardCode)
    {
      $this->BoardCode = $BoardCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getPMSCode()
    {
      return $this->PMSCode;
    }

    /**
     * @param string $PMSCode
     * @return \Dingus\SyncroService\StopSalesRec
     */
    public function setPMSCode($PMSCode)
    {
      $this->PMSCode = $PMSCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getRoomCode()
    {
      return $this->RoomCode;
    }

    /**
     * @param string $RoomCode
     * @return \Dingus\SyncroService\StopSalesRec
     */
    public function setRoomCode($RoomCode)
    {
      $this->RoomCode = $RoomCode;
      return $this;
    }

    /**
     * @return int
     */
    public function getMinStay()
    {
      return $this->MinStay;
    }

    /**
     * @param int $MinStay
     * @return \Dingus\SyncroService\StopSalesRec
     */
    public function setMinStay($MinStay)
    {
      $this->MinStay = $MinStay;
      return $this;
    }

    /**
     * @return int
     */
    public function getMaxStay()
    {
      return $this->MaxStay;
    }

    /**
     * @param int $MaxStay
     * @return \Dingus\SyncroService\StopSalesRec
     */
    public function setMaxStay($MaxStay)
    {
      $this->MaxStay = $MaxStay;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getMonday()
    {
      return $this->Monday;
    }

    /**
     * @param boolean $Monday
     * @return \Dingus\SyncroService\StopSalesRec
     */
    public function setMonday($Monday)
    {
      $this->Monday = $Monday;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getTuesday()
    {
      return $this->Tuesday;
    }

    /**
     * @param boolean $Tuesday
     * @return \Dingus\SyncroService\StopSalesRec
     */
    public function setTuesday($Tuesday)
    {
      $this->Tuesday = $Tuesday;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getWednesday()
    {
      return $this->Wednesday;
    }

    /**
     * @param boolean $Wednesday
     * @return \Dingus\SyncroService\StopSalesRec
     */
    public function setWednesday($Wednesday)
    {
      $this->Wednesday = $Wednesday;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getThursday()
    {
      return $this->Thursday;
    }

    /**
     * @param boolean $Thursday
     * @return \Dingus\SyncroService\StopSalesRec
     */
    public function setThursday($Thursday)
    {
      $this->Thursday = $Thursday;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getFriday()
    {
      return $this->Friday;
    }

    /**
     * @param boolean $Friday
     * @return \Dingus\SyncroService\StopSalesRec
     */
    public function setFriday($Friday)
    {
      $this->Friday = $Friday;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getSaturday()
    {
      return $this->Saturday;
    }

    /**
     * @param boolean $Saturday
     * @return \Dingus\SyncroService\StopSalesRec
     */
    public function setSaturday($Saturday)
    {
      $this->Saturday = $Saturday;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getSunday()
    {
      return $this->Sunday;
    }

    /**
     * @param boolean $Sunday
     * @return \Dingus\SyncroService\StopSalesRec
     */
    public function setSunday($Sunday)
    {
      $this->Sunday = $Sunday;
      return $this;
    }

    /**
     * @return int
     */
    public function getAdults()
    {
      return $this->Adults;
    }

    /**
     * @param int $Adults
     * @return \Dingus\SyncroService\StopSalesRec
     */
    public function setAdults($Adults)
    {
      $this->Adults = $Adults;
      return $this;
    }

    /**
     * @return int
     */
    public function getChildren()
    {
      return $this->Children;
    }

    /**
     * @param int $Children
     * @return \Dingus\SyncroService\StopSalesRec
     */
    public function setChildren($Children)
    {
      $this->Children = $Children;
      return $this;
    }

    /**
     * @return ArrayOfCustomer
     */
    public function getCustomerList()
    {
      return $this->CustomerList;
    }

    /**
     * @param ArrayOfCustomer $CustomerList
     * @return \Dingus\SyncroService\StopSalesRec
     */
    public function setCustomerList($CustomerList)
    {
      $this->CustomerList = $CustomerList;
      return $this;
    }

}
