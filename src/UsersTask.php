<?php

namespace Dingus\SyncroService;

class UsersTask implements \JsonSerializable
{

    /**
     * @var Action $Action
     */
    protected $Action = null;

    /**
     * @var int $UserID
     */
    protected $UserID = null;

    /**
     * @var string $UserName
     */
    protected $UserName = null;

    /**
     * @param Action $Action
     * @param int $UserID
     */
    public function __construct($Action, $UserID)
    {
      $this->Action = $Action;
      $this->UserID = $UserID;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'Action' => $this->getAction(),
        'UserID' => $this->getUserID(),
        'UserName' => $this->getUserName(),
      );
    }

    /**
     * @return Action
     */
    public function getAction()
    {
      return $this->Action;
    }

    /**
     * @param Action $Action
     * @return \Dingus\SyncroService\UsersTask
     */
    public function setAction($Action)
    {
      $this->Action = $Action;
      return $this;
    }

    /**
     * @return int
     */
    public function getUserID()
    {
      return $this->UserID;
    }

    /**
     * @param int $UserID
     * @return \Dingus\SyncroService\UsersTask
     */
    public function setUserID($UserID)
    {
      $this->UserID = $UserID;
      return $this;
    }

    /**
     * @return string
     */
    public function getUserName()
    {
      return $this->UserName;
    }

    /**
     * @param string $UserName
     * @return \Dingus\SyncroService\UsersTask
     */
    public function setUserName($UserName)
    {
      $this->UserName = $UserName;
      return $this;
    }

}
