<?php

namespace Dingus\SyncroService;

class ComissionRS implements \JsonSerializable
{

    /**
     * @var RateType $ComissionType
     */
    protected $ComissionType = null;

    /**
     * @var float $Percent
     */
    protected $Percent = null;

    /**
     * @var float $ComissionAmount
     */
    protected $ComissionAmount = null;

    /**
     * @var float $NetPrice
     */
    protected $NetPrice = null;

    /**
     * @var float $MiniumPrice
     */
    protected $MiniumPrice = null;

    /**
     * @param RateType $ComissionType
     * @param float $Percent
     * @param float $ComissionAmount
     * @param float $NetPrice
     * @param float $MiniumPrice
     */
    public function __construct($ComissionType, $Percent, $ComissionAmount, $NetPrice, $MiniumPrice)
    {
      $this->ComissionType = $ComissionType;
      $this->Percent = $Percent;
      $this->ComissionAmount = $ComissionAmount;
      $this->NetPrice = $NetPrice;
      $this->MiniumPrice = $MiniumPrice;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'ComissionType' => $this->getComissionType(),
        'Percent' => $this->getPercent(),
        'ComissionAmount' => $this->getComissionAmount(),
        'NetPrice' => $this->getNetPrice(),
        'MiniumPrice' => $this->getMiniumPrice(),
      );
    }

    /**
     * @return RateType
     */
    public function getComissionType()
    {
      return $this->ComissionType;
    }

    /**
     * @param RateType $ComissionType
     * @return \Dingus\SyncroService\ComissionRS
     */
    public function setComissionType($ComissionType)
    {
      $this->ComissionType = $ComissionType;
      return $this;
    }

    /**
     * @return float
     */
    public function getPercent()
    {
      return $this->Percent;
    }

    /**
     * @param float $Percent
     * @return \Dingus\SyncroService\ComissionRS
     */
    public function setPercent($Percent)
    {
      $this->Percent = $Percent;
      return $this;
    }

    /**
     * @return float
     */
    public function getComissionAmount()
    {
      return $this->ComissionAmount;
    }

    /**
     * @param float $ComissionAmount
     * @return \Dingus\SyncroService\ComissionRS
     */
    public function setComissionAmount($ComissionAmount)
    {
      $this->ComissionAmount = $ComissionAmount;
      return $this;
    }

    /**
     * @return float
     */
    public function getNetPrice()
    {
      return $this->NetPrice;
    }

    /**
     * @param float $NetPrice
     * @return \Dingus\SyncroService\ComissionRS
     */
    public function setNetPrice($NetPrice)
    {
      $this->NetPrice = $NetPrice;
      return $this;
    }

    /**
     * @return float
     */
    public function getMiniumPrice()
    {
      return $this->MiniumPrice;
    }

    /**
     * @param float $MiniumPrice
     * @return \Dingus\SyncroService\ComissionRS
     */
    public function setMiniumPrice($MiniumPrice)
    {
      $this->MiniumPrice = $MiniumPrice;
      return $this;
    }

}
