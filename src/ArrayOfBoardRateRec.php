<?php

namespace Dingus\SyncroService;

class ArrayOfBoardRateRec implements \JsonSerializable, \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var BoardRateRec[] $BoardRateRec
     */
    protected $BoardRateRec = null;

    
    public function __construct()
    {
    
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'BoardRateRec' => $this->getBoardRateRec(),
      );
    }

    /**
     * @return BoardRateRec[]
     */
    public function getBoardRateRec()
    {
      return $this->BoardRateRec;
    }

    /**
     * @param BoardRateRec[] $BoardRateRec
     * @return \Dingus\SyncroService\ArrayOfBoardRateRec
     */
    public function setBoardRateRec(array $BoardRateRec = null)
    {
      $this->BoardRateRec = $BoardRateRec;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->BoardRateRec[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return BoardRateRec
     */
    public function offsetGet($offset)
    {
      return $this->BoardRateRec[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param BoardRateRec $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      $this->BoardRateRec[$offset] = $value;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->BoardRateRec[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return BoardRateRec Return the current element
     */
    public function current()
    {
      return current($this->BoardRateRec);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->BoardRateRec);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->BoardRateRec);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->BoardRateRec);
    }

    /**
     * Countable implementation
     *
     * @return BoardRateRec Return count of elements
     */
    public function count()
    {
      return count($this->BoardRateRec);
    }

}
