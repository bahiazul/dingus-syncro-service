<?php

namespace Dingus\SyncroService;

class Extra implements \JsonSerializable
{

    /**
     * @var \DateTime $DateFrom
     */
    protected $DateFrom = null;

    /**
     * @var \DateTime $DateTo
     */
    protected $DateTo = null;

    /**
     * @var string $ExtraCode
     */
    protected $ExtraCode = null;

    /**
     * @var string $ExtraName
     */
    protected $ExtraName = null;

    /**
     * @var int $ExtraQuantity
     */
    protected $ExtraQuantity = null;

    /**
     * @var int $NoPax
     */
    protected $NoPax = null;

    /**
     * @var string $PromotionCode
     */
    protected $PromotionCode = null;

    /**
     * @param \DateTime $DateFrom
     * @param \DateTime $DateTo
     * @param int $ExtraQuantity
     * @param int $NoPax
     */
    public function __construct(\DateTime $DateFrom, \DateTime $DateTo, $ExtraQuantity, $NoPax)
    {
      $this->DateFrom = $DateFrom->format(\DateTime::ATOM);
      $this->DateTo = $DateTo->format(\DateTime::ATOM);
      $this->ExtraQuantity = $ExtraQuantity;
      $this->NoPax = $NoPax;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'DateFrom' => $this->getDateFrom(),
        'DateTo' => $this->getDateTo(),
        'ExtraCode' => $this->getExtraCode(),
        'ExtraName' => $this->getExtraName(),
        'ExtraQuantity' => $this->getExtraQuantity(),
        'NoPax' => $this->getNoPax(),
        'PromotionCode' => $this->getPromotionCode(),
      );
    }

    /**
     * @return \DateTime
     */
    public function getDateFrom()
    {
      if ($this->DateFrom == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->DateFrom);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $DateFrom
     * @return \Dingus\SyncroService\Extra
     */
    public function setDateFrom(\DateTime $DateFrom)
    {
      $this->DateFrom = $DateFrom->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateTo()
    {
      if ($this->DateTo == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->DateTo);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $DateTo
     * @return \Dingus\SyncroService\Extra
     */
    public function setDateTo(\DateTime $DateTo)
    {
      $this->DateTo = $DateTo->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return string
     */
    public function getExtraCode()
    {
      return $this->ExtraCode;
    }

    /**
     * @param string $ExtraCode
     * @return \Dingus\SyncroService\Extra
     */
    public function setExtraCode($ExtraCode)
    {
      $this->ExtraCode = $ExtraCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getExtraName()
    {
      return $this->ExtraName;
    }

    /**
     * @param string $ExtraName
     * @return \Dingus\SyncroService\Extra
     */
    public function setExtraName($ExtraName)
    {
      $this->ExtraName = $ExtraName;
      return $this;
    }

    /**
     * @return int
     */
    public function getExtraQuantity()
    {
      return $this->ExtraQuantity;
    }

    /**
     * @param int $ExtraQuantity
     * @return \Dingus\SyncroService\Extra
     */
    public function setExtraQuantity($ExtraQuantity)
    {
      $this->ExtraQuantity = $ExtraQuantity;
      return $this;
    }

    /**
     * @return int
     */
    public function getNoPax()
    {
      return $this->NoPax;
    }

    /**
     * @param int $NoPax
     * @return \Dingus\SyncroService\Extra
     */
    public function setNoPax($NoPax)
    {
      $this->NoPax = $NoPax;
      return $this;
    }

    /**
     * @return string
     */
    public function getPromotionCode()
    {
      return $this->PromotionCode;
    }

    /**
     * @param string $PromotionCode
     * @return \Dingus\SyncroService\Extra
     */
    public function setPromotionCode($PromotionCode)
    {
      $this->PromotionCode = $PromotionCode;
      return $this;
    }

}
