<?php

namespace Dingus\SyncroService;

class GetBookingsByHotelResponse implements \JsonSerializable
{

    /**
     * @var GetBookinsRS $GetBookingsByHotelResult
     */
    protected $GetBookingsByHotelResult = null;

    /**
     * @param GetBookinsRS $GetBookingsByHotelResult
     */
    public function __construct($GetBookingsByHotelResult)
    {
      $this->GetBookingsByHotelResult = $GetBookingsByHotelResult;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'GetBookingsByHotelResult' => $this->getGetBookingsByHotelResult(),
      );
    }

    /**
     * @return GetBookinsRS
     */
    public function getGetBookingsByHotelResult()
    {
      return $this->GetBookingsByHotelResult;
    }

    /**
     * @param GetBookinsRS $GetBookingsByHotelResult
     * @return \Dingus\SyncroService\GetBookingsByHotelResponse
     */
    public function setGetBookingsByHotelResult($GetBookingsByHotelResult)
    {
      $this->GetBookingsByHotelResult = $GetBookingsByHotelResult;
      return $this;
    }

}
