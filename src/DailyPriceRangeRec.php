<?php

namespace Dingus\SyncroService;

class DailyPriceRangeRec implements \JsonSerializable
{

    /**
     * @var Action $Action
     */
    protected $Action = null;

    /**
     * @var int $Id
     */
    protected $Id = null;

    /**
     * @var PaxType $PaxType
     */
    protected $PaxType = null;

    /**
     * @var int $AgeFrom
     */
    protected $AgeFrom = null;

    /**
     * @var int $AgeTo
     */
    protected $AgeTo = null;

    /**
     * @var int $Range
     */
    protected $Range = null;

    /**
     * @var string $Reference
     */
    protected $Reference = null;

    /**
     * @param Action $Action
     * @param int $Id
     * @param PaxType $PaxType
     * @param int $AgeFrom
     * @param int $AgeTo
     * @param int $Range
     */
    public function __construct($Action, $Id, $PaxType, $AgeFrom, $AgeTo, $Range)
    {
      $this->Action = $Action;
      $this->Id = $Id;
      $this->PaxType = $PaxType;
      $this->AgeFrom = $AgeFrom;
      $this->AgeTo = $AgeTo;
      $this->Range = $Range;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'Action' => $this->getAction(),
        'Id' => $this->getId(),
        'PaxType' => $this->getPaxType(),
        'AgeFrom' => $this->getAgeFrom(),
        'AgeTo' => $this->getAgeTo(),
        'Range' => $this->getRange(),
        'Reference' => $this->getReference(),
      );
    }

    /**
     * @return Action
     */
    public function getAction()
    {
      return $this->Action;
    }

    /**
     * @param Action $Action
     * @return \Dingus\SyncroService\DailyPriceRangeRec
     */
    public function setAction($Action)
    {
      $this->Action = $Action;
      return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
      return $this->Id;
    }

    /**
     * @param int $Id
     * @return \Dingus\SyncroService\DailyPriceRangeRec
     */
    public function setId($Id)
    {
      $this->Id = $Id;
      return $this;
    }

    /**
     * @return PaxType
     */
    public function getPaxType()
    {
      return $this->PaxType;
    }

    /**
     * @param PaxType $PaxType
     * @return \Dingus\SyncroService\DailyPriceRangeRec
     */
    public function setPaxType($PaxType)
    {
      $this->PaxType = $PaxType;
      return $this;
    }

    /**
     * @return int
     */
    public function getAgeFrom()
    {
      return $this->AgeFrom;
    }

    /**
     * @param int $AgeFrom
     * @return \Dingus\SyncroService\DailyPriceRangeRec
     */
    public function setAgeFrom($AgeFrom)
    {
      $this->AgeFrom = $AgeFrom;
      return $this;
    }

    /**
     * @return int
     */
    public function getAgeTo()
    {
      return $this->AgeTo;
    }

    /**
     * @param int $AgeTo
     * @return \Dingus\SyncroService\DailyPriceRangeRec
     */
    public function setAgeTo($AgeTo)
    {
      $this->AgeTo = $AgeTo;
      return $this;
    }

    /**
     * @return int
     */
    public function getRange()
    {
      return $this->Range;
    }

    /**
     * @param int $Range
     * @return \Dingus\SyncroService\DailyPriceRangeRec
     */
    public function setRange($Range)
    {
      $this->Range = $Range;
      return $this;
    }

    /**
     * @return string
     */
    public function getReference()
    {
      return $this->Reference;
    }

    /**
     * @param string $Reference
     * @return \Dingus\SyncroService\DailyPriceRangeRec
     */
    public function setReference($Reference)
    {
      $this->Reference = $Reference;
      return $this;
    }

}
