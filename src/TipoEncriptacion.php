<?php

namespace Dingus\SyncroService;

class TipoEncriptacion
{
    const __default = 'Rijndael';
    const Rijndael = 'Rijndael';
    const RC2 = 'RC2';
    const DES = 'DES';
    const TripleDES = 'TripleDES';


}
