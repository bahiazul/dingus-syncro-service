<?php

namespace Dingus\SyncroService;

class Pax implements \JsonSerializable
{

    /**
     * @var int $PaxAge
     */
    protected $PaxAge = null;

    /**
     * @var string $PaxName
     */
    protected $PaxName = null;

    /**
     * @var PaxType $PaxType
     */
    protected $PaxType = null;

    /**
     * @var string $PaxTypeCode
     */
    protected $PaxTypeCode = null;

    /**
     * @param int $PaxAge
     * @param PaxType $PaxType
     */
    public function __construct($PaxAge, $PaxType)
    {
      $this->PaxAge = $PaxAge;
      $this->PaxType = $PaxType;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'PaxAge' => $this->getPaxAge(),
        'PaxName' => $this->getPaxName(),
        'PaxType' => $this->getPaxType(),
        'PaxTypeCode' => $this->getPaxTypeCode(),
      );
    }

    /**
     * @return int
     */
    public function getPaxAge()
    {
      return $this->PaxAge;
    }

    /**
     * @param int $PaxAge
     * @return \Dingus\SyncroService\Pax
     */
    public function setPaxAge($PaxAge)
    {
      $this->PaxAge = $PaxAge;
      return $this;
    }

    /**
     * @return string
     */
    public function getPaxName()
    {
      return $this->PaxName;
    }

    /**
     * @param string $PaxName
     * @return \Dingus\SyncroService\Pax
     */
    public function setPaxName($PaxName)
    {
      $this->PaxName = $PaxName;
      return $this;
    }

    /**
     * @return PaxType
     */
    public function getPaxType()
    {
      return $this->PaxType;
    }

    /**
     * @param PaxType $PaxType
     * @return \Dingus\SyncroService\Pax
     */
    public function setPaxType($PaxType)
    {
      $this->PaxType = $PaxType;
      return $this;
    }

    /**
     * @return string
     */
    public function getPaxTypeCode()
    {
      return $this->PaxTypeCode;
    }

    /**
     * @param string $PaxTypeCode
     * @return \Dingus\SyncroService\Pax
     */
    public function setPaxTypeCode($PaxTypeCode)
    {
      $this->PaxTypeCode = $PaxTypeCode;
      return $this;
    }

}
