<?php

namespace Dingus\SyncroService;

class CustomerPromotionRec implements \JsonSerializable
{

    /**
     * @var Action $Action
     */
    protected $Action = null;

    /**
     * @var int $IdCustomerPromotion
     */
    protected $IdCustomerPromotion = null;

    /**
     * @var string $CustomerCode
     */
    protected $CustomerCode = null;

    /**
     * @var string $CustomerName
     */
    protected $CustomerName = null;

    /**
     * @param Action $Action
     * @param int $IdCustomerPromotion
     */
    public function __construct($Action, $IdCustomerPromotion)
    {
      $this->Action = $Action;
      $this->IdCustomerPromotion = $IdCustomerPromotion;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'Action' => $this->getAction(),
        'IdCustomerPromotion' => $this->getIdCustomerPromotion(),
        'CustomerCode' => $this->getCustomerCode(),
        'CustomerName' => $this->getCustomerName(),
      );
    }

    /**
     * @return Action
     */
    public function getAction()
    {
      return $this->Action;
    }

    /**
     * @param Action $Action
     * @return \Dingus\SyncroService\CustomerPromotionRec
     */
    public function setAction($Action)
    {
      $this->Action = $Action;
      return $this;
    }

    /**
     * @return int
     */
    public function getIdCustomerPromotion()
    {
      return $this->IdCustomerPromotion;
    }

    /**
     * @param int $IdCustomerPromotion
     * @return \Dingus\SyncroService\CustomerPromotionRec
     */
    public function setIdCustomerPromotion($IdCustomerPromotion)
    {
      $this->IdCustomerPromotion = $IdCustomerPromotion;
      return $this;
    }

    /**
     * @return string
     */
    public function getCustomerCode()
    {
      return $this->CustomerCode;
    }

    /**
     * @param string $CustomerCode
     * @return \Dingus\SyncroService\CustomerPromotionRec
     */
    public function setCustomerCode($CustomerCode)
    {
      $this->CustomerCode = $CustomerCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getCustomerName()
    {
      return $this->CustomerName;
    }

    /**
     * @param string $CustomerName
     * @return \Dingus\SyncroService\CustomerPromotionRec
     */
    public function setCustomerName($CustomerName)
    {
      $this->CustomerName = $CustomerName;
      return $this;
    }

}
