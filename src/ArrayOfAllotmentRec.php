<?php

namespace Dingus\SyncroService;

class ArrayOfAllotmentRec implements \JsonSerializable, \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var AllotmentRec[] $AllotmentRec
     */
    protected $AllotmentRec = null;

    
    public function __construct()
    {
    
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'AllotmentRec' => $this->getAllotmentRec(),
      );
    }

    /**
     * @return AllotmentRec[]
     */
    public function getAllotmentRec()
    {
      return $this->AllotmentRec;
    }

    /**
     * @param AllotmentRec[] $AllotmentRec
     * @return \Dingus\SyncroService\ArrayOfAllotmentRec
     */
    public function setAllotmentRec(array $AllotmentRec = null)
    {
      $this->AllotmentRec = $AllotmentRec;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->AllotmentRec[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return AllotmentRec
     */
    public function offsetGet($offset)
    {
      return $this->AllotmentRec[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param AllotmentRec $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      $this->AllotmentRec[$offset] = $value;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->AllotmentRec[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return AllotmentRec Return the current element
     */
    public function current()
    {
      return current($this->AllotmentRec);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->AllotmentRec);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->AllotmentRec);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->AllotmentRec);
    }

    /**
     * Countable implementation
     *
     * @return AllotmentRec Return count of elements
     */
    public function count()
    {
      return count($this->AllotmentRec);
    }

}
