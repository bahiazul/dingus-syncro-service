<?php

namespace Dingus\SyncroService;

class PutSyncroBuffer implements \JsonSerializable
{

    /**
     * @var Credentials $Credentials
     */
    protected $Credentials = null;

    /**
     * @var SyncroBufferRS $SyncroBufferRS
     */
    protected $SyncroBufferRS = null;

    /**
     * @param Credentials $Credentials
     * @param SyncroBufferRS $SyncroBufferRS
     */
    public function __construct($Credentials, $SyncroBufferRS)
    {
      $this->Credentials = $Credentials;
      $this->SyncroBufferRS = $SyncroBufferRS;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'Credentials' => $this->getCredentials(),
        'SyncroBufferRS' => $this->getSyncroBufferRS(),
      );
    }

    /**
     * @return Credentials
     */
    public function getCredentials()
    {
      return $this->Credentials;
    }

    /**
     * @param Credentials $Credentials
     * @return \Dingus\SyncroService\PutSyncroBuffer
     */
    public function setCredentials($Credentials)
    {
      $this->Credentials = $Credentials;
      return $this;
    }

    /**
     * @return SyncroBufferRS
     */
    public function getSyncroBufferRS()
    {
      return $this->SyncroBufferRS;
    }

    /**
     * @param SyncroBufferRS $SyncroBufferRS
     * @return \Dingus\SyncroService\PutSyncroBuffer
     */
    public function setSyncroBufferRS($SyncroBufferRS)
    {
      $this->SyncroBufferRS = $SyncroBufferRS;
      return $this;
    }

}
