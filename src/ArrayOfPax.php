<?php

namespace Dingus\SyncroService;

class ArrayOfPax implements \JsonSerializable, \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var Pax[] $Pax
     */
    protected $Pax = null;

    
    public function __construct()
    {
    
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'Pax' => $this->getPax(),
      );
    }

    /**
     * @return Pax[]
     */
    public function getPax()
    {
      return $this->Pax;
    }

    /**
     * @param Pax[] $Pax
     * @return \Dingus\SyncroService\ArrayOfPax
     */
    public function setPax(array $Pax = null)
    {
      $this->Pax = $Pax;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->Pax[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return Pax
     */
    public function offsetGet($offset)
    {
      return $this->Pax[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param Pax $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      $this->Pax[$offset] = $value;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->Pax[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return Pax Return the current element
     */
    public function current()
    {
      return current($this->Pax);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->Pax);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->Pax);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->Pax);
    }

    /**
     * Countable implementation
     *
     * @return Pax Return count of elements
     */
    public function count()
    {
      return count($this->Pax);
    }

}
