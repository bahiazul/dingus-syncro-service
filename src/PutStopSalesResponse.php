<?php

namespace Dingus\SyncroService;

class PutStopSalesResponse implements \JsonSerializable
{

    /**
     * @var SyncroRS $PutStopSalesResult
     */
    protected $PutStopSalesResult = null;

    /**
     * @param SyncroRS $PutStopSalesResult
     */
    public function __construct($PutStopSalesResult)
    {
      $this->PutStopSalesResult = $PutStopSalesResult;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'PutStopSalesResult' => $this->getPutStopSalesResult(),
      );
    }

    /**
     * @return SyncroRS
     */
    public function getPutStopSalesResult()
    {
      return $this->PutStopSalesResult;
    }

    /**
     * @param SyncroRS $PutStopSalesResult
     * @return \Dingus\SyncroService\PutStopSalesResponse
     */
    public function setPutStopSalesResult($PutStopSalesResult)
    {
      $this->PutStopSalesResult = $PutStopSalesResult;
      return $this;
    }

}
