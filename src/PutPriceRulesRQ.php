<?php

namespace Dingus\SyncroService;

class PutPriceRulesRQ implements \JsonSerializable
{

    /**
     * @var ArrayOfPriceRulesRec $Recs
     */
    protected $Recs = null;

    
    public function __construct()
    {
    
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'Recs' => $this->getRecs(),
      );
    }

    /**
     * @return ArrayOfPriceRulesRec
     */
    public function getRecs()
    {
      return $this->Recs;
    }

    /**
     * @param ArrayOfPriceRulesRec $Recs
     * @return \Dingus\SyncroService\PutPriceRulesRQ
     */
    public function setRecs($Recs)
    {
      $this->Recs = $Recs;
      return $this;
    }

}
