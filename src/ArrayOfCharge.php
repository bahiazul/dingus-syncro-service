<?php

namespace Dingus\SyncroService;

class ArrayOfCharge implements \JsonSerializable, \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var Charge[] $Charge
     */
    protected $Charge = null;

    
    public function __construct()
    {
    
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'Charge' => $this->getCharge(),
      );
    }

    /**
     * @return Charge[]
     */
    public function getCharge()
    {
      return $this->Charge;
    }

    /**
     * @param Charge[] $Charge
     * @return \Dingus\SyncroService\ArrayOfCharge
     */
    public function setCharge(array $Charge = null)
    {
      $this->Charge = $Charge;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->Charge[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return Charge
     */
    public function offsetGet($offset)
    {
      return $this->Charge[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param Charge $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      $this->Charge[$offset] = $value;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->Charge[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return Charge Return the current element
     */
    public function current()
    {
      return current($this->Charge);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->Charge);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->Charge);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->Charge);
    }

    /**
     * Countable implementation
     *
     * @return Charge Return count of elements
     */
    public function count()
    {
      return count($this->Charge);
    }

}
