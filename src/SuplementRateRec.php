<?php

namespace Dingus\SyncroService;

class SuplementRateRec implements \JsonSerializable
{

    /**
     * @var Action $Action
     */
    protected $Action = null;

    /**
     * @var SuplType $Type
     */
    protected $Type = null;

    /**
     * @var int $AdtFrom
     */
    protected $AdtFrom = null;

    /**
     * @var int $AdtTo
     */
    protected $AdtTo = null;

    /**
     * @var int $AgeFrom
     */
    protected $AgeFrom = null;

    /**
     * @var int $AgeTo
     */
    protected $AgeTo = null;

    /**
     * @var boolean $ApplyToBoard
     */
    protected $ApplyToBoard = null;

    /**
     * @var boolean $ApplyToRoom
     */
    protected $ApplyToRoom = null;

    /**
     * @var boolean $ApplyToSupl
     */
    protected $ApplyToSupl = null;

    /**
     * @var string $BoardCode
     */
    protected $BoardCode = null;

    /**
     * @var int $ChdFrom
     */
    protected $ChdFrom = null;

    /**
     * @var int $ChdTo
     */
    protected $ChdTo = null;

    /**
     * @var float $ChidlPrice
     */
    protected $ChidlPrice = null;

    /**
     * @var string $Code
     */
    protected $Code = null;

    /**
     * @var \DateTime $EffectiveDate
     */
    protected $EffectiveDate = null;

    /**
     * @var \DateTime $DateFrom
     */
    protected $DateFrom = null;

    /**
     * @var \DateTime $DateFrom2
     */
    protected $DateFrom2 = null;

    /**
     * @var \DateTime $HourFrom
     */
    protected $HourFrom = null;

    /**
     * @var DatesType $DatesType
     */
    protected $DatesType = null;

    /**
     * @var \DateTime $DateTo
     */
    protected $DateTo = null;

    /**
     * @var \DateTime $DateTo2
     */
    protected $DateTo2 = null;

    /**
     * @var \DateTime $HourTo
     */
    protected $HourTo = null;

    /**
     * @var int $DaysBeforeArrivalFrom
     */
    protected $DaysBeforeArrivalFrom = null;

    /**
     * @var int $DaysBeforeArrivalTo
     */
    protected $DaysBeforeArrivalTo = null;

    /**
     * @var PaxType $PaxType
     */
    protected $PaxType = null;

    /**
     * @var boolean $PaxTypeSpecified
     */
    protected $PaxTypeSpecified = null;

    /**
     * @var float $EnfandPrice
     */
    protected $EnfandPrice = null;

    /**
     * @var float $FourthChildPrice
     */
    protected $FourthChildPrice = null;

    /**
     * @var float $FourthPaxPrice
     */
    protected $FourthPaxPrice = null;

    /**
     * @var boolean $IsPercent
     */
    protected $IsPercent = null;

    /**
     * @var int $PaxFrom
     */
    protected $PaxFrom = null;

    /**
     * @var int $PaxNo
     */
    protected $PaxNo = null;

    /**
     * @var int $PaxTo
     */
    protected $PaxTo = null;

    /**
     * @var string $PMSCode
     */
    protected $PMSCode = null;

    /**
     * @var float $Price
     */
    protected $Price = null;

    /**
     * @var string $RoomCode
     */
    protected $RoomCode = null;

    /**
     * @var string $CustomerCode
     */
    protected $CustomerCode = null;

    /**
     * @var float $SecondChidlPrice
     */
    protected $SecondChidlPrice = null;

    /**
     * @var int $NightFrom
     */
    protected $NightFrom = null;

    /**
     * @var int $NightTo
     */
    protected $NightTo = null;

    /**
     * @var int $Stay
     */
    protected $Stay = null;

    /**
     * @var int $StayToInvice
     */
    protected $StayToInvice = null;

    /**
     * @var float $ThirdChidlPrice
     */
    protected $ThirdChidlPrice = null;

    /**
     * @var float $ThirdPaxPrice
     */
    protected $ThirdPaxPrice = null;

    /**
     * @var boolean $Excluding
     */
    protected $Excluding = null;

    /**
     * @var int $PriorityExcluding
     */
    protected $PriorityExcluding = null;

    /**
     * @var boolean $UniqueApplication
     */
    protected $UniqueApplication = null;

    /**
     * @var boolean $Monday
     */
    protected $Monday = null;

    /**
     * @var boolean $Tuesday
     */
    protected $Tuesday = null;

    /**
     * @var boolean $Wednesday
     */
    protected $Wednesday = null;

    /**
     * @var boolean $Thursday
     */
    protected $Thursday = null;

    /**
     * @var boolean $Friday
     */
    protected $Friday = null;

    /**
     * @var boolean $Saturday
     */
    protected $Saturday = null;

    /**
     * @var boolean $Sunday
     */
    protected $Sunday = null;

    /**
     * @var boolean $StayAxBNoExact
     */
    protected $StayAxBNoExact = null;

    /**
     * @var TipoNocheDescontarAxB $NightTypeDiscountAxB
     */
    protected $NightTypeDiscountAxB = null;

    /**
     * @var boolean $ArrivalAtMonday
     */
    protected $ArrivalAtMonday = null;

    /**
     * @var boolean $ArrivalAtTuesday
     */
    protected $ArrivalAtTuesday = null;

    /**
     * @var boolean $ArrivalAtWednesday
     */
    protected $ArrivalAtWednesday = null;

    /**
     * @var boolean $ArrivalAtThursday
     */
    protected $ArrivalAtThursday = null;

    /**
     * @var boolean $ArrivalAtFriday
     */
    protected $ArrivalAtFriday = null;

    /**
     * @var boolean $ArrivalAtSaturday
     */
    protected $ArrivalAtSaturday = null;

    /**
     * @var boolean $ArrivalAtSunday
     */
    protected $ArrivalAtSunday = null;

    /**
     * @var boolean $Guaranteed
     */
    protected $Guaranteed = null;

    /**
     * @param Action $Action
     * @param SuplType $Type
     * @param int $AdtFrom
     * @param int $AdtTo
     * @param int $AgeFrom
     * @param int $AgeTo
     * @param boolean $ApplyToBoard
     * @param boolean $ApplyToRoom
     * @param boolean $ApplyToSupl
     * @param int $ChdFrom
     * @param int $ChdTo
     * @param float $ChidlPrice
     * @param \DateTime $EffectiveDate
     * @param \DateTime $DateFrom
     * @param \DateTime $DateFrom2
     * @param \DateTime $HourFrom
     * @param DatesType $DatesType
     * @param \DateTime $DateTo
     * @param \DateTime $DateTo2
     * @param \DateTime $HourTo
     * @param int $DaysBeforeArrivalFrom
     * @param int $DaysBeforeArrivalTo
     * @param boolean $PaxTypeSpecified
     * @param float $EnfandPrice
     * @param float $FourthChildPrice
     * @param float $FourthPaxPrice
     * @param boolean $IsPercent
     * @param int $PaxFrom
     * @param int $PaxNo
     * @param int $PaxTo
     * @param float $Price
     * @param float $SecondChidlPrice
     * @param int $NightFrom
     * @param int $NightTo
     * @param int $Stay
     * @param int $StayToInvice
     * @param float $ThirdChidlPrice
     * @param float $ThirdPaxPrice
     * @param boolean $Excluding
     * @param int $PriorityExcluding
     * @param boolean $UniqueApplication
     * @param boolean $Monday
     * @param boolean $Tuesday
     * @param boolean $Wednesday
     * @param boolean $Thursday
     * @param boolean $Friday
     * @param boolean $Saturday
     * @param boolean $Sunday
     * @param boolean $StayAxBNoExact
     * @param TipoNocheDescontarAxB $NightTypeDiscountAxB
     * @param boolean $ArrivalAtMonday
     * @param boolean $ArrivalAtTuesday
     * @param boolean $ArrivalAtWednesday
     * @param boolean $ArrivalAtThursday
     * @param boolean $ArrivalAtFriday
     * @param boolean $ArrivalAtSaturday
     * @param boolean $ArrivalAtSunday
     * @param boolean $Guaranteed
     */
    public function __construct($Action, $Type, $AdtFrom, $AdtTo, $AgeFrom, $AgeTo, $ApplyToBoard, $ApplyToRoom, $ApplyToSupl, $ChdFrom, $ChdTo, $ChidlPrice, \DateTime $EffectiveDate, \DateTime $DateFrom, \DateTime $DateFrom2, \DateTime $HourFrom, $DatesType, \DateTime $DateTo, \DateTime $DateTo2, \DateTime $HourTo, $DaysBeforeArrivalFrom, $DaysBeforeArrivalTo, $PaxTypeSpecified, $EnfandPrice, $FourthChildPrice, $FourthPaxPrice, $IsPercent, $PaxFrom, $PaxNo, $PaxTo, $Price, $SecondChidlPrice, $NightFrom, $NightTo, $Stay, $StayToInvice, $ThirdChidlPrice, $ThirdPaxPrice, $Excluding, $PriorityExcluding, $UniqueApplication, $Monday, $Tuesday, $Wednesday, $Thursday, $Friday, $Saturday, $Sunday, $StayAxBNoExact, $NightTypeDiscountAxB, $ArrivalAtMonday, $ArrivalAtTuesday, $ArrivalAtWednesday, $ArrivalAtThursday, $ArrivalAtFriday, $ArrivalAtSaturday, $ArrivalAtSunday, $Guaranteed)
    {
      $this->Action = $Action;
      $this->Type = $Type;
      $this->AdtFrom = $AdtFrom;
      $this->AdtTo = $AdtTo;
      $this->AgeFrom = $AgeFrom;
      $this->AgeTo = $AgeTo;
      $this->ApplyToBoard = $ApplyToBoard;
      $this->ApplyToRoom = $ApplyToRoom;
      $this->ApplyToSupl = $ApplyToSupl;
      $this->ChdFrom = $ChdFrom;
      $this->ChdTo = $ChdTo;
      $this->ChidlPrice = $ChidlPrice;
      $this->EffectiveDate = $EffectiveDate->format(\DateTime::ATOM);
      $this->DateFrom = $DateFrom->format(\DateTime::ATOM);
      $this->DateFrom2 = $DateFrom2->format(\DateTime::ATOM);
      $this->HourFrom = $HourFrom->format(\DateTime::ATOM);
      $this->DatesType = $DatesType;
      $this->DateTo = $DateTo->format(\DateTime::ATOM);
      $this->DateTo2 = $DateTo2->format(\DateTime::ATOM);
      $this->HourTo = $HourTo->format(\DateTime::ATOM);
      $this->DaysBeforeArrivalFrom = $DaysBeforeArrivalFrom;
      $this->DaysBeforeArrivalTo = $DaysBeforeArrivalTo;
      $this->PaxTypeSpecified = $PaxTypeSpecified;
      $this->EnfandPrice = $EnfandPrice;
      $this->FourthChildPrice = $FourthChildPrice;
      $this->FourthPaxPrice = $FourthPaxPrice;
      $this->IsPercent = $IsPercent;
      $this->PaxFrom = $PaxFrom;
      $this->PaxNo = $PaxNo;
      $this->PaxTo = $PaxTo;
      $this->Price = $Price;
      $this->SecondChidlPrice = $SecondChidlPrice;
      $this->NightFrom = $NightFrom;
      $this->NightTo = $NightTo;
      $this->Stay = $Stay;
      $this->StayToInvice = $StayToInvice;
      $this->ThirdChidlPrice = $ThirdChidlPrice;
      $this->ThirdPaxPrice = $ThirdPaxPrice;
      $this->Excluding = $Excluding;
      $this->PriorityExcluding = $PriorityExcluding;
      $this->UniqueApplication = $UniqueApplication;
      $this->Monday = $Monday;
      $this->Tuesday = $Tuesday;
      $this->Wednesday = $Wednesday;
      $this->Thursday = $Thursday;
      $this->Friday = $Friday;
      $this->Saturday = $Saturday;
      $this->Sunday = $Sunday;
      $this->StayAxBNoExact = $StayAxBNoExact;
      $this->NightTypeDiscountAxB = $NightTypeDiscountAxB;
      $this->ArrivalAtMonday = $ArrivalAtMonday;
      $this->ArrivalAtTuesday = $ArrivalAtTuesday;
      $this->ArrivalAtWednesday = $ArrivalAtWednesday;
      $this->ArrivalAtThursday = $ArrivalAtThursday;
      $this->ArrivalAtFriday = $ArrivalAtFriday;
      $this->ArrivalAtSaturday = $ArrivalAtSaturday;
      $this->ArrivalAtSunday = $ArrivalAtSunday;
      $this->Guaranteed = $Guaranteed;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'Action' => $this->getAction(),
        'Type' => $this->getType(),
        'AdtFrom' => $this->getAdtFrom(),
        'AdtTo' => $this->getAdtTo(),
        'AgeFrom' => $this->getAgeFrom(),
        'AgeTo' => $this->getAgeTo(),
        'ApplyToBoard' => $this->getApplyToBoard(),
        'ApplyToRoom' => $this->getApplyToRoom(),
        'ApplyToSupl' => $this->getApplyToSupl(),
        'BoardCode' => $this->getBoardCode(),
        'ChdFrom' => $this->getChdFrom(),
        'ChdTo' => $this->getChdTo(),
        'ChidlPrice' => $this->getChidlPrice(),
        'Code' => $this->getCode(),
        'EffectiveDate' => $this->getEffectiveDate(),
        'DateFrom' => $this->getDateFrom(),
        'DateFrom2' => $this->getDateFrom2(),
        'HourFrom' => $this->getHourFrom(),
        'DatesType' => $this->getDatesType(),
        'DateTo' => $this->getDateTo(),
        'DateTo2' => $this->getDateTo2(),
        'HourTo' => $this->getHourTo(),
        'DaysBeforeArrivalFrom' => $this->getDaysBeforeArrivalFrom(),
        'DaysBeforeArrivalTo' => $this->getDaysBeforeArrivalTo(),
        'PaxType' => $this->getPaxType(),
        'PaxTypeSpecified' => $this->getPaxTypeSpecified(),
        'EnfandPrice' => $this->getEnfandPrice(),
        'FourthChildPrice' => $this->getFourthChildPrice(),
        'FourthPaxPrice' => $this->getFourthPaxPrice(),
        'IsPercent' => $this->getIsPercent(),
        'PaxFrom' => $this->getPaxFrom(),
        'PaxNo' => $this->getPaxNo(),
        'PaxTo' => $this->getPaxTo(),
        'PMSCode' => $this->getPMSCode(),
        'Price' => $this->getPrice(),
        'RoomCode' => $this->getRoomCode(),
        'CustomerCode' => $this->getCustomerCode(),
        'SecondChidlPrice' => $this->getSecondChidlPrice(),
        'NightFrom' => $this->getNightFrom(),
        'NightTo' => $this->getNightTo(),
        'Stay' => $this->getStay(),
        'StayToInvice' => $this->getStayToInvice(),
        'ThirdChidlPrice' => $this->getThirdChidlPrice(),
        'ThirdPaxPrice' => $this->getThirdPaxPrice(),
        'Excluding' => $this->getExcluding(),
        'PriorityExcluding' => $this->getPriorityExcluding(),
        'UniqueApplication' => $this->getUniqueApplication(),
        'Monday' => $this->getMonday(),
        'Tuesday' => $this->getTuesday(),
        'Wednesday' => $this->getWednesday(),
        'Thursday' => $this->getThursday(),
        'Friday' => $this->getFriday(),
        'Saturday' => $this->getSaturday(),
        'Sunday' => $this->getSunday(),
        'StayAxBNoExact' => $this->getStayAxBNoExact(),
        'NightTypeDiscountAxB' => $this->getNightTypeDiscountAxB(),
        'ArrivalAtMonday' => $this->getArrivalAtMonday(),
        'ArrivalAtTuesday' => $this->getArrivalAtTuesday(),
        'ArrivalAtWednesday' => $this->getArrivalAtWednesday(),
        'ArrivalAtThursday' => $this->getArrivalAtThursday(),
        'ArrivalAtFriday' => $this->getArrivalAtFriday(),
        'ArrivalAtSaturday' => $this->getArrivalAtSaturday(),
        'ArrivalAtSunday' => $this->getArrivalAtSunday(),
        'Guaranteed' => $this->getGuaranteed(),
      );
    }

    /**
     * @return Action
     */
    public function getAction()
    {
      return $this->Action;
    }

    /**
     * @param Action $Action
     * @return \Dingus\SyncroService\SuplementRateRec
     */
    public function setAction($Action)
    {
      $this->Action = $Action;
      return $this;
    }

    /**
     * @return SuplType
     */
    public function getType()
    {
      return $this->Type;
    }

    /**
     * @param SuplType $Type
     * @return \Dingus\SyncroService\SuplementRateRec
     */
    public function setType($Type)
    {
      $this->Type = $Type;
      return $this;
    }

    /**
     * @return int
     */
    public function getAdtFrom()
    {
      return $this->AdtFrom;
    }

    /**
     * @param int $AdtFrom
     * @return \Dingus\SyncroService\SuplementRateRec
     */
    public function setAdtFrom($AdtFrom)
    {
      $this->AdtFrom = $AdtFrom;
      return $this;
    }

    /**
     * @return int
     */
    public function getAdtTo()
    {
      return $this->AdtTo;
    }

    /**
     * @param int $AdtTo
     * @return \Dingus\SyncroService\SuplementRateRec
     */
    public function setAdtTo($AdtTo)
    {
      $this->AdtTo = $AdtTo;
      return $this;
    }

    /**
     * @return int
     */
    public function getAgeFrom()
    {
      return $this->AgeFrom;
    }

    /**
     * @param int $AgeFrom
     * @return \Dingus\SyncroService\SuplementRateRec
     */
    public function setAgeFrom($AgeFrom)
    {
      $this->AgeFrom = $AgeFrom;
      return $this;
    }

    /**
     * @return int
     */
    public function getAgeTo()
    {
      return $this->AgeTo;
    }

    /**
     * @param int $AgeTo
     * @return \Dingus\SyncroService\SuplementRateRec
     */
    public function setAgeTo($AgeTo)
    {
      $this->AgeTo = $AgeTo;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getApplyToBoard()
    {
      return $this->ApplyToBoard;
    }

    /**
     * @param boolean $ApplyToBoard
     * @return \Dingus\SyncroService\SuplementRateRec
     */
    public function setApplyToBoard($ApplyToBoard)
    {
      $this->ApplyToBoard = $ApplyToBoard;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getApplyToRoom()
    {
      return $this->ApplyToRoom;
    }

    /**
     * @param boolean $ApplyToRoom
     * @return \Dingus\SyncroService\SuplementRateRec
     */
    public function setApplyToRoom($ApplyToRoom)
    {
      $this->ApplyToRoom = $ApplyToRoom;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getApplyToSupl()
    {
      return $this->ApplyToSupl;
    }

    /**
     * @param boolean $ApplyToSupl
     * @return \Dingus\SyncroService\SuplementRateRec
     */
    public function setApplyToSupl($ApplyToSupl)
    {
      $this->ApplyToSupl = $ApplyToSupl;
      return $this;
    }

    /**
     * @return string
     */
    public function getBoardCode()
    {
      return $this->BoardCode;
    }

    /**
     * @param string $BoardCode
     * @return \Dingus\SyncroService\SuplementRateRec
     */
    public function setBoardCode($BoardCode)
    {
      $this->BoardCode = $BoardCode;
      return $this;
    }

    /**
     * @return int
     */
    public function getChdFrom()
    {
      return $this->ChdFrom;
    }

    /**
     * @param int $ChdFrom
     * @return \Dingus\SyncroService\SuplementRateRec
     */
    public function setChdFrom($ChdFrom)
    {
      $this->ChdFrom = $ChdFrom;
      return $this;
    }

    /**
     * @return int
     */
    public function getChdTo()
    {
      return $this->ChdTo;
    }

    /**
     * @param int $ChdTo
     * @return \Dingus\SyncroService\SuplementRateRec
     */
    public function setChdTo($ChdTo)
    {
      $this->ChdTo = $ChdTo;
      return $this;
    }

    /**
     * @return float
     */
    public function getChidlPrice()
    {
      return $this->ChidlPrice;
    }

    /**
     * @param float $ChidlPrice
     * @return \Dingus\SyncroService\SuplementRateRec
     */
    public function setChidlPrice($ChidlPrice)
    {
      $this->ChidlPrice = $ChidlPrice;
      return $this;
    }

    /**
     * @return string
     */
    public function getCode()
    {
      return $this->Code;
    }

    /**
     * @param string $Code
     * @return \Dingus\SyncroService\SuplementRateRec
     */
    public function setCode($Code)
    {
      $this->Code = $Code;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getEffectiveDate()
    {
      if ($this->EffectiveDate == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->EffectiveDate);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $EffectiveDate
     * @return \Dingus\SyncroService\SuplementRateRec
     */
    public function setEffectiveDate(\DateTime $EffectiveDate)
    {
      $this->EffectiveDate = $EffectiveDate->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateFrom()
    {
      if ($this->DateFrom == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->DateFrom);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $DateFrom
     * @return \Dingus\SyncroService\SuplementRateRec
     */
    public function setDateFrom(\DateTime $DateFrom)
    {
      $this->DateFrom = $DateFrom->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateFrom2()
    {
      if ($this->DateFrom2 == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->DateFrom2);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $DateFrom2
     * @return \Dingus\SyncroService\SuplementRateRec
     */
    public function setDateFrom2(\DateTime $DateFrom2)
    {
      $this->DateFrom2 = $DateFrom2->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getHourFrom()
    {
      if ($this->HourFrom == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->HourFrom);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $HourFrom
     * @return \Dingus\SyncroService\SuplementRateRec
     */
    public function setHourFrom(\DateTime $HourFrom)
    {
      $this->HourFrom = $HourFrom->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return DatesType
     */
    public function getDatesType()
    {
      return $this->DatesType;
    }

    /**
     * @param DatesType $DatesType
     * @return \Dingus\SyncroService\SuplementRateRec
     */
    public function setDatesType($DatesType)
    {
      $this->DatesType = $DatesType;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateTo()
    {
      if ($this->DateTo == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->DateTo);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $DateTo
     * @return \Dingus\SyncroService\SuplementRateRec
     */
    public function setDateTo(\DateTime $DateTo)
    {
      $this->DateTo = $DateTo->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateTo2()
    {
      if ($this->DateTo2 == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->DateTo2);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $DateTo2
     * @return \Dingus\SyncroService\SuplementRateRec
     */
    public function setDateTo2(\DateTime $DateTo2)
    {
      $this->DateTo2 = $DateTo2->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getHourTo()
    {
      if ($this->HourTo == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->HourTo);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $HourTo
     * @return \Dingus\SyncroService\SuplementRateRec
     */
    public function setHourTo(\DateTime $HourTo)
    {
      $this->HourTo = $HourTo->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return int
     */
    public function getDaysBeforeArrivalFrom()
    {
      return $this->DaysBeforeArrivalFrom;
    }

    /**
     * @param int $DaysBeforeArrivalFrom
     * @return \Dingus\SyncroService\SuplementRateRec
     */
    public function setDaysBeforeArrivalFrom($DaysBeforeArrivalFrom)
    {
      $this->DaysBeforeArrivalFrom = $DaysBeforeArrivalFrom;
      return $this;
    }

    /**
     * @return int
     */
    public function getDaysBeforeArrivalTo()
    {
      return $this->DaysBeforeArrivalTo;
    }

    /**
     * @param int $DaysBeforeArrivalTo
     * @return \Dingus\SyncroService\SuplementRateRec
     */
    public function setDaysBeforeArrivalTo($DaysBeforeArrivalTo)
    {
      $this->DaysBeforeArrivalTo = $DaysBeforeArrivalTo;
      return $this;
    }

    /**
     * @return PaxType
     */
    public function getPaxType()
    {
      return $this->PaxType;
    }

    /**
     * @param PaxType $PaxType
     * @return \Dingus\SyncroService\SuplementRateRec
     */
    public function setPaxType($PaxType)
    {
      $this->PaxType = $PaxType;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getPaxTypeSpecified()
    {
      return $this->PaxTypeSpecified;
    }

    /**
     * @param boolean $PaxTypeSpecified
     * @return \Dingus\SyncroService\SuplementRateRec
     */
    public function setPaxTypeSpecified($PaxTypeSpecified)
    {
      $this->PaxTypeSpecified = $PaxTypeSpecified;
      return $this;
    }

    /**
     * @return float
     */
    public function getEnfandPrice()
    {
      return $this->EnfandPrice;
    }

    /**
     * @param float $EnfandPrice
     * @return \Dingus\SyncroService\SuplementRateRec
     */
    public function setEnfandPrice($EnfandPrice)
    {
      $this->EnfandPrice = $EnfandPrice;
      return $this;
    }

    /**
     * @return float
     */
    public function getFourthChildPrice()
    {
      return $this->FourthChildPrice;
    }

    /**
     * @param float $FourthChildPrice
     * @return \Dingus\SyncroService\SuplementRateRec
     */
    public function setFourthChildPrice($FourthChildPrice)
    {
      $this->FourthChildPrice = $FourthChildPrice;
      return $this;
    }

    /**
     * @return float
     */
    public function getFourthPaxPrice()
    {
      return $this->FourthPaxPrice;
    }

    /**
     * @param float $FourthPaxPrice
     * @return \Dingus\SyncroService\SuplementRateRec
     */
    public function setFourthPaxPrice($FourthPaxPrice)
    {
      $this->FourthPaxPrice = $FourthPaxPrice;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getIsPercent()
    {
      return $this->IsPercent;
    }

    /**
     * @param boolean $IsPercent
     * @return \Dingus\SyncroService\SuplementRateRec
     */
    public function setIsPercent($IsPercent)
    {
      $this->IsPercent = $IsPercent;
      return $this;
    }

    /**
     * @return int
     */
    public function getPaxFrom()
    {
      return $this->PaxFrom;
    }

    /**
     * @param int $PaxFrom
     * @return \Dingus\SyncroService\SuplementRateRec
     */
    public function setPaxFrom($PaxFrom)
    {
      $this->PaxFrom = $PaxFrom;
      return $this;
    }

    /**
     * @return int
     */
    public function getPaxNo()
    {
      return $this->PaxNo;
    }

    /**
     * @param int $PaxNo
     * @return \Dingus\SyncroService\SuplementRateRec
     */
    public function setPaxNo($PaxNo)
    {
      $this->PaxNo = $PaxNo;
      return $this;
    }

    /**
     * @return int
     */
    public function getPaxTo()
    {
      return $this->PaxTo;
    }

    /**
     * @param int $PaxTo
     * @return \Dingus\SyncroService\SuplementRateRec
     */
    public function setPaxTo($PaxTo)
    {
      $this->PaxTo = $PaxTo;
      return $this;
    }

    /**
     * @return string
     */
    public function getPMSCode()
    {
      return $this->PMSCode;
    }

    /**
     * @param string $PMSCode
     * @return \Dingus\SyncroService\SuplementRateRec
     */
    public function setPMSCode($PMSCode)
    {
      $this->PMSCode = $PMSCode;
      return $this;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
      return $this->Price;
    }

    /**
     * @param float $Price
     * @return \Dingus\SyncroService\SuplementRateRec
     */
    public function setPrice($Price)
    {
      $this->Price = $Price;
      return $this;
    }

    /**
     * @return string
     */
    public function getRoomCode()
    {
      return $this->RoomCode;
    }

    /**
     * @param string $RoomCode
     * @return \Dingus\SyncroService\SuplementRateRec
     */
    public function setRoomCode($RoomCode)
    {
      $this->RoomCode = $RoomCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getCustomerCode()
    {
      return $this->CustomerCode;
    }

    /**
     * @param string $CustomerCode
     * @return \Dingus\SyncroService\SuplementRateRec
     */
    public function setCustomerCode($CustomerCode)
    {
      $this->CustomerCode = $CustomerCode;
      return $this;
    }

    /**
     * @return float
     */
    public function getSecondChidlPrice()
    {
      return $this->SecondChidlPrice;
    }

    /**
     * @param float $SecondChidlPrice
     * @return \Dingus\SyncroService\SuplementRateRec
     */
    public function setSecondChidlPrice($SecondChidlPrice)
    {
      $this->SecondChidlPrice = $SecondChidlPrice;
      return $this;
    }

    /**
     * @return int
     */
    public function getNightFrom()
    {
      return $this->NightFrom;
    }

    /**
     * @param int $NightFrom
     * @return \Dingus\SyncroService\SuplementRateRec
     */
    public function setNightFrom($NightFrom)
    {
      $this->NightFrom = $NightFrom;
      return $this;
    }

    /**
     * @return int
     */
    public function getNightTo()
    {
      return $this->NightTo;
    }

    /**
     * @param int $NightTo
     * @return \Dingus\SyncroService\SuplementRateRec
     */
    public function setNightTo($NightTo)
    {
      $this->NightTo = $NightTo;
      return $this;
    }

    /**
     * @return int
     */
    public function getStay()
    {
      return $this->Stay;
    }

    /**
     * @param int $Stay
     * @return \Dingus\SyncroService\SuplementRateRec
     */
    public function setStay($Stay)
    {
      $this->Stay = $Stay;
      return $this;
    }

    /**
     * @return int
     */
    public function getStayToInvice()
    {
      return $this->StayToInvice;
    }

    /**
     * @param int $StayToInvice
     * @return \Dingus\SyncroService\SuplementRateRec
     */
    public function setStayToInvice($StayToInvice)
    {
      $this->StayToInvice = $StayToInvice;
      return $this;
    }

    /**
     * @return float
     */
    public function getThirdChidlPrice()
    {
      return $this->ThirdChidlPrice;
    }

    /**
     * @param float $ThirdChidlPrice
     * @return \Dingus\SyncroService\SuplementRateRec
     */
    public function setThirdChidlPrice($ThirdChidlPrice)
    {
      $this->ThirdChidlPrice = $ThirdChidlPrice;
      return $this;
    }

    /**
     * @return float
     */
    public function getThirdPaxPrice()
    {
      return $this->ThirdPaxPrice;
    }

    /**
     * @param float $ThirdPaxPrice
     * @return \Dingus\SyncroService\SuplementRateRec
     */
    public function setThirdPaxPrice($ThirdPaxPrice)
    {
      $this->ThirdPaxPrice = $ThirdPaxPrice;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getExcluding()
    {
      return $this->Excluding;
    }

    /**
     * @param boolean $Excluding
     * @return \Dingus\SyncroService\SuplementRateRec
     */
    public function setExcluding($Excluding)
    {
      $this->Excluding = $Excluding;
      return $this;
    }

    /**
     * @return int
     */
    public function getPriorityExcluding()
    {
      return $this->PriorityExcluding;
    }

    /**
     * @param int $PriorityExcluding
     * @return \Dingus\SyncroService\SuplementRateRec
     */
    public function setPriorityExcluding($PriorityExcluding)
    {
      $this->PriorityExcluding = $PriorityExcluding;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getUniqueApplication()
    {
      return $this->UniqueApplication;
    }

    /**
     * @param boolean $UniqueApplication
     * @return \Dingus\SyncroService\SuplementRateRec
     */
    public function setUniqueApplication($UniqueApplication)
    {
      $this->UniqueApplication = $UniqueApplication;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getMonday()
    {
      return $this->Monday;
    }

    /**
     * @param boolean $Monday
     * @return \Dingus\SyncroService\SuplementRateRec
     */
    public function setMonday($Monday)
    {
      $this->Monday = $Monday;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getTuesday()
    {
      return $this->Tuesday;
    }

    /**
     * @param boolean $Tuesday
     * @return \Dingus\SyncroService\SuplementRateRec
     */
    public function setTuesday($Tuesday)
    {
      $this->Tuesday = $Tuesday;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getWednesday()
    {
      return $this->Wednesday;
    }

    /**
     * @param boolean $Wednesday
     * @return \Dingus\SyncroService\SuplementRateRec
     */
    public function setWednesday($Wednesday)
    {
      $this->Wednesday = $Wednesday;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getThursday()
    {
      return $this->Thursday;
    }

    /**
     * @param boolean $Thursday
     * @return \Dingus\SyncroService\SuplementRateRec
     */
    public function setThursday($Thursday)
    {
      $this->Thursday = $Thursday;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getFriday()
    {
      return $this->Friday;
    }

    /**
     * @param boolean $Friday
     * @return \Dingus\SyncroService\SuplementRateRec
     */
    public function setFriday($Friday)
    {
      $this->Friday = $Friday;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getSaturday()
    {
      return $this->Saturday;
    }

    /**
     * @param boolean $Saturday
     * @return \Dingus\SyncroService\SuplementRateRec
     */
    public function setSaturday($Saturday)
    {
      $this->Saturday = $Saturday;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getSunday()
    {
      return $this->Sunday;
    }

    /**
     * @param boolean $Sunday
     * @return \Dingus\SyncroService\SuplementRateRec
     */
    public function setSunday($Sunday)
    {
      $this->Sunday = $Sunday;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getStayAxBNoExact()
    {
      return $this->StayAxBNoExact;
    }

    /**
     * @param boolean $StayAxBNoExact
     * @return \Dingus\SyncroService\SuplementRateRec
     */
    public function setStayAxBNoExact($StayAxBNoExact)
    {
      $this->StayAxBNoExact = $StayAxBNoExact;
      return $this;
    }

    /**
     * @return TipoNocheDescontarAxB
     */
    public function getNightTypeDiscountAxB()
    {
      return $this->NightTypeDiscountAxB;
    }

    /**
     * @param TipoNocheDescontarAxB $NightTypeDiscountAxB
     * @return \Dingus\SyncroService\SuplementRateRec
     */
    public function setNightTypeDiscountAxB($NightTypeDiscountAxB)
    {
      $this->NightTypeDiscountAxB = $NightTypeDiscountAxB;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getArrivalAtMonday()
    {
      return $this->ArrivalAtMonday;
    }

    /**
     * @param boolean $ArrivalAtMonday
     * @return \Dingus\SyncroService\SuplementRateRec
     */
    public function setArrivalAtMonday($ArrivalAtMonday)
    {
      $this->ArrivalAtMonday = $ArrivalAtMonday;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getArrivalAtTuesday()
    {
      return $this->ArrivalAtTuesday;
    }

    /**
     * @param boolean $ArrivalAtTuesday
     * @return \Dingus\SyncroService\SuplementRateRec
     */
    public function setArrivalAtTuesday($ArrivalAtTuesday)
    {
      $this->ArrivalAtTuesday = $ArrivalAtTuesday;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getArrivalAtWednesday()
    {
      return $this->ArrivalAtWednesday;
    }

    /**
     * @param boolean $ArrivalAtWednesday
     * @return \Dingus\SyncroService\SuplementRateRec
     */
    public function setArrivalAtWednesday($ArrivalAtWednesday)
    {
      $this->ArrivalAtWednesday = $ArrivalAtWednesday;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getArrivalAtThursday()
    {
      return $this->ArrivalAtThursday;
    }

    /**
     * @param boolean $ArrivalAtThursday
     * @return \Dingus\SyncroService\SuplementRateRec
     */
    public function setArrivalAtThursday($ArrivalAtThursday)
    {
      $this->ArrivalAtThursday = $ArrivalAtThursday;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getArrivalAtFriday()
    {
      return $this->ArrivalAtFriday;
    }

    /**
     * @param boolean $ArrivalAtFriday
     * @return \Dingus\SyncroService\SuplementRateRec
     */
    public function setArrivalAtFriday($ArrivalAtFriday)
    {
      $this->ArrivalAtFriday = $ArrivalAtFriday;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getArrivalAtSaturday()
    {
      return $this->ArrivalAtSaturday;
    }

    /**
     * @param boolean $ArrivalAtSaturday
     * @return \Dingus\SyncroService\SuplementRateRec
     */
    public function setArrivalAtSaturday($ArrivalAtSaturday)
    {
      $this->ArrivalAtSaturday = $ArrivalAtSaturday;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getArrivalAtSunday()
    {
      return $this->ArrivalAtSunday;
    }

    /**
     * @param boolean $ArrivalAtSunday
     * @return \Dingus\SyncroService\SuplementRateRec
     */
    public function setArrivalAtSunday($ArrivalAtSunday)
    {
      $this->ArrivalAtSunday = $ArrivalAtSunday;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getGuaranteed()
    {
      return $this->Guaranteed;
    }

    /**
     * @param boolean $Guaranteed
     * @return \Dingus\SyncroService\SuplementRateRec
     */
    public function setGuaranteed($Guaranteed)
    {
      $this->Guaranteed = $Guaranteed;
      return $this;
    }

}
