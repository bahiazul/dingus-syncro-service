<?php

namespace Dingus\SyncroService;

class ArrayOfStayAllotmentRec implements \JsonSerializable, \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var StayAllotmentRec[] $StayAllotmentRec
     */
    protected $StayAllotmentRec = null;

    
    public function __construct()
    {
    
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'StayAllotmentRec' => $this->getStayAllotmentRec(),
      );
    }

    /**
     * @return StayAllotmentRec[]
     */
    public function getStayAllotmentRec()
    {
      return $this->StayAllotmentRec;
    }

    /**
     * @param StayAllotmentRec[] $StayAllotmentRec
     * @return \Dingus\SyncroService\ArrayOfStayAllotmentRec
     */
    public function setStayAllotmentRec(array $StayAllotmentRec = null)
    {
      $this->StayAllotmentRec = $StayAllotmentRec;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->StayAllotmentRec[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return StayAllotmentRec
     */
    public function offsetGet($offset)
    {
      return $this->StayAllotmentRec[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param StayAllotmentRec $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      $this->StayAllotmentRec[$offset] = $value;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->StayAllotmentRec[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return StayAllotmentRec Return the current element
     */
    public function current()
    {
      return current($this->StayAllotmentRec);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->StayAllotmentRec);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->StayAllotmentRec);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->StayAllotmentRec);
    }

    /**
     * Countable implementation
     *
     * @return StayAllotmentRec Return count of elements
     */
    public function count()
    {
      return count($this->StayAllotmentRec);
    }

}
