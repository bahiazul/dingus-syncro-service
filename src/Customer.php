<?php

namespace Dingus\SyncroService;

class Customer implements \JsonSerializable
{

    /**
     * @var string $CustomerCode
     */
    protected $CustomerCode = null;

    /**
     * @var string $CustomerName
     */
    protected $CustomerName = null;

    /**
     * @var boolean $DailyRate
     */
    protected $DailyRate = null;

    /**
     * @param boolean $DailyRate
     */
    public function __construct($DailyRate)
    {
      $this->DailyRate = $DailyRate;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'CustomerCode' => $this->getCustomerCode(),
        'CustomerName' => $this->getCustomerName(),
        'DailyRate' => $this->getDailyRate(),
      );
    }

    /**
     * @return string
     */
    public function getCustomerCode()
    {
      return $this->CustomerCode;
    }

    /**
     * @param string $CustomerCode
     * @return \Dingus\SyncroService\Customer
     */
    public function setCustomerCode($CustomerCode)
    {
      $this->CustomerCode = $CustomerCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getCustomerName()
    {
      return $this->CustomerName;
    }

    /**
     * @param string $CustomerName
     * @return \Dingus\SyncroService\Customer
     */
    public function setCustomerName($CustomerName)
    {
      $this->CustomerName = $CustomerName;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getDailyRate()
    {
      return $this->DailyRate;
    }

    /**
     * @param boolean $DailyRate
     * @return \Dingus\SyncroService\Customer
     */
    public function setDailyRate($DailyRate)
    {
      $this->DailyRate = $DailyRate;
      return $this;
    }

}
