<?php

namespace Dingus\SyncroService;

class PutCustomerDailyRateMultiGroupRateRQ implements \JsonSerializable
{

    /**
     * @var string $HotelCode
     */
    protected $HotelCode = null;

    /**
     * @var ArrayOfString $GroupRateCodeList
     */
    protected $GroupRateCodeList = null;

    /**
     * @var ArrayOfPutCustomerDailyRatesRQRec $Recs
     */
    protected $Recs = null;

    
    public function __construct()
    {
    
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'HotelCode' => $this->getHotelCode(),
        'GroupRateCodeList' => $this->getGroupRateCodeList(),
        'Recs' => $this->getRecs(),
      );
    }

    /**
     * @return string
     */
    public function getHotelCode()
    {
      return $this->HotelCode;
    }

    /**
     * @param string $HotelCode
     * @return \Dingus\SyncroService\PutCustomerDailyRateMultiGroupRateRQ
     */
    public function setHotelCode($HotelCode)
    {
      $this->HotelCode = $HotelCode;
      return $this;
    }

    /**
     * @return ArrayOfString
     */
    public function getGroupRateCodeList()
    {
      return $this->GroupRateCodeList;
    }

    /**
     * @param ArrayOfString $GroupRateCodeList
     * @return \Dingus\SyncroService\PutCustomerDailyRateMultiGroupRateRQ
     */
    public function setGroupRateCodeList($GroupRateCodeList)
    {
      $this->GroupRateCodeList = $GroupRateCodeList;
      return $this;
    }

    /**
     * @return ArrayOfPutCustomerDailyRatesRQRec
     */
    public function getRecs()
    {
      return $this->Recs;
    }

    /**
     * @param ArrayOfPutCustomerDailyRatesRQRec $Recs
     * @return \Dingus\SyncroService\PutCustomerDailyRateMultiGroupRateRQ
     */
    public function setRecs($Recs)
    {
      $this->Recs = $Recs;
      return $this;
    }

}
