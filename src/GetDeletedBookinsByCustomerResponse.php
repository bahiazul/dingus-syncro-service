<?php

namespace Dingus\SyncroService;

class GetDeletedBookinsByCustomerResponse implements \JsonSerializable
{

    /**
     * @var ArrayOfGetDeletedBookingsResult $GetDeletedBookinsByCustomerResult
     */
    protected $GetDeletedBookinsByCustomerResult = null;

    /**
     * @param ArrayOfGetDeletedBookingsResult $GetDeletedBookinsByCustomerResult
     */
    public function __construct($GetDeletedBookinsByCustomerResult)
    {
      $this->GetDeletedBookinsByCustomerResult = $GetDeletedBookinsByCustomerResult;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'GetDeletedBookinsByCustomerResult' => $this->getGetDeletedBookinsByCustomerResult(),
      );
    }

    /**
     * @return ArrayOfGetDeletedBookingsResult
     */
    public function getGetDeletedBookinsByCustomerResult()
    {
      return $this->GetDeletedBookinsByCustomerResult;
    }

    /**
     * @param ArrayOfGetDeletedBookingsResult $GetDeletedBookinsByCustomerResult
     * @return \Dingus\SyncroService\GetDeletedBookinsByCustomerResponse
     */
    public function setGetDeletedBookinsByCustomerResult($GetDeletedBookinsByCustomerResult)
    {
      $this->GetDeletedBookinsByCustomerResult = $GetDeletedBookinsByCustomerResult;
      return $this;
    }

}
