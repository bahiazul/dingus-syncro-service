<?php

namespace Dingus\SyncroService;

class MeetingRoom implements \JsonSerializable
{

    /**
     * @var ArrayOfMlText $DescriptionMlList
     */
    protected $DescriptionMlList = null;

    /**
     * @var ArrayOfPicture $Pictures
     */
    protected $Pictures = null;

    /**
     * @var Action $Action
     */
    protected $Action = null;

    /**
     * @var int $Id
     */
    protected $Id = null;

    /**
     * @var string $Name
     */
    protected $Name = null;

    /**
     * @var string $Description
     */
    protected $Description = null;

    /**
     * @var int $Capacity
     */
    protected $Capacity = null;

    /**
     * @var boolean $Computer
     */
    protected $Computer = null;

    /**
     * @var boolean $Video
     */
    protected $Video = null;

    /**
     * @var boolean $AudioVisual
     */
    protected $AudioVisual = null;

    /**
     * @var boolean $Internet
     */
    protected $Internet = null;

    /**
     * @var boolean $Wifi
     */
    protected $Wifi = null;

    /**
     * @var boolean $Scene
     */
    protected $Scene = null;

    /**
     * @var boolean $Fax
     */
    protected $Fax = null;

    /**
     * @var boolean $FlipChart
     */
    protected $FlipChart = null;

    /**
     * @var boolean $AreasLighting
     */
    protected $AreasLighting = null;

    /**
     * @var boolean $Microphone
     */
    protected $Microphone = null;

    /**
     * @var boolean $PanelsMurals
     */
    protected $PanelsMurals = null;

    /**
     * @var boolean $LCD
     */
    protected $LCD = null;

    /**
     * @var boolean $PanelsDesplazables
     */
    protected $PanelsDesplazables = null;

    /**
     * @var boolean $ProjectorLCD
     */
    protected $ProjectorLCD = null;

    /**
     * @var boolean $TransaparenciesProjector
     */
    protected $TransaparenciesProjector = null;

    /**
     * @var boolean $RetroProjector
     */
    protected $RetroProjector = null;

    /**
     * @var boolean $Secretariat
     */
    protected $Secretariat = null;

    /**
     * @var boolean $TelephoneWifi
     */
    protected $TelephoneWifi = null;

    /**
     * @var boolean $Translation
     */
    protected $Translation = null;

    /**
     * @var boolean $TV
     */
    protected $TV = null;

    /**
     * @var boolean $VideoConfers
     */
    protected $VideoConfers = null;

    /**
     * @param Action $Action
     * @param int $Id
     * @param int $Capacity
     * @param boolean $Computer
     * @param boolean $Video
     * @param boolean $AudioVisual
     * @param boolean $Internet
     * @param boolean $Wifi
     * @param boolean $Scene
     * @param boolean $Fax
     * @param boolean $FlipChart
     * @param boolean $AreasLighting
     * @param boolean $Microphone
     * @param boolean $PanelsMurals
     * @param boolean $LCD
     * @param boolean $PanelsDesplazables
     * @param boolean $ProjectorLCD
     * @param boolean $TransaparenciesProjector
     * @param boolean $RetroProjector
     * @param boolean $Secretariat
     * @param boolean $TelephoneWifi
     * @param boolean $Translation
     * @param boolean $TV
     * @param boolean $VideoConfers
     */
    public function __construct($Action, $Id, $Capacity, $Computer, $Video, $AudioVisual, $Internet, $Wifi, $Scene, $Fax, $FlipChart, $AreasLighting, $Microphone, $PanelsMurals, $LCD, $PanelsDesplazables, $ProjectorLCD, $TransaparenciesProjector, $RetroProjector, $Secretariat, $TelephoneWifi, $Translation, $TV, $VideoConfers)
    {
      $this->Action = $Action;
      $this->Id = $Id;
      $this->Capacity = $Capacity;
      $this->Computer = $Computer;
      $this->Video = $Video;
      $this->AudioVisual = $AudioVisual;
      $this->Internet = $Internet;
      $this->Wifi = $Wifi;
      $this->Scene = $Scene;
      $this->Fax = $Fax;
      $this->FlipChart = $FlipChart;
      $this->AreasLighting = $AreasLighting;
      $this->Microphone = $Microphone;
      $this->PanelsMurals = $PanelsMurals;
      $this->LCD = $LCD;
      $this->PanelsDesplazables = $PanelsDesplazables;
      $this->ProjectorLCD = $ProjectorLCD;
      $this->TransaparenciesProjector = $TransaparenciesProjector;
      $this->RetroProjector = $RetroProjector;
      $this->Secretariat = $Secretariat;
      $this->TelephoneWifi = $TelephoneWifi;
      $this->Translation = $Translation;
      $this->TV = $TV;
      $this->VideoConfers = $VideoConfers;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'DescriptionMlList' => $this->getDescriptionMlList(),
        'Pictures' => $this->getPictures(),
        'Action' => $this->getAction(),
        'Id' => $this->getId(),
        'Name' => $this->getName(),
        'Description' => $this->getDescription(),
        'Capacity' => $this->getCapacity(),
        'Computer' => $this->getComputer(),
        'Video' => $this->getVideo(),
        'AudioVisual' => $this->getAudioVisual(),
        'Internet' => $this->getInternet(),
        'Wifi' => $this->getWifi(),
        'Scene' => $this->getScene(),
        'Fax' => $this->getFax(),
        'FlipChart' => $this->getFlipChart(),
        'AreasLighting' => $this->getAreasLighting(),
        'Microphone' => $this->getMicrophone(),
        'PanelsMurals' => $this->getPanelsMurals(),
        'LCD' => $this->getLCD(),
        'PanelsDesplazables' => $this->getPanelsDesplazables(),
        'ProjectorLCD' => $this->getProjectorLCD(),
        'TransaparenciesProjector' => $this->getTransaparenciesProjector(),
        'RetroProjector' => $this->getRetroProjector(),
        'Secretariat' => $this->getSecretariat(),
        'TelephoneWifi' => $this->getTelephoneWifi(),
        'Translation' => $this->getTranslation(),
        'TV' => $this->getTV(),
        'VideoConfers' => $this->getVideoConfers(),
      );
    }

    /**
     * @return ArrayOfMlText
     */
    public function getDescriptionMlList()
    {
      return $this->DescriptionMlList;
    }

    /**
     * @param ArrayOfMlText $DescriptionMlList
     * @return \Dingus\SyncroService\MeetingRoom
     */
    public function setDescriptionMlList($DescriptionMlList)
    {
      $this->DescriptionMlList = $DescriptionMlList;
      return $this;
    }

    /**
     * @return ArrayOfPicture
     */
    public function getPictures()
    {
      return $this->Pictures;
    }

    /**
     * @param ArrayOfPicture $Pictures
     * @return \Dingus\SyncroService\MeetingRoom
     */
    public function setPictures($Pictures)
    {
      $this->Pictures = $Pictures;
      return $this;
    }

    /**
     * @return Action
     */
    public function getAction()
    {
      return $this->Action;
    }

    /**
     * @param Action $Action
     * @return \Dingus\SyncroService\MeetingRoom
     */
    public function setAction($Action)
    {
      $this->Action = $Action;
      return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
      return $this->Id;
    }

    /**
     * @param int $Id
     * @return \Dingus\SyncroService\MeetingRoom
     */
    public function setId($Id)
    {
      $this->Id = $Id;
      return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
      return $this->Name;
    }

    /**
     * @param string $Name
     * @return \Dingus\SyncroService\MeetingRoom
     */
    public function setName($Name)
    {
      $this->Name = $Name;
      return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
      return $this->Description;
    }

    /**
     * @param string $Description
     * @return \Dingus\SyncroService\MeetingRoom
     */
    public function setDescription($Description)
    {
      $this->Description = $Description;
      return $this;
    }

    /**
     * @return int
     */
    public function getCapacity()
    {
      return $this->Capacity;
    }

    /**
     * @param int $Capacity
     * @return \Dingus\SyncroService\MeetingRoom
     */
    public function setCapacity($Capacity)
    {
      $this->Capacity = $Capacity;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getComputer()
    {
      return $this->Computer;
    }

    /**
     * @param boolean $Computer
     * @return \Dingus\SyncroService\MeetingRoom
     */
    public function setComputer($Computer)
    {
      $this->Computer = $Computer;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getVideo()
    {
      return $this->Video;
    }

    /**
     * @param boolean $Video
     * @return \Dingus\SyncroService\MeetingRoom
     */
    public function setVideo($Video)
    {
      $this->Video = $Video;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getAudioVisual()
    {
      return $this->AudioVisual;
    }

    /**
     * @param boolean $AudioVisual
     * @return \Dingus\SyncroService\MeetingRoom
     */
    public function setAudioVisual($AudioVisual)
    {
      $this->AudioVisual = $AudioVisual;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getInternet()
    {
      return $this->Internet;
    }

    /**
     * @param boolean $Internet
     * @return \Dingus\SyncroService\MeetingRoom
     */
    public function setInternet($Internet)
    {
      $this->Internet = $Internet;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getWifi()
    {
      return $this->Wifi;
    }

    /**
     * @param boolean $Wifi
     * @return \Dingus\SyncroService\MeetingRoom
     */
    public function setWifi($Wifi)
    {
      $this->Wifi = $Wifi;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getScene()
    {
      return $this->Scene;
    }

    /**
     * @param boolean $Scene
     * @return \Dingus\SyncroService\MeetingRoom
     */
    public function setScene($Scene)
    {
      $this->Scene = $Scene;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getFax()
    {
      return $this->Fax;
    }

    /**
     * @param boolean $Fax
     * @return \Dingus\SyncroService\MeetingRoom
     */
    public function setFax($Fax)
    {
      $this->Fax = $Fax;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getFlipChart()
    {
      return $this->FlipChart;
    }

    /**
     * @param boolean $FlipChart
     * @return \Dingus\SyncroService\MeetingRoom
     */
    public function setFlipChart($FlipChart)
    {
      $this->FlipChart = $FlipChart;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getAreasLighting()
    {
      return $this->AreasLighting;
    }

    /**
     * @param boolean $AreasLighting
     * @return \Dingus\SyncroService\MeetingRoom
     */
    public function setAreasLighting($AreasLighting)
    {
      $this->AreasLighting = $AreasLighting;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getMicrophone()
    {
      return $this->Microphone;
    }

    /**
     * @param boolean $Microphone
     * @return \Dingus\SyncroService\MeetingRoom
     */
    public function setMicrophone($Microphone)
    {
      $this->Microphone = $Microphone;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getPanelsMurals()
    {
      return $this->PanelsMurals;
    }

    /**
     * @param boolean $PanelsMurals
     * @return \Dingus\SyncroService\MeetingRoom
     */
    public function setPanelsMurals($PanelsMurals)
    {
      $this->PanelsMurals = $PanelsMurals;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getLCD()
    {
      return $this->LCD;
    }

    /**
     * @param boolean $LCD
     * @return \Dingus\SyncroService\MeetingRoom
     */
    public function setLCD($LCD)
    {
      $this->LCD = $LCD;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getPanelsDesplazables()
    {
      return $this->PanelsDesplazables;
    }

    /**
     * @param boolean $PanelsDesplazables
     * @return \Dingus\SyncroService\MeetingRoom
     */
    public function setPanelsDesplazables($PanelsDesplazables)
    {
      $this->PanelsDesplazables = $PanelsDesplazables;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getProjectorLCD()
    {
      return $this->ProjectorLCD;
    }

    /**
     * @param boolean $ProjectorLCD
     * @return \Dingus\SyncroService\MeetingRoom
     */
    public function setProjectorLCD($ProjectorLCD)
    {
      $this->ProjectorLCD = $ProjectorLCD;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getTransaparenciesProjector()
    {
      return $this->TransaparenciesProjector;
    }

    /**
     * @param boolean $TransaparenciesProjector
     * @return \Dingus\SyncroService\MeetingRoom
     */
    public function setTransaparenciesProjector($TransaparenciesProjector)
    {
      $this->TransaparenciesProjector = $TransaparenciesProjector;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getRetroProjector()
    {
      return $this->RetroProjector;
    }

    /**
     * @param boolean $RetroProjector
     * @return \Dingus\SyncroService\MeetingRoom
     */
    public function setRetroProjector($RetroProjector)
    {
      $this->RetroProjector = $RetroProjector;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getSecretariat()
    {
      return $this->Secretariat;
    }

    /**
     * @param boolean $Secretariat
     * @return \Dingus\SyncroService\MeetingRoom
     */
    public function setSecretariat($Secretariat)
    {
      $this->Secretariat = $Secretariat;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getTelephoneWifi()
    {
      return $this->TelephoneWifi;
    }

    /**
     * @param boolean $TelephoneWifi
     * @return \Dingus\SyncroService\MeetingRoom
     */
    public function setTelephoneWifi($TelephoneWifi)
    {
      $this->TelephoneWifi = $TelephoneWifi;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getTranslation()
    {
      return $this->Translation;
    }

    /**
     * @param boolean $Translation
     * @return \Dingus\SyncroService\MeetingRoom
     */
    public function setTranslation($Translation)
    {
      $this->Translation = $Translation;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getTV()
    {
      return $this->TV;
    }

    /**
     * @param boolean $TV
     * @return \Dingus\SyncroService\MeetingRoom
     */
    public function setTV($TV)
    {
      $this->TV = $TV;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getVideoConfers()
    {
      return $this->VideoConfers;
    }

    /**
     * @param boolean $VideoConfers
     * @return \Dingus\SyncroService\MeetingRoom
     */
    public function setVideoConfers($VideoConfers)
    {
      $this->VideoConfers = $VideoConfers;
      return $this;
    }

}
