<?php

namespace Dingus\SyncroService;

class Price implements \JsonSerializable
{

    /**
     * @var int $RateID
     */
    protected $RateID = null;

    /**
     * @var int $RateLineID
     */
    protected $RateLineID = null;

    /**
     * @var float $Amount
     */
    protected $Amount = null;

    /**
     * @var string $Code
     */
    protected $Code = null;

    /**
     * @var \DateTime $Day
     */
    protected $Day = null;

    /**
     * @var int $PaxNo
     */
    protected $PaxNo = null;

    /**
     * @var int $RoomNo
     */
    protected $RoomNo = null;

    /**
     * @var PaxType $PaxType
     */
    protected $PaxType = null;

    /**
     * @var PriceLineType $PriceLineType
     */
    protected $PriceLineType = null;

    /**
     * @var PriceType $PriceType
     */
    protected $PriceType = null;

    /**
     * @var PriceSuplType $SuplType
     */
    protected $SuplType = null;

    /**
     * @var boolean $RoomPrice
     */
    protected $RoomPrice = null;

    /**
     * @var string $RateCode
     */
    protected $RateCode = null;

    /**
     * @var boolean $Guaranteed
     */
    protected $Guaranteed = null;

    /**
     * @param int $RateID
     * @param int $RateLineID
     * @param float $Amount
     * @param \DateTime $Day
     * @param int $PaxNo
     * @param int $RoomNo
     * @param PaxType $PaxType
     * @param PriceLineType $PriceLineType
     * @param PriceType $PriceType
     * @param PriceSuplType $SuplType
     * @param boolean $RoomPrice
     * @param boolean $Guaranteed
     */
    public function __construct($RateID, $RateLineID, $Amount, \DateTime $Day, $PaxNo, $RoomNo, $PaxType, $PriceLineType, $PriceType, $SuplType, $RoomPrice, $Guaranteed)
    {
      $this->RateID = $RateID;
      $this->RateLineID = $RateLineID;
      $this->Amount = $Amount;
      $this->Day = $Day->format(\DateTime::ATOM);
      $this->PaxNo = $PaxNo;
      $this->RoomNo = $RoomNo;
      $this->PaxType = $PaxType;
      $this->PriceLineType = $PriceLineType;
      $this->PriceType = $PriceType;
      $this->SuplType = $SuplType;
      $this->RoomPrice = $RoomPrice;
      $this->Guaranteed = $Guaranteed;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'RateID' => $this->getRateID(),
        'RateLineID' => $this->getRateLineID(),
        'Amount' => $this->getAmount(),
        'Code' => $this->getCode(),
        'Day' => $this->getDay(),
        'PaxNo' => $this->getPaxNo(),
        'RoomNo' => $this->getRoomNo(),
        'PaxType' => $this->getPaxType(),
        'PriceLineType' => $this->getPriceLineType(),
        'PriceType' => $this->getPriceType(),
        'SuplType' => $this->getSuplType(),
        'RoomPrice' => $this->getRoomPrice(),
        'RateCode' => $this->getRateCode(),
        'Guaranteed' => $this->getGuaranteed(),
      );
    }

    /**
     * @return int
     */
    public function getRateID()
    {
      return $this->RateID;
    }

    /**
     * @param int $RateID
     * @return \Dingus\SyncroService\Price
     */
    public function setRateID($RateID)
    {
      $this->RateID = $RateID;
      return $this;
    }

    /**
     * @return int
     */
    public function getRateLineID()
    {
      return $this->RateLineID;
    }

    /**
     * @param int $RateLineID
     * @return \Dingus\SyncroService\Price
     */
    public function setRateLineID($RateLineID)
    {
      $this->RateLineID = $RateLineID;
      return $this;
    }

    /**
     * @return float
     */
    public function getAmount()
    {
      return $this->Amount;
    }

    /**
     * @param float $Amount
     * @return \Dingus\SyncroService\Price
     */
    public function setAmount($Amount)
    {
      $this->Amount = $Amount;
      return $this;
    }

    /**
     * @return string
     */
    public function getCode()
    {
      return $this->Code;
    }

    /**
     * @param string $Code
     * @return \Dingus\SyncroService\Price
     */
    public function setCode($Code)
    {
      $this->Code = $Code;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDay()
    {
      if ($this->Day == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->Day);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $Day
     * @return \Dingus\SyncroService\Price
     */
    public function setDay(\DateTime $Day)
    {
      $this->Day = $Day->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return int
     */
    public function getPaxNo()
    {
      return $this->PaxNo;
    }

    /**
     * @param int $PaxNo
     * @return \Dingus\SyncroService\Price
     */
    public function setPaxNo($PaxNo)
    {
      $this->PaxNo = $PaxNo;
      return $this;
    }

    /**
     * @return int
     */
    public function getRoomNo()
    {
      return $this->RoomNo;
    }

    /**
     * @param int $RoomNo
     * @return \Dingus\SyncroService\Price
     */
    public function setRoomNo($RoomNo)
    {
      $this->RoomNo = $RoomNo;
      return $this;
    }

    /**
     * @return PaxType
     */
    public function getPaxType()
    {
      return $this->PaxType;
    }

    /**
     * @param PaxType $PaxType
     * @return \Dingus\SyncroService\Price
     */
    public function setPaxType($PaxType)
    {
      $this->PaxType = $PaxType;
      return $this;
    }

    /**
     * @return PriceLineType
     */
    public function getPriceLineType()
    {
      return $this->PriceLineType;
    }

    /**
     * @param PriceLineType $PriceLineType
     * @return \Dingus\SyncroService\Price
     */
    public function setPriceLineType($PriceLineType)
    {
      $this->PriceLineType = $PriceLineType;
      return $this;
    }

    /**
     * @return PriceType
     */
    public function getPriceType()
    {
      return $this->PriceType;
    }

    /**
     * @param PriceType $PriceType
     * @return \Dingus\SyncroService\Price
     */
    public function setPriceType($PriceType)
    {
      $this->PriceType = $PriceType;
      return $this;
    }

    /**
     * @return PriceSuplType
     */
    public function getSuplType()
    {
      return $this->SuplType;
    }

    /**
     * @param PriceSuplType $SuplType
     * @return \Dingus\SyncroService\Price
     */
    public function setSuplType($SuplType)
    {
      $this->SuplType = $SuplType;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getRoomPrice()
    {
      return $this->RoomPrice;
    }

    /**
     * @param boolean $RoomPrice
     * @return \Dingus\SyncroService\Price
     */
    public function setRoomPrice($RoomPrice)
    {
      $this->RoomPrice = $RoomPrice;
      return $this;
    }

    /**
     * @return string
     */
    public function getRateCode()
    {
      return $this->RateCode;
    }

    /**
     * @param string $RateCode
     * @return \Dingus\SyncroService\Price
     */
    public function setRateCode($RateCode)
    {
      $this->RateCode = $RateCode;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getGuaranteed()
    {
      return $this->Guaranteed;
    }

    /**
     * @param boolean $Guaranteed
     * @return \Dingus\SyncroService\Price
     */
    public function setGuaranteed($Guaranteed)
    {
      $this->Guaranteed = $Guaranteed;
      return $this;
    }

}
