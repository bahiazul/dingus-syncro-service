<?php

namespace Dingus\SyncroService;

class RateGroupPromotionRec implements \JsonSerializable
{

    /**
     * @var Action $Action
     */
    protected $Action = null;

    /**
     * @var int $IdRateGroupPromotion
     */
    protected $IdRateGroupPromotion = null;

    /**
     * @var string $RateGroupCode
     */
    protected $RateGroupCode = null;

    /**
     * @var string $RateGroupDescription
     */
    protected $RateGroupDescription = null;

    /**
     * @param Action $Action
     * @param int $IdRateGroupPromotion
     */
    public function __construct($Action, $IdRateGroupPromotion)
    {
      $this->Action = $Action;
      $this->IdRateGroupPromotion = $IdRateGroupPromotion;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'Action' => $this->getAction(),
        'IdRateGroupPromotion' => $this->getIdRateGroupPromotion(),
        'RateGroupCode' => $this->getRateGroupCode(),
        'RateGroupDescription' => $this->getRateGroupDescription(),
      );
    }

    /**
     * @return Action
     */
    public function getAction()
    {
      return $this->Action;
    }

    /**
     * @param Action $Action
     * @return \Dingus\SyncroService\RateGroupPromotionRec
     */
    public function setAction($Action)
    {
      $this->Action = $Action;
      return $this;
    }

    /**
     * @return int
     */
    public function getIdRateGroupPromotion()
    {
      return $this->IdRateGroupPromotion;
    }

    /**
     * @param int $IdRateGroupPromotion
     * @return \Dingus\SyncroService\RateGroupPromotionRec
     */
    public function setIdRateGroupPromotion($IdRateGroupPromotion)
    {
      $this->IdRateGroupPromotion = $IdRateGroupPromotion;
      return $this;
    }

    /**
     * @return string
     */
    public function getRateGroupCode()
    {
      return $this->RateGroupCode;
    }

    /**
     * @param string $RateGroupCode
     * @return \Dingus\SyncroService\RateGroupPromotionRec
     */
    public function setRateGroupCode($RateGroupCode)
    {
      $this->RateGroupCode = $RateGroupCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getRateGroupDescription()
    {
      return $this->RateGroupDescription;
    }

    /**
     * @param string $RateGroupDescription
     * @return \Dingus\SyncroService\RateGroupPromotionRec
     */
    public function setRateGroupDescription($RateGroupDescription)
    {
      $this->RateGroupDescription = $RateGroupDescription;
      return $this;
    }

}
