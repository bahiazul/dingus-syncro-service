<?php

namespace Dingus\SyncroService;

class PutTasksResponse implements \JsonSerializable
{

    /**
     * @var SyncroRS $PutTasksResult
     */
    protected $PutTasksResult = null;

    /**
     * @param SyncroRS $PutTasksResult
     */
    public function __construct($PutTasksResult)
    {
      $this->PutTasksResult = $PutTasksResult;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'PutTasksResult' => $this->getPutTasksResult(),
      );
    }

    /**
     * @return SyncroRS
     */
    public function getPutTasksResult()
    {
      return $this->PutTasksResult;
    }

    /**
     * @param SyncroRS $PutTasksResult
     * @return \Dingus\SyncroService\PutTasksResponse
     */
    public function setPutTasksResult($PutTasksResult)
    {
      $this->PutTasksResult = $PutTasksResult;
      return $this;
    }

}
