<?php

namespace Dingus\SyncroService;

class DataTask implements \JsonSerializable
{

    /**
     * @var Action $Action
     */
    protected $Action = null;

    /**
     * @var TipoDatoTarea $DataType
     */
    protected $DataType = null;

    /**
     * @var string $Value
     */
    protected $Value = null;

    /**
     * @param Action $Action
     * @param TipoDatoTarea $DataType
     */
    public function __construct($Action, $DataType)
    {
      $this->Action = $Action;
      $this->DataType = $DataType;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'Action' => $this->getAction(),
        'DataType' => $this->getDataType(),
        'Value' => $this->getValue(),
      );
    }

    /**
     * @return Action
     */
    public function getAction()
    {
      return $this->Action;
    }

    /**
     * @param Action $Action
     * @return \Dingus\SyncroService\DataTask
     */
    public function setAction($Action)
    {
      $this->Action = $Action;
      return $this;
    }

    /**
     * @return TipoDatoTarea
     */
    public function getDataType()
    {
      return $this->DataType;
    }

    /**
     * @param TipoDatoTarea $DataType
     * @return \Dingus\SyncroService\DataTask
     */
    public function setDataType($DataType)
    {
      $this->DataType = $DataType;
      return $this;
    }

    /**
     * @return string
     */
    public function getValue()
    {
      return $this->Value;
    }

    /**
     * @param string $Value
     * @return \Dingus\SyncroService\DataTask
     */
    public function setValue($Value)
    {
      $this->Value = $Value;
      return $this;
    }

}
