<?php

namespace Dingus\SyncroService;

class MarkDeletedBookingsAsNoReceivedResponse implements \JsonSerializable
{

    /**
     * @var SyncroRS $MarkDeletedBookingsAsNoReceivedResult
     */
    protected $MarkDeletedBookingsAsNoReceivedResult = null;

    /**
     * @param SyncroRS $MarkDeletedBookingsAsNoReceivedResult
     */
    public function __construct($MarkDeletedBookingsAsNoReceivedResult)
    {
      $this->MarkDeletedBookingsAsNoReceivedResult = $MarkDeletedBookingsAsNoReceivedResult;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'MarkDeletedBookingsAsNoReceivedResult' => $this->getMarkDeletedBookingsAsNoReceivedResult(),
      );
    }

    /**
     * @return SyncroRS
     */
    public function getMarkDeletedBookingsAsNoReceivedResult()
    {
      return $this->MarkDeletedBookingsAsNoReceivedResult;
    }

    /**
     * @param SyncroRS $MarkDeletedBookingsAsNoReceivedResult
     * @return \Dingus\SyncroService\MarkDeletedBookingsAsNoReceivedResponse
     */
    public function setMarkDeletedBookingsAsNoReceivedResult($MarkDeletedBookingsAsNoReceivedResult)
    {
      $this->MarkDeletedBookingsAsNoReceivedResult = $MarkDeletedBookingsAsNoReceivedResult;
      return $this;
    }

}
