<?php

namespace Dingus\SyncroService;

class ContainedServiceRec implements \JsonSerializable
{

    /**
     * @var ArrayOfMlText $TextContainedServiceMlList
     */
    protected $TextContainedServiceMlList = null;

    /**
     * @var ArrayOfCriterionRec $Criteria
     */
    protected $Criteria = null;

    /**
     * @var Action $Action
     */
    protected $Action = null;

    /**
     * @var int $IdContainedService
     */
    protected $IdContainedService = null;

    /**
     * @var string $TextContainedService
     */
    protected $TextContainedService = null;

    /**
     * @var Picture $Picture
     */
    protected $Picture = null;

    /**
     * @param Action $Action
     * @param int $IdContainedService
     */
    public function __construct($Action, $IdContainedService)
    {
      $this->Action = $Action;
      $this->IdContainedService = $IdContainedService;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'TextContainedServiceMlList' => $this->getTextContainedServiceMlList(),
        'Criteria' => $this->getCriteria(),
        'Action' => $this->getAction(),
        'IdContainedService' => $this->getIdContainedService(),
        'TextContainedService' => $this->getTextContainedService(),
        'Picture' => $this->getPicture(),
      );
    }

    /**
     * @return ArrayOfMlText
     */
    public function getTextContainedServiceMlList()
    {
      return $this->TextContainedServiceMlList;
    }

    /**
     * @param ArrayOfMlText $TextContainedServiceMlList
     * @return \Dingus\SyncroService\ContainedServiceRec
     */
    public function setTextContainedServiceMlList($TextContainedServiceMlList)
    {
      $this->TextContainedServiceMlList = $TextContainedServiceMlList;
      return $this;
    }

    /**
     * @return ArrayOfCriterionRec
     */
    public function getCriteria()
    {
      return $this->Criteria;
    }

    /**
     * @param ArrayOfCriterionRec $Criteria
     * @return \Dingus\SyncroService\ContainedServiceRec
     */
    public function setCriteria($Criteria)
    {
      $this->Criteria = $Criteria;
      return $this;
    }

    /**
     * @return Action
     */
    public function getAction()
    {
      return $this->Action;
    }

    /**
     * @param Action $Action
     * @return \Dingus\SyncroService\ContainedServiceRec
     */
    public function setAction($Action)
    {
      $this->Action = $Action;
      return $this;
    }

    /**
     * @return int
     */
    public function getIdContainedService()
    {
      return $this->IdContainedService;
    }

    /**
     * @param int $IdContainedService
     * @return \Dingus\SyncroService\ContainedServiceRec
     */
    public function setIdContainedService($IdContainedService)
    {
      $this->IdContainedService = $IdContainedService;
      return $this;
    }

    /**
     * @return string
     */
    public function getTextContainedService()
    {
      return $this->TextContainedService;
    }

    /**
     * @param string $TextContainedService
     * @return \Dingus\SyncroService\ContainedServiceRec
     */
    public function setTextContainedService($TextContainedService)
    {
      $this->TextContainedService = $TextContainedService;
      return $this;
    }

    /**
     * @return Picture
     */
    public function getPicture()
    {
      return $this->Picture;
    }

    /**
     * @param Picture $Picture
     * @return \Dingus\SyncroService\ContainedServiceRec
     */
    public function setPicture($Picture)
    {
      $this->Picture = $Picture;
      return $this;
    }

}
