<?php

namespace Dingus\SyncroService;

class TipoTarea
{
    const __default = 'Personalizada';
    const Personalizada = 'Personalizada';
    const SubirCupos = 'SubirCupos';
    const BajarReservas = 'BajarReservas';
    const ProcesarBufferReservas = 'ProcesarBufferReservas';
    const ProcesarBufferCupos = 'ProcesarBufferCupos';
    const BajarReservasPersonalizada = 'BajarReservasPersonalizada';
    const SubirCuposPersonalizada = 'SubirCuposPersonalizada';
    const All = 'All';
    const ConfirmarBuffersPendientes = 'ConfirmarBuffersPendientes';


}
