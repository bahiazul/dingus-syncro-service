<?php

namespace Dingus\SyncroService;

class PutRatesRQ implements \JsonSerializable
{

    /**
     * @var ArrayOfRoomRateRec $RoomPrices
     */
    protected $RoomPrices = null;

    /**
     * @var ArrayOfBoardRateRec $BoardPrices
     */
    protected $BoardPrices = null;

    /**
     * @var ArrayOfSuplementRateRec $SuplementPrices
     */
    protected $SuplementPrices = null;

    /**
     * @var ArrayOfCriterion $Criteria
     */
    protected $Criteria = null;

    /**
     * @var ArrayOfCustomer $CustomerList
     */
    protected $CustomerList = null;

    /**
     * @var string $RateCode
     */
    protected $RateCode = null;

    /**
     * @var string $RateName
     */
    protected $RateName = null;

    /**
     * @var string $CustomerRatesGroupCode
     */
    protected $CustomerRatesGroupCode = null;

    /**
     * @var string $CustomerRatesGroupName
     */
    protected $CustomerRatesGroupName = null;

    /**
     * @var ArrayOfExtraRateRec $ExtraPrices
     */
    protected $ExtraPrices = null;

    /**
     * @var string $HotelCode
     */
    protected $HotelCode = null;

    /**
     * @var RateType $RateType
     */
    protected $RateType = null;

    /**
     * @var boolean $Active
     */
    protected $Active = null;

    /**
     * @var boolean $ByDefault
     */
    protected $ByDefault = null;

    /**
     * @var string $CurrencyCode
     */
    protected $CurrencyCode = null;

    /**
     * @var string $CurrencyName
     */
    protected $CurrencyName = null;

    /**
     * @var RateParentInfo $rateParentInfo
     */
    protected $rateParentInfo = null;

    /**
     * @param RateType $RateType
     * @param boolean $Active
     * @param boolean $ByDefault
     */
    public function __construct($RateType, $Active, $ByDefault)
    {
      $this->RateType = $RateType;
      $this->Active = $Active;
      $this->ByDefault = $ByDefault;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'RoomPrices' => $this->getRoomPrices(),
        'BoardPrices' => $this->getBoardPrices(),
        'SuplementPrices' => $this->getSuplementPrices(),
        'Criteria' => $this->getCriteria(),
        'CustomerList' => $this->getCustomerList(),
        'RateCode' => $this->getRateCode(),
        'RateName' => $this->getRateName(),
        'CustomerRatesGroupCode' => $this->getCustomerRatesGroupCode(),
        'CustomerRatesGroupName' => $this->getCustomerRatesGroupName(),
        'ExtraPrices' => $this->getExtraPrices(),
        'HotelCode' => $this->getHotelCode(),
        'RateType' => $this->getRateType(),
        'Active' => $this->getActive(),
        'ByDefault' => $this->getByDefault(),
        'CurrencyCode' => $this->getCurrencyCode(),
        'CurrencyName' => $this->getCurrencyName(),
        'rateParentInfo' => $this->getRateParentInfo(),
      );
    }

    /**
     * @return ArrayOfRoomRateRec
     */
    public function getRoomPrices()
    {
      return $this->RoomPrices;
    }

    /**
     * @param ArrayOfRoomRateRec $RoomPrices
     * @return \Dingus\SyncroService\PutRatesRQ
     */
    public function setRoomPrices($RoomPrices)
    {
      $this->RoomPrices = $RoomPrices;
      return $this;
    }

    /**
     * @return ArrayOfBoardRateRec
     */
    public function getBoardPrices()
    {
      return $this->BoardPrices;
    }

    /**
     * @param ArrayOfBoardRateRec $BoardPrices
     * @return \Dingus\SyncroService\PutRatesRQ
     */
    public function setBoardPrices($BoardPrices)
    {
      $this->BoardPrices = $BoardPrices;
      return $this;
    }

    /**
     * @return ArrayOfSuplementRateRec
     */
    public function getSuplementPrices()
    {
      return $this->SuplementPrices;
    }

    /**
     * @param ArrayOfSuplementRateRec $SuplementPrices
     * @return \Dingus\SyncroService\PutRatesRQ
     */
    public function setSuplementPrices($SuplementPrices)
    {
      $this->SuplementPrices = $SuplementPrices;
      return $this;
    }

    /**
     * @return ArrayOfCriterion
     */
    public function getCriteria()
    {
      return $this->Criteria;
    }

    /**
     * @param ArrayOfCriterion $Criteria
     * @return \Dingus\SyncroService\PutRatesRQ
     */
    public function setCriteria($Criteria)
    {
      $this->Criteria = $Criteria;
      return $this;
    }

    /**
     * @return ArrayOfCustomer
     */
    public function getCustomerList()
    {
      return $this->CustomerList;
    }

    /**
     * @param ArrayOfCustomer $CustomerList
     * @return \Dingus\SyncroService\PutRatesRQ
     */
    public function setCustomerList($CustomerList)
    {
      $this->CustomerList = $CustomerList;
      return $this;
    }

    /**
     * @return string
     */
    public function getRateCode()
    {
      return $this->RateCode;
    }

    /**
     * @param string $RateCode
     * @return \Dingus\SyncroService\PutRatesRQ
     */
    public function setRateCode($RateCode)
    {
      $this->RateCode = $RateCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getRateName()
    {
      return $this->RateName;
    }

    /**
     * @param string $RateName
     * @return \Dingus\SyncroService\PutRatesRQ
     */
    public function setRateName($RateName)
    {
      $this->RateName = $RateName;
      return $this;
    }

    /**
     * @return string
     */
    public function getCustomerRatesGroupCode()
    {
      return $this->CustomerRatesGroupCode;
    }

    /**
     * @param string $CustomerRatesGroupCode
     * @return \Dingus\SyncroService\PutRatesRQ
     */
    public function setCustomerRatesGroupCode($CustomerRatesGroupCode)
    {
      $this->CustomerRatesGroupCode = $CustomerRatesGroupCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getCustomerRatesGroupName()
    {
      return $this->CustomerRatesGroupName;
    }

    /**
     * @param string $CustomerRatesGroupName
     * @return \Dingus\SyncroService\PutRatesRQ
     */
    public function setCustomerRatesGroupName($CustomerRatesGroupName)
    {
      $this->CustomerRatesGroupName = $CustomerRatesGroupName;
      return $this;
    }

    /**
     * @return ArrayOfExtraRateRec
     */
    public function getExtraPrices()
    {
      return $this->ExtraPrices;
    }

    /**
     * @param ArrayOfExtraRateRec $ExtraPrices
     * @return \Dingus\SyncroService\PutRatesRQ
     */
    public function setExtraPrices($ExtraPrices)
    {
      $this->ExtraPrices = $ExtraPrices;
      return $this;
    }

    /**
     * @return string
     */
    public function getHotelCode()
    {
      return $this->HotelCode;
    }

    /**
     * @param string $HotelCode
     * @return \Dingus\SyncroService\PutRatesRQ
     */
    public function setHotelCode($HotelCode)
    {
      $this->HotelCode = $HotelCode;
      return $this;
    }

    /**
     * @return RateType
     */
    public function getRateType()
    {
      return $this->RateType;
    }

    /**
     * @param RateType $RateType
     * @return \Dingus\SyncroService\PutRatesRQ
     */
    public function setRateType($RateType)
    {
      $this->RateType = $RateType;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getActive()
    {
      return $this->Active;
    }

    /**
     * @param boolean $Active
     * @return \Dingus\SyncroService\PutRatesRQ
     */
    public function setActive($Active)
    {
      $this->Active = $Active;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getByDefault()
    {
      return $this->ByDefault;
    }

    /**
     * @param boolean $ByDefault
     * @return \Dingus\SyncroService\PutRatesRQ
     */
    public function setByDefault($ByDefault)
    {
      $this->ByDefault = $ByDefault;
      return $this;
    }

    /**
     * @return string
     */
    public function getCurrencyCode()
    {
      return $this->CurrencyCode;
    }

    /**
     * @param string $CurrencyCode
     * @return \Dingus\SyncroService\PutRatesRQ
     */
    public function setCurrencyCode($CurrencyCode)
    {
      $this->CurrencyCode = $CurrencyCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getCurrencyName()
    {
      return $this->CurrencyName;
    }

    /**
     * @param string $CurrencyName
     * @return \Dingus\SyncroService\PutRatesRQ
     */
    public function setCurrencyName($CurrencyName)
    {
      $this->CurrencyName = $CurrencyName;
      return $this;
    }

    /**
     * @return RateParentInfo
     */
    public function getRateParentInfo()
    {
      return $this->rateParentInfo;
    }

    /**
     * @param RateParentInfo $rateParentInfo
     * @return \Dingus\SyncroService\PutRatesRQ
     */
    public function setRateParentInfo($rateParentInfo)
    {
      $this->rateParentInfo = $rateParentInfo;
      return $this;
    }

}
