<?php

namespace Dingus\SyncroService;

class PutCustomerDailyRatesByHotelListRQ implements \JsonSerializable
{

    /**
     * @var ArrayOfString $HotelCodeList
     */
    protected $HotelCodeList = null;

    /**
     * @var string $GroupRateCode
     */
    protected $GroupRateCode = null;

    /**
     * @var ArrayOfPutCustomerDailyRatesRQRec $Recs
     */
    protected $Recs = null;

    
    public function __construct()
    {
    
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'HotelCodeList' => $this->getHotelCodeList(),
        'GroupRateCode' => $this->getGroupRateCode(),
        'Recs' => $this->getRecs(),
      );
    }

    /**
     * @return ArrayOfString
     */
    public function getHotelCodeList()
    {
      return $this->HotelCodeList;
    }

    /**
     * @param ArrayOfString $HotelCodeList
     * @return \Dingus\SyncroService\PutCustomerDailyRatesByHotelListRQ
     */
    public function setHotelCodeList($HotelCodeList)
    {
      $this->HotelCodeList = $HotelCodeList;
      return $this;
    }

    /**
     * @return string
     */
    public function getGroupRateCode()
    {
      return $this->GroupRateCode;
    }

    /**
     * @param string $GroupRateCode
     * @return \Dingus\SyncroService\PutCustomerDailyRatesByHotelListRQ
     */
    public function setGroupRateCode($GroupRateCode)
    {
      $this->GroupRateCode = $GroupRateCode;
      return $this;
    }

    /**
     * @return ArrayOfPutCustomerDailyRatesRQRec
     */
    public function getRecs()
    {
      return $this->Recs;
    }

    /**
     * @param ArrayOfPutCustomerDailyRatesRQRec $Recs
     * @return \Dingus\SyncroService\PutCustomerDailyRatesByHotelListRQ
     */
    public function setRecs($Recs)
    {
      $this->Recs = $Recs;
      return $this;
    }

}
