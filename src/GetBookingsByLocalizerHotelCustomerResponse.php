<?php

namespace Dingus\SyncroService;

class GetBookingsByLocalizerHotelCustomerResponse implements \JsonSerializable
{

    /**
     * @var GetBookinsRS $GetBookingsByLocalizerHotelCustomerResult
     */
    protected $GetBookingsByLocalizerHotelCustomerResult = null;

    /**
     * @param GetBookinsRS $GetBookingsByLocalizerHotelCustomerResult
     */
    public function __construct($GetBookingsByLocalizerHotelCustomerResult)
    {
      $this->GetBookingsByLocalizerHotelCustomerResult = $GetBookingsByLocalizerHotelCustomerResult;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'GetBookingsByLocalizerHotelCustomerResult' => $this->getGetBookingsByLocalizerHotelCustomerResult(),
      );
    }

    /**
     * @return GetBookinsRS
     */
    public function getGetBookingsByLocalizerHotelCustomerResult()
    {
      return $this->GetBookingsByLocalizerHotelCustomerResult;
    }

    /**
     * @param GetBookinsRS $GetBookingsByLocalizerHotelCustomerResult
     * @return \Dingus\SyncroService\GetBookingsByLocalizerHotelCustomerResponse
     */
    public function setGetBookingsByLocalizerHotelCustomerResult($GetBookingsByLocalizerHotelCustomerResult)
    {
      $this->GetBookingsByLocalizerHotelCustomerResult = $GetBookingsByLocalizerHotelCustomerResult;
      return $this;
    }

}
