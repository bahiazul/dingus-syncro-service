<?php

namespace Dingus\SyncroService;

class ArrayOfBookingRS implements \JsonSerializable, \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var BookingRS[] $BookingRS
     */
    protected $BookingRS = null;

    
    public function __construct()
    {
    
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'BookingRS' => $this->getBookingRS(),
      );
    }

    /**
     * @return BookingRS[]
     */
    public function getBookingRS()
    {
      return $this->BookingRS;
    }

    /**
     * @param BookingRS[] $BookingRS
     * @return \Dingus\SyncroService\ArrayOfBookingRS
     */
    public function setBookingRS(array $BookingRS = null)
    {
      $this->BookingRS = $BookingRS;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->BookingRS[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return BookingRS
     */
    public function offsetGet($offset)
    {
      return $this->BookingRS[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param BookingRS $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      $this->BookingRS[$offset] = $value;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->BookingRS[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return BookingRS Return the current element
     */
    public function current()
    {
      return current($this->BookingRS);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->BookingRS);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->BookingRS);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->BookingRS);
    }

    /**
     * Countable implementation
     *
     * @return BookingRS Return count of elements
     */
    public function count()
    {
      return count($this->BookingRS);
    }

}
