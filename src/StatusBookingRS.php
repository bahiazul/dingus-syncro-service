<?php

namespace Dingus\SyncroService;

class StatusBookingRS
{
    const __default = 'aNew';
    const aNew = 'New';
    const Modify = 'Modify';
    const Cancel = 'Cancel';
    const Rejected = 'Rejected';
    const Waiting = 'Waiting';
    const PendingUpdate = 'PendingUpdate';
    const PendingCancel = 'PendingCancel';


}
