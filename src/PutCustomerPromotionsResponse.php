<?php

namespace Dingus\SyncroService;

class PutCustomerPromotionsResponse implements \JsonSerializable
{

    /**
     * @var SyncroRS $PutCustomerPromotionsResult
     */
    protected $PutCustomerPromotionsResult = null;

    /**
     * @param SyncroRS $PutCustomerPromotionsResult
     */
    public function __construct($PutCustomerPromotionsResult)
    {
      $this->PutCustomerPromotionsResult = $PutCustomerPromotionsResult;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'PutCustomerPromotionsResult' => $this->getPutCustomerPromotionsResult(),
      );
    }

    /**
     * @return SyncroRS
     */
    public function getPutCustomerPromotionsResult()
    {
      return $this->PutCustomerPromotionsResult;
    }

    /**
     * @param SyncroRS $PutCustomerPromotionsResult
     * @return \Dingus\SyncroService\PutCustomerPromotionsResponse
     */
    public function setPutCustomerPromotionsResult($PutCustomerPromotionsResult)
    {
      $this->PutCustomerPromotionsResult = $PutCustomerPromotionsResult;
      return $this;
    }

}
