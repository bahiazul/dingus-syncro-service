<?php

namespace Dingus\SyncroService;

class MarkDeletedBookingsAsNoReceived2Response implements \JsonSerializable
{

    /**
     * @var SyncroRS $MarkDeletedBookingsAsNoReceived2Result
     */
    protected $MarkDeletedBookingsAsNoReceived2Result = null;

    /**
     * @param SyncroRS $MarkDeletedBookingsAsNoReceived2Result
     */
    public function __construct($MarkDeletedBookingsAsNoReceived2Result)
    {
      $this->MarkDeletedBookingsAsNoReceived2Result = $MarkDeletedBookingsAsNoReceived2Result;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'MarkDeletedBookingsAsNoReceived2Result' => $this->getMarkDeletedBookingsAsNoReceived2Result(),
      );
    }

    /**
     * @return SyncroRS
     */
    public function getMarkDeletedBookingsAsNoReceived2Result()
    {
      return $this->MarkDeletedBookingsAsNoReceived2Result;
    }

    /**
     * @param SyncroRS $MarkDeletedBookingsAsNoReceived2Result
     * @return \Dingus\SyncroService\MarkDeletedBookingsAsNoReceived2Response
     */
    public function setMarkDeletedBookingsAsNoReceived2Result($MarkDeletedBookingsAsNoReceived2Result)
    {
      $this->MarkDeletedBookingsAsNoReceived2Result = $MarkDeletedBookingsAsNoReceived2Result;
      return $this;
    }

}
