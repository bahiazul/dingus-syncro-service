<?php

namespace Dingus\SyncroService;

class PutHotelResponse implements \JsonSerializable
{

    /**
     * @var SyncroRS $PutHotelResult
     */
    protected $PutHotelResult = null;

    /**
     * @param SyncroRS $PutHotelResult
     */
    public function __construct($PutHotelResult)
    {
      $this->PutHotelResult = $PutHotelResult;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'PutHotelResult' => $this->getPutHotelResult(),
      );
    }

    /**
     * @return SyncroRS
     */
    public function getPutHotelResult()
    {
      return $this->PutHotelResult;
    }

    /**
     * @param SyncroRS $PutHotelResult
     * @return \Dingus\SyncroService\PutHotelResponse
     */
    public function setPutHotelResult($PutHotelResult)
    {
      $this->PutHotelResult = $PutHotelResult;
      return $this;
    }

}
