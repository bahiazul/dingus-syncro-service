<?php

namespace Dingus\SyncroService;

class PutHotelGroupsByHotelListResponse implements \JsonSerializable
{

    /**
     * @var SyncroRS $PutHotelGroupsByHotelListResult
     */
    protected $PutHotelGroupsByHotelListResult = null;

    /**
     * @param SyncroRS $PutHotelGroupsByHotelListResult
     */
    public function __construct($PutHotelGroupsByHotelListResult)
    {
      $this->PutHotelGroupsByHotelListResult = $PutHotelGroupsByHotelListResult;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'PutHotelGroupsByHotelListResult' => $this->getPutHotelGroupsByHotelListResult(),
      );
    }

    /**
     * @return SyncroRS
     */
    public function getPutHotelGroupsByHotelListResult()
    {
      return $this->PutHotelGroupsByHotelListResult;
    }

    /**
     * @param SyncroRS $PutHotelGroupsByHotelListResult
     * @return \Dingus\SyncroService\PutHotelGroupsByHotelListResponse
     */
    public function setPutHotelGroupsByHotelListResult($PutHotelGroupsByHotelListResult)
    {
      $this->PutHotelGroupsByHotelListResult = $PutHotelGroupsByHotelListResult;
      return $this;
    }

}
