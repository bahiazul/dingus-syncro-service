<?php

namespace Dingus\SyncroService;

class PutDetailsHotel implements \JsonSerializable
{

    /**
     * @var Credentials $Credentials
     */
    protected $Credentials = null;

    /**
     * @var PutHotelRQ $PutHotelRQ
     */
    protected $PutHotelRQ = null;

    /**
     * @param Credentials $Credentials
     * @param PutHotelRQ $PutHotelRQ
     */
    public function __construct($Credentials, $PutHotelRQ)
    {
      $this->Credentials = $Credentials;
      $this->PutHotelRQ = $PutHotelRQ;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'Credentials' => $this->getCredentials(),
        'PutHotelRQ' => $this->getPutHotelRQ(),
      );
    }

    /**
     * @return Credentials
     */
    public function getCredentials()
    {
      return $this->Credentials;
    }

    /**
     * @param Credentials $Credentials
     * @return \Dingus\SyncroService\PutDetailsHotel
     */
    public function setCredentials($Credentials)
    {
      $this->Credentials = $Credentials;
      return $this;
    }

    /**
     * @return PutHotelRQ
     */
    public function getPutHotelRQ()
    {
      return $this->PutHotelRQ;
    }

    /**
     * @param PutHotelRQ $PutHotelRQ
     * @return \Dingus\SyncroService\PutDetailsHotel
     */
    public function setPutHotelRQ($PutHotelRQ)
    {
      $this->PutHotelRQ = $PutHotelRQ;
      return $this;
    }

}
