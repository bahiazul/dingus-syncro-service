<?php

namespace Dingus\SyncroService;

class ArrayOfHotelsTask implements \JsonSerializable, \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var HotelsTask[] $HotelsTask
     */
    protected $HotelsTask = null;

    
    public function __construct()
    {
    
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'HotelsTask' => $this->getHotelsTask(),
      );
    }

    /**
     * @return HotelsTask[]
     */
    public function getHotelsTask()
    {
      return $this->HotelsTask;
    }

    /**
     * @param HotelsTask[] $HotelsTask
     * @return \Dingus\SyncroService\ArrayOfHotelsTask
     */
    public function setHotelsTask(array $HotelsTask = null)
    {
      $this->HotelsTask = $HotelsTask;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->HotelsTask[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return HotelsTask
     */
    public function offsetGet($offset)
    {
      return $this->HotelsTask[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param HotelsTask $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      $this->HotelsTask[$offset] = $value;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->HotelsTask[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return HotelsTask Return the current element
     */
    public function current()
    {
      return current($this->HotelsTask);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->HotelsTask);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->HotelsTask);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->HotelsTask);
    }

    /**
     * Countable implementation
     *
     * @return HotelsTask Return count of elements
     */
    public function count()
    {
      return count($this->HotelsTask);
    }

}
