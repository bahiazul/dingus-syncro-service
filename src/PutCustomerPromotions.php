<?php

namespace Dingus\SyncroService;

class PutCustomerPromotions implements \JsonSerializable
{

    /**
     * @var Credentials $Credentials
     */
    protected $Credentials = null;

    /**
     * @var PutCustomerPromotionsRQ $PutCustomerPromotionsRQ
     */
    protected $PutCustomerPromotionsRQ = null;

    /**
     * @param Credentials $Credentials
     * @param PutCustomerPromotionsRQ $PutCustomerPromotionsRQ
     */
    public function __construct($Credentials, $PutCustomerPromotionsRQ)
    {
      $this->Credentials = $Credentials;
      $this->PutCustomerPromotionsRQ = $PutCustomerPromotionsRQ;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'Credentials' => $this->getCredentials(),
        'PutCustomerPromotionsRQ' => $this->getPutCustomerPromotionsRQ(),
      );
    }

    /**
     * @return Credentials
     */
    public function getCredentials()
    {
      return $this->Credentials;
    }

    /**
     * @param Credentials $Credentials
     * @return \Dingus\SyncroService\PutCustomerPromotions
     */
    public function setCredentials($Credentials)
    {
      $this->Credentials = $Credentials;
      return $this;
    }

    /**
     * @return PutCustomerPromotionsRQ
     */
    public function getPutCustomerPromotionsRQ()
    {
      return $this->PutCustomerPromotionsRQ;
    }

    /**
     * @param PutCustomerPromotionsRQ $PutCustomerPromotionsRQ
     * @return \Dingus\SyncroService\PutCustomerPromotions
     */
    public function setPutCustomerPromotionsRQ($PutCustomerPromotionsRQ)
    {
      $this->PutCustomerPromotionsRQ = $PutCustomerPromotionsRQ;
      return $this;
    }

}
