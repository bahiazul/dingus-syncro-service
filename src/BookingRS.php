<?php

namespace Dingus\SyncroService;

class BookingRS implements \JsonSerializable
{

    /**
     * @var int $SequenceID
     */
    protected $SequenceID = null;

    /**
     * @var string $AllotmentBlockCode
     */
    protected $AllotmentBlockCode = null;

    /**
     * @var StatusBookingRS $Status
     */
    protected $Status = null;

    /**
     * @var boolean $RQ
     */
    protected $RQ = null;

    /**
     * @var string $BoardCode
     */
    protected $BoardCode = null;

    /**
     * @var string $BoardName
     */
    protected $BoardName = null;

    /**
     * @var string $ChainCode
     */
    protected $ChainCode = null;

    /**
     * @var string $ChainName
     */
    protected $ChainName = null;

    /**
     * @var ArrayOfCriterion $Criteria
     */
    protected $Criteria = null;

    /**
     * @var string $CustomerCode
     */
    protected $CustomerCode = null;

    /**
     * @var string $CustomerName
     */
    protected $CustomerName = null;

    /**
     * @var \DateTime $DateFrom
     */
    protected $DateFrom = null;

    /**
     * @var \DateTime $DateTo
     */
    protected $DateTo = null;

    /**
     * @var \DateTime $DateSale
     */
    protected $DateSale = null;

    /**
     * @var boolean $Error
     */
    protected $Error = null;

    /**
     * @var ArrayOfError $Errors
     */
    protected $Errors = null;

    /**
     * @var ArrayOfExtra $Extras
     */
    protected $Extras = null;

    /**
     * @var ArrayOfData $BookingData
     */
    protected $BookingData = null;

    /**
     * @var ArrayOfPayment $Payments
     */
    protected $Payments = null;

    /**
     * @var ArrayOfCharge $Charges
     */
    protected $Charges = null;

    /**
     * @var ArrayOfStay $Stays
     */
    protected $Stays = null;

    /**
     * @var ComissionRS $ComissionInfo
     */
    protected $ComissionInfo = null;

    /**
     * @var string $HotelCode
     */
    protected $HotelCode = null;

    /**
     * @var string $HotelName
     */
    protected $HotelName = null;

    /**
     * @var string $Localizer
     */
    protected $Localizer = null;

    /**
     * @var boolean $Send
     */
    protected $Send = null;

    /**
     * @var boolean $SendPMS
     */
    protected $SendPMS = null;

    /**
     * @var boolean $ErrorPMS
     */
    protected $ErrorPMS = null;

    /**
     * @var string $ErrorPMSText
     */
    protected $ErrorPMSText = null;

    /**
     * @var boolean $OfferApplied
     */
    protected $OfferApplied = null;

    /**
     * @var boolean $BookingOutOfAllotment
     */
    protected $BookingOutOfAllotment = null;

    /**
     * @var ArrayOfPax $Paxes
     */
    protected $Paxes = null;

    /**
     * @var ArrayOfPrice $PriceDetails
     */
    protected $PriceDetails = null;

    /**
     * @var ArrayOfPrice $ContractPriceDetails
     */
    protected $ContractPriceDetails = null;

    /**
     * @var string $ResRef
     */
    protected $ResRef = null;

    /**
     * @var string $RoomCode
     */
    protected $RoomCode = null;

    /**
     * @var string $RoomName
     */
    protected $RoomName = null;

    /**
     * @var float $TotalPrice
     */
    protected $TotalPrice = null;

    /**
     * @var string $VendorCode
     */
    protected $VendorCode = null;

    /**
     * @var string $VendorName
     */
    protected $VendorName = null;

    /**
     * @var string $SalesRoomCode
     */
    protected $SalesRoomCode = null;

    /**
     * @var string $RoomReference
     */
    protected $RoomReference = null;

    /**
     * @var string $CurrencyCode
     */
    protected $CurrencyCode = null;

    /**
     * @var string $CustomerAllotmentGroup
     */
    protected $CustomerAllotmentGroup = null;

    /**
     * @var string $CustomerRateGroup
     */
    protected $CustomerRateGroup = null;

    /**
     * @var string $PromotionCode
     */
    protected $PromotionCode = null;

    /**
     * @var string $PromotionDescription
     */
    protected $PromotionDescription = null;

    /**
     * @var string $PromotionCodeDiscount
     */
    protected $PromotionCodeDiscount = null;

    /**
     * @var boolean $BookingInCheckOut
     */
    protected $BookingInCheckOut = null;

    /**
     * @var string $PMSCode
     */
    protected $PMSCode = null;

    /**
     * @var string $InsertUser
     */
    protected $InsertUser = null;

    /**
     * @var \DateTime $InserDateTime
     */
    protected $InserDateTime = null;

    /**
     * @var string $ModifyUser
     */
    protected $ModifyUser = null;

    /**
     * @var \DateTime $ModifyDateTime
     */
    protected $ModifyDateTime = null;

    /**
     * @var ArrayOfCancellationInfo $CancellationInfoCollection
     */
    protected $CancellationInfoCollection = null;

    /**
     * @param int $SequenceID
     * @param StatusBookingRS $Status
     * @param boolean $RQ
     * @param \DateTime $DateFrom
     * @param \DateTime $DateTo
     * @param \DateTime $DateSale
     * @param boolean $Error
     * @param boolean $Send
     * @param boolean $SendPMS
     * @param boolean $ErrorPMS
     * @param boolean $OfferApplied
     * @param boolean $BookingOutOfAllotment
     * @param float $TotalPrice
     * @param boolean $BookingInCheckOut
     * @param \DateTime $InserDateTime
     * @param \DateTime $ModifyDateTime
     */
    public function __construct($SequenceID, $Status, $RQ, \DateTime $DateFrom, \DateTime $DateTo, \DateTime $DateSale, $Error, $Send, $SendPMS, $ErrorPMS, $OfferApplied, $BookingOutOfAllotment, $TotalPrice, $BookingInCheckOut, \DateTime $InserDateTime, \DateTime $ModifyDateTime)
    {
      $this->SequenceID = $SequenceID;
      $this->Status = $Status;
      $this->RQ = $RQ;
      $this->DateFrom = $DateFrom->format(\DateTime::ATOM);
      $this->DateTo = $DateTo->format(\DateTime::ATOM);
      $this->DateSale = $DateSale->format(\DateTime::ATOM);
      $this->Error = $Error;
      $this->Send = $Send;
      $this->SendPMS = $SendPMS;
      $this->ErrorPMS = $ErrorPMS;
      $this->OfferApplied = $OfferApplied;
      $this->BookingOutOfAllotment = $BookingOutOfAllotment;
      $this->TotalPrice = $TotalPrice;
      $this->BookingInCheckOut = $BookingInCheckOut;
      $this->InserDateTime = $InserDateTime->format(\DateTime::ATOM);
      $this->ModifyDateTime = $ModifyDateTime->format(\DateTime::ATOM);
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'SequenceID' => $this->getSequenceID(),
        'AllotmentBlockCode' => $this->getAllotmentBlockCode(),
        'Status' => $this->getStatus(),
        'RQ' => $this->getRQ(),
        'BoardCode' => $this->getBoardCode(),
        'BoardName' => $this->getBoardName(),
        'ChainCode' => $this->getChainCode(),
        'ChainName' => $this->getChainName(),
        'Criteria' => $this->getCriteria(),
        'CustomerCode' => $this->getCustomerCode(),
        'CustomerName' => $this->getCustomerName(),
        'DateFrom' => $this->getDateFrom(),
        'DateTo' => $this->getDateTo(),
        'DateSale' => $this->getDateSale(),
        'Error' => $this->getError(),
        'Errors' => $this->getErrors(),
        'Extras' => $this->getExtras(),
        'BookingData' => $this->getBookingData(),
        'Payments' => $this->getPayments(),
        'Charges' => $this->getCharges(),
        'Stays' => $this->getStays(),
        'ComissionInfo' => $this->getComissionInfo(),
        'HotelCode' => $this->getHotelCode(),
        'HotelName' => $this->getHotelName(),
        'Localizer' => $this->getLocalizer(),
        'Send' => $this->getSend(),
        'SendPMS' => $this->getSendPMS(),
        'ErrorPMS' => $this->getErrorPMS(),
        'ErrorPMSText' => $this->getErrorPMSText(),
        'OfferApplied' => $this->getOfferApplied(),
        'BookingOutOfAllotment' => $this->getBookingOutOfAllotment(),
        'Paxes' => $this->getPaxes(),
        'PriceDetails' => $this->getPriceDetails(),
        'ContractPriceDetails' => $this->getContractPriceDetails(),
        'ResRef' => $this->getResRef(),
        'RoomCode' => $this->getRoomCode(),
        'RoomName' => $this->getRoomName(),
        'TotalPrice' => $this->getTotalPrice(),
        'VendorCode' => $this->getVendorCode(),
        'VendorName' => $this->getVendorName(),
        'SalesRoomCode' => $this->getSalesRoomCode(),
        'RoomReference' => $this->getRoomReference(),
        'CurrencyCode' => $this->getCurrencyCode(),
        'CustomerAllotmentGroup' => $this->getCustomerAllotmentGroup(),
        'CustomerRateGroup' => $this->getCustomerRateGroup(),
        'PromotionCode' => $this->getPromotionCode(),
        'PromotionDescription' => $this->getPromotionDescription(),
        'PromotionCodeDiscount' => $this->getPromotionCodeDiscount(),
        'BookingInCheckOut' => $this->getBookingInCheckOut(),
        'PMSCode' => $this->getPMSCode(),
        'InsertUser' => $this->getInsertUser(),
        'InserDateTime' => $this->getInserDateTime(),
        'ModifyUser' => $this->getModifyUser(),
        'ModifyDateTime' => $this->getModifyDateTime(),
        'CancellationInfoCollection' => $this->getCancellationInfoCollection(),
      );
    }

    /**
     * @return int
     */
    public function getSequenceID()
    {
      return $this->SequenceID;
    }

    /**
     * @param int $SequenceID
     * @return \Dingus\SyncroService\BookingRS
     */
    public function setSequenceID($SequenceID)
    {
      $this->SequenceID = $SequenceID;
      return $this;
    }

    /**
     * @return string
     */
    public function getAllotmentBlockCode()
    {
      return $this->AllotmentBlockCode;
    }

    /**
     * @param string $AllotmentBlockCode
     * @return \Dingus\SyncroService\BookingRS
     */
    public function setAllotmentBlockCode($AllotmentBlockCode)
    {
      $this->AllotmentBlockCode = $AllotmentBlockCode;
      return $this;
    }

    /**
     * @return StatusBookingRS
     */
    public function getStatus()
    {
      return $this->Status;
    }

    /**
     * @param StatusBookingRS $Status
     * @return \Dingus\SyncroService\BookingRS
     */
    public function setStatus($Status)
    {
      $this->Status = $Status;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getRQ()
    {
      return $this->RQ;
    }

    /**
     * @param boolean $RQ
     * @return \Dingus\SyncroService\BookingRS
     */
    public function setRQ($RQ)
    {
      $this->RQ = $RQ;
      return $this;
    }

    /**
     * @return string
     */
    public function getBoardCode()
    {
      return $this->BoardCode;
    }

    /**
     * @param string $BoardCode
     * @return \Dingus\SyncroService\BookingRS
     */
    public function setBoardCode($BoardCode)
    {
      $this->BoardCode = $BoardCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getBoardName()
    {
      return $this->BoardName;
    }

    /**
     * @param string $BoardName
     * @return \Dingus\SyncroService\BookingRS
     */
    public function setBoardName($BoardName)
    {
      $this->BoardName = $BoardName;
      return $this;
    }

    /**
     * @return string
     */
    public function getChainCode()
    {
      return $this->ChainCode;
    }

    /**
     * @param string $ChainCode
     * @return \Dingus\SyncroService\BookingRS
     */
    public function setChainCode($ChainCode)
    {
      $this->ChainCode = $ChainCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getChainName()
    {
      return $this->ChainName;
    }

    /**
     * @param string $ChainName
     * @return \Dingus\SyncroService\BookingRS
     */
    public function setChainName($ChainName)
    {
      $this->ChainName = $ChainName;
      return $this;
    }

    /**
     * @return ArrayOfCriterion
     */
    public function getCriteria()
    {
      return $this->Criteria;
    }

    /**
     * @param ArrayOfCriterion $Criteria
     * @return \Dingus\SyncroService\BookingRS
     */
    public function setCriteria($Criteria)
    {
      $this->Criteria = $Criteria;
      return $this;
    }

    /**
     * @return string
     */
    public function getCustomerCode()
    {
      return $this->CustomerCode;
    }

    /**
     * @param string $CustomerCode
     * @return \Dingus\SyncroService\BookingRS
     */
    public function setCustomerCode($CustomerCode)
    {
      $this->CustomerCode = $CustomerCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getCustomerName()
    {
      return $this->CustomerName;
    }

    /**
     * @param string $CustomerName
     * @return \Dingus\SyncroService\BookingRS
     */
    public function setCustomerName($CustomerName)
    {
      $this->CustomerName = $CustomerName;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateFrom()
    {
      if ($this->DateFrom == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->DateFrom);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $DateFrom
     * @return \Dingus\SyncroService\BookingRS
     */
    public function setDateFrom(\DateTime $DateFrom)
    {
      $this->DateFrom = $DateFrom->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateTo()
    {
      if ($this->DateTo == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->DateTo);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $DateTo
     * @return \Dingus\SyncroService\BookingRS
     */
    public function setDateTo(\DateTime $DateTo)
    {
      $this->DateTo = $DateTo->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateSale()
    {
      if ($this->DateSale == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->DateSale);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $DateSale
     * @return \Dingus\SyncroService\BookingRS
     */
    public function setDateSale(\DateTime $DateSale)
    {
      $this->DateSale = $DateSale->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return boolean
     */
    public function getError()
    {
      return $this->Error;
    }

    /**
     * @param boolean $Error
     * @return \Dingus\SyncroService\BookingRS
     */
    public function setError($Error)
    {
      $this->Error = $Error;
      return $this;
    }

    /**
     * @return ArrayOfError
     */
    public function getErrors()
    {
      return $this->Errors;
    }

    /**
     * @param ArrayOfError $Errors
     * @return \Dingus\SyncroService\BookingRS
     */
    public function setErrors($Errors)
    {
      $this->Errors = $Errors;
      return $this;
    }

    /**
     * @return ArrayOfExtra
     */
    public function getExtras()
    {
      return $this->Extras;
    }

    /**
     * @param ArrayOfExtra $Extras
     * @return \Dingus\SyncroService\BookingRS
     */
    public function setExtras($Extras)
    {
      $this->Extras = $Extras;
      return $this;
    }

    /**
     * @return ArrayOfData
     */
    public function getBookingData()
    {
      return $this->BookingData;
    }

    /**
     * @param ArrayOfData $BookingData
     * @return \Dingus\SyncroService\BookingRS
     */
    public function setBookingData($BookingData)
    {
      $this->BookingData = $BookingData;
      return $this;
    }

    /**
     * @return ArrayOfPayment
     */
    public function getPayments()
    {
      return $this->Payments;
    }

    /**
     * @param ArrayOfPayment $Payments
     * @return \Dingus\SyncroService\BookingRS
     */
    public function setPayments($Payments)
    {
      $this->Payments = $Payments;
      return $this;
    }

    /**
     * @return ArrayOfCharge
     */
    public function getCharges()
    {
      return $this->Charges;
    }

    /**
     * @param ArrayOfCharge $Charges
     * @return \Dingus\SyncroService\BookingRS
     */
    public function setCharges($Charges)
    {
      $this->Charges = $Charges;
      return $this;
    }

    /**
     * @return ArrayOfStay
     */
    public function getStays()
    {
      return $this->Stays;
    }

    /**
     * @param ArrayOfStay $Stays
     * @return \Dingus\SyncroService\BookingRS
     */
    public function setStays($Stays)
    {
      $this->Stays = $Stays;
      return $this;
    }

    /**
     * @return ComissionRS
     */
    public function getComissionInfo()
    {
      return $this->ComissionInfo;
    }

    /**
     * @param ComissionRS $ComissionInfo
     * @return \Dingus\SyncroService\BookingRS
     */
    public function setComissionInfo($ComissionInfo)
    {
      $this->ComissionInfo = $ComissionInfo;
      return $this;
    }

    /**
     * @return string
     */
    public function getHotelCode()
    {
      return $this->HotelCode;
    }

    /**
     * @param string $HotelCode
     * @return \Dingus\SyncroService\BookingRS
     */
    public function setHotelCode($HotelCode)
    {
      $this->HotelCode = $HotelCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getHotelName()
    {
      return $this->HotelName;
    }

    /**
     * @param string $HotelName
     * @return \Dingus\SyncroService\BookingRS
     */
    public function setHotelName($HotelName)
    {
      $this->HotelName = $HotelName;
      return $this;
    }

    /**
     * @return string
     */
    public function getLocalizer()
    {
      return $this->Localizer;
    }

    /**
     * @param string $Localizer
     * @return \Dingus\SyncroService\BookingRS
     */
    public function setLocalizer($Localizer)
    {
      $this->Localizer = $Localizer;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getSend()
    {
      return $this->Send;
    }

    /**
     * @param boolean $Send
     * @return \Dingus\SyncroService\BookingRS
     */
    public function setSend($Send)
    {
      $this->Send = $Send;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getSendPMS()
    {
      return $this->SendPMS;
    }

    /**
     * @param boolean $SendPMS
     * @return \Dingus\SyncroService\BookingRS
     */
    public function setSendPMS($SendPMS)
    {
      $this->SendPMS = $SendPMS;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getErrorPMS()
    {
      return $this->ErrorPMS;
    }

    /**
     * @param boolean $ErrorPMS
     * @return \Dingus\SyncroService\BookingRS
     */
    public function setErrorPMS($ErrorPMS)
    {
      $this->ErrorPMS = $ErrorPMS;
      return $this;
    }

    /**
     * @return string
     */
    public function getErrorPMSText()
    {
      return $this->ErrorPMSText;
    }

    /**
     * @param string $ErrorPMSText
     * @return \Dingus\SyncroService\BookingRS
     */
    public function setErrorPMSText($ErrorPMSText)
    {
      $this->ErrorPMSText = $ErrorPMSText;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getOfferApplied()
    {
      return $this->OfferApplied;
    }

    /**
     * @param boolean $OfferApplied
     * @return \Dingus\SyncroService\BookingRS
     */
    public function setOfferApplied($OfferApplied)
    {
      $this->OfferApplied = $OfferApplied;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getBookingOutOfAllotment()
    {
      return $this->BookingOutOfAllotment;
    }

    /**
     * @param boolean $BookingOutOfAllotment
     * @return \Dingus\SyncroService\BookingRS
     */
    public function setBookingOutOfAllotment($BookingOutOfAllotment)
    {
      $this->BookingOutOfAllotment = $BookingOutOfAllotment;
      return $this;
    }

    /**
     * @return ArrayOfPax
     */
    public function getPaxes()
    {
      return $this->Paxes;
    }

    /**
     * @param ArrayOfPax $Paxes
     * @return \Dingus\SyncroService\BookingRS
     */
    public function setPaxes($Paxes)
    {
      $this->Paxes = $Paxes;
      return $this;
    }

    /**
     * @return ArrayOfPrice
     */
    public function getPriceDetails()
    {
      return $this->PriceDetails;
    }

    /**
     * @param ArrayOfPrice $PriceDetails
     * @return \Dingus\SyncroService\BookingRS
     */
    public function setPriceDetails($PriceDetails)
    {
      $this->PriceDetails = $PriceDetails;
      return $this;
    }

    /**
     * @return ArrayOfPrice
     */
    public function getContractPriceDetails()
    {
      return $this->ContractPriceDetails;
    }

    /**
     * @param ArrayOfPrice $ContractPriceDetails
     * @return \Dingus\SyncroService\BookingRS
     */
    public function setContractPriceDetails($ContractPriceDetails)
    {
      $this->ContractPriceDetails = $ContractPriceDetails;
      return $this;
    }

    /**
     * @return string
     */
    public function getResRef()
    {
      return $this->ResRef;
    }

    /**
     * @param string $ResRef
     * @return \Dingus\SyncroService\BookingRS
     */
    public function setResRef($ResRef)
    {
      $this->ResRef = $ResRef;
      return $this;
    }

    /**
     * @return string
     */
    public function getRoomCode()
    {
      return $this->RoomCode;
    }

    /**
     * @param string $RoomCode
     * @return \Dingus\SyncroService\BookingRS
     */
    public function setRoomCode($RoomCode)
    {
      $this->RoomCode = $RoomCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getRoomName()
    {
      return $this->RoomName;
    }

    /**
     * @param string $RoomName
     * @return \Dingus\SyncroService\BookingRS
     */
    public function setRoomName($RoomName)
    {
      $this->RoomName = $RoomName;
      return $this;
    }

    /**
     * @return float
     */
    public function getTotalPrice()
    {
      return $this->TotalPrice;
    }

    /**
     * @param float $TotalPrice
     * @return \Dingus\SyncroService\BookingRS
     */
    public function setTotalPrice($TotalPrice)
    {
      $this->TotalPrice = $TotalPrice;
      return $this;
    }

    /**
     * @return string
     */
    public function getVendorCode()
    {
      return $this->VendorCode;
    }

    /**
     * @param string $VendorCode
     * @return \Dingus\SyncroService\BookingRS
     */
    public function setVendorCode($VendorCode)
    {
      $this->VendorCode = $VendorCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getVendorName()
    {
      return $this->VendorName;
    }

    /**
     * @param string $VendorName
     * @return \Dingus\SyncroService\BookingRS
     */
    public function setVendorName($VendorName)
    {
      $this->VendorName = $VendorName;
      return $this;
    }

    /**
     * @return string
     */
    public function getSalesRoomCode()
    {
      return $this->SalesRoomCode;
    }

    /**
     * @param string $SalesRoomCode
     * @return \Dingus\SyncroService\BookingRS
     */
    public function setSalesRoomCode($SalesRoomCode)
    {
      $this->SalesRoomCode = $SalesRoomCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getRoomReference()
    {
      return $this->RoomReference;
    }

    /**
     * @param string $RoomReference
     * @return \Dingus\SyncroService\BookingRS
     */
    public function setRoomReference($RoomReference)
    {
      $this->RoomReference = $RoomReference;
      return $this;
    }

    /**
     * @return string
     */
    public function getCurrencyCode()
    {
      return $this->CurrencyCode;
    }

    /**
     * @param string $CurrencyCode
     * @return \Dingus\SyncroService\BookingRS
     */
    public function setCurrencyCode($CurrencyCode)
    {
      $this->CurrencyCode = $CurrencyCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getCustomerAllotmentGroup()
    {
      return $this->CustomerAllotmentGroup;
    }

    /**
     * @param string $CustomerAllotmentGroup
     * @return \Dingus\SyncroService\BookingRS
     */
    public function setCustomerAllotmentGroup($CustomerAllotmentGroup)
    {
      $this->CustomerAllotmentGroup = $CustomerAllotmentGroup;
      return $this;
    }

    /**
     * @return string
     */
    public function getCustomerRateGroup()
    {
      return $this->CustomerRateGroup;
    }

    /**
     * @param string $CustomerRateGroup
     * @return \Dingus\SyncroService\BookingRS
     */
    public function setCustomerRateGroup($CustomerRateGroup)
    {
      $this->CustomerRateGroup = $CustomerRateGroup;
      return $this;
    }

    /**
     * @return string
     */
    public function getPromotionCode()
    {
      return $this->PromotionCode;
    }

    /**
     * @param string $PromotionCode
     * @return \Dingus\SyncroService\BookingRS
     */
    public function setPromotionCode($PromotionCode)
    {
      $this->PromotionCode = $PromotionCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getPromotionDescription()
    {
      return $this->PromotionDescription;
    }

    /**
     * @param string $PromotionDescription
     * @return \Dingus\SyncroService\BookingRS
     */
    public function setPromotionDescription($PromotionDescription)
    {
      $this->PromotionDescription = $PromotionDescription;
      return $this;
    }

    /**
     * @return string
     */
    public function getPromotionCodeDiscount()
    {
      return $this->PromotionCodeDiscount;
    }

    /**
     * @param string $PromotionCodeDiscount
     * @return \Dingus\SyncroService\BookingRS
     */
    public function setPromotionCodeDiscount($PromotionCodeDiscount)
    {
      $this->PromotionCodeDiscount = $PromotionCodeDiscount;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getBookingInCheckOut()
    {
      return $this->BookingInCheckOut;
    }

    /**
     * @param boolean $BookingInCheckOut
     * @return \Dingus\SyncroService\BookingRS
     */
    public function setBookingInCheckOut($BookingInCheckOut)
    {
      $this->BookingInCheckOut = $BookingInCheckOut;
      return $this;
    }

    /**
     * @return string
     */
    public function getPMSCode()
    {
      return $this->PMSCode;
    }

    /**
     * @param string $PMSCode
     * @return \Dingus\SyncroService\BookingRS
     */
    public function setPMSCode($PMSCode)
    {
      $this->PMSCode = $PMSCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getInsertUser()
    {
      return $this->InsertUser;
    }

    /**
     * @param string $InsertUser
     * @return \Dingus\SyncroService\BookingRS
     */
    public function setInsertUser($InsertUser)
    {
      $this->InsertUser = $InsertUser;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getInserDateTime()
    {
      if ($this->InserDateTime == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->InserDateTime);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $InserDateTime
     * @return \Dingus\SyncroService\BookingRS
     */
    public function setInserDateTime(\DateTime $InserDateTime)
    {
      $this->InserDateTime = $InserDateTime->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return string
     */
    public function getModifyUser()
    {
      return $this->ModifyUser;
    }

    /**
     * @param string $ModifyUser
     * @return \Dingus\SyncroService\BookingRS
     */
    public function setModifyUser($ModifyUser)
    {
      $this->ModifyUser = $ModifyUser;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getModifyDateTime()
    {
      if ($this->ModifyDateTime == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->ModifyDateTime);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $ModifyDateTime
     * @return \Dingus\SyncroService\BookingRS
     */
    public function setModifyDateTime(\DateTime $ModifyDateTime)
    {
      $this->ModifyDateTime = $ModifyDateTime->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return ArrayOfCancellationInfo
     */
    public function getCancellationInfoCollection()
    {
      return $this->CancellationInfoCollection;
    }

    /**
     * @param ArrayOfCancellationInfo $CancellationInfoCollection
     * @return \Dingus\SyncroService\BookingRS
     */
    public function setCancellationInfoCollection($CancellationInfoCollection)
    {
      $this->CancellationInfoCollection = $CancellationInfoCollection;
      return $this;
    }

}
