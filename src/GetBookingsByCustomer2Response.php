<?php

namespace Dingus\SyncroService;

class GetBookingsByCustomer2Response implements \JsonSerializable
{

    /**
     * @var GetBookinsRS $GetBookingsByCustomer2Result
     */
    protected $GetBookingsByCustomer2Result = null;

    /**
     * @param GetBookinsRS $GetBookingsByCustomer2Result
     */
    public function __construct($GetBookingsByCustomer2Result)
    {
      $this->GetBookingsByCustomer2Result = $GetBookingsByCustomer2Result;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'GetBookingsByCustomer2Result' => $this->getGetBookingsByCustomer2Result(),
      );
    }

    /**
     * @return GetBookinsRS
     */
    public function getGetBookingsByCustomer2Result()
    {
      return $this->GetBookingsByCustomer2Result;
    }

    /**
     * @param GetBookinsRS $GetBookingsByCustomer2Result
     * @return \Dingus\SyncroService\GetBookingsByCustomer2Response
     */
    public function setGetBookingsByCustomer2Result($GetBookingsByCustomer2Result)
    {
      $this->GetBookingsByCustomer2Result = $GetBookingsByCustomer2Result;
      return $this;
    }

}
