<?php

namespace Dingus\SyncroService;

class PutUsersConfiguration implements \JsonSerializable
{

    /**
     * @var Credentials $Credentials
     */
    protected $Credentials = null;

    /**
     * @var PutUserConfigurationRQ $RQ
     */
    protected $RQ = null;

    /**
     * @param Credentials $Credentials
     * @param PutUserConfigurationRQ $RQ
     */
    public function __construct($Credentials, $RQ)
    {
      $this->Credentials = $Credentials;
      $this->RQ = $RQ;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'Credentials' => $this->getCredentials(),
        'RQ' => $this->getRQ(),
      );
    }

    /**
     * @return Credentials
     */
    public function getCredentials()
    {
      return $this->Credentials;
    }

    /**
     * @param Credentials $Credentials
     * @return \Dingus\SyncroService\PutUsersConfiguration
     */
    public function setCredentials($Credentials)
    {
      $this->Credentials = $Credentials;
      return $this;
    }

    /**
     * @return PutUserConfigurationRQ
     */
    public function getRQ()
    {
      return $this->RQ;
    }

    /**
     * @param PutUserConfigurationRQ $RQ
     * @return \Dingus\SyncroService\PutUsersConfiguration
     */
    public function setRQ($RQ)
    {
      $this->RQ = $RQ;
      return $this;
    }

}
