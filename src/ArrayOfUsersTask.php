<?php

namespace Dingus\SyncroService;

class ArrayOfUsersTask implements \JsonSerializable, \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var UsersTask[] $UsersTask
     */
    protected $UsersTask = null;

    
    public function __construct()
    {
    
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'UsersTask' => $this->getUsersTask(),
      );
    }

    /**
     * @return UsersTask[]
     */
    public function getUsersTask()
    {
      return $this->UsersTask;
    }

    /**
     * @param UsersTask[] $UsersTask
     * @return \Dingus\SyncroService\ArrayOfUsersTask
     */
    public function setUsersTask(array $UsersTask = null)
    {
      $this->UsersTask = $UsersTask;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->UsersTask[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return UsersTask
     */
    public function offsetGet($offset)
    {
      return $this->UsersTask[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param UsersTask $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      $this->UsersTask[$offset] = $value;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->UsersTask[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return UsersTask Return the current element
     */
    public function current()
    {
      return current($this->UsersTask);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->UsersTask);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->UsersTask);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->UsersTask);
    }

    /**
     * Countable implementation
     *
     * @return UsersTask Return count of elements
     */
    public function count()
    {
      return count($this->UsersTask);
    }

}
