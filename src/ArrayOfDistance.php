<?php

namespace Dingus\SyncroService;

class ArrayOfDistance implements \JsonSerializable, \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var Distance[] $Distance
     */
    protected $Distance = null;

    
    public function __construct()
    {
    
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'Distance' => $this->getDistance(),
      );
    }

    /**
     * @return Distance[]
     */
    public function getDistance()
    {
      return $this->Distance;
    }

    /**
     * @param Distance[] $Distance
     * @return \Dingus\SyncroService\ArrayOfDistance
     */
    public function setDistance(array $Distance = null)
    {
      $this->Distance = $Distance;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->Distance[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return Distance
     */
    public function offsetGet($offset)
    {
      return $this->Distance[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param Distance $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      $this->Distance[$offset] = $value;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->Distance[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return Distance Return the current element
     */
    public function current()
    {
      return current($this->Distance);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->Distance);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->Distance);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->Distance);
    }

    /**
     * Countable implementation
     *
     * @return Distance Return count of elements
     */
    public function count()
    {
      return count($this->Distance);
    }

}
