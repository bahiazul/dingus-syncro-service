<?php

namespace Dingus\SyncroService;

class PutCustomerDailyRatesRQ implements \JsonSerializable
{

    /**
     * @var string $HotelCode
     */
    protected $HotelCode = null;

    /**
     * @var string $GroupRateCode
     */
    protected $GroupRateCode = null;

    /**
     * @var ArrayOfPutCustomerDailyRatesRQRec $Recs
     */
    protected $Recs = null;

    
    public function __construct()
    {
    
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'HotelCode' => $this->getHotelCode(),
        'GroupRateCode' => $this->getGroupRateCode(),
        'Recs' => $this->getRecs(),
      );
    }

    /**
     * @return string
     */
    public function getHotelCode()
    {
      return $this->HotelCode;
    }

    /**
     * @param string $HotelCode
     * @return \Dingus\SyncroService\PutCustomerDailyRatesRQ
     */
    public function setHotelCode($HotelCode)
    {
      $this->HotelCode = $HotelCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getGroupRateCode()
    {
      return $this->GroupRateCode;
    }

    /**
     * @param string $GroupRateCode
     * @return \Dingus\SyncroService\PutCustomerDailyRatesRQ
     */
    public function setGroupRateCode($GroupRateCode)
    {
      $this->GroupRateCode = $GroupRateCode;
      return $this;
    }

    /**
     * @return ArrayOfPutCustomerDailyRatesRQRec
     */
    public function getRecs()
    {
      return $this->Recs;
    }

    /**
     * @param ArrayOfPutCustomerDailyRatesRQRec $Recs
     * @return \Dingus\SyncroService\PutCustomerDailyRatesRQ
     */
    public function setRecs($Recs)
    {
      $this->Recs = $Recs;
      return $this;
    }

}
