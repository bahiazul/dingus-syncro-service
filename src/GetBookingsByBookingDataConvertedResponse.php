<?php

namespace Dingus\SyncroService;

class GetBookingsByBookingDataConvertedResponse implements \JsonSerializable
{

    /**
     * @var ArrayOfBookingRS $GetBookingsByBookingDataConvertedResult
     */
    protected $GetBookingsByBookingDataConvertedResult = null;

    /**
     * @param ArrayOfBookingRS $GetBookingsByBookingDataConvertedResult
     */
    public function __construct($GetBookingsByBookingDataConvertedResult)
    {
      $this->GetBookingsByBookingDataConvertedResult = $GetBookingsByBookingDataConvertedResult;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'GetBookingsByBookingDataConvertedResult' => $this->getGetBookingsByBookingDataConvertedResult(),
      );
    }

    /**
     * @return ArrayOfBookingRS
     */
    public function getGetBookingsByBookingDataConvertedResult()
    {
      return $this->GetBookingsByBookingDataConvertedResult;
    }

    /**
     * @param ArrayOfBookingRS $GetBookingsByBookingDataConvertedResult
     * @return \Dingus\SyncroService\GetBookingsByBookingDataConvertedResponse
     */
    public function setGetBookingsByBookingDataConvertedResult($GetBookingsByBookingDataConvertedResult)
    {
      $this->GetBookingsByBookingDataConvertedResult = $GetBookingsByBookingDataConvertedResult;
      return $this;
    }

}
