<?php

namespace Dingus\SyncroService;

class PutDailyRates implements \JsonSerializable
{

    /**
     * @var Credentials $Credentials
     */
    protected $Credentials = null;

    /**
     * @var DailyRatesRQ $dailyRatesRQ
     */
    protected $dailyRatesRQ = null;

    /**
     * @param Credentials $Credentials
     * @param DailyRatesRQ $dailyRatesRQ
     */
    public function __construct($Credentials, $dailyRatesRQ)
    {
      $this->Credentials = $Credentials;
      $this->dailyRatesRQ = $dailyRatesRQ;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'Credentials' => $this->getCredentials(),
        'dailyRatesRQ' => $this->getDailyRatesRQ(),
      );
    }

    /**
     * @return Credentials
     */
    public function getCredentials()
    {
      return $this->Credentials;
    }

    /**
     * @param Credentials $Credentials
     * @return \Dingus\SyncroService\PutDailyRates
     */
    public function setCredentials($Credentials)
    {
      $this->Credentials = $Credentials;
      return $this;
    }

    /**
     * @return DailyRatesRQ
     */
    public function getDailyRatesRQ()
    {
      return $this->dailyRatesRQ;
    }

    /**
     * @param DailyRatesRQ $dailyRatesRQ
     * @return \Dingus\SyncroService\PutDailyRates
     */
    public function setDailyRatesRQ($dailyRatesRQ)
    {
      $this->dailyRatesRQ = $dailyRatesRQ;
      return $this;
    }

}
