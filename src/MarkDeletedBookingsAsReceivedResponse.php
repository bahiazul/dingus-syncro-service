<?php

namespace Dingus\SyncroService;

class MarkDeletedBookingsAsReceivedResponse implements \JsonSerializable
{

    /**
     * @var SyncroRS $MarkDeletedBookingsAsReceivedResult
     */
    protected $MarkDeletedBookingsAsReceivedResult = null;

    /**
     * @param SyncroRS $MarkDeletedBookingsAsReceivedResult
     */
    public function __construct($MarkDeletedBookingsAsReceivedResult)
    {
      $this->MarkDeletedBookingsAsReceivedResult = $MarkDeletedBookingsAsReceivedResult;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'MarkDeletedBookingsAsReceivedResult' => $this->getMarkDeletedBookingsAsReceivedResult(),
      );
    }

    /**
     * @return SyncroRS
     */
    public function getMarkDeletedBookingsAsReceivedResult()
    {
      return $this->MarkDeletedBookingsAsReceivedResult;
    }

    /**
     * @param SyncroRS $MarkDeletedBookingsAsReceivedResult
     * @return \Dingus\SyncroService\MarkDeletedBookingsAsReceivedResponse
     */
    public function setMarkDeletedBookingsAsReceivedResult($MarkDeletedBookingsAsReceivedResult)
    {
      $this->MarkDeletedBookingsAsReceivedResult = $MarkDeletedBookingsAsReceivedResult;
      return $this;
    }

}
