<?php

namespace Dingus\SyncroService;

class PutPromotionalCodesByQuantity implements \JsonSerializable
{

    /**
     * @var Credentials $Credentials
     */
    protected $Credentials = null;

    /**
     * @var string $PromotionCode
     */
    protected $PromotionCode = null;

    /**
     * @var int $Quantity
     */
    protected $Quantity = null;

    /**
     * @var boolean $OnlyUse
     */
    protected $OnlyUse = null;

    /**
     * @param Credentials $Credentials
     * @param string $PromotionCode
     * @param int $Quantity
     * @param boolean $OnlyUse
     */
    public function __construct($Credentials, $PromotionCode, $Quantity, $OnlyUse)
    {
      $this->Credentials = $Credentials;
      $this->PromotionCode = $PromotionCode;
      $this->Quantity = $Quantity;
      $this->OnlyUse = $OnlyUse;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'Credentials' => $this->getCredentials(),
        'PromotionCode' => $this->getPromotionCode(),
        'Quantity' => $this->getQuantity(),
        'OnlyUse' => $this->getOnlyUse(),
      );
    }

    /**
     * @return Credentials
     */
    public function getCredentials()
    {
      return $this->Credentials;
    }

    /**
     * @param Credentials $Credentials
     * @return \Dingus\SyncroService\PutPromotionalCodesByQuantity
     */
    public function setCredentials($Credentials)
    {
      $this->Credentials = $Credentials;
      return $this;
    }

    /**
     * @return string
     */
    public function getPromotionCode()
    {
      return $this->PromotionCode;
    }

    /**
     * @param string $PromotionCode
     * @return \Dingus\SyncroService\PutPromotionalCodesByQuantity
     */
    public function setPromotionCode($PromotionCode)
    {
      $this->PromotionCode = $PromotionCode;
      return $this;
    }

    /**
     * @return int
     */
    public function getQuantity()
    {
      return $this->Quantity;
    }

    /**
     * @param int $Quantity
     * @return \Dingus\SyncroService\PutPromotionalCodesByQuantity
     */
    public function setQuantity($Quantity)
    {
      $this->Quantity = $Quantity;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getOnlyUse()
    {
      return $this->OnlyUse;
    }

    /**
     * @param boolean $OnlyUse
     * @return \Dingus\SyncroService\PutPromotionalCodesByQuantity
     */
    public function setOnlyUse($OnlyUse)
    {
      $this->OnlyUse = $OnlyUse;
      return $this;
    }

}
