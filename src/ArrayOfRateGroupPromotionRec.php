<?php

namespace Dingus\SyncroService;

class ArrayOfRateGroupPromotionRec implements \JsonSerializable, \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var RateGroupPromotionRec[] $RateGroupPromotionRec
     */
    protected $RateGroupPromotionRec = null;

    
    public function __construct()
    {
    
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'RateGroupPromotionRec' => $this->getRateGroupPromotionRec(),
      );
    }

    /**
     * @return RateGroupPromotionRec[]
     */
    public function getRateGroupPromotionRec()
    {
      return $this->RateGroupPromotionRec;
    }

    /**
     * @param RateGroupPromotionRec[] $RateGroupPromotionRec
     * @return \Dingus\SyncroService\ArrayOfRateGroupPromotionRec
     */
    public function setRateGroupPromotionRec(array $RateGroupPromotionRec = null)
    {
      $this->RateGroupPromotionRec = $RateGroupPromotionRec;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->RateGroupPromotionRec[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return RateGroupPromotionRec
     */
    public function offsetGet($offset)
    {
      return $this->RateGroupPromotionRec[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param RateGroupPromotionRec $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      $this->RateGroupPromotionRec[$offset] = $value;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->RateGroupPromotionRec[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return RateGroupPromotionRec Return the current element
     */
    public function current()
    {
      return current($this->RateGroupPromotionRec);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->RateGroupPromotionRec);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->RateGroupPromotionRec);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->RateGroupPromotionRec);
    }

    /**
     * Countable implementation
     *
     * @return RateGroupPromotionRec Return count of elements
     */
    public function count()
    {
      return count($this->RateGroupPromotionRec);
    }

}
