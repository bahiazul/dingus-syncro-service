<?php

namespace Dingus\SyncroService;

class ArrayOfPutCustomerDailyRatesRQRec implements \JsonSerializable, \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var PutCustomerDailyRatesRQRec[] $PutCustomerDailyRatesRQRec
     */
    protected $PutCustomerDailyRatesRQRec = null;

    
    public function __construct()
    {
    
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'PutCustomerDailyRatesRQRec' => $this->getPutCustomerDailyRatesRQRec(),
      );
    }

    /**
     * @return PutCustomerDailyRatesRQRec[]
     */
    public function getPutCustomerDailyRatesRQRec()
    {
      return $this->PutCustomerDailyRatesRQRec;
    }

    /**
     * @param PutCustomerDailyRatesRQRec[] $PutCustomerDailyRatesRQRec
     * @return \Dingus\SyncroService\ArrayOfPutCustomerDailyRatesRQRec
     */
    public function setPutCustomerDailyRatesRQRec(array $PutCustomerDailyRatesRQRec = null)
    {
      $this->PutCustomerDailyRatesRQRec = $PutCustomerDailyRatesRQRec;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->PutCustomerDailyRatesRQRec[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return PutCustomerDailyRatesRQRec
     */
    public function offsetGet($offset)
    {
      return $this->PutCustomerDailyRatesRQRec[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param PutCustomerDailyRatesRQRec $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      $this->PutCustomerDailyRatesRQRec[$offset] = $value;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->PutCustomerDailyRatesRQRec[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return PutCustomerDailyRatesRQRec Return the current element
     */
    public function current()
    {
      return current($this->PutCustomerDailyRatesRQRec);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->PutCustomerDailyRatesRQRec);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->PutCustomerDailyRatesRQRec);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->PutCustomerDailyRatesRQRec);
    }

    /**
     * Countable implementation
     *
     * @return PutCustomerDailyRatesRQRec Return count of elements
     */
    public function count()
    {
      return count($this->PutCustomerDailyRatesRQRec);
    }

}
