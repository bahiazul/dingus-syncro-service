<?php

namespace Dingus\SyncroService;

class Picture implements \JsonSerializable
{

    /**
     * @var ArrayOfMlText $DescriptionMlList
     */
    protected $DescriptionMlList = null;

    /**
     * @var Action $Action
     */
    protected $Action = null;

    /**
     * @var int $Id
     */
    protected $Id = null;

    /**
     * @var string $Description
     */
    protected $Description = null;

    /**
     * @var boolean $IsMap
     */
    protected $IsMap = null;

    /**
     * @var string $Url
     */
    protected $Url = null;

    /**
     * @param Action $Action
     * @param int $Id
     * @param boolean $IsMap
     */
    public function __construct($Action, $Id, $IsMap)
    {
      $this->Action = $Action;
      $this->Id = $Id;
      $this->IsMap = $IsMap;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'DescriptionMlList' => $this->getDescriptionMlList(),
        'Action' => $this->getAction(),
        'Id' => $this->getId(),
        'Description' => $this->getDescription(),
        'IsMap' => $this->getIsMap(),
        'Url' => $this->getUrl(),
      );
    }

    /**
     * @return ArrayOfMlText
     */
    public function getDescriptionMlList()
    {
      return $this->DescriptionMlList;
    }

    /**
     * @param ArrayOfMlText $DescriptionMlList
     * @return \Dingus\SyncroService\Picture
     */
    public function setDescriptionMlList($DescriptionMlList)
    {
      $this->DescriptionMlList = $DescriptionMlList;
      return $this;
    }

    /**
     * @return Action
     */
    public function getAction()
    {
      return $this->Action;
    }

    /**
     * @param Action $Action
     * @return \Dingus\SyncroService\Picture
     */
    public function setAction($Action)
    {
      $this->Action = $Action;
      return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
      return $this->Id;
    }

    /**
     * @param int $Id
     * @return \Dingus\SyncroService\Picture
     */
    public function setId($Id)
    {
      $this->Id = $Id;
      return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
      return $this->Description;
    }

    /**
     * @param string $Description
     * @return \Dingus\SyncroService\Picture
     */
    public function setDescription($Description)
    {
      $this->Description = $Description;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getIsMap()
    {
      return $this->IsMap;
    }

    /**
     * @param boolean $IsMap
     * @return \Dingus\SyncroService\Picture
     */
    public function setIsMap($IsMap)
    {
      $this->IsMap = $IsMap;
      return $this;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
      return $this->Url;
    }

    /**
     * @param string $Url
     * @return \Dingus\SyncroService\Picture
     */
    public function setUrl($Url)
    {
      $this->Url = $Url;
      return $this;
    }

}
