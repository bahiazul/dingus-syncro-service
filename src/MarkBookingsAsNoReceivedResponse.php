<?php

namespace Dingus\SyncroService;

class MarkBookingsAsNoReceivedResponse implements \JsonSerializable
{

    /**
     * @var SyncroRS $MarkBookingsAsNoReceivedResult
     */
    protected $MarkBookingsAsNoReceivedResult = null;

    /**
     * @param SyncroRS $MarkBookingsAsNoReceivedResult
     */
    public function __construct($MarkBookingsAsNoReceivedResult)
    {
      $this->MarkBookingsAsNoReceivedResult = $MarkBookingsAsNoReceivedResult;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'MarkBookingsAsNoReceivedResult' => $this->getMarkBookingsAsNoReceivedResult(),
      );
    }

    /**
     * @return SyncroRS
     */
    public function getMarkBookingsAsNoReceivedResult()
    {
      return $this->MarkBookingsAsNoReceivedResult;
    }

    /**
     * @param SyncroRS $MarkBookingsAsNoReceivedResult
     * @return \Dingus\SyncroService\MarkBookingsAsNoReceivedResponse
     */
    public function setMarkBookingsAsNoReceivedResult($MarkBookingsAsNoReceivedResult)
    {
      $this->MarkBookingsAsNoReceivedResult = $MarkBookingsAsNoReceivedResult;
      return $this;
    }

}
