<?php

namespace Dingus\SyncroService;

class PriceRulesRec implements \JsonSerializable
{

    /**
     * @var Action $Action
     */
    protected $Action = null;

    /**
     * @var string $Code
     */
    protected $Code = null;

    /**
     * @var string $Description
     */
    protected $Description = null;

    /**
     * @var string $VendorCode
     */
    protected $VendorCode = null;

    /**
     * @var string $VendorName
     */
    protected $VendorName = null;

    /**
     * @var string $GroupRateCode
     */
    protected $GroupRateCode = null;

    /**
     * @var string $GroupRateName
     */
    protected $GroupRateName = null;

    /**
     * @var \DateTime $DateFrom
     */
    protected $DateFrom = null;

    /**
     * @var \DateTime $DateTo
     */
    protected $DateTo = null;

    /**
     * @var boolean $AppliedRoom
     */
    protected $AppliedRoom = null;

    /**
     * @var boolean $AppliedBoard
     */
    protected $AppliedBoard = null;

    /**
     * @var boolean $AppliedSuplement
     */
    protected $AppliedSuplement = null;

    /**
     * @var boolean $AppliedExtra
     */
    protected $AppliedExtra = null;

    /**
     * @var float $Percent
     */
    protected $Percent = null;

    /**
     * @var ArrayOfCustomersPriceRules $LCustomers
     */
    protected $LCustomers = null;

    /**
     * @var ArrayOfHotelsPriceRules $LHotels
     */
    protected $LHotels = null;

    /**
     * @var ArrayOfCriterionPriceRules $LCriterion
     */
    protected $LCriterion = null;

    /**
     * @param Action $Action
     * @param \DateTime $DateFrom
     * @param \DateTime $DateTo
     * @param boolean $AppliedRoom
     * @param boolean $AppliedBoard
     * @param boolean $AppliedSuplement
     * @param boolean $AppliedExtra
     * @param float $Percent
     */
    public function __construct($Action, \DateTime $DateFrom, \DateTime $DateTo, $AppliedRoom, $AppliedBoard, $AppliedSuplement, $AppliedExtra, $Percent)
    {
      $this->Action = $Action;
      $this->DateFrom = $DateFrom->format(\DateTime::ATOM);
      $this->DateTo = $DateTo->format(\DateTime::ATOM);
      $this->AppliedRoom = $AppliedRoom;
      $this->AppliedBoard = $AppliedBoard;
      $this->AppliedSuplement = $AppliedSuplement;
      $this->AppliedExtra = $AppliedExtra;
      $this->Percent = $Percent;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'Action' => $this->getAction(),
        'Code' => $this->getCode(),
        'Description' => $this->getDescription(),
        'VendorCode' => $this->getVendorCode(),
        'VendorName' => $this->getVendorName(),
        'GroupRateCode' => $this->getGroupRateCode(),
        'GroupRateName' => $this->getGroupRateName(),
        'DateFrom' => $this->getDateFrom(),
        'DateTo' => $this->getDateTo(),
        'AppliedRoom' => $this->getAppliedRoom(),
        'AppliedBoard' => $this->getAppliedBoard(),
        'AppliedSuplement' => $this->getAppliedSuplement(),
        'AppliedExtra' => $this->getAppliedExtra(),
        'Percent' => $this->getPercent(),
        'LCustomers' => $this->getLCustomers(),
        'LHotels' => $this->getLHotels(),
        'LCriterion' => $this->getLCriterion(),
      );
    }

    /**
     * @return Action
     */
    public function getAction()
    {
      return $this->Action;
    }

    /**
     * @param Action $Action
     * @return \Dingus\SyncroService\PriceRulesRec
     */
    public function setAction($Action)
    {
      $this->Action = $Action;
      return $this;
    }

    /**
     * @return string
     */
    public function getCode()
    {
      return $this->Code;
    }

    /**
     * @param string $Code
     * @return \Dingus\SyncroService\PriceRulesRec
     */
    public function setCode($Code)
    {
      $this->Code = $Code;
      return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
      return $this->Description;
    }

    /**
     * @param string $Description
     * @return \Dingus\SyncroService\PriceRulesRec
     */
    public function setDescription($Description)
    {
      $this->Description = $Description;
      return $this;
    }

    /**
     * @return string
     */
    public function getVendorCode()
    {
      return $this->VendorCode;
    }

    /**
     * @param string $VendorCode
     * @return \Dingus\SyncroService\PriceRulesRec
     */
    public function setVendorCode($VendorCode)
    {
      $this->VendorCode = $VendorCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getVendorName()
    {
      return $this->VendorName;
    }

    /**
     * @param string $VendorName
     * @return \Dingus\SyncroService\PriceRulesRec
     */
    public function setVendorName($VendorName)
    {
      $this->VendorName = $VendorName;
      return $this;
    }

    /**
     * @return string
     */
    public function getGroupRateCode()
    {
      return $this->GroupRateCode;
    }

    /**
     * @param string $GroupRateCode
     * @return \Dingus\SyncroService\PriceRulesRec
     */
    public function setGroupRateCode($GroupRateCode)
    {
      $this->GroupRateCode = $GroupRateCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getGroupRateName()
    {
      return $this->GroupRateName;
    }

    /**
     * @param string $GroupRateName
     * @return \Dingus\SyncroService\PriceRulesRec
     */
    public function setGroupRateName($GroupRateName)
    {
      $this->GroupRateName = $GroupRateName;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateFrom()
    {
      if ($this->DateFrom == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->DateFrom);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $DateFrom
     * @return \Dingus\SyncroService\PriceRulesRec
     */
    public function setDateFrom(\DateTime $DateFrom)
    {
      $this->DateFrom = $DateFrom->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateTo()
    {
      if ($this->DateTo == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->DateTo);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $DateTo
     * @return \Dingus\SyncroService\PriceRulesRec
     */
    public function setDateTo(\DateTime $DateTo)
    {
      $this->DateTo = $DateTo->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return boolean
     */
    public function getAppliedRoom()
    {
      return $this->AppliedRoom;
    }

    /**
     * @param boolean $AppliedRoom
     * @return \Dingus\SyncroService\PriceRulesRec
     */
    public function setAppliedRoom($AppliedRoom)
    {
      $this->AppliedRoom = $AppliedRoom;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getAppliedBoard()
    {
      return $this->AppliedBoard;
    }

    /**
     * @param boolean $AppliedBoard
     * @return \Dingus\SyncroService\PriceRulesRec
     */
    public function setAppliedBoard($AppliedBoard)
    {
      $this->AppliedBoard = $AppliedBoard;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getAppliedSuplement()
    {
      return $this->AppliedSuplement;
    }

    /**
     * @param boolean $AppliedSuplement
     * @return \Dingus\SyncroService\PriceRulesRec
     */
    public function setAppliedSuplement($AppliedSuplement)
    {
      $this->AppliedSuplement = $AppliedSuplement;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getAppliedExtra()
    {
      return $this->AppliedExtra;
    }

    /**
     * @param boolean $AppliedExtra
     * @return \Dingus\SyncroService\PriceRulesRec
     */
    public function setAppliedExtra($AppliedExtra)
    {
      $this->AppliedExtra = $AppliedExtra;
      return $this;
    }

    /**
     * @return float
     */
    public function getPercent()
    {
      return $this->Percent;
    }

    /**
     * @param float $Percent
     * @return \Dingus\SyncroService\PriceRulesRec
     */
    public function setPercent($Percent)
    {
      $this->Percent = $Percent;
      return $this;
    }

    /**
     * @return ArrayOfCustomersPriceRules
     */
    public function getLCustomers()
    {
      return $this->LCustomers;
    }

    /**
     * @param ArrayOfCustomersPriceRules $LCustomers
     * @return \Dingus\SyncroService\PriceRulesRec
     */
    public function setLCustomers($LCustomers)
    {
      $this->LCustomers = $LCustomers;
      return $this;
    }

    /**
     * @return ArrayOfHotelsPriceRules
     */
    public function getLHotels()
    {
      return $this->LHotels;
    }

    /**
     * @param ArrayOfHotelsPriceRules $LHotels
     * @return \Dingus\SyncroService\PriceRulesRec
     */
    public function setLHotels($LHotels)
    {
      $this->LHotels = $LHotels;
      return $this;
    }

    /**
     * @return ArrayOfCriterionPriceRules
     */
    public function getLCriterion()
    {
      return $this->LCriterion;
    }

    /**
     * @param ArrayOfCriterionPriceRules $LCriterion
     * @return \Dingus\SyncroService\PriceRulesRec
     */
    public function setLCriterion($LCriterion)
    {
      $this->LCriterion = $LCriterion;
      return $this;
    }

}
