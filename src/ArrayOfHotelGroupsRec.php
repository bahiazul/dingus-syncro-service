<?php

namespace Dingus\SyncroService;

class ArrayOfHotelGroupsRec implements \JsonSerializable, \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var HotelGroupsRec[] $HotelGroupsRec
     */
    protected $HotelGroupsRec = null;

    
    public function __construct()
    {
    
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'HotelGroupsRec' => $this->getHotelGroupsRec(),
      );
    }

    /**
     * @return HotelGroupsRec[]
     */
    public function getHotelGroupsRec()
    {
      return $this->HotelGroupsRec;
    }

    /**
     * @param HotelGroupsRec[] $HotelGroupsRec
     * @return \Dingus\SyncroService\ArrayOfHotelGroupsRec
     */
    public function setHotelGroupsRec(array $HotelGroupsRec = null)
    {
      $this->HotelGroupsRec = $HotelGroupsRec;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->HotelGroupsRec[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return HotelGroupsRec
     */
    public function offsetGet($offset)
    {
      return $this->HotelGroupsRec[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param HotelGroupsRec $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      $this->HotelGroupsRec[$offset] = $value;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->HotelGroupsRec[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return HotelGroupsRec Return the current element
     */
    public function current()
    {
      return current($this->HotelGroupsRec);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->HotelGroupsRec);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->HotelGroupsRec);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->HotelGroupsRec);
    }

    /**
     * Countable implementation
     *
     * @return HotelGroupsRec Return count of elements
     */
    public function count()
    {
      return count($this->HotelGroupsRec);
    }

}
