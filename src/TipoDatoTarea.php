<?php

namespace Dingus\SyncroService;

class TipoDatoTarea
{
    const __default = 'Customer';
    const Customer = 'Customer';
    const PseudoCityCode = 'PseudoCityCode';
    const Dias = 'Dias';
    const eBOXVendor = 'eBOXVendor';
    const MailNotificacion = 'MailNotificacion';
    const SegundosModoError = 'SegundosModoError';
    const SegundosOriginales = 'SegundosOriginales';
    const ContadorErrores = 'ContadorErrores';
    const EnErrorDesde = 'EnErrorDesde';
    const Retrasada = 'Retrasada';
    const Revisada = 'Revisada';
    const FechaRevision = 'FechaRevision';
    const Responsable = 'Responsable';
    const TipoError = 'TipoError';


}
