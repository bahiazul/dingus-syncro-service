<?php

namespace Dingus\SyncroService;

class GetDeletedBookingsResult implements \JsonSerializable
{

    /**
     * @var string $CustomerCode
     */
    protected $CustomerCode = null;

    /**
     * @var string $HotelCode
     */
    protected $HotelCode = null;

    /**
     * @var string $Lozalizer
     */
    protected $Lozalizer = null;

    
    public function __construct()
    {
    
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'CustomerCode' => $this->getCustomerCode(),
        'HotelCode' => $this->getHotelCode(),
        'Lozalizer' => $this->getLozalizer(),
      );
    }

    /**
     * @return string
     */
    public function getCustomerCode()
    {
      return $this->CustomerCode;
    }

    /**
     * @param string $CustomerCode
     * @return \Dingus\SyncroService\GetDeletedBookingsResult
     */
    public function setCustomerCode($CustomerCode)
    {
      $this->CustomerCode = $CustomerCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getHotelCode()
    {
      return $this->HotelCode;
    }

    /**
     * @param string $HotelCode
     * @return \Dingus\SyncroService\GetDeletedBookingsResult
     */
    public function setHotelCode($HotelCode)
    {
      $this->HotelCode = $HotelCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getLozalizer()
    {
      return $this->Lozalizer;
    }

    /**
     * @param string $Lozalizer
     * @return \Dingus\SyncroService\GetDeletedBookingsResult
     */
    public function setLozalizer($Lozalizer)
    {
      $this->Lozalizer = $Lozalizer;
      return $this;
    }

}
