<?php

namespace Dingus\SyncroService;

class GetBookingsByHotel2 implements \JsonSerializable
{

    /**
     * @var string $UserName
     */
    protected $UserName = null;

    /**
     * @var string $PassWord
     */
    protected $PassWord = null;

    /**
     * @var string $HotelCode
     */
    protected $HotelCode = null;

    /**
     * @var \DateTime $DateFrom
     */
    protected $DateFrom = null;

    /**
     * @var \DateTime $DateTo
     */
    protected $DateTo = null;

    /**
     * @var boolean $NotFullBooking
     */
    protected $NotFullBooking = null;

    /**
     * @param string $UserName
     * @param string $PassWord
     * @param string $HotelCode
     * @param \DateTime $DateFrom
     * @param \DateTime $DateTo
     * @param boolean $NotFullBooking
     */
    public function __construct($UserName, $PassWord, $HotelCode, \DateTime $DateFrom, \DateTime $DateTo, $NotFullBooking)
    {
      $this->UserName = $UserName;
      $this->PassWord = $PassWord;
      $this->HotelCode = $HotelCode;
      $this->DateFrom = $DateFrom->format(\DateTime::ATOM);
      $this->DateTo = $DateTo->format(\DateTime::ATOM);
      $this->NotFullBooking = $NotFullBooking;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'UserName' => $this->getUserName(),
        'PassWord' => $this->getPassWord(),
        'HotelCode' => $this->getHotelCode(),
        'DateFrom' => $this->getDateFrom(),
        'DateTo' => $this->getDateTo(),
        'NotFullBooking' => $this->getNotFullBooking(),
      );
    }

    /**
     * @return string
     */
    public function getUserName()
    {
      return $this->UserName;
    }

    /**
     * @param string $UserName
     * @return \Dingus\SyncroService\GetBookingsByHotel2
     */
    public function setUserName($UserName)
    {
      $this->UserName = $UserName;
      return $this;
    }

    /**
     * @return string
     */
    public function getPassWord()
    {
      return $this->PassWord;
    }

    /**
     * @param string $PassWord
     * @return \Dingus\SyncroService\GetBookingsByHotel2
     */
    public function setPassWord($PassWord)
    {
      $this->PassWord = $PassWord;
      return $this;
    }

    /**
     * @return string
     */
    public function getHotelCode()
    {
      return $this->HotelCode;
    }

    /**
     * @param string $HotelCode
     * @return \Dingus\SyncroService\GetBookingsByHotel2
     */
    public function setHotelCode($HotelCode)
    {
      $this->HotelCode = $HotelCode;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateFrom()
    {
      if ($this->DateFrom == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->DateFrom);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $DateFrom
     * @return \Dingus\SyncroService\GetBookingsByHotel2
     */
    public function setDateFrom(\DateTime $DateFrom)
    {
      $this->DateFrom = $DateFrom->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateTo()
    {
      if ($this->DateTo == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->DateTo);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $DateTo
     * @return \Dingus\SyncroService\GetBookingsByHotel2
     */
    public function setDateTo(\DateTime $DateTo)
    {
      $this->DateTo = $DateTo->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return boolean
     */
    public function getNotFullBooking()
    {
      return $this->NotFullBooking;
    }

    /**
     * @param boolean $NotFullBooking
     * @return \Dingus\SyncroService\GetBookingsByHotel2
     */
    public function setNotFullBooking($NotFullBooking)
    {
      $this->NotFullBooking = $NotFullBooking;
      return $this;
    }

}
