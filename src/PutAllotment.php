<?php

namespace Dingus\SyncroService;

class PutAllotment implements \JsonSerializable
{

    /**
     * @var Credentials $Credentials
     */
    protected $Credentials = null;

    /**
     * @var PutAllotmentRQ $PutAllotmentRQ
     */
    protected $PutAllotmentRQ = null;

    /**
     * @param Credentials $Credentials
     * @param PutAllotmentRQ $PutAllotmentRQ
     */
    public function __construct($Credentials, $PutAllotmentRQ)
    {
      $this->Credentials = $Credentials;
      $this->PutAllotmentRQ = $PutAllotmentRQ;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'Credentials' => $this->getCredentials(),
        'PutAllotmentRQ' => $this->getPutAllotmentRQ(),
      );
    }

    /**
     * @return Credentials
     */
    public function getCredentials()
    {
      return $this->Credentials;
    }

    /**
     * @param Credentials $Credentials
     * @return \Dingus\SyncroService\PutAllotment
     */
    public function setCredentials($Credentials)
    {
      $this->Credentials = $Credentials;
      return $this;
    }

    /**
     * @return PutAllotmentRQ
     */
    public function getPutAllotmentRQ()
    {
      return $this->PutAllotmentRQ;
    }

    /**
     * @param PutAllotmentRQ $PutAllotmentRQ
     * @return \Dingus\SyncroService\PutAllotment
     */
    public function setPutAllotmentRQ($PutAllotmentRQ)
    {
      $this->PutAllotmentRQ = $PutAllotmentRQ;
      return $this;
    }

}
