<?php

namespace Dingus\SyncroService;

class PutDetailsHotelResponse implements \JsonSerializable
{

    /**
     * @var SyncroRS $PutDetailsHotelResult
     */
    protected $PutDetailsHotelResult = null;

    /**
     * @param SyncroRS $PutDetailsHotelResult
     */
    public function __construct($PutDetailsHotelResult)
    {
      $this->PutDetailsHotelResult = $PutDetailsHotelResult;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'PutDetailsHotelResult' => $this->getPutDetailsHotelResult(),
      );
    }

    /**
     * @return SyncroRS
     */
    public function getPutDetailsHotelResult()
    {
      return $this->PutDetailsHotelResult;
    }

    /**
     * @param SyncroRS $PutDetailsHotelResult
     * @return \Dingus\SyncroService\PutDetailsHotelResponse
     */
    public function setPutDetailsHotelResult($PutDetailsHotelResult)
    {
      $this->PutDetailsHotelResult = $PutDetailsHotelResult;
      return $this;
    }

}
