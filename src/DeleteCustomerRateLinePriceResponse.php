<?php

namespace Dingus\SyncroService;

class DeleteCustomerRateLinePriceResponse implements \JsonSerializable
{

    /**
     * @var SyncroRS $DeleteCustomerRateLinePriceResult
     */
    protected $DeleteCustomerRateLinePriceResult = null;

    /**
     * @param SyncroRS $DeleteCustomerRateLinePriceResult
     */
    public function __construct($DeleteCustomerRateLinePriceResult)
    {
      $this->DeleteCustomerRateLinePriceResult = $DeleteCustomerRateLinePriceResult;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'DeleteCustomerRateLinePriceResult' => $this->getDeleteCustomerRateLinePriceResult(),
      );
    }

    /**
     * @return SyncroRS
     */
    public function getDeleteCustomerRateLinePriceResult()
    {
      return $this->DeleteCustomerRateLinePriceResult;
    }

    /**
     * @param SyncroRS $DeleteCustomerRateLinePriceResult
     * @return \Dingus\SyncroService\DeleteCustomerRateLinePriceResponse
     */
    public function setDeleteCustomerRateLinePriceResult($DeleteCustomerRateLinePriceResult)
    {
      $this->DeleteCustomerRateLinePriceResult = $DeleteCustomerRateLinePriceResult;
      return $this;
    }

}
