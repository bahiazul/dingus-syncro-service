<?php

namespace Dingus\SyncroService;

class CustomerRateSetUpRS implements \JsonSerializable
{

    /**
     * @var ArrayOfCustomerRateLine $lCustomerRatesLines
     */
    protected $lCustomerRatesLines = null;

    /**
     * @var ArrayOfAllotmentAlertCustomerRateLine $lAllotmentAlerts
     */
    protected $lAllotmentAlerts = null;

    /**
     * @var Action $Action
     */
    protected $Action = null;

    /**
     * @var int $IdCustomerRate
     */
    protected $IdCustomerRate = null;

    /**
     * @var string $CustomerCode
     */
    protected $CustomerCode = null;

    /**
     * @var string $CustomerName
     */
    protected $CustomerName = null;

    /**
     * @var string $HotelCode
     */
    protected $HotelCode = null;

    /**
     * @var string $HotelName
     */
    protected $HotelName = null;

    /**
     * @var string $Contract
     */
    protected $Contract = null;

    /**
     * @var string $UserCustomerRate
     */
    protected $UserCustomerRate = null;

    /**
     * @var string $PasswordCustomerRate
     */
    protected $PasswordCustomerRate = null;

    /**
     * @var boolean $PendingChanges
     */
    protected $PendingChanges = null;

    /**
     * @var boolean $InTransaction
     */
    protected $InTransaction = null;

    /**
     * @var string $CustomerHotelCode
     */
    protected $CustomerHotelCode = null;

    /**
     * @var boolean $ToShareAllotment
     */
    protected $ToShareAllotment = null;

    /**
     * @var boolean $RelatedRate
     */
    protected $RelatedRate = null;

    /**
     * @var boolean $RelatedRoom
     */
    protected $RelatedRoom = null;

    /**
     * @var boolean $RelatedBoard
     */
    protected $RelatedBoard = null;

    /**
     * @var boolean $CacheConversion
     */
    protected $CacheConversion = null;

    /**
     * @var boolean $AllotmentCustomerCodes
     */
    protected $AllotmentCustomerCodes = null;

    /**
     * @var string $ConversionKey
     */
    protected $ConversionKey = null;

    /**
     * @param Action $Action
     * @param int $IdCustomerRate
     * @param boolean $PendingChanges
     * @param boolean $InTransaction
     * @param boolean $ToShareAllotment
     * @param boolean $RelatedRate
     * @param boolean $RelatedRoom
     * @param boolean $RelatedBoard
     * @param boolean $CacheConversion
     * @param boolean $AllotmentCustomerCodes
     */
    public function __construct($Action, $IdCustomerRate, $PendingChanges, $InTransaction, $ToShareAllotment, $RelatedRate, $RelatedRoom, $RelatedBoard, $CacheConversion, $AllotmentCustomerCodes)
    {
      $this->Action = $Action;
      $this->IdCustomerRate = $IdCustomerRate;
      $this->PendingChanges = $PendingChanges;
      $this->InTransaction = $InTransaction;
      $this->ToShareAllotment = $ToShareAllotment;
      $this->RelatedRate = $RelatedRate;
      $this->RelatedRoom = $RelatedRoom;
      $this->RelatedBoard = $RelatedBoard;
      $this->CacheConversion = $CacheConversion;
      $this->AllotmentCustomerCodes = $AllotmentCustomerCodes;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'lCustomerRatesLines' => $this->getLCustomerRatesLines(),
        'lAllotmentAlerts' => $this->getLAllotmentAlerts(),
        'Action' => $this->getAction(),
        'IdCustomerRate' => $this->getIdCustomerRate(),
        'CustomerCode' => $this->getCustomerCode(),
        'CustomerName' => $this->getCustomerName(),
        'HotelCode' => $this->getHotelCode(),
        'HotelName' => $this->getHotelName(),
        'Contract' => $this->getContract(),
        'UserCustomerRate' => $this->getUserCustomerRate(),
        'PasswordCustomerRate' => $this->getPasswordCustomerRate(),
        'PendingChanges' => $this->getPendingChanges(),
        'InTransaction' => $this->getInTransaction(),
        'CustomerHotelCode' => $this->getCustomerHotelCode(),
        'ToShareAllotment' => $this->getToShareAllotment(),
        'RelatedRate' => $this->getRelatedRate(),
        'RelatedRoom' => $this->getRelatedRoom(),
        'RelatedBoard' => $this->getRelatedBoard(),
        'CacheConversion' => $this->getCacheConversion(),
        'AllotmentCustomerCodes' => $this->getAllotmentCustomerCodes(),
        'ConversionKey' => $this->getConversionKey(),
      );
    }

    /**
     * @return ArrayOfCustomerRateLine
     */
    public function getLCustomerRatesLines()
    {
      return $this->lCustomerRatesLines;
    }

    /**
     * @param ArrayOfCustomerRateLine $lCustomerRatesLines
     * @return \Dingus\SyncroService\CustomerRateSetUpRS
     */
    public function setLCustomerRatesLines($lCustomerRatesLines)
    {
      $this->lCustomerRatesLines = $lCustomerRatesLines;
      return $this;
    }

    /**
     * @return ArrayOfAllotmentAlertCustomerRateLine
     */
    public function getLAllotmentAlerts()
    {
      return $this->lAllotmentAlerts;
    }

    /**
     * @param ArrayOfAllotmentAlertCustomerRateLine $lAllotmentAlerts
     * @return \Dingus\SyncroService\CustomerRateSetUpRS
     */
    public function setLAllotmentAlerts($lAllotmentAlerts)
    {
      $this->lAllotmentAlerts = $lAllotmentAlerts;
      return $this;
    }

    /**
     * @return Action
     */
    public function getAction()
    {
      return $this->Action;
    }

    /**
     * @param Action $Action
     * @return \Dingus\SyncroService\CustomerRateSetUpRS
     */
    public function setAction($Action)
    {
      $this->Action = $Action;
      return $this;
    }

    /**
     * @return int
     */
    public function getIdCustomerRate()
    {
      return $this->IdCustomerRate;
    }

    /**
     * @param int $IdCustomerRate
     * @return \Dingus\SyncroService\CustomerRateSetUpRS
     */
    public function setIdCustomerRate($IdCustomerRate)
    {
      $this->IdCustomerRate = $IdCustomerRate;
      return $this;
    }

    /**
     * @return string
     */
    public function getCustomerCode()
    {
      return $this->CustomerCode;
    }

    /**
     * @param string $CustomerCode
     * @return \Dingus\SyncroService\CustomerRateSetUpRS
     */
    public function setCustomerCode($CustomerCode)
    {
      $this->CustomerCode = $CustomerCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getCustomerName()
    {
      return $this->CustomerName;
    }

    /**
     * @param string $CustomerName
     * @return \Dingus\SyncroService\CustomerRateSetUpRS
     */
    public function setCustomerName($CustomerName)
    {
      $this->CustomerName = $CustomerName;
      return $this;
    }

    /**
     * @return string
     */
    public function getHotelCode()
    {
      return $this->HotelCode;
    }

    /**
     * @param string $HotelCode
     * @return \Dingus\SyncroService\CustomerRateSetUpRS
     */
    public function setHotelCode($HotelCode)
    {
      $this->HotelCode = $HotelCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getHotelName()
    {
      return $this->HotelName;
    }

    /**
     * @param string $HotelName
     * @return \Dingus\SyncroService\CustomerRateSetUpRS
     */
    public function setHotelName($HotelName)
    {
      $this->HotelName = $HotelName;
      return $this;
    }

    /**
     * @return string
     */
    public function getContract()
    {
      return $this->Contract;
    }

    /**
     * @param string $Contract
     * @return \Dingus\SyncroService\CustomerRateSetUpRS
     */
    public function setContract($Contract)
    {
      $this->Contract = $Contract;
      return $this;
    }

    /**
     * @return string
     */
    public function getUserCustomerRate()
    {
      return $this->UserCustomerRate;
    }

    /**
     * @param string $UserCustomerRate
     * @return \Dingus\SyncroService\CustomerRateSetUpRS
     */
    public function setUserCustomerRate($UserCustomerRate)
    {
      $this->UserCustomerRate = $UserCustomerRate;
      return $this;
    }

    /**
     * @return string
     */
    public function getPasswordCustomerRate()
    {
      return $this->PasswordCustomerRate;
    }

    /**
     * @param string $PasswordCustomerRate
     * @return \Dingus\SyncroService\CustomerRateSetUpRS
     */
    public function setPasswordCustomerRate($PasswordCustomerRate)
    {
      $this->PasswordCustomerRate = $PasswordCustomerRate;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getPendingChanges()
    {
      return $this->PendingChanges;
    }

    /**
     * @param boolean $PendingChanges
     * @return \Dingus\SyncroService\CustomerRateSetUpRS
     */
    public function setPendingChanges($PendingChanges)
    {
      $this->PendingChanges = $PendingChanges;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getInTransaction()
    {
      return $this->InTransaction;
    }

    /**
     * @param boolean $InTransaction
     * @return \Dingus\SyncroService\CustomerRateSetUpRS
     */
    public function setInTransaction($InTransaction)
    {
      $this->InTransaction = $InTransaction;
      return $this;
    }

    /**
     * @return string
     */
    public function getCustomerHotelCode()
    {
      return $this->CustomerHotelCode;
    }

    /**
     * @param string $CustomerHotelCode
     * @return \Dingus\SyncroService\CustomerRateSetUpRS
     */
    public function setCustomerHotelCode($CustomerHotelCode)
    {
      $this->CustomerHotelCode = $CustomerHotelCode;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getToShareAllotment()
    {
      return $this->ToShareAllotment;
    }

    /**
     * @param boolean $ToShareAllotment
     * @return \Dingus\SyncroService\CustomerRateSetUpRS
     */
    public function setToShareAllotment($ToShareAllotment)
    {
      $this->ToShareAllotment = $ToShareAllotment;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getRelatedRate()
    {
      return $this->RelatedRate;
    }

    /**
     * @param boolean $RelatedRate
     * @return \Dingus\SyncroService\CustomerRateSetUpRS
     */
    public function setRelatedRate($RelatedRate)
    {
      $this->RelatedRate = $RelatedRate;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getRelatedRoom()
    {
      return $this->RelatedRoom;
    }

    /**
     * @param boolean $RelatedRoom
     * @return \Dingus\SyncroService\CustomerRateSetUpRS
     */
    public function setRelatedRoom($RelatedRoom)
    {
      $this->RelatedRoom = $RelatedRoom;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getRelatedBoard()
    {
      return $this->RelatedBoard;
    }

    /**
     * @param boolean $RelatedBoard
     * @return \Dingus\SyncroService\CustomerRateSetUpRS
     */
    public function setRelatedBoard($RelatedBoard)
    {
      $this->RelatedBoard = $RelatedBoard;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getCacheConversion()
    {
      return $this->CacheConversion;
    }

    /**
     * @param boolean $CacheConversion
     * @return \Dingus\SyncroService\CustomerRateSetUpRS
     */
    public function setCacheConversion($CacheConversion)
    {
      $this->CacheConversion = $CacheConversion;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getAllotmentCustomerCodes()
    {
      return $this->AllotmentCustomerCodes;
    }

    /**
     * @param boolean $AllotmentCustomerCodes
     * @return \Dingus\SyncroService\CustomerRateSetUpRS
     */
    public function setAllotmentCustomerCodes($AllotmentCustomerCodes)
    {
      $this->AllotmentCustomerCodes = $AllotmentCustomerCodes;
      return $this;
    }

    /**
     * @return string
     */
    public function getConversionKey()
    {
      return $this->ConversionKey;
    }

    /**
     * @param string $ConversionKey
     * @return \Dingus\SyncroService\CustomerRateSetUpRS
     */
    public function setConversionKey($ConversionKey)
    {
      $this->ConversionKey = $ConversionKey;
      return $this;
    }

}
