<?php

namespace Dingus\SyncroService;

class PutCustomerRateSetUpRQ implements \JsonSerializable
{

    /**
     * @var ArrayOfCustomerRateSetUpRS $Recs
     */
    protected $Recs = null;

    
    public function __construct()
    {
    
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'Recs' => $this->getRecs(),
      );
    }

    /**
     * @return ArrayOfCustomerRateSetUpRS
     */
    public function getRecs()
    {
      return $this->Recs;
    }

    /**
     * @param ArrayOfCustomerRateSetUpRS $Recs
     * @return \Dingus\SyncroService\PutCustomerRateSetUpRQ
     */
    public function setRecs($Recs)
    {
      $this->Recs = $Recs;
      return $this;
    }

}
