<?php

namespace Dingus\SyncroService;

class PutPriceRulesResponse implements \JsonSerializable
{

    /**
     * @var SyncroRS $PutPriceRulesResult
     */
    protected $PutPriceRulesResult = null;

    /**
     * @param SyncroRS $PutPriceRulesResult
     */
    public function __construct($PutPriceRulesResult)
    {
      $this->PutPriceRulesResult = $PutPriceRulesResult;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'PutPriceRulesResult' => $this->getPutPriceRulesResult(),
      );
    }

    /**
     * @return SyncroRS
     */
    public function getPutPriceRulesResult()
    {
      return $this->PutPriceRulesResult;
    }

    /**
     * @param SyncroRS $PutPriceRulesResult
     * @return \Dingus\SyncroService\PutPriceRulesResponse
     */
    public function setPutPriceRulesResult($PutPriceRulesResult)
    {
      $this->PutPriceRulesResult = $PutPriceRulesResult;
      return $this;
    }

}
