<?php

namespace Dingus\SyncroService;

class ArrayOfComissionHotelRec implements \JsonSerializable, \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var ComissionHotelRec[] $ComissionHotelRec
     */
    protected $ComissionHotelRec = null;

    
    public function __construct()
    {
    
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'ComissionHotelRec' => $this->getComissionHotelRec(),
      );
    }

    /**
     * @return ComissionHotelRec[]
     */
    public function getComissionHotelRec()
    {
      return $this->ComissionHotelRec;
    }

    /**
     * @param ComissionHotelRec[] $ComissionHotelRec
     * @return \Dingus\SyncroService\ArrayOfComissionHotelRec
     */
    public function setComissionHotelRec(array $ComissionHotelRec = null)
    {
      $this->ComissionHotelRec = $ComissionHotelRec;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->ComissionHotelRec[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return ComissionHotelRec
     */
    public function offsetGet($offset)
    {
      return $this->ComissionHotelRec[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param ComissionHotelRec $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      $this->ComissionHotelRec[$offset] = $value;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->ComissionHotelRec[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return ComissionHotelRec Return the current element
     */
    public function current()
    {
      return current($this->ComissionHotelRec);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->ComissionHotelRec);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->ComissionHotelRec);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->ComissionHotelRec);
    }

    /**
     * Countable implementation
     *
     * @return ComissionHotelRec Return count of elements
     */
    public function count()
    {
      return count($this->ComissionHotelRec);
    }

}
