<?php

namespace Dingus\SyncroService;

class PutCustomerRatesByHotelListResponse implements \JsonSerializable
{

    /**
     * @var SyncroRS $PutCustomerRatesByHotelListResult
     */
    protected $PutCustomerRatesByHotelListResult = null;

    /**
     * @param SyncroRS $PutCustomerRatesByHotelListResult
     */
    public function __construct($PutCustomerRatesByHotelListResult)
    {
      $this->PutCustomerRatesByHotelListResult = $PutCustomerRatesByHotelListResult;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'PutCustomerRatesByHotelListResult' => $this->getPutCustomerRatesByHotelListResult(),
      );
    }

    /**
     * @return SyncroRS
     */
    public function getPutCustomerRatesByHotelListResult()
    {
      return $this->PutCustomerRatesByHotelListResult;
    }

    /**
     * @param SyncroRS $PutCustomerRatesByHotelListResult
     * @return \Dingus\SyncroService\PutCustomerRatesByHotelListResponse
     */
    public function setPutCustomerRatesByHotelListResult($PutCustomerRatesByHotelListResult)
    {
      $this->PutCustomerRatesByHotelListResult = $PutCustomerRatesByHotelListResult;
      return $this;
    }

}
