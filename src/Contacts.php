<?php

namespace Dingus\SyncroService;

class Contacts implements \JsonSerializable
{

    /**
     * @var ArrayOfPicture $Pictures
     */
    protected $Pictures = null;

    /**
     * @var Action $Action
     */
    protected $Action = null;

    /**
     * @var int $Id
     */
    protected $Id = null;

    /**
     * @var string $Code
     */
    protected $Code = null;

    /**
     * @var string $Description
     */
    protected $Description = null;

    /**
     * @var string $Telephone
     */
    protected $Telephone = null;

    /**
     * @var string $Telephone2
     */
    protected $Telephone2 = null;

    /**
     * @var string $Fax
     */
    protected $Fax = null;

    /**
     * @var string $Email
     */
    protected $Email = null;

    /**
     * @var string $AnotherContact
     */
    protected $AnotherContact = null;

    /**
     * @var string $AnotherContact2
     */
    protected $AnotherContact2 = null;

    /**
     * @param Action $Action
     * @param int $Id
     */
    public function __construct($Action, $Id)
    {
      $this->Action = $Action;
      $this->Id = $Id;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'Pictures' => $this->getPictures(),
        'Action' => $this->getAction(),
        'Id' => $this->getId(),
        'Code' => $this->getCode(),
        'Description' => $this->getDescription(),
        'Telephone' => $this->getTelephone(),
        'Telephone2' => $this->getTelephone2(),
        'Fax' => $this->getFax(),
        'Email' => $this->getEmail(),
        'AnotherContact' => $this->getAnotherContact(),
        'AnotherContact2' => $this->getAnotherContact2(),
      );
    }

    /**
     * @return ArrayOfPicture
     */
    public function getPictures()
    {
      return $this->Pictures;
    }

    /**
     * @param ArrayOfPicture $Pictures
     * @return \Dingus\SyncroService\Contacts
     */
    public function setPictures($Pictures)
    {
      $this->Pictures = $Pictures;
      return $this;
    }

    /**
     * @return Action
     */
    public function getAction()
    {
      return $this->Action;
    }

    /**
     * @param Action $Action
     * @return \Dingus\SyncroService\Contacts
     */
    public function setAction($Action)
    {
      $this->Action = $Action;
      return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
      return $this->Id;
    }

    /**
     * @param int $Id
     * @return \Dingus\SyncroService\Contacts
     */
    public function setId($Id)
    {
      $this->Id = $Id;
      return $this;
    }

    /**
     * @return string
     */
    public function getCode()
    {
      return $this->Code;
    }

    /**
     * @param string $Code
     * @return \Dingus\SyncroService\Contacts
     */
    public function setCode($Code)
    {
      $this->Code = $Code;
      return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
      return $this->Description;
    }

    /**
     * @param string $Description
     * @return \Dingus\SyncroService\Contacts
     */
    public function setDescription($Description)
    {
      $this->Description = $Description;
      return $this;
    }

    /**
     * @return string
     */
    public function getTelephone()
    {
      return $this->Telephone;
    }

    /**
     * @param string $Telephone
     * @return \Dingus\SyncroService\Contacts
     */
    public function setTelephone($Telephone)
    {
      $this->Telephone = $Telephone;
      return $this;
    }

    /**
     * @return string
     */
    public function getTelephone2()
    {
      return $this->Telephone2;
    }

    /**
     * @param string $Telephone2
     * @return \Dingus\SyncroService\Contacts
     */
    public function setTelephone2($Telephone2)
    {
      $this->Telephone2 = $Telephone2;
      return $this;
    }

    /**
     * @return string
     */
    public function getFax()
    {
      return $this->Fax;
    }

    /**
     * @param string $Fax
     * @return \Dingus\SyncroService\Contacts
     */
    public function setFax($Fax)
    {
      $this->Fax = $Fax;
      return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
      return $this->Email;
    }

    /**
     * @param string $Email
     * @return \Dingus\SyncroService\Contacts
     */
    public function setEmail($Email)
    {
      $this->Email = $Email;
      return $this;
    }

    /**
     * @return string
     */
    public function getAnotherContact()
    {
      return $this->AnotherContact;
    }

    /**
     * @param string $AnotherContact
     * @return \Dingus\SyncroService\Contacts
     */
    public function setAnotherContact($AnotherContact)
    {
      $this->AnotherContact = $AnotherContact;
      return $this;
    }

    /**
     * @return string
     */
    public function getAnotherContact2()
    {
      return $this->AnotherContact2;
    }

    /**
     * @param string $AnotherContact2
     * @return \Dingus\SyncroService\Contacts
     */
    public function setAnotherContact2($AnotherContact2)
    {
      $this->AnotherContact2 = $AnotherContact2;
      return $this;
    }

}
