<?php

namespace Dingus\SyncroService;

class Service implements \JsonSerializable
{

    /**
     * @var ArrayOfMlText $DescriptionMlList
     */
    protected $DescriptionMlList = null;

    /**
     * @var ArrayOfPicture $Pictures
     */
    protected $Pictures = null;

    /**
     * @var Action $Action
     */
    protected $Action = null;

    /**
     * @var int $Id
     */
    protected $Id = null;

    /**
     * @var string $Name
     */
    protected $Name = null;

    /**
     * @var string $Description
     */
    protected $Description = null;

    /**
     * @param Action $Action
     * @param int $Id
     */
    public function __construct($Action, $Id)
    {
      $this->Action = $Action;
      $this->Id = $Id;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'DescriptionMlList' => $this->getDescriptionMlList(),
        'Pictures' => $this->getPictures(),
        'Action' => $this->getAction(),
        'Id' => $this->getId(),
        'Name' => $this->getName(),
        'Description' => $this->getDescription(),
      );
    }

    /**
     * @return ArrayOfMlText
     */
    public function getDescriptionMlList()
    {
      return $this->DescriptionMlList;
    }

    /**
     * @param ArrayOfMlText $DescriptionMlList
     * @return \Dingus\SyncroService\Service
     */
    public function setDescriptionMlList($DescriptionMlList)
    {
      $this->DescriptionMlList = $DescriptionMlList;
      return $this;
    }

    /**
     * @return ArrayOfPicture
     */
    public function getPictures()
    {
      return $this->Pictures;
    }

    /**
     * @param ArrayOfPicture $Pictures
     * @return \Dingus\SyncroService\Service
     */
    public function setPictures($Pictures)
    {
      $this->Pictures = $Pictures;
      return $this;
    }

    /**
     * @return Action
     */
    public function getAction()
    {
      return $this->Action;
    }

    /**
     * @param Action $Action
     * @return \Dingus\SyncroService\Service
     */
    public function setAction($Action)
    {
      $this->Action = $Action;
      return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
      return $this->Id;
    }

    /**
     * @param int $Id
     * @return \Dingus\SyncroService\Service
     */
    public function setId($Id)
    {
      $this->Id = $Id;
      return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
      return $this->Name;
    }

    /**
     * @param string $Name
     * @return \Dingus\SyncroService\Service
     */
    public function setName($Name)
    {
      $this->Name = $Name;
      return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
      return $this->Description;
    }

    /**
     * @param string $Description
     * @return \Dingus\SyncroService\Service
     */
    public function setDescription($Description)
    {
      $this->Description = $Description;
      return $this;
    }

}
