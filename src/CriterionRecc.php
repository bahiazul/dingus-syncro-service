<?php

namespace Dingus\SyncroService;

class CriterionRecc implements \JsonSerializable
{

    /**
     * @var Action $Action
     */
    protected $Action = null;

    /**
     * @var int $IdCriterionContainedService
     */
    protected $IdCriterionContainedService = null;

    /**
     * @var string $CriterionType
     */
    protected $CriterionType = null;

    /**
     * @var string $CriterionValue
     */
    protected $CriterionValue = null;

    /**
     * @var string $CriterionDescription
     */
    protected $CriterionDescription = null;

    /**
     * @param Action $Action
     * @param int $IdCriterionContainedService
     */
    public function __construct($Action, $IdCriterionContainedService)
    {
      $this->Action = $Action;
      $this->IdCriterionContainedService = $IdCriterionContainedService;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'Action' => $this->getAction(),
        'IdCriterionContainedService' => $this->getIdCriterionContainedService(),
        'CriterionType' => $this->getCriterionType(),
        'CriterionValue' => $this->getCriterionValue(),
        'CriterionDescription' => $this->getCriterionDescription(),
      );
    }

    /**
     * @return Action
     */
    public function getAction()
    {
      return $this->Action;
    }

    /**
     * @param Action $Action
     * @return \Dingus\SyncroService\CriterionRecc
     */
    public function setAction($Action)
    {
      $this->Action = $Action;
      return $this;
    }

    /**
     * @return int
     */
    public function getIdCriterionContainedService()
    {
      return $this->IdCriterionContainedService;
    }

    /**
     * @param int $IdCriterionContainedService
     * @return \Dingus\SyncroService\CriterionRecc
     */
    public function setIdCriterionContainedService($IdCriterionContainedService)
    {
      $this->IdCriterionContainedService = $IdCriterionContainedService;
      return $this;
    }

    /**
     * @return string
     */
    public function getCriterionType()
    {
      return $this->CriterionType;
    }

    /**
     * @param string $CriterionType
     * @return \Dingus\SyncroService\CriterionRecc
     */
    public function setCriterionType($CriterionType)
    {
      $this->CriterionType = $CriterionType;
      return $this;
    }

    /**
     * @return string
     */
    public function getCriterionValue()
    {
      return $this->CriterionValue;
    }

    /**
     * @param string $CriterionValue
     * @return \Dingus\SyncroService\CriterionRecc
     */
    public function setCriterionValue($CriterionValue)
    {
      $this->CriterionValue = $CriterionValue;
      return $this;
    }

    /**
     * @return string
     */
    public function getCriterionDescription()
    {
      return $this->CriterionDescription;
    }

    /**
     * @param string $CriterionDescription
     * @return \Dingus\SyncroService\CriterionRecc
     */
    public function setCriterionDescription($CriterionDescription)
    {
      $this->CriterionDescription = $CriterionDescription;
      return $this;
    }

}
