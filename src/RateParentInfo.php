<?php

namespace Dingus\SyncroService;

class RateParentInfo implements \JsonSerializable
{

    /**
     * @var string $HotelCode
     */
    protected $HotelCode = null;

    /**
     * @var string $RateCode
     */
    protected $RateCode = null;

    /**
     * @var string $CustomerRatesGroupCode
     */
    protected $CustomerRatesGroupCode = null;

    
    public function __construct()
    {
    
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'HotelCode' => $this->getHotelCode(),
        'RateCode' => $this->getRateCode(),
        'CustomerRatesGroupCode' => $this->getCustomerRatesGroupCode(),
      );
    }

    /**
     * @return string
     */
    public function getHotelCode()
    {
      return $this->HotelCode;
    }

    /**
     * @param string $HotelCode
     * @return \Dingus\SyncroService\RateParentInfo
     */
    public function setHotelCode($HotelCode)
    {
      $this->HotelCode = $HotelCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getRateCode()
    {
      return $this->RateCode;
    }

    /**
     * @param string $RateCode
     * @return \Dingus\SyncroService\RateParentInfo
     */
    public function setRateCode($RateCode)
    {
      $this->RateCode = $RateCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getCustomerRatesGroupCode()
    {
      return $this->CustomerRatesGroupCode;
    }

    /**
     * @param string $CustomerRatesGroupCode
     * @return \Dingus\SyncroService\RateParentInfo
     */
    public function setCustomerRatesGroupCode($CustomerRatesGroupCode)
    {
      $this->CustomerRatesGroupCode = $CustomerRatesGroupCode;
      return $this;
    }

}
