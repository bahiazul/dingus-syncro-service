<?php

namespace Dingus\SyncroService;

class SyncroService extends \SoapClient
{

    /**
     * @var array $classmap The defined classes
     */
    private static $classmap = array (
      'PutHotelGroups' => 'Dingus\\SyncroService\\PutHotelGroups',
      'Credentials' => 'Dingus\\SyncroService\\Credentials',
      'PutHotelGroupsRQ' => 'Dingus\\SyncroService\\PutHotelGroupsRQ',
      'ArrayOfHotelGroupsRec' => 'Dingus\\SyncroService\\ArrayOfHotelGroupsRec',
      'HotelGroupsRec' => 'Dingus\\SyncroService\\HotelGroupsRec',
      'ContainedServiceRec' => 'Dingus\\SyncroService\\ContainedServiceRec',
      'ArrayOfMlText' => 'Dingus\\SyncroService\\ArrayOfMlText',
      'MlText' => 'Dingus\\SyncroService\\MlText',
      'ArrayOfCriterionRec' => 'Dingus\\SyncroService\\ArrayOfCriterionRec',
      'CriterionRec' => 'Dingus\\SyncroService\\CriterionRec',
      'Picture' => 'Dingus\\SyncroService\\Picture',
      'PutHotelGroupsResponse' => 'Dingus\\SyncroService\\PutHotelGroupsResponse',
      'SyncroRS' => 'Dingus\\SyncroService\\SyncroRS',
      'ArrayOfString' => 'Dingus\\SyncroService\\ArrayOfString',
      'PutHotelGroupsByHotelList' => 'Dingus\\SyncroService\\PutHotelGroupsByHotelList',
      'PutHotelGroupsByHotelListRQ' => 'Dingus\\SyncroService\\PutHotelGroupsByHotelListRQ',
      'PutHotelGroupsByHotelListResponse' => 'Dingus\\SyncroService\\PutHotelGroupsByHotelListResponse',
      'PutPriceRules' => 'Dingus\\SyncroService\\PutPriceRules',
      'PutPriceRulesRQ' => 'Dingus\\SyncroService\\PutPriceRulesRQ',
      'ArrayOfPriceRulesRec' => 'Dingus\\SyncroService\\ArrayOfPriceRulesRec',
      'PriceRulesRec' => 'Dingus\\SyncroService\\PriceRulesRec',
      'ArrayOfCustomersPriceRules' => 'Dingus\\SyncroService\\ArrayOfCustomersPriceRules',
      'CustomersPriceRules' => 'Dingus\\SyncroService\\CustomersPriceRules',
      'ArrayOfHotelsPriceRules' => 'Dingus\\SyncroService\\ArrayOfHotelsPriceRules',
      'HotelsPriceRules' => 'Dingus\\SyncroService\\HotelsPriceRules',
      'ArrayOfCriterionPriceRules' => 'Dingus\\SyncroService\\ArrayOfCriterionPriceRules',
      'CriterionPriceRules' => 'Dingus\\SyncroService\\CriterionPriceRules',
      'PutPriceRulesResponse' => 'Dingus\\SyncroService\\PutPriceRulesResponse',
      'PutUsersConfiguration' => 'Dingus\\SyncroService\\PutUsersConfiguration',
      'PutUserConfigurationRQ' => 'Dingus\\SyncroService\\PutUserConfigurationRQ',
      'ArrayOfUserConfigurationRec' => 'Dingus\\SyncroService\\ArrayOfUserConfigurationRec',
      'UserConfigurationRec' => 'Dingus\\SyncroService\\UserConfigurationRec',
      'PutUsersConfigurationResponse' => 'Dingus\\SyncroService\\PutUsersConfigurationResponse',
      'PutHotel' => 'Dingus\\SyncroService\\PutHotel',
      'PutHotelRQ' => 'Dingus\\SyncroService\\PutHotelRQ',
      'ArrayOfRoom' => 'Dingus\\SyncroService\\ArrayOfRoom',
      'Room' => 'Dingus\\SyncroService\\Room',
      'ArrayOfPicture' => 'Dingus\\SyncroService\\ArrayOfPicture',
      'ArrayOfInstalaciones' => 'Dingus\\SyncroService\\ArrayOfInstalaciones',
      'Instalaciones' => 'Dingus\\SyncroService\\Instalaciones',
      'ArrayOfBoard' => 'Dingus\\SyncroService\\ArrayOfBoard',
      'Board' => 'Dingus\\SyncroService\\Board',
      'ArrayOfExtraHotel' => 'Dingus\\SyncroService\\ArrayOfExtraHotel',
      'ExtraHotel' => 'Dingus\\SyncroService\\ExtraHotel',
      'ContainedServiceRecc' => 'Dingus\\SyncroService\\ContainedServiceRecc',
      'ArrayOfCriterionRecc' => 'Dingus\\SyncroService\\ArrayOfCriterionRecc',
      'CriterionRecc' => 'Dingus\\SyncroService\\CriterionRecc',
      'ArrayOfClose' => 'Dingus\\SyncroService\\ArrayOfClose',
      'Close' => 'Dingus\\SyncroService\\Close',
      'ArrayOfFacilities' => 'Dingus\\SyncroService\\ArrayOfFacilities',
      'Facilities' => 'Dingus\\SyncroService\\Facilities',
      'ArrayOfContacts' => 'Dingus\\SyncroService\\ArrayOfContacts',
      'Contacts' => 'Dingus\\SyncroService\\Contacts',
      'ArrayOfDistance' => 'Dingus\\SyncroService\\ArrayOfDistance',
      'Distance' => 'Dingus\\SyncroService\\Distance',
      'ArrayOfMeetingRoom' => 'Dingus\\SyncroService\\ArrayOfMeetingRoom',
      'MeetingRoom' => 'Dingus\\SyncroService\\MeetingRoom',
      'ArrayOfRestaurant' => 'Dingus\\SyncroService\\ArrayOfRestaurant',
      'Restaurant' => 'Dingus\\SyncroService\\Restaurant',
      'ArrayOfService' => 'Dingus\\SyncroService\\ArrayOfService',
      'Service' => 'Dingus\\SyncroService\\Service',
      'ArrayOfControlPrice' => 'Dingus\\SyncroService\\ArrayOfControlPrice',
      'ControlPrice' => 'Dingus\\SyncroService\\ControlPrice',
      'ArrayOfCriterion' => 'Dingus\\SyncroService\\ArrayOfCriterion',
      'Criterion' => 'Dingus\\SyncroService\\Criterion',
      'Comission' => 'Dingus\\SyncroService\\Comission',
      'PutHotelResponse' => 'Dingus\\SyncroService\\PutHotelResponse',
      'PutCustomerRateSetUp' => 'Dingus\\SyncroService\\PutCustomerRateSetUp',
      'CustomerRateSetUpRS' => 'Dingus\\SyncroService\\CustomerRateSetUpRS',
      'ArrayOfCustomerRateLine' => 'Dingus\\SyncroService\\ArrayOfCustomerRateLine',
      'CustomerRateLine' => 'Dingus\\SyncroService\\CustomerRateLine',
      'ArrayOfAllotmentAlertCustomerRateLine' => 'Dingus\\SyncroService\\ArrayOfAllotmentAlertCustomerRateLine',
      'AllotmentAlertCustomerRateLine' => 'Dingus\\SyncroService\\AllotmentAlertCustomerRateLine',
      'PutCustomerRateSetUpResponse' => 'Dingus\\SyncroService\\PutCustomerRateSetUpResponse',
      'DeleteCustomerRateLinePrice' => 'Dingus\\SyncroService\\DeleteCustomerRateLinePrice',
      'CustomerRateLinePriceRQ' => 'Dingus\\SyncroService\\CustomerRateLinePriceRQ',
      'DeleteCustomerRateLinePriceResponse' => 'Dingus\\SyncroService\\DeleteCustomerRateLinePriceResponse',
      'PutCancellationFee' => 'Dingus\\SyncroService\\PutCancellationFee',
      'PutCancellationFeeRQ' => 'Dingus\\SyncroService\\PutCancellationFeeRQ',
      'ArrayOfCancellationFeeRec' => 'Dingus\\SyncroService\\ArrayOfCancellationFeeRec',
      'CancellationFeeRec' => 'Dingus\\SyncroService\\CancellationFeeRec',
      'ArrayOfPaymentFormCancellationFeeRS' => 'Dingus\\SyncroService\\ArrayOfPaymentFormCancellationFeeRS',
      'PaymentFormCancellationFeeRS' => 'Dingus\\SyncroService\\PaymentFormCancellationFeeRS',
      'ArrayOfLineCancellationFeeRS' => 'Dingus\\SyncroService\\ArrayOfLineCancellationFeeRS',
      'LineCancellationFeeRS' => 'Dingus\\SyncroService\\LineCancellationFeeRS',
      'PutCancellationFeeResponse' => 'Dingus\\SyncroService\\PutCancellationFeeResponse',
      'PutComissionsHotel' => 'Dingus\\SyncroService\\PutComissionsHotel',
      'PutComissionHotelRQ' => 'Dingus\\SyncroService\\PutComissionHotelRQ',
      'ArrayOfComissionHotelRec' => 'Dingus\\SyncroService\\ArrayOfComissionHotelRec',
      'ComissionHotelRec' => 'Dingus\\SyncroService\\ComissionHotelRec',
      'ArrayOfLineComissionHotelRec' => 'Dingus\\SyncroService\\ArrayOfLineComissionHotelRec',
      'LineComissionHotelRec' => 'Dingus\\SyncroService\\LineComissionHotelRec',
      'PutComissionsHotelResponse' => 'Dingus\\SyncroService\\PutComissionsHotelResponse',
      'PutTasks' => 'Dingus\\SyncroService\\PutTasks',
      'ArrayOfTaskRS' => 'Dingus\\SyncroService\\ArrayOfTaskRS',
      'TaskRS' => 'Dingus\\SyncroService\\TaskRS',
      'ArrayOfErrorsTask' => 'Dingus\\SyncroService\\ArrayOfErrorsTask',
      'ErrorsTask' => 'Dingus\\SyncroService\\ErrorsTask',
      'ArrayOfDataTask' => 'Dingus\\SyncroService\\ArrayOfDataTask',
      'DataTask' => 'Dingus\\SyncroService\\DataTask',
      'ArrayOfHotelsTask' => 'Dingus\\SyncroService\\ArrayOfHotelsTask',
      'HotelsTask' => 'Dingus\\SyncroService\\HotelsTask',
      'ArrayOfCustomersTask' => 'Dingus\\SyncroService\\ArrayOfCustomersTask',
      'CustomersTask' => 'Dingus\\SyncroService\\CustomersTask',
      'ArrayOfUsersTask' => 'Dingus\\SyncroService\\ArrayOfUsersTask',
      'UsersTask' => 'Dingus\\SyncroService\\UsersTask',
      'PutTasksResponse' => 'Dingus\\SyncroService\\PutTasksResponse',
      'PutRatePromotion' => 'Dingus\\SyncroService\\PutRatePromotion',
      'PutPromotionRQ' => 'Dingus\\SyncroService\\PutPromotionRQ',
      'ArrayOfCustomerPromotionRec' => 'Dingus\\SyncroService\\ArrayOfCustomerPromotionRec',
      'CustomerPromotionRec' => 'Dingus\\SyncroService\\CustomerPromotionRec',
      'ArrayOfRateGroupPromotionRec' => 'Dingus\\SyncroService\\ArrayOfRateGroupPromotionRec',
      'RateGroupPromotionRec' => 'Dingus\\SyncroService\\RateGroupPromotionRec',
      'ArrayOfPromotionLineRec' => 'Dingus\\SyncroService\\ArrayOfPromotionLineRec',
      'PromotionLineRec' => 'Dingus\\SyncroService\\PromotionLineRec',
      'ArrayOfExtraPromotionRec' => 'Dingus\\SyncroService\\ArrayOfExtraPromotionRec',
      'ExtraPromotionRec' => 'Dingus\\SyncroService\\ExtraPromotionRec',
      'ArrayOfStopSalesPromotionRec' => 'Dingus\\SyncroService\\ArrayOfStopSalesPromotionRec',
      'StopSalesPromotionRec' => 'Dingus\\SyncroService\\StopSalesPromotionRec',
      'PutRatePromotionResponse' => 'Dingus\\SyncroService\\PutRatePromotionResponse',
      'PutPromotionalCodesByQuantity' => 'Dingus\\SyncroService\\PutPromotionalCodesByQuantity',
      'PutPromotionalCodesByQuantityResponse' => 'Dingus\\SyncroService\\PutPromotionalCodesByQuantityResponse',
      'PutDetailsHotel' => 'Dingus\\SyncroService\\PutDetailsHotel',
      'PutDetailsHotelResponse' => 'Dingus\\SyncroService\\PutDetailsHotelResponse',
      'PutAllotment' => 'Dingus\\SyncroService\\PutAllotment',
      'PutAllotmentRQ' => 'Dingus\\SyncroService\\PutAllotmentRQ',
      'ArrayOfCustomer' => 'Dingus\\SyncroService\\ArrayOfCustomer',
      'Customer' => 'Dingus\\SyncroService\\Customer',
      'ArrayOfAllotmentRec' => 'Dingus\\SyncroService\\ArrayOfAllotmentRec',
      'AllotmentRec' => 'Dingus\\SyncroService\\AllotmentRec',
      'ArrayOfExtraAllotmentRec' => 'Dingus\\SyncroService\\ArrayOfExtraAllotmentRec',
      'ExtraAllotmentRec' => 'Dingus\\SyncroService\\ExtraAllotmentRec',
      'ArrayOfStayAllotmentRec' => 'Dingus\\SyncroService\\ArrayOfStayAllotmentRec',
      'StayAllotmentRec' => 'Dingus\\SyncroService\\StayAllotmentRec',
      'PutAllotmentResponse' => 'Dingus\\SyncroService\\PutAllotmentResponse',
      'PutRates' => 'Dingus\\SyncroService\\PutRates',
      'PutRatesRQ' => 'Dingus\\SyncroService\\PutRatesRQ',
      'ArrayOfRoomRateRec' => 'Dingus\\SyncroService\\ArrayOfRoomRateRec',
      'RoomRateRec' => 'Dingus\\SyncroService\\RoomRateRec',
      'ArrayOfBoardRateRec' => 'Dingus\\SyncroService\\ArrayOfBoardRateRec',
      'BoardRateRec' => 'Dingus\\SyncroService\\BoardRateRec',
      'ArrayOfSuplementRateRec' => 'Dingus\\SyncroService\\ArrayOfSuplementRateRec',
      'SuplementRateRec' => 'Dingus\\SyncroService\\SuplementRateRec',
      'ArrayOfExtraRateRec' => 'Dingus\\SyncroService\\ArrayOfExtraRateRec',
      'ExtraRateRec' => 'Dingus\\SyncroService\\ExtraRateRec',
      'RateParentInfo' => 'Dingus\\SyncroService\\RateParentInfo',
      'PutRatesResponse' => 'Dingus\\SyncroService\\PutRatesResponse',
      'PutRatesOnlyHeader' => 'Dingus\\SyncroService\\PutRatesOnlyHeader',
      'PutRatesOnlyHeaderRQ' => 'Dingus\\SyncroService\\PutRatesOnlyHeaderRQ',
      'PutRatesOnlyHeaderResponse' => 'Dingus\\SyncroService\\PutRatesOnlyHeaderResponse',
      'PutDailyRates' => 'Dingus\\SyncroService\\PutDailyRates',
      'DailyRatesRQ' => 'Dingus\\SyncroService\\DailyRatesRQ',
      'ArrayOfDailyPriceRec' => 'Dingus\\SyncroService\\ArrayOfDailyPriceRec',
      'DailyPriceRec' => 'Dingus\\SyncroService\\DailyPriceRec',
      'ArrayOfDailyPricePricesRec' => 'Dingus\\SyncroService\\ArrayOfDailyPricePricesRec',
      'DailyPricePricesRec' => 'Dingus\\SyncroService\\DailyPricePricesRec',
      'ArrayOfDailyPriceOcupationRec' => 'Dingus\\SyncroService\\ArrayOfDailyPriceOcupationRec',
      'DailyPriceOcupationRec' => 'Dingus\\SyncroService\\DailyPriceOcupationRec',
      'ArrayOfDailyPriceOcupationPositionRec' => 'Dingus\\SyncroService\\ArrayOfDailyPriceOcupationPositionRec',
      'DailyPriceOcupationPositionRec' => 'Dingus\\SyncroService\\DailyPriceOcupationPositionRec',
      'ArrayOfDailyPriceRangeRec' => 'Dingus\\SyncroService\\ArrayOfDailyPriceRangeRec',
      'DailyPriceRangeRec' => 'Dingus\\SyncroService\\DailyPriceRangeRec',
      'PutDailyRatesResponse' => 'Dingus\\SyncroService\\PutDailyRatesResponse',
      'PutStopSales' => 'Dingus\\SyncroService\\PutStopSales',
      'PutStopSalesRQ' => 'Dingus\\SyncroService\\PutStopSalesRQ',
      'ArrayOfStopSalesRec' => 'Dingus\\SyncroService\\ArrayOfStopSalesRec',
      'StopSalesRec' => 'Dingus\\SyncroService\\StopSalesRec',
      'PutStopSalesResponse' => 'Dingus\\SyncroService\\PutStopSalesResponse',
      'PutCustomerRates' => 'Dingus\\SyncroService\\PutCustomerRates',
      'PutCustomerDailyRatesRQ' => 'Dingus\\SyncroService\\PutCustomerDailyRatesRQ',
      'ArrayOfPutCustomerDailyRatesRQRec' => 'Dingus\\SyncroService\\ArrayOfPutCustomerDailyRatesRQRec',
      'PutCustomerDailyRatesRQRec' => 'Dingus\\SyncroService\\PutCustomerDailyRatesRQRec',
      'PutCustomerRatesResponse' => 'Dingus\\SyncroService\\PutCustomerRatesResponse',
      'PutCustomerRatesByHotelList' => 'Dingus\\SyncroService\\PutCustomerRatesByHotelList',
      'PutCustomerDailyRatesByHotelListRQ' => 'Dingus\\SyncroService\\PutCustomerDailyRatesByHotelListRQ',
      'PutCustomerRatesByHotelListResponse' => 'Dingus\\SyncroService\\PutCustomerRatesByHotelListResponse',
      'PutCustomerRatesMultiGroupRate' => 'Dingus\\SyncroService\\PutCustomerRatesMultiGroupRate',
      'PutCustomerDailyRateMultiGroupRateRQ' => 'Dingus\\SyncroService\\PutCustomerDailyRateMultiGroupRateRQ',
      'PutCustomerRatesMultiGroupRateResponse' => 'Dingus\\SyncroService\\PutCustomerRatesMultiGroupRateResponse',
      'PutCustomerPromotions' => 'Dingus\\SyncroService\\PutCustomerPromotions',
      'PutCustomerPromotionsRQ' => 'Dingus\\SyncroService\\PutCustomerPromotionsRQ',
      'ArrayOfCustomerPromotionsRec' => 'Dingus\\SyncroService\\ArrayOfCustomerPromotionsRec',
      'CustomerPromotionsRec' => 'Dingus\\SyncroService\\CustomerPromotionsRec',
      'PutCustomerPromotionsResponse' => 'Dingus\\SyncroService\\PutCustomerPromotionsResponse',
      'PutSyncroBuffer' => 'Dingus\\SyncroService\\PutSyncroBuffer',
      'SyncroBufferRS' => 'Dingus\\SyncroService\\SyncroBufferRS',
      'PutSyncroBufferResponse' => 'Dingus\\SyncroService\\PutSyncroBufferResponse',
      'GetBookingsByHotel' => 'Dingus\\SyncroService\\GetBookingsByHotel',
      'GetBookingsByHotelResponse' => 'Dingus\\SyncroService\\GetBookingsByHotelResponse',
      'GetBookinsRS' => 'Dingus\\SyncroService\\GetBookinsRS',
      'ArrayOfBookingRS' => 'Dingus\\SyncroService\\ArrayOfBookingRS',
      'BookingRS' => 'Dingus\\SyncroService\\BookingRS',
      'ArrayOfError' => 'Dingus\\SyncroService\\ArrayOfError',
      'Error' => 'Dingus\\SyncroService\\Error',
      'ArrayOfExtra' => 'Dingus\\SyncroService\\ArrayOfExtra',
      'Extra' => 'Dingus\\SyncroService\\Extra',
      'ArrayOfData' => 'Dingus\\SyncroService\\ArrayOfData',
      'Data' => 'Dingus\\SyncroService\\Data',
      'ArrayOfPayment' => 'Dingus\\SyncroService\\ArrayOfPayment',
      'Payment' => 'Dingus\\SyncroService\\Payment',
      'ArrayOfCharge' => 'Dingus\\SyncroService\\ArrayOfCharge',
      'Charge' => 'Dingus\\SyncroService\\Charge',
      'ArrayOfStay' => 'Dingus\\SyncroService\\ArrayOfStay',
      'Stay' => 'Dingus\\SyncroService\\Stay',
      'ComissionRS' => 'Dingus\\SyncroService\\ComissionRS',
      'ArrayOfPax' => 'Dingus\\SyncroService\\ArrayOfPax',
      'Pax' => 'Dingus\\SyncroService\\Pax',
      'ArrayOfPrice' => 'Dingus\\SyncroService\\ArrayOfPrice',
      'Price' => 'Dingus\\SyncroService\\Price',
      'ArrayOfCancellationInfo' => 'Dingus\\SyncroService\\ArrayOfCancellationInfo',
      'CancellationInfo' => 'Dingus\\SyncroService\\CancellationInfo',
      'GetBookingsByHotel2' => 'Dingus\\SyncroService\\GetBookingsByHotel2',
      'GetBookingsByHotel2Response' => 'Dingus\\SyncroService\\GetBookingsByHotel2Response',
      'GetBookingsByCustomer' => 'Dingus\\SyncroService\\GetBookingsByCustomer',
      'GetBookingsByCustomerResponse' => 'Dingus\\SyncroService\\GetBookingsByCustomerResponse',
      'GetBookingsByCustomer2' => 'Dingus\\SyncroService\\GetBookingsByCustomer2',
      'GetBookingsByCustomer2Response' => 'Dingus\\SyncroService\\GetBookingsByCustomer2Response',
      'GetBookingsByLocalizerHotelCustomer' => 'Dingus\\SyncroService\\GetBookingsByLocalizerHotelCustomer',
      'GetBookingsByLocalizerHotelCustomerResponse' => 'Dingus\\SyncroService\\GetBookingsByLocalizerHotelCustomerResponse',
      'GetBookingsByBookingData' => 'Dingus\\SyncroService\\GetBookingsByBookingData',
      'GetBookingsByBookingDataResponse' => 'Dingus\\SyncroService\\GetBookingsByBookingDataResponse',
      'GetBookingsByBookingDataConverted' => 'Dingus\\SyncroService\\GetBookingsByBookingDataConverted',
      'GetBookingsByBookingDataConvertedResponse' => 'Dingus\\SyncroService\\GetBookingsByBookingDataConvertedResponse',
      'MarkBookingsAsReceived2' => 'Dingus\\SyncroService\\MarkBookingsAsReceived2',
      'MarkBookingsAsReceived2Response' => 'Dingus\\SyncroService\\MarkBookingsAsReceived2Response',
      'MarkBookingsAsReceived' => 'Dingus\\SyncroService\\MarkBookingsAsReceived',
      'MarkBookingsAsReceivedResponse' => 'Dingus\\SyncroService\\MarkBookingsAsReceivedResponse',
      'MarkBookingsAsNoReceived2' => 'Dingus\\SyncroService\\MarkBookingsAsNoReceived2',
      'MarkBookingsAsNoReceived2Response' => 'Dingus\\SyncroService\\MarkBookingsAsNoReceived2Response',
      'MarkBookingsAsNoReceived' => 'Dingus\\SyncroService\\MarkBookingsAsNoReceived',
      'MarkBookingsAsNoReceivedResponse' => 'Dingus\\SyncroService\\MarkBookingsAsNoReceivedResponse',
      'GetDeletedBookinsByCustomer' => 'Dingus\\SyncroService\\GetDeletedBookinsByCustomer',
      'GetDeletedBookinsByCustomerResponse' => 'Dingus\\SyncroService\\GetDeletedBookinsByCustomerResponse',
      'ArrayOfGetDeletedBookingsResult' => 'Dingus\\SyncroService\\ArrayOfGetDeletedBookingsResult',
      'GetDeletedBookingsResult' => 'Dingus\\SyncroService\\GetDeletedBookingsResult',
      'GetDeletedBookinsByHotel' => 'Dingus\\SyncroService\\GetDeletedBookinsByHotel',
      'GetDeletedBookinsByHotelResponse' => 'Dingus\\SyncroService\\GetDeletedBookinsByHotelResponse',
      'MarkDeletedBookingsAsReceived' => 'Dingus\\SyncroService\\MarkDeletedBookingsAsReceived',
      'MarkDeletedBookingsAsReceivedResponse' => 'Dingus\\SyncroService\\MarkDeletedBookingsAsReceivedResponse',
      'MarkDeletedBookingsAsReceived2' => 'Dingus\\SyncroService\\MarkDeletedBookingsAsReceived2',
      'MarkDeletedBookingsAsReceived2Response' => 'Dingus\\SyncroService\\MarkDeletedBookingsAsReceived2Response',
      'MarkDeletedBookingsAsNoReceived' => 'Dingus\\SyncroService\\MarkDeletedBookingsAsNoReceived',
      'MarkDeletedBookingsAsNoReceivedResponse' => 'Dingus\\SyncroService\\MarkDeletedBookingsAsNoReceivedResponse',
      'MarkDeletedBookingsAsNoReceived2' => 'Dingus\\SyncroService\\MarkDeletedBookingsAsNoReceived2',
      'MarkDeletedBookingsAsNoReceived2Response' => 'Dingus\\SyncroService\\MarkDeletedBookingsAsNoReceived2Response',
      'PutBlockAllotment' => 'Dingus\\SyncroService\\PutBlockAllotment',
      'ArrayOfBlockAllotmentRQ' => 'Dingus\\SyncroService\\ArrayOfBlockAllotmentRQ',
      'BlockAllotmentRQ' => 'Dingus\\SyncroService\\BlockAllotmentRQ',
      'PutBlockAllotmentResponse' => 'Dingus\\SyncroService\\PutBlockAllotmentResponse',
      'PutCustomerRateSetUpList' => 'Dingus\\SyncroService\\PutCustomerRateSetUpList',
      'PutCustomerRateSetUpRQ' => 'Dingus\\SyncroService\\PutCustomerRateSetUpRQ',
      'ArrayOfCustomerRateSetUpRS' => 'Dingus\\SyncroService\\ArrayOfCustomerRateSetUpRS',
      'PutCustomerRateSetUpListResponse' => 'Dingus\\SyncroService\\PutCustomerRateSetUpListResponse',
    );

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     */
    public function __construct(array $options = array(), $wsdl = 'http://dingus2.dingus-services.com/services/tazzy/syncroservice.asmx?wsdl')
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }
      $options = array_merge(array (
      'features' => 1,
    ), $options);
      parent::__construct($wsdl, $options);
    }

    /**
     * @param PutHotelGroups $parameters
     * @return PutHotelGroupsResponse
     */
    public function PutHotelGroups(PutHotelGroups $parameters)
    {
      return $this->__soapCall('PutHotelGroups', array($parameters));
    }

    /**
     * @param PutHotelGroupsByHotelList $parameters
     * @return PutHotelGroupsByHotelListResponse
     */
    public function PutHotelGroupsByHotelList(PutHotelGroupsByHotelList $parameters)
    {
      return $this->__soapCall('PutHotelGroupsByHotelList', array($parameters));
    }

    /**
     * @param PutPriceRules $parameters
     * @return PutPriceRulesResponse
     */
    public function PutPriceRules(PutPriceRules $parameters)
    {
      return $this->__soapCall('PutPriceRules', array($parameters));
    }

    /**
     * @param PutUsersConfiguration $parameters
     * @return PutUsersConfigurationResponse
     */
    public function PutUsersConfiguration(PutUsersConfiguration $parameters)
    {
      return $this->__soapCall('PutUsersConfiguration', array($parameters));
    }

    /**
     * @param PutHotel $parameters
     * @return PutHotelResponse
     */
    public function PutHotel(PutHotel $parameters)
    {
      return $this->__soapCall('PutHotel', array($parameters));
    }

    /**
     * @param PutCustomerRateSetUp $parameters
     * @return PutCustomerRateSetUpResponse
     */
    public function PutCustomerRateSetUp(PutCustomerRateSetUp $parameters)
    {
      return $this->__soapCall('PutCustomerRateSetUp', array($parameters));
    }

    /**
     * @param DeleteCustomerRateLinePrice $parameters
     * @return DeleteCustomerRateLinePriceResponse
     */
    public function DeleteCustomerRateLinePrice(DeleteCustomerRateLinePrice $parameters)
    {
      return $this->__soapCall('DeleteCustomerRateLinePrice', array($parameters));
    }

    /**
     * @param PutCancellationFee $parameters
     * @return PutCancellationFeeResponse
     */
    public function PutCancellationFee(PutCancellationFee $parameters)
    {
      return $this->__soapCall('PutCancellationFee', array($parameters));
    }

    /**
     * @param PutComissionsHotel $parameters
     * @return PutComissionsHotelResponse
     */
    public function PutComissionsHotel(PutComissionsHotel $parameters)
    {
      return $this->__soapCall('PutComissionsHotel', array($parameters));
    }

    /**
     * @param PutTasks $parameters
     * @return PutTasksResponse
     */
    public function PutTasks(PutTasks $parameters)
    {
      return $this->__soapCall('PutTasks', array($parameters));
    }

    /**
     * @param PutRatePromotion $parameters
     * @return PutRatePromotionResponse
     */
    public function PutRatePromotion(PutRatePromotion $parameters)
    {
      return $this->__soapCall('PutRatePromotion', array($parameters));
    }

    /**
     * @param PutPromotionalCodesByQuantity $parameters
     * @return PutPromotionalCodesByQuantityResponse
     */
    public function PutPromotionalCodesByQuantity(PutPromotionalCodesByQuantity $parameters)
    {
      return $this->__soapCall('PutPromotionalCodesByQuantity', array($parameters));
    }

    /**
     * @param PutDetailsHotel $parameters
     * @return PutDetailsHotelResponse
     */
    public function PutDetailsHotel(PutDetailsHotel $parameters)
    {
      return $this->__soapCall('PutDetailsHotel', array($parameters));
    }

    /**
     * @param PutAllotment $parameters
     * @return PutAllotmentResponse
     */
    public function PutAllotment(PutAllotment $parameters)
    {
      return $this->__soapCall('PutAllotment', array($parameters));
    }

    /**
     * @param PutRates $parameters
     * @return PutRatesResponse
     */
    public function PutRates(PutRates $parameters)
    {
      return $this->__soapCall('PutRates', array($parameters));
    }

    /**
     * @param PutRatesOnlyHeader $parameters
     * @return PutRatesOnlyHeaderResponse
     */
    public function PutRatesOnlyHeader(PutRatesOnlyHeader $parameters)
    {
      return $this->__soapCall('PutRatesOnlyHeader', array($parameters));
    }

    /**
     * @param PutDailyRates $parameters
     * @return PutDailyRatesResponse
     */
    public function PutDailyRates(PutDailyRates $parameters)
    {
      return $this->__soapCall('PutDailyRates', array($parameters));
    }

    /**
     * @param PutStopSales $parameters
     * @return PutStopSalesResponse
     */
    public function PutStopSales(PutStopSales $parameters)
    {
      return $this->__soapCall('PutStopSales', array($parameters));
    }

    /**
     * @param PutCustomerRates $parameters
     * @return PutCustomerRatesResponse
     */
    public function PutCustomerRates(PutCustomerRates $parameters)
    {
      return $this->__soapCall('PutCustomerRates', array($parameters));
    }

    /**
     * @param PutCustomerRatesByHotelList $parameters
     * @return PutCustomerRatesByHotelListResponse
     */
    public function PutCustomerRatesByHotelList(PutCustomerRatesByHotelList $parameters)
    {
      return $this->__soapCall('PutCustomerRatesByHotelList', array($parameters));
    }

    /**
     * @param PutCustomerRatesMultiGroupRate $parameters
     * @return PutCustomerRatesMultiGroupRateResponse
     */
    public function PutCustomerRatesMultiGroupRate(PutCustomerRatesMultiGroupRate $parameters)
    {
      return $this->__soapCall('PutCustomerRatesMultiGroupRate', array($parameters));
    }

    /**
     * @param PutCustomerPromotions $parameters
     * @return PutCustomerPromotionsResponse
     */
    public function PutCustomerPromotions(PutCustomerPromotions $parameters)
    {
      return $this->__soapCall('PutCustomerPromotions', array($parameters));
    }

    /**
     * @param PutSyncroBuffer $parameters
     * @return PutSyncroBufferResponse
     */
    public function PutSyncroBuffer(PutSyncroBuffer $parameters)
    {
      return $this->__soapCall('PutSyncroBuffer', array($parameters));
    }

    /**
     * @param GetBookingsByHotel $parameters
     * @return GetBookingsByHotelResponse
     */
    public function GetBookingsByHotel(GetBookingsByHotel $parameters)
    {
      return $this->__soapCall('GetBookingsByHotel', array($parameters));
    }

    /**
     * @param GetBookingsByHotel2 $parameters
     * @return GetBookingsByHotel2Response
     */
    public function GetBookingsByHotel2(GetBookingsByHotel2 $parameters)
    {
      return $this->__soapCall('GetBookingsByHotel2', array($parameters));
    }

    /**
     * @param GetBookingsByCustomer $parameters
     * @return GetBookingsByCustomerResponse
     */
    public function GetBookingsByCustomer(GetBookingsByCustomer $parameters)
    {
      return $this->__soapCall('GetBookingsByCustomer', array($parameters));
    }

    /**
     * @param GetBookingsByCustomer2 $parameters
     * @return GetBookingsByCustomer2Response
     */
    public function GetBookingsByCustomer2(GetBookingsByCustomer2 $parameters)
    {
      return $this->__soapCall('GetBookingsByCustomer2', array($parameters));
    }

    /**
     * @param GetBookingsByLocalizerHotelCustomer $parameters
     * @return GetBookingsByLocalizerHotelCustomerResponse
     */
    public function GetBookingsByLocalizerHotelCustomer(GetBookingsByLocalizerHotelCustomer $parameters)
    {
      return $this->__soapCall('GetBookingsByLocalizerHotelCustomer', array($parameters));
    }

    /**
     * @param GetBookingsByBookingData $parameters
     * @return GetBookingsByBookingDataResponse
     */
    public function GetBookingsByBookingData(GetBookingsByBookingData $parameters)
    {
      return $this->__soapCall('GetBookingsByBookingData', array($parameters));
    }

    /**
     * @param GetBookingsByBookingDataConverted $parameters
     * @return GetBookingsByBookingDataConvertedResponse
     */
    public function GetBookingsByBookingDataConverted(GetBookingsByBookingDataConverted $parameters)
    {
      return $this->__soapCall('GetBookingsByBookingDataConverted', array($parameters));
    }

    /**
     * @param MarkBookingsAsReceived2 $parameters
     * @return MarkBookingsAsReceived2Response
     */
    public function MarkBookingsAsReceived2(MarkBookingsAsReceived2 $parameters)
    {
      return $this->__soapCall('MarkBookingsAsReceived2', array($parameters));
    }

    /**
     * @param MarkBookingsAsReceived $parameters
     * @return MarkBookingsAsReceivedResponse
     */
    public function MarkBookingsAsReceived(MarkBookingsAsReceived $parameters)
    {
      return $this->__soapCall('MarkBookingsAsReceived', array($parameters));
    }

    /**
     * @param MarkBookingsAsNoReceived2 $parameters
     * @return MarkBookingsAsNoReceived2Response
     */
    public function MarkBookingsAsNoReceived2(MarkBookingsAsNoReceived2 $parameters)
    {
      return $this->__soapCall('MarkBookingsAsNoReceived2', array($parameters));
    }

    /**
     * @param MarkBookingsAsNoReceived $parameters
     * @return MarkBookingsAsNoReceivedResponse
     */
    public function MarkBookingsAsNoReceived(MarkBookingsAsNoReceived $parameters)
    {
      return $this->__soapCall('MarkBookingsAsNoReceived', array($parameters));
    }

    /**
     * @param GetDeletedBookinsByCustomer $parameters
     * @return GetDeletedBookinsByCustomerResponse
     */
    public function GetDeletedBookinsByCustomer(GetDeletedBookinsByCustomer $parameters)
    {
      return $this->__soapCall('GetDeletedBookinsByCustomer', array($parameters));
    }

    /**
     * @param GetDeletedBookinsByHotel $parameters
     * @return GetDeletedBookinsByHotelResponse
     */
    public function GetDeletedBookinsByHotel(GetDeletedBookinsByHotel $parameters)
    {
      return $this->__soapCall('GetDeletedBookinsByHotel', array($parameters));
    }

    /**
     * @param MarkDeletedBookingsAsReceived $parameters
     * @return MarkDeletedBookingsAsReceivedResponse
     */
    public function MarkDeletedBookingsAsReceived(MarkDeletedBookingsAsReceived $parameters)
    {
      return $this->__soapCall('MarkDeletedBookingsAsReceived', array($parameters));
    }

    /**
     * @param MarkDeletedBookingsAsReceived2 $parameters
     * @return MarkDeletedBookingsAsReceived2Response
     */
    public function MarkDeletedBookingsAsReceived2(MarkDeletedBookingsAsReceived2 $parameters)
    {
      return $this->__soapCall('MarkDeletedBookingsAsReceived2', array($parameters));
    }

    /**
     * @param MarkDeletedBookingsAsNoReceived $parameters
     * @return MarkDeletedBookingsAsNoReceivedResponse
     */
    public function MarkDeletedBookingsAsNoReceived(MarkDeletedBookingsAsNoReceived $parameters)
    {
      return $this->__soapCall('MarkDeletedBookingsAsNoReceived', array($parameters));
    }

    /**
     * @param MarkDeletedBookingsAsNoReceived2 $parameters
     * @return MarkDeletedBookingsAsNoReceived2Response
     */
    public function MarkDeletedBookingsAsNoReceived2(MarkDeletedBookingsAsNoReceived2 $parameters)
    {
      return $this->__soapCall('MarkDeletedBookingsAsNoReceived2', array($parameters));
    }

    /**
     * @param PutBlockAllotment $parameters
     * @return PutBlockAllotmentResponse
     */
    public function PutBlockAllotment(PutBlockAllotment $parameters)
    {
      return $this->__soapCall('PutBlockAllotment', array($parameters));
    }

    /**
     * @param PutCustomerRateSetUpList $parameters
     * @return PutCustomerRateSetUpListResponse
     */
    public function PutCustomerRateSetUpList(PutCustomerRateSetUpList $parameters)
    {
      return $this->__soapCall('PutCustomerRateSetUpList', array($parameters));
    }

}
