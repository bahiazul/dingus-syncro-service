<?php

namespace Dingus\SyncroService;

class ArrayOfCustomersPriceRules implements \JsonSerializable, \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var CustomersPriceRules[] $CustomersPriceRules
     */
    protected $CustomersPriceRules = null;

    
    public function __construct()
    {
    
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'CustomersPriceRules' => $this->getCustomersPriceRules(),
      );
    }

    /**
     * @return CustomersPriceRules[]
     */
    public function getCustomersPriceRules()
    {
      return $this->CustomersPriceRules;
    }

    /**
     * @param CustomersPriceRules[] $CustomersPriceRules
     * @return \Dingus\SyncroService\ArrayOfCustomersPriceRules
     */
    public function setCustomersPriceRules(array $CustomersPriceRules = null)
    {
      $this->CustomersPriceRules = $CustomersPriceRules;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->CustomersPriceRules[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return CustomersPriceRules
     */
    public function offsetGet($offset)
    {
      return $this->CustomersPriceRules[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param CustomersPriceRules $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      $this->CustomersPriceRules[$offset] = $value;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->CustomersPriceRules[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return CustomersPriceRules Return the current element
     */
    public function current()
    {
      return current($this->CustomersPriceRules);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->CustomersPriceRules);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->CustomersPriceRules);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->CustomersPriceRules);
    }

    /**
     * Countable implementation
     *
     * @return CustomersPriceRules Return count of elements
     */
    public function count()
    {
      return count($this->CustomersPriceRules);
    }

}
