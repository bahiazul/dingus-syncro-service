<?php

namespace Dingus\SyncroService;

class ArrayOfBlockAllotmentRQ implements \JsonSerializable, \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var BlockAllotmentRQ[] $BlockAllotmentRQ
     */
    protected $BlockAllotmentRQ = null;

    
    public function __construct()
    {
    
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'BlockAllotmentRQ' => $this->getBlockAllotmentRQ(),
      );
    }

    /**
     * @return BlockAllotmentRQ[]
     */
    public function getBlockAllotmentRQ()
    {
      return $this->BlockAllotmentRQ;
    }

    /**
     * @param BlockAllotmentRQ[] $BlockAllotmentRQ
     * @return \Dingus\SyncroService\ArrayOfBlockAllotmentRQ
     */
    public function setBlockAllotmentRQ(array $BlockAllotmentRQ = null)
    {
      $this->BlockAllotmentRQ = $BlockAllotmentRQ;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->BlockAllotmentRQ[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return BlockAllotmentRQ
     */
    public function offsetGet($offset)
    {
      return $this->BlockAllotmentRQ[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param BlockAllotmentRQ $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      $this->BlockAllotmentRQ[$offset] = $value;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->BlockAllotmentRQ[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return BlockAllotmentRQ Return the current element
     */
    public function current()
    {
      return current($this->BlockAllotmentRQ);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->BlockAllotmentRQ);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->BlockAllotmentRQ);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->BlockAllotmentRQ);
    }

    /**
     * Countable implementation
     *
     * @return BlockAllotmentRQ Return count of elements
     */
    public function count()
    {
      return count($this->BlockAllotmentRQ);
    }

}
