<?php

namespace Dingus\SyncroService;

class ArrayOfCriterionRecc implements \JsonSerializable, \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var CriterionRecc[] $CriterionRecc
     */
    protected $CriterionRecc = null;

    
    public function __construct()
    {
    
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'CriterionRecc' => $this->getCriterionRecc(),
      );
    }

    /**
     * @return CriterionRecc[]
     */
    public function getCriterionRecc()
    {
      return $this->CriterionRecc;
    }

    /**
     * @param CriterionRecc[] $CriterionRecc
     * @return \Dingus\SyncroService\ArrayOfCriterionRecc
     */
    public function setCriterionRecc(array $CriterionRecc = null)
    {
      $this->CriterionRecc = $CriterionRecc;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->CriterionRecc[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return CriterionRecc
     */
    public function offsetGet($offset)
    {
      return $this->CriterionRecc[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param CriterionRecc $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      $this->CriterionRecc[$offset] = $value;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->CriterionRecc[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return CriterionRecc Return the current element
     */
    public function current()
    {
      return current($this->CriterionRecc);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->CriterionRecc);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->CriterionRecc);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->CriterionRecc);
    }

    /**
     * Countable implementation
     *
     * @return CriterionRecc Return count of elements
     */
    public function count()
    {
      return count($this->CriterionRecc);
    }

}
