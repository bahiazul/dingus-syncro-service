<?php

namespace Dingus\SyncroService;

class ArrayOfFacilities implements \JsonSerializable, \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var Facilities[] $Facilities
     */
    protected $Facilities = null;

    
    public function __construct()
    {
    
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'Facilities' => $this->getFacilities(),
      );
    }

    /**
     * @return Facilities[]
     */
    public function getFacilities()
    {
      return $this->Facilities;
    }

    /**
     * @param Facilities[] $Facilities
     * @return \Dingus\SyncroService\ArrayOfFacilities
     */
    public function setFacilities(array $Facilities = null)
    {
      $this->Facilities = $Facilities;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->Facilities[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return Facilities
     */
    public function offsetGet($offset)
    {
      return $this->Facilities[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param Facilities $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      $this->Facilities[$offset] = $value;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->Facilities[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return Facilities Return the current element
     */
    public function current()
    {
      return current($this->Facilities);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->Facilities);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->Facilities);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->Facilities);
    }

    /**
     * Countable implementation
     *
     * @return Facilities Return count of elements
     */
    public function count()
    {
      return count($this->Facilities);
    }

}
