<?php

namespace Dingus\SyncroService;

class GetBookingsByBookingData implements \JsonSerializable
{

    /**
     * @var Credentials $Credentials
     */
    protected $Credentials = null;

    /**
     * @var string $BookingDataCode
     */
    protected $BookingDataCode = null;

    /**
     * @var string $BookingDataValue
     */
    protected $BookingDataValue = null;

    /**
     * @param Credentials $Credentials
     * @param string $BookingDataCode
     * @param string $BookingDataValue
     */
    public function __construct($Credentials, $BookingDataCode, $BookingDataValue)
    {
      $this->Credentials = $Credentials;
      $this->BookingDataCode = $BookingDataCode;
      $this->BookingDataValue = $BookingDataValue;
    }

    /**
     * JsonSerializable implementation
     *
     * @return array
     */
    public function jsonSerialize()
    {
      return array(
        'Credentials' => $this->getCredentials(),
        'BookingDataCode' => $this->getBookingDataCode(),
        'BookingDataValue' => $this->getBookingDataValue(),
      );
    }

    /**
     * @return Credentials
     */
    public function getCredentials()
    {
      return $this->Credentials;
    }

    /**
     * @param Credentials $Credentials
     * @return \Dingus\SyncroService\GetBookingsByBookingData
     */
    public function setCredentials($Credentials)
    {
      $this->Credentials = $Credentials;
      return $this;
    }

    /**
     * @return string
     */
    public function getBookingDataCode()
    {
      return $this->BookingDataCode;
    }

    /**
     * @param string $BookingDataCode
     * @return \Dingus\SyncroService\GetBookingsByBookingData
     */
    public function setBookingDataCode($BookingDataCode)
    {
      $this->BookingDataCode = $BookingDataCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getBookingDataValue()
    {
      return $this->BookingDataValue;
    }

    /**
     * @param string $BookingDataValue
     * @return \Dingus\SyncroService\GetBookingsByBookingData
     */
    public function setBookingDataValue($BookingDataValue)
    {
      $this->BookingDataValue = $BookingDataValue;
      return $this;
    }

}
