<?php

namespace Dingus\SyncroService;

class TipoRestauranteHotel
{
    const __default = 'Bar';
    const Bar = 'Bar';
    const BarPiscina = 'BarPiscina';
    const BarSalon = 'BarSalon';
    const Bufe = 'Bufe';
    const Cafe = 'Cafe';
    const Cafeteria = 'Cafeteria';
    const CoffeShop = 'CoffeShop';
    const FastFood = 'FastFood';
    const LoobyBar = 'LoobyBar';
    const ParaLlevar = 'ParaLlevar';
    const Quiosco = 'Quiosco';
    const RestauranteConServicio = 'RestauranteConServicio';
    const SnackBar = 'SnackBar';
    const SoloBebidas = 'SoloBebidas';


}
